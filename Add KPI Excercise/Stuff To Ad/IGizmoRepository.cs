﻿using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IGizmoRepository
    {
        Task<bool> Exists(string auditDataID);
        Task<Gizmo> Create(Gizmo obj);
        Gizmo Update(Gizmo obj);
        Task Remove(string id);
        Task<Gizmo> FindById(string id);
        List<Gizmo> FindAll(string query);
        Task<PagedResultBase<Gizmo>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
