﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class GizmosRepository : IGizmoRepository
    {
        private readonly ApplicationDbContext _context;

        public GizmosRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public Task<Gizmo> Create(Gizmo obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(string auditDataID)
        {
            throw new NotImplementedException();
        }

        public List<Gizmo> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.Gizmos.OrderBy(o => o.StatsDate).ToList<Gizmo>();
            else
                return _context.Gizmos.Where(o =>
                    o.RegionID.regionName.Value.Contains(query)
                ).OrderBy(o => o.StatsDate).ToList<Gizmo>();
        }

        public async Task<PagedResultBase<Gizmo>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<Gizmo> result = new PagedResultBase<Gizmo>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Gizmo> query = _context.Gizmos
                .Include(x => x.RegionID)
                .OrderByDescending(x => x.StatsDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.StatsDate.Value >= dateFrom && x.StatsDate.Value <= dateTo);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<Gizmo> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<Gizmo> FindById(string id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(string id)
        {
            throw new NotImplementedException();
        }

        public Gizmo Update(Gizmo obj)
        {
            throw new NotImplementedException();
        }
    }
}
