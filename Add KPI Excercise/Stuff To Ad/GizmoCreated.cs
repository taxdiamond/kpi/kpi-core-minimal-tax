﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    class GizmoCreated
    {
        public DateTime StatsDate { get; set; }
        public int NumberOfGizmos { get; set; }
        public string RegionID { get; set; }
    }
}
