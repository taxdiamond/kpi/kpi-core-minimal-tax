SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_GizmosByRegion]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @gizmoRegionResults as table(yearStats int, regionID nvarchar(10), numberOfGizmos int)

	insert into @gizmoRegionResults
	select DATEPART(year, statsDate), regionID, sum([numberOfGizmos]) as amount
	from [dbo].[Gizmos]
	where DATEPART(year, statsDate) > @startYear and DATEPART(year, statsDate) <= @limitYear
	group by regionID, DATEPART(year, statsDate)
	order by DATEPART(year, statsDate) asc

	select r.yearStats as [year], reg.regionName, numberOfGizmos
	from @gizmoRegionResults as r
	join [dbo].[Region] as reg on r.regionID = reg.regionID
	order by r.regionID asc, [year] asc
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GIZMO_GetLastDate]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([statsDate]) as LastDate
	FROM [dbo].[Gizmos]
END
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[GIZMO_ReportGizmoData]
	@dateReported date,
	@regionID nvarchar(10),
	@numberOfGizmos int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	INSERT INTO [dbo].[Gizmos]
           ([statsDate]
           ,[numberOfGizmos]
           ,[regionID])
     VALUES
           (@dateReported
           ,@numberOfGizmos
           ,@regionID)
END
GO



