﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class GizmoEntityTypeConfiguration : IEntityTypeConfiguration<Gizmo>
    {
        public void Configure(EntityTypeBuilder<Gizmo> builder)
        {
            builder.Property(x => x.GizmosID)
                .HasColumnName("gizmoStatsID")
                .UseIdentityColumn();
            builder.HasKey(x => x.GizmosID);
            builder.Ignore(x => x.Id);

            builder.OwnsOne(x => x.StatsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("statsDate");

            builder.OwnsOne(x => x.NumberOfGizmos)
                .Property(y => y.Value)
                .HasColumnName("numberOfGizmos");
        }
    }
}
