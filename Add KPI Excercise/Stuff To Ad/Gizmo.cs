﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    public class Gizmo : AggregateRoot<IdInt>
    {
        public Int32 GizmosID { get; set; }
        public DateType StatsDate { get; set; }
        public PositiveInteger NumberOfGizmos { get; set; }
        public Region.Region RegionID { get; set; }

        protected override void ValidateStatus()
        {
            // Explain what we do here
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case GizmoCreated e:
                    this.StatsDate = DateType.FromDateTime(e.StatsDate);
                    this.NumberOfGizmos = PositiveInteger.FromInt(e.NumberOfGizmos);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    break;
            }
        }
    }
}
