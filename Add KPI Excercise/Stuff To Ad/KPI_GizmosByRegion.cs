﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "region", // Group
        "year" // Time Period
        )]
    public class KPI_GizmosByRegion : KPI
    {
        public KPI_GizmosByRegion() : base(
            KPIPeriodType.Year,
            KPIGroupType.Region
            )
        {
            SP_Name = "KPI_DATA_GizmosByRegion";
            SP_ValueFieldID = "numberOfGizmos";
            SP_GroupFieldID = "regionName";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
