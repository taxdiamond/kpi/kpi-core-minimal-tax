from datetime import date
from dateutil.rrule import rrule, MONTHLY

region = ["KOR", "KUN", "PAR", "PARK", "PLA"]
office = ["KORN1", "KORN2", "KORN3", "NKUN", "PARCEN", "PARGAR", "PARKTA1", "PARKTA2", "PARKTA3", "PARKTA4", "PARKTA5", "PARPER", "PLAB", "PLAG", "SKUN"]
segment = ["LARGE", "MED", "SMALL"]
tax = ["Corporate Income Tax", "Education Tax", "Excise Duty", "Individual Income Tax", "Obligatory Contributions to Non-budget Funds", "Property Tax", "Value Added Tax"]

start_date = date(2010, 1, 1)
end_date = date(2021, 6, 1)

with open('c:\\temp\\output.txt', 'w') as f:
    f.write("dateReported,numberOfRegisteredTaxPayers,numberOfActiveTaxPayers,numberOfTaxPayersFiled,numberTaxPayersFiledInTime,numberOfTaxPayers70PercentRevenue,regionID,officeID,segmentID,conceptID\n")
    for dt in rrule(MONTHLY, dtstart=start_date, until=end_date):
        for r in region:
            for o in office:
                for s in segment:
                    for t in tax:
                        date = dt.strftime("%Y-%m-%d")
                        str = date + ",,,,,," + r + "," + o + ","+s+","+t+"\n"
                        f.write(str)