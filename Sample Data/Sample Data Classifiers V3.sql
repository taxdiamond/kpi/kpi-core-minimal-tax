use KPICoreDB
GO

INSERT INTO [dbo].[CustomsPost] ([customsPostID], [customsPostName]) VALUES 
	('COS', 'Cosamel'),
	('FEZ', 'Fezamas'),
	('LAR', 'Laramourg'),
	('PRA', 'Prachitel'),
	('TRA', 'Transtory')

INSERT INTO [dbo].[Office] ([officeID],[officeName]) VALUES
    ('KORN1','Korian Node 1'),
	('KORN2', 'Korian Node 2'),
	('KORN3', 'Korian Node 3'),
	('NKUN', 'North Kunzel'),
	('PARCEN', 'Parcel City Center'),
	('PARGAR', 'Parcel Garden Zone'),
	('PARKTA1', 'Parket TA District 1'),
	('PARKTA2', 'Parket TA District 2'),
	('PARKTA3', 'Parket TA District 3'),
	('PARKTA4', 'Parket TA District 4'),
	('PARKTA5', 'Parket TA District 5'),
	('PARPER', 'Parcel Perifery'),
	('PLAB', 'Platz Business Office'),
	('PLAG', 'Platz General Office'),
	('SKUN', 'South Kunzel')

INSERT INTO [dbo].[Region]([regionID],[regionName]) VALUES
    ('KOR', 'Korian'),
    ('KUN', 'Kunzel'),
    ('PAR', 'Parcel'),
    ('PARK', 'Parket'),
    ('PLA', 'Platz')

INSERT INTO [dbo].[Segment] ([segmentID],[segmentName]) VALUES
    ('LARGE', 'Large Taxpayers'),
	('MED', 'Medium Taxpayers'),
	('SMALL', 'Small Taxpayers')

INSERT INTO [dbo].[Settings] ([valueAddedTaxName],[corporateIncomeTaxname]) VALUES
    ('Value Added Tax', 'Corporate Income Tax')

INSERT INTO [dbo].[CustomsStaff] ([customsStaffID], [customsStaffName]) VALUES
        ('0003566713','Aadil Hayes'),
        ('0003638092','Aaron Hughes'),
        ('0003646412','Abdirahman Higgs'),
        ('0003723546','Abigail Frame'),
        ('0003748268','Adeeb Christian'),
        ('0003786954','Adele Vaughn'),
        ('0003797878','Aislinn Davey'),
        ('0003810528','Alessandro Bean'),
        ('0003908733','Alessia Levy'),
        ('0003920341','Alia Pate'),
        ('0003976345','Alix Singleton'),
        ('0004045100','Allegra Velasquez'),
        ('0004054475','Allison Matthews'),
        ('0004134193','Alma Coulson'),
        ('0004216279','Amal Hendrix'),
        ('0004270899','Amalia Johnston'),
        ('0004288648','Amanda Rhodes'),
        ('0004301048','Amarah Myers'),
        ('0004369001','Ameerah Ellis'),
        ('0004419406','Amelia-Grace Rooney'),
        ('0004484218','Amirah Parkinson'),
        ('0004535977','Amiyah Samuels'),
        ('0004589375','Ammar Ireland'),
        ('0004592232','Amna Butt'),
        ('0004608237','Ana Randall'),
        ('0004630050','Anabel Wood'),
        ('0004631891','Anaiya Mansell'),
        ('0004636548','Andrea Barlow'),
        ('0004719433','Andreas Mackenzie'),
        ('0004816984','Andreea Haney'),
        ('0004822491','Anees Nguyen'),
        ('0004828248','Aniyah Wolfe'),
        ('0004833970','Antoni Parry'),
        ('0004882339','Aoife Wilder'),
        ('0004971550','April Haas'),
        ('0004980765','Aqib Ball'),
        ('0005026692','Arianna Cassidy'),
        ('0005084275','Arley Foreman'),
        ('0005150788','Arnas Wills'),
        ('0005242523','Arnav Easton'),
        ('0005279186','Arnie Bentley'),
        ('0005333538','Arwel Hardy'),
        ('0005400830','Aryaan Pierce'),
        ('0005467588','Asha Mckeown'),
        ('0005476662','Ashanti Benson'),
        ('0005511256','Ashlee Vickers'),
        ('0005554761','Atif Arnold'),
        ('0005595454','Atticus Sanders'),
        ('0005633882','Aubree Burton'),
        ('0005698496','Ava Gibson'),
        ('0005778680','Ayra Bond'),
        ('0005805587','Beatrix Zavala'),
        ('0005903062','Beck Maldonado'),
        ('0005948467','Bella-Rose Edwards'),
        ('0005988193','Ben Woodard'),
        ('0006063870','Benedict Madden'),
        ('0006129080','Berat Baker'),
        ('0006145400','Bertram Prosser'),
        ('0006216105','Bevan Jeffery'),
        ('0006308382','Beverley Andrew'),
        ('0006327166','Bhavik Farrell'),
        ('0006415466','Bibi Povey'),
        ('0006466160','Bilaal Franco'),
        ('0006473692','Blake Wade'),
        ('0006524394','Bogdan Hayden'),
        ('0006591979','Branden Murray'),
        ('0006687806','Bret Barry'),
        ('0006739726','Brian Cousins'),
        ('0006758838','Brielle Ratcliffe'),
        ('0006759324','Britany Hagan'),
        ('0006816675','Britney Webb'),
        ('0006864669','Brook Stanton'),
        ('0006927433','Caiden Blackburn'),
        ('0006954473','Caleb Andersen'),
        ('0007011961','Carla Tomlinson'),
        ('0007061442','Carmen Le'),
        ('0007130655','Carter Lancaster'),
        ('0007224129','Caspar Parsons'),
        ('0007257349','Cassandra Drake'),
        ('0007309220','Cassia Willis'),
        ('0007314221','Catriona Morse'),
        ('0007336094','Cecelia Warner'),
        ('0007422267','Chanice Ortiz'),
        ('0007516366','Chante Freeman'),
        ('0007527382','Charleigh Grainger'),
        ('0007604003','Charleigh Lake'),
        ('0007675150','Charly Phan'),
        ('0007771356','Cherie Shelton'),
        ('0007852405','Cherise Bender'),
        ('0007928003','Cherish Schaefer'),
        ('0007953319','Christiana Kemp'),
        ('0007954960','Christos Zamora'),
        ('0007965016','Chyna Robson'),
        ('0008034804','Cienna Rodgers'),
        ('0008061134','Clarice Clifford'),
        ('0008074163','Clement Fitzgerald'),
        ('0008154347','Codey Yang'),
        ('0008166302','Colby Davison'),
        ('0008263700','Conah Frost'),
        ('0008316299','Conan Waters'),
        ('0008405966','Conna Santos'),
        ('0008487476','Connah Conley'),
        ('0008501538','Conor Jaramillo'),
        ('0008581770','Corban Robin'),
        ('0008661640','Daanyaal Harper'),
        ('0008726403','Daisie Pineda'),
        ('0008809166','Dalton Pike'),
        ('0008833006','Daniaal Cullen'),
        ('0008894645','Danniella Guevara'),
        ('0008960996','Darragh Cobb'),
        ('0009003812','Darren Frost'),
        ('0009084048','Dawud Jones'),
        ('0009132728','Debbie Daniel'),
        ('0009165389','Della Donald'),
        ('0009189016','Denny Meyers'),
        ('0009286146','Destiny Davison'),
        ('0009326589','Devan Walmsley'),
        ('0009357186','Dhruv Floyd'),
        ('0009365073','Dilara Nguyen'),
        ('0009421086','Dione Nava'),
        ('0009467026','Divine Greer'),
        ('0009533209','Dominika Moyer'),
        ('0009559200','Donovan Hardy'),
        ('0009590023','Donovan Knowles'),
        ('0009595544','Donte Benton'),
        ('0009649177','Drew Caldwell'),
        ('0009736238','Eamon Poole'),
        ('0009803610','Ebrahim Sloan'),
        ('0009857906','Eesha Gallegos'),
        ('0009946851','Eilidh Wagner'),
        ('0009950347','Eiliyah Chambers'),
        ('0010012106','Elaina Allen'),
        ('0010047310','Elaine Foreman'),
        ('0010143848','Eleni Clements'),
        ('0010232473','Elias Palmer'),
        ('0010324506','Elias Simon'),
        ('0010343291','Ella-Grace Nieves'),
        ('0010357704','Elleanor Love'),
        ('0010446702','Ellena Brandt'),
        ('0010447143','Elly Bloggs'),
        ('0010495053','Eloisa Lee'),
        ('0010541084','Elyas Fleming'),
        ('0010549061','Eryn Gomez'),
        ('0010569887','Eshaal Delarosa'),
        ('0010650486','Eshaal Steele'),
        ('0010667728','Eugene Bond'),
        ('0010763502','Evie-Grace Bateman'),
        ('0010803835','Fabien Morgan'),
        ('0010882424','Faizah Kirkland'),
        ('0010932815','Fay Nixon'),
        ('0010972477','Finn Black'),
        ('0011070381','Frances Burnett'),
        ('0011114041','Franco Mcintyre'),
        ('0011212968','Georgia Enriquez'),
        ('0011248081','Georgia Leblanc'),
        ('0011257989','Gerald Hendrix'),
        ('0011275608','Gerrard Morin'),
        ('0011364689','Gia Cohen'),
        ('0011451757','Gianni Heath'),
        ('0011510797','Giorgia Kouma'),
        ('0011589807','Glenda Mcculloch'),
        ('0011607732','Gloria Irving'),
        ('0011620323','Gracie-May Escobar'),
        ('0011715582','Grayson Dalton'),
        ('0011801424','Gregory Rose'),
        ('0011856854','Grover Fernandez'),
        ('0011946312','Gwen Kumar'),
        ('0012010521','Habibah Jeffery'),
        ('0012060075','Haley Benson'),
        ('0012120797','Hammad Cooke'),
        ('0012167455','Hayley Gilmour'),
        ('0012225653','Herbert Hyde'),
        ('0012250738','Homer Lynn'),
        ('0012258171','Huey Sharma'),
        ('0012308995','Hughie Ashley'),
        ('0012407796','Husna Pearce'),
        ('0012501886','Hussain Battle'),
        ('0012578851','Huw Pike'),
        ('0012591003','Igor Lovell'),
        ('0012666780','Ilyas Meza'),
        ('0012766218','Indiana Petersen'),
        ('0012776936','Ingrid Merrill'),
        ('0012843207','Iosif Foreman'),
        ('0012921914','Isa Good'),
        ('0012978311','Isabelle Mayer'),
        ('0012980537','Isla-Mae Levy'),
        ('0013079813','Ismael Richmond'),
        ('0013164641','Jacques Sandoval'),
        ('0013232723','Jaden Glover'),
        ('0013234631','Jakub Bone'),
        ('0013291793','Jamila Haworth'),
        ('0013326169','Jana Reilly'),
        ('0013412925','Jana Soto'),
        ('0013447455','Jarod Knapp'),
        ('0013536313','Javan Dickens'),
        ('0013567713','Jaxson Devlin'),
        ('0013653704','Jayce Yoder'),
        ('0013711927','Jayde Sinclair'),
        ('0013771752','Jaye Gilbert'),
        ('0013810819','Jay-Jay Mcnamara'),
        ('0013884497','Jaylen Buck'),
        ('0013926580','Jazmine Weir'),
        ('0014005746','Jazmyn Battle'),
        ('0014076195','Jean-Luc Best'),
        ('0014165703','Jeff Bonner'),
        ('0014215485','Jeffrey Fitzpatrick'),
        ('0014306510','Jeremiah Dale'),
        ('0014308924','Jermaine Collier'),
        ('0014398037','Jessie Hassan'),
        ('0014413780','Joanna Sykes'),
        ('0014471919','Jokubas Mcneil'),
        ('0014519004','Jorja Fisher'),
        ('0014551632','Josephine Harrell'),
        ('0014623513','Judah Schultz'),
        ('0014667269','Jules Walters'),
        ('0014690088','Julie Searle'),
        ('0014769973','June Crossley'),
        ('0014828360','Kaiser Muir'),
        ('0014916746','Kajal Monroe'),
        ('0014938599','Kalvin Macleod'),
        ('0014978725','Katharine Howarth'),
        ('0015037701','Kathleen Hines'),
        ('0015063428','Kathy Delacruz'),
        ('0015109061','Katlyn Bush'),
        ('0015142588','Kavita Ferreira'),
        ('0015214853','Kaydan Derrick'),
        ('0015302236','Kayson Almond'),
        ('0015311982','Keelan Christian'),
        ('0015339123','Keiran Hodges'),
        ('0015385394','Kelis Schmitt'),
        ('0015426433','Kiya Roberts'),
        ('0015517581','Korban Gillespie'),
        ('0015525246','Krystian Whittle'),
        ('0015603076','Kurtis Mccormick'),
        ('0015676070','Kush Horner'),
        ('0015766467','Kyan Warren'),
        ('0015787444','Laurence Ewing'),
        ('0015821429','Layla-Mae Fisher'),
        ('0015859418','Leena Edmonds'),
        ('0015923950','Leia Mclellan'),
        ('0016002760','Lennon Bush'),
        ('0016063735','Lester Mccann'),
        ('0016076320','Lexie Norris'),
        ('0016093194','Lexi-Mai Whitehead'),
        ('0016166582','Lilianna Ortega'),
        ('0016192272','Lily-Grace Edmonds'),
        ('0016202340','Liya Chapman'),
        ('0016266530','Liyana Hatfield'),
        ('0016295191','Luciana Meadows'),
        ('0016342547','Lyndon Gallegos'),
        ('0016390876','Macy Swanson'),
        ('0016435468','Maddy Crawford'),
        ('0016453176','Madeleine Lambert'),
        ('0016511498','Mahad Winters'),
        ('0016513095','Mahima Duffy'),
        ('0016542439','Malia Wiley'),
        ('0016549625','Malika Holden'),
        ('0016559628','Manveer Vega'),
        ('0016634622','Marcelina Walmsley'),
        ('0016689042','Marco Roach'),
        ('0016787321','Marek England'),
        ('0016811520','Marlon Ratliff'),
        ('0016850104','Martha Hammond'),
        ('0016917095','Martina Rich'),
        ('0016957062','Marwa Vaughan'),
        ('0017034609','Mason Black'),
        ('0017045631','Matteo Bowler'),
        ('0017128985','Meg Knights'),
        ('0017131047','Meghan Armstrong'),
        ('0017179032','Michalina Dunn'),
        ('0017213405','Mila Romero'),
        ('0017222023','Moesha Peralta'),
        ('0017319238','Mohamed Storey'),
        ('0017379700','Monique Bishop'),
        ('0017470777','Musa Peck'),
        ('0017557607','Myla Snyder'),
        ('0017562455','Mylah Mcfadden'),
        ('0017619177','Nada Roy'),
        ('0017676088','Nadia Samuels'),
        ('0017766935','Nana Matthews'),
        ('0017829551','Nancie Hood'),
        ('0017846587','Nansi O''Ryan'),
        ('0017889442','Nataniel Robson'),
        ('0017972085','Nathaniel Brook'),
        ('0018009640','Neal Irving'),
        ('0018047893','Ned Patel'),
        ('0018059735','Neel Penn'),
        ('0018078221','Neve Kay'),
        ('0018092626','Nicholas Love'),
        ('0018110074','Nicky Holland'),
        ('0018177605','Nikkita Gilliam'),
        ('0018200420','Nikola Warren'),
        ('0018217655','Nolan Ewing'),
        ('0018246649','Norah West'),
        ('0018311075','Nusaybah Mohammed'),
        ('0018372254','Olivia-Rose Pacheco'),
        ('0018466143','Olivier Lin'),
        ('0018506513','Olly Kramer'),
        ('0018558034','Onur Thornton'),
        ('0018607215','Opal Forbes'),
        ('0018639400','Ophelia Hamilton'),
        ('0018652981','Orson Lovell'),
        ('0018679188','Padraig Fleming'),
        ('0018751468','Parris Leal'),
        ('0018818418','Pawel Mcleod'),
        ('0018880229','Percy Corona'),
        ('0018948878','Philippa Olson'),
        ('0019019003','Pranav Waller'),
        ('0019082750','Raj Knott'),
        ('0019106471','Ralphy Mcloughlin'),
        ('0019203575','Ramone James'),
        ('0019249179','Ray Stanley'),
        ('0019290900','Rebeca Mills'),
        ('0019349784','Rebekka Milner'),
        ('0019372709','Regan O''Reilly'),
        ('0019449455','Reya Lindsey'),
        ('0019521614','Rian Pritchard'),
        ('0019612554','Rihanna Wise'),
        ('0019656227','Rima Conroy'),
        ('0019713574','Robin Griffiths'),
        ('0019808505','Robin Strickland'),
        ('0019841528','Rodney Garner'),
        ('0019913905','Romario Fowler'),
        ('0019968521','Roshan Townsend'),
        ('0020042536','Roy Austin'),
        ('0020120976','Roza Portillo'),
        ('0020193418','Ruby-Leigh Cardenas'),
        ('0020275097','Rufus Nieves'),
        ('0020358212','Ryker Humphreys'),
        ('0020438125','Ryley Henderson'),
        ('0020454743','Sabah Merritt'),
        ('0020552333','Sadie Espinoza'),
        ('0020633667','Saffron Sargent'),
        ('0020723624','Saffron Zavala'),
        ('0020788136','Samara Baker'),
        ('0020822260','Sameer Browning'),
        ('0020905927','Sameer Frost'),
        ('0020954586','Sami Odom'),
        ('0020995166','Samson Burch'),
        ('0020999625','Sana Benjamin'),
        ('0021064755','Sandra Wagstaff'),
        ('0021144632','Santiago Wilkins'),
        ('0021233445','Sapphire Begum'),
        ('0021328007','Sara Foley'),
        ('0021380587','Savanna Clifford'),
        ('0021414857','Savannah Tait'),
        ('0021497845','Scott Morton'),
        ('0021541121','Shahid Rahman'),
        ('0021593064','Shannan Morrison'),
        ('0021656441','Shayne Hawkins'),
        ('0021695276','Shelley Beltran'),
        ('0021763197','Shiv Williams'),
        ('0021833039','Shivani Bray'),
        ('0021884612','Shona Stark'),
        ('0021967085','Shuaib Farmer'),
        ('0022011536','Sid Ingram'),
        ('0022082955','Sonnie Ashley'),
        ('0022141421','Stevie Tait'),
        ('0022204109','Sukhmani Herring'),
        ('0022280494','Suleman Mansell'),
        ('0022285314','Tabitha Delarosa'),
        ('0022381122','Tai Bostock'),
        ('0022385793','Talha Howard'),
        ('0022437704','Tallulah Sykes'),
        ('0022521308','Tamera Ferrell'),
        ('0022584226','Taran Trejo'),
        ('0022595749','Tasnia Mccullough'),
        ('0022628995','Tayah Davis'),
        ('0022640296','Taylor Richards'),
        ('0022704752','Teresa Raymond'),
        ('0022753873','Teri Sloan'),
        ('0022852726','Thierry Busby'),
        ('0022853057','Tiegan Cain'),
        ('0022861297','Tillie Bright'),
        ('0022904765','Tiya David'),
        ('0022964210','Tiya Humphries'),
        ('0023062400','Tommie Pace'),
        ('0023081780','Tony Fox'),
        ('0023156027','Toyah Sheldon'),
        ('0023196862','Trent Bryant'),
        ('0023217161','Trixie Churchill'),
        ('0023235297','Trystan Melendez'),
        ('0023252420','Tudor Coles'),
        ('0023314151','Ty Justice'),
        ('0023357060','Tylor Trevino'),
        ('0023397761','Tylor Turnbull'),
        ('0023401837','Tyron Blankenship'),
        ('0023470815','Umaima Sears'),
        ('0023516020','Uwais Duggan'),
        ('0023539856','Vicky Dunkley'),
        ('0023583359','Waleed Hardin'),
        ('0023658221','Wiktoria Whitehead'),
        ('0023752928','Xavier Price'),
        ('0023820428','Yusef Dillard'),
        ('0023835986','Zaara Couch'),
        ('0023894941','Zahraa Pineda'),
        ('0023902027','Zakir Rivers'),
        ('0023989685','Zane Crowther'),
        ('0024083854','Zarah Lane'),
        ('0024150551','Zayaan Ward')
GO

INSERT INTO [dbo].[CustomsTradePartner] ([customsTradePartnerID],[tradePartnerType],[tradePartnerName]) VALUES
		('BRO-00001', 'Broker', 'Aaron Alexander'),
		('BRO-00002', 'Broker', 'Abraham May'),
		('BRO-00003', 'Broker', 'Ada Sherman'),
		('BRO-00004', 'Broker', 'Alex Fitzgerald'),
		('BRO-00005', 'Broker', 'Alice Farmer'),
		('BRO-00006', 'Broker', 'Annette Daniel'),
		('BRO-00007', 'Broker', 'Anthony Lloyd'),
		('BRO-00008', 'Broker', 'Antoinette Taylor'),
		('BRO-00009', 'Broker', 'April Hicks'),
		('BRO-00010', 'Broker', 'Bernadette Mccoy'),
		('BRO-00011', 'Broker', 'Brian Elliott'),
		('BRO-00012', 'Broker', 'Carroll Alvarez'),
		('BRO-00013', 'Broker', 'Cesar Watkins'),
		('BRO-00014', 'Broker', 'Christopher Black'),
		('BRO-00015', 'Broker', 'Clark Mccormick'),
		('BRO-00016', 'Broker', 'Claude Reese'),
		('BRO-00017', 'Broker', 'Corey Mckenzie'),
		('BRO-00018', 'Broker', 'Cornelius Young'),
		('BRO-00019', 'Broker', 'Darryl Owens'),
		('BRO-00020', 'Broker', 'Daryl Ruiz'),
		('BRO-00021', 'Broker', 'Delbert Cross'),
		('BRO-00022', 'Broker', 'Della Cannon'),
		('BRO-00023', 'Broker', 'Dianna Clayton'),
		('BRO-00024', 'Broker', 'Donna Welch'),
		('BRO-00025', 'Broker', 'Doyle Cooper'),
		('BRO-00026', 'Broker', 'Dwight Barrett'),
		('BRO-00027', 'Broker', 'Earnest Osborne'),
		('BRO-00028', 'Broker', 'Eddie Robbins'),
		('BRO-00029', 'Broker', 'Eleanor Bradley'),
		('BRO-00030', 'Broker', 'Elmer Ramos'),
		('BRO-00031', 'Broker', 'Erick Dennis'),
		('BRO-00032', 'Broker', 'Francis Moran'),
		('BRO-00033', 'Broker', 'Frankie Wilson'),
		('BRO-00034', 'Broker', 'Gene Morton'),
		('BRO-00035', 'Broker', 'Geneva Nichols'),
		('BRO-00036', 'Broker', 'Gilbert Clarke'),
		('BRO-00037', 'Broker', 'Gilberto Mcguire'),
		('BRO-00038', 'Broker', 'Gloria Sims'),
		('BRO-00039', 'Broker', 'Guy Jennings'),
		('BRO-00040', 'Broker', 'Harold Cohen'),
		('BRO-00041', 'Broker', 'Harvey Willis'),
		('BRO-00042', 'Broker', 'Hazel Holt'),
		('BRO-00043', 'Broker', 'Henrietta Allen'),
		('BRO-00044', 'Broker', 'Hope Christensen'),
		('BRO-00045', 'Broker', 'Hugo Perkins'),
		('BRO-00046', 'Broker', 'Ira Carr'),
		('BRO-00047', 'Broker', 'Irma Terry'),
		('BRO-00048', 'Broker', 'Jamie Fowler'),
		('BRO-00049', 'Broker', 'Jeremiah Bennett'),
		('BRO-00050', 'Broker', 'Jermaine Hale'),
		('BRO-00051', 'Broker', 'Jessie Mcdonald'),
		('BRO-00052', 'Broker', 'Joann Castro'),
		('BRO-00053', 'Broker', 'Jonathan Maxwell'),
		('BRO-00054', 'Broker', 'Jonathon Drake'),
		('BRO-00055', 'Broker', 'Juan Ryan'),
		('BRO-00056', 'Broker', 'Julian Lawson'),
		('BRO-00057', 'Broker', 'Kate Roberts'),
		('BRO-00058', 'Broker', 'Kayla French'),
		('BRO-00059', 'Broker', 'Kelly Cruz'),
		('BRO-00060', 'Broker', 'Latoya Hines'),
		('BRO-00061', 'Broker', 'Leigh Burgess'),
		('BRO-00062', 'Broker', 'Lori Rowe'),
		('BRO-00063', 'Broker', 'Lynn Richards'),
		('BRO-00064', 'Broker', 'Margarita Byrd'),
		('BRO-00065', 'Broker', 'Marian Perez'),
		('BRO-00066', 'Broker', 'Marie Page'),
		('BRO-00067', 'Broker', 'Martin Kim'),
		('BRO-00068', 'Broker', 'Matt Hubbard'),
		('BRO-00069', 'Broker', 'Melinda Reyes'),
		('BRO-00070', 'Broker', 'Michelle Simon'),
		('BRO-00071', 'Broker', 'Molly Houston'),
		('BRO-00072', 'Broker', 'Morris Parker'),
		('BRO-00073', 'Broker', 'Nelson Mitchell'),
		('BRO-00074', 'Broker', 'Nick Walters'),
		('BRO-00075', 'Broker', 'Paul Harris'),
		('BRO-00076', 'Broker', 'Peggy Dunn'),
		('BRO-00077', 'Broker', 'Percy Estrada'),
		('BRO-00078', 'Broker', 'Phil Jefferson'),
		('BRO-00079', 'Broker', 'Rafael Miller'),
		('BRO-00080', 'Broker', 'Roderick Hampton'),
		('BRO-00081', 'Broker', 'Ross Murphy'),
		('BRO-00082', 'Broker', 'Roy Nash'),
		('BRO-00083', 'Broker', 'Russell Soto'),
		('BRO-00084', 'Broker', 'Ryan Duncan'),
		('BRO-00085', 'Broker', 'Sergio Bates'),
		('BRO-00086', 'Broker', 'Shannon Morrison'),
		('BRO-00087', 'Broker', 'Sheila Waters'),
		('BRO-00088', 'Broker', 'Sherman Rivera'),
		('BRO-00089', 'Broker', 'Stanley Stephens'),
		('BRO-00090', 'Broker', 'Steven Curry'),
		('BRO-00091', 'Broker', 'Stewart Berry'),
		('BRO-00092', 'Broker', 'Tara Wise'),
		('BRO-00093', 'Broker', 'Terrance Singleton'),
		('BRO-00094', 'Broker', 'Toby Flowers'),
		('BRO-00095', 'Broker', 'Tommy Gross'),
		('BRO-00096', 'Broker', 'Velma Bailey'),
		('BRO-00097', 'Broker', 'Vicky Lewis'),
		('BRO-00098', 'Broker', 'Victor Hall'),
		('BRO-00099', 'Broker', 'Winifred Neal'),
		('BRO-00100', 'Broker', 'Zachary Erickson'),
		('WA-00001', 'Warehouse', 'Cargo Center'),
		('WA-00002', 'Warehouse', 'Cargo Drop'),
		('WA-00003', 'Warehouse', 'Cargo Port'),
		('WA-00004', 'Warehouse', 'Cargo Prime'),
		('WA-00005', 'Warehouse', 'Cargo Type'),
		('WA-00006', 'Warehouse', 'Clear Depot'),
		('WA-00007', 'Warehouse', 'Contactless Capsule'),
		('WA-00008', 'Warehouse', 'Contactless Level'),
		('WA-00009', 'Warehouse', 'Contactlessus'),
		('WA-00010', 'Warehouse', 'Core Express'),
		('WA-00011', 'Warehouse', 'Core Logistics'),
		('WA-00012', 'Warehouse', 'Costoreroom'),
		('WA-00013', 'Warehouse', 'Courier Focus'),
		('WA-00014', 'Warehouse', 'Courier Solutions'),
		('WA-00015', 'Warehouse', 'Cowarehouse'),
		('WA-00016', 'Warehouse', 'Deliver Case'),
		('WA-00017', 'Warehouse', 'Deliver Lab'),
		('WA-00018', 'Warehouse', 'Deliver Logic'),
		('WA-00019', 'Warehouse', 'Deliver Rise'),
		('WA-00020', 'Warehouse', 'Deliveriness'),
		('WA-00021', 'Warehouse', 'Delivery Services'),
		('WA-00022', 'Warehouse', 'Delivery Solutions'),
		('WA-00023', 'Warehouse', 'Delivery Storm'),
		('WA-00024', 'Warehouse', 'Depot Base'),
		('WA-00025', 'Warehouse', 'Dowarehouse'),
		('WA-00026', 'Warehouse', 'Encourier'),
		('WA-00027', 'Warehouse', 'Enwarehouse'),
		('WA-00028', 'Warehouse', 'Express Dash'),
		('WA-00029', 'Warehouse', 'Express Engine'),
		('WA-00030', 'Warehouse', 'Express Glow'),
		('WA-00031', 'Warehouse', 'Express Mode'),
		('WA-00032', 'Warehouse', 'Express Now'),
		('WA-00033', 'Warehouse', 'Express Rise'),
		('WA-00034', 'Warehouse', 'Expressied'),
		('WA-00035', 'Warehouse', 'Hyper Depot'),
		('WA-00036', 'Warehouse', 'Logistics Bay'),
		('WA-00037', 'Warehouse', 'Logistics Case'),
		('WA-00038', 'Warehouse', 'Logistics Hub'),
		('WA-00039', 'Warehouse', 'Logistics Line'),
		('WA-00040', 'Warehouse', 'Logistics Push'),
		('WA-00041', 'Warehouse', 'Logistics Scale'),
		('WA-00042', 'Warehouse', 'Logistics Scout'),
		('WA-00043', 'Warehouse', 'Mail Sense'),
		('WA-00044', 'Warehouse', 'Meta Express'),
		('WA-00045', 'Warehouse', 'Meta Warehouse'),
		('WA-00046', 'Warehouse', 'Real Store'),
		('WA-00047', 'Warehouse', 'Real Warehouse'),
		('WA-00048', 'Warehouse', 'Shipping Scout'),
		('WA-00049', 'Warehouse', 'Shipping Services'),
		('WA-00050', 'Warehouse', 'Shipping Verge'),
		('WA-00051', 'Warehouse', 'Speed Space'),
		('WA-00052', 'Warehouse', 'Speed Span'),
		('WA-00053', 'Warehouse', 'Speedist'),
		('WA-00054', 'Warehouse', 'Start Transit'),
		('WA-00055', 'Warehouse', 'Store Cast'),
		('WA-00056', 'Warehouse', 'Store Focus'),
		('WA-00057', 'Warehouse', 'Storeroomal'),
		('WA-00058', 'Warehouse', 'Storeroomium'),
		('WA-00059', 'Warehouse', 'Storeroomness'),
		('WA-00060', 'Warehouse', 'The Contactless Company'),
		('WA-00061', 'Warehouse', 'The Deliver Company'),
		('WA-00062', 'Warehouse', 'The Delivery Project'),
		('WA-00063', 'Warehouse', 'The Express Club'),
		('WA-00064', 'Warehouse', 'The Shipping Connection'),
		('WA-00065', 'Warehouse', 'The Shipping Group'),
		('WA-00066', 'Warehouse', 'The Warehouse Studio'),
		('WA-00067', 'Warehouse', 'Transit Method'),
		('WA-00068', 'Warehouse', 'Transitic'),
		('WA-00069', 'Warehouse', 'Transitify'),
		('WA-00070', 'Warehouse', 'Transport Design'),
		('WA-00071', 'Warehouse', 'Transport Focus'),
		('WA-00072', 'Warehouse', 'Transport Scout'),
		('WA-00073', 'Warehouse', 'Transport Sense'),
		('WA-00074', 'Warehouse', 'Transport Solutions'),
		('WA-00075', 'Warehouse', 'Transport Vibe'),
		('WA-00076', 'Warehouse', 'Transportly'),
		('WA-00077', 'Warehouse', 'Transportus'),
		('WA-00078', 'Warehouse', 'True Deliver'),
		('WA-00079', 'Warehouse', 'True Transport'),
		('WA-00080', 'Warehouse', 'Updeliver'),
		('WA-00081', 'Warehouse', 'Warehouse Blog'),
		('WA-00082', 'Warehouse', 'Warehouse Capsule'),
		('WA-00083', 'Warehouse', 'Warehouse Center'),
		('WA-00084', 'Warehouse', 'Warehouse Click'),
		('WA-00085', 'Warehouse', 'Warehouse Gram'),
		('WA-00086', 'Warehouse', 'Warehouse Layer'),
		('WA-00087', 'Warehouse', 'Warehouse Logic'),
		('WA-00088', 'Warehouse', 'Warehouse Now'),
		('WA-00089', 'Warehouse', 'Warehouse Pass'),
		('WA-00090', 'Warehouse', 'Warehouse Post'),
		('WA-00091', 'Warehouse', 'Warehouse Press'),
		('WA-00092', 'Warehouse', 'Warehouse Sense'),
		('WA-00093', 'Warehouse', 'Warehouse Side'),
		('WA-00094', 'Warehouse', 'Warehouse Space'),
		('WA-00095', 'Warehouse', 'Warehouse Span'),
		('WA-00096', 'Warehouse', 'Warehouse Sync'),
		('WA-00097', 'Warehouse', 'Warehouse Type'),
		('WA-00098', 'Warehouse', 'Warehouse Yard'),
		('WA-00099', 'Warehouse', 'Warehouseced'),
		('WA-00100', 'Warehouse', 'Warehouseked'),
		('CA-00001', 'Carrier', 'Active Carrier'),
		('CA-00002', 'Carrier', 'Aircraft Carrier Boost'),
		('CA-00003', 'Carrier', 'Cargo Drop'),
		('CA-00004', 'Carrier', 'Cargo Layer'),
		('CA-00005', 'Carrier', 'Cargo Port'),
		('CA-00006', 'Carrier', 'Carrier Bay'),
		('CA-00007', 'Carrier', 'Carrier Center'),
		('CA-00008', 'Carrier', 'Carrier Click'),
		('CA-00009', 'Carrier', 'Carrier Dock'),
		('CA-00010', 'Carrier', 'Carrier Engine'),
		('CA-00011', 'Carrier', 'Carrier Glow'),
		('CA-00012', 'Carrier', 'Carrier Load'),
		('CA-00013', 'Carrier', 'Carrier Mark'),
		('CA-00014', 'Carrier', 'Carrier Mode'),
		('CA-00015', 'Carrier', 'Carrier Now'),
		('CA-00016', 'Carrier', 'Carrier Pass'),
		('CA-00017', 'Carrier', 'Carrier Prime'),
		('CA-00018', 'Carrier', 'Carrier Scale'),
		('CA-00019', 'Carrier', 'Carrier Space'),
		('CA-00020', 'Carrier', 'Carrier Storm'),
		('CA-00021', 'Carrier', 'Carrier Stripe'),
		('CA-00022', 'Carrier', 'Carrier Vibe'),
		('CA-00023', 'Carrier', 'Carrierates'),
		('CA-00024', 'Carrier', 'Carriereable'),
		('CA-00025', 'Carrier', 'Carrierent'),
		('CA-00026', 'Carrier', 'Carrierian'),
		('CA-00027', 'Carrier', 'Carrierid'),
		('CA-00028', 'Carrier', 'Carrieried'),
		('CA-00029', 'Carrier', 'Carrierium'),
		('CA-00030', 'Carrier', 'Carrierness'),
		('CA-00031', 'Carrier', 'Carrierried'),
		('CA-00032', 'Carrier', 'Core Cargo'),
		('CA-00033', 'Carrier', 'Core Express'),
		('CA-00034', 'Carrier', 'Core Logistics'),
		('CA-00035', 'Carrier', 'Core Mailman'),
		('CA-00036', 'Carrier', 'Core Transport'),
		('CA-00037', 'Carrier', 'Courier Focus'),
		('CA-00038', 'Carrier', 'Courier Kit'),
		('CA-00039', 'Carrier', 'Courier Layer'),
		('CA-00040', 'Carrier', 'Courier Pass'),
		('CA-00041', 'Carrier', 'Courier Solutions'),
		('CA-00042', 'Carrier', 'Deliver Rise'),
		('CA-00043', 'Carrier', 'Deliveriness'),
		('CA-00044', 'Carrier', 'Delivery Ideas'),
		('CA-00045', 'Carrier', 'Delivery Solutions'),
		('CA-00046', 'Carrier', 'Delivery Vibe'),
		('CA-00047', 'Carrier', 'Delivery Yard'),
		('CA-00048', 'Carrier', 'Encarrier'),
		('CA-00049', 'Carrier', 'Express Now'),
		('CA-00050', 'Carrier', 'Express Rise'),
		('CA-00051', 'Carrier', 'Express Span'),
		('CA-00052', 'Carrier', 'Expressful'),
		('CA-00053', 'Carrier', 'Hyper Transit'),
		('CA-00054', 'Carrier', 'Logistics Hub'),
		('CA-00055', 'Carrier', 'Logistics Scale'),
		('CA-00056', 'Carrier', 'Logistics Scout'),
		('CA-00057', 'Carrier', 'Mail Post'),
		('CA-00058', 'Carrier', 'Mail Value'),
		('CA-00059', 'Carrier', 'Mailman Spark'),
		('CA-00060', 'Carrier', 'Mailmanged'),
		('CA-00061', 'Carrier', 'Make Carrier'),
		('CA-00062', 'Carrier', 'Meta Carrier'),
		('CA-00063', 'Carrier', 'Meta Shipping'),
		('CA-00064', 'Carrier', 'Postman Store'),
		('CA-00065', 'Carrier', 'Real Transport'),
		('CA-00066', 'Carrier', 'Shipping Services'),
		('CA-00067', 'Carrier', 'Shipping Sync'),
		('CA-00068', 'Carrier', 'Start Mailman'),
		('CA-00069', 'Carrier', 'The Cargo Club'),
		('CA-00070', 'Carrier', 'The Carrier Studio'),
		('CA-00071', 'Carrier', 'The Contactless Company'),
		('CA-00072', 'Carrier', 'The Courier Factory'),
		('CA-00073', 'Carrier', 'The Deliver Company'),
		('CA-00074', 'Carrier', 'The Delivery Project'),
		('CA-00075', 'Carrier', 'The Express Company'),
		('CA-00076', 'Carrier', 'The Logistics Factory'),
		('CA-00077', 'Carrier', 'The Shipping Group'),
		('CA-00078', 'Carrier', 'The Transport Project'),
		('CA-00079', 'Carrier', 'Toter Hub'),
		('CA-00080', 'Carrier', 'Transit Method'),
		('CA-00081', 'Carrier', 'Transitic'),
		('CA-00082', 'Carrier', 'Transitify'),
		('CA-00083', 'Carrier', 'Transport Design'),
		('CA-00084', 'Carrier', 'Transport Drop'),
		('CA-00085', 'Carrier', 'Transport Hub'),
		('CA-00086', 'Carrier', 'Transport Scout'),
		('CA-00087', 'Carrier', 'Transport Solutions'),
		('CA-00088', 'Carrier', 'Transport Spot'),
		('CA-00089', 'Carrier', 'Transport Vibe'),
		('CA-00090', 'Carrier', 'Transportation Design'),
		('CA-00091', 'Carrier', 'Transportation Lab'),
		('CA-00092', 'Carrier', 'Transportation Now'),
		('CA-00093', 'Carrier', 'Transportation Solutions'),
		('CA-00094', 'Carrier', 'Transportation Span'),
		('CA-00095', 'Carrier', 'Transportation Sync'),
		('CA-00096', 'Carrier', 'Transportationeable'),
		('CA-00097', 'Carrier', 'Transportish'),
		('CA-00098', 'Carrier', 'Transportly'),
		('CA-00099', 'Carrier', 'Transportus'),
		('CA-00100', 'Carrier', 'Zen Toter')
GO

