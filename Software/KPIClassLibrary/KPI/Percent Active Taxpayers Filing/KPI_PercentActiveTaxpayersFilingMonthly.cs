﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxpayersegment", // Group
        "month" // Time Period
        )]
    public class KPI_PercentActiveTaxpayersFilingMonthly : KPI
    {
        public KPI_PercentActiveTaxpayersFilingMonthly() : base(
            KPIPeriodType.Month,
            KPIGroupType.TaxPayerSegment
            )   
        {
            SP_Name = "KPI_DATA_KPIPercentActiveTaxpayersFilingMonthly";
            SP_ValueFieldID = "percentTaxPayersFiling";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "segmentName";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@NumberOfMonths", System.Data.SqlDbType.Int, null);
        }
    }
}
