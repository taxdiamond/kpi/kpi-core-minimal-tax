﻿using System;

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxpayersegment", // Group
        "year" // Time Period
        )]
    public class KPI_PercentActiveTaxpayersFilingYearly : KPI
    {
        public KPI_PercentActiveTaxpayersFilingYearly() : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxPayerSegment
            )
        {
            SP_Name = "KPI_DATA_KPIPercentActiveTaxpayersFilingYearly";
            SP_ValueFieldID = "percentTaxPayersFiling";
            SP_AutomaticFiltering = false;
            SP_GroupFieldID = "segmentName";

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
