﻿using System;

// NEW 21-06-2021

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "office", // Group
        "year" // Time Period
        )]
    public class KPI_NumberRegisteredTaxPayersYearlyByOffice : KPI
    {
        public KPI_NumberRegisteredTaxPayersYearlyByOffice() : base(
            KPIPeriodType.Year,
            KPIGroupType.Office
         )
        {
            SP_Name = "KPI_DATA_NumberOfRegisteredTaxPayersYearlyByOffice";
            SP_ValueFieldID = "numberOfRegisteredTaxPayers";
            SP_GroupFieldID = "officeID";

            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
