﻿using System;

// NEW 21-06-2021

namespace KPIClassLibrary.KPI
{
    [KPIMetaData(
        "taxtype", // Group
        "year" // Time Period
        )]
    public class KPI_NumberRegisteredTaxPayersYearlyByTypeOfTax : KPI
    {
        public KPI_NumberRegisteredTaxPayersYearlyByTypeOfTax() : base(
            KPIPeriodType.Year,
            KPIGroupType.TaxType
         )
        {
            SP_Name = "KPI_DATA_NumberOfRegisteredTaxPayersYearlyByTypeOfTax";
            SP_ValueFieldID = "numberOfRegisteredTaxPayers";
            SP_GroupFieldID = "conceptID";

            SP_AutomaticFiltering = false;

            AddSupportedChartType(KPIChartType.Bar_MultiSeries);
            AddSupportedChartType(KPIChartType.Column_Multiseries);
            AddSupportedChartType(KPIChartType.SingleValue_Multiseries);

            AddSPParameter("@LimitYear", System.Data.SqlDbType.Int, null);
            AddSPParameter("@NumberOfYears", System.Data.SqlDbType.Int, null);
        }
    }
}
