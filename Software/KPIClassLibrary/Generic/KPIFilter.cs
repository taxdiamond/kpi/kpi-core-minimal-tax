﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    public class KPIFilter
    {
        public KPIFilterType FilterType { get; set; }
        public string FilterValue { get; set; }

        public KPIFilter()
        {
        }

        public KPIFilter(KPIFilterType type, string value)
        {
            this.FilterType = type;
            this.FilterValue = value;
        }
    }
}
