﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KPIClassLibrary.KPI
{
    public class KPIMetaDataInformation
    {
        public string ID { get; set; }
        public string GroupID { get; set; }
        public List<KPICatalogParameter> Parameters { get; set; }
        public KPIMetaDataInformation()
        {
        }

        public static List<KPIMetaDataInformation> LoadMetadataInformationFromJSONCOnfigurationFile(string jsonConfigurationFile)
        {
            List<KPIMetaDataInformation> information = new List<KPIMetaDataInformation>();

            if (!File.Exists(jsonConfigurationFile))
                throw new ArgumentException($"Unknown KPI Metadata Information JSON configration file {jsonConfigurationFile}");

            String JSONtxt = File.ReadAllText(jsonConfigurationFile);
            information = Newtonsoft.Json.JsonConvert.DeserializeObject<List<KPIMetaDataInformation>>(JSONtxt);

            if (information == null || information.Count == 0)
                throw new ArgumentException($"Empty KPI Metadata Information JSON configration file {jsonConfigurationFile}"); ;

            return information;
        }
    }
}
