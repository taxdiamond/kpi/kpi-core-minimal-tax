﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace KPIClassLibrary.KPI
{
    class GenericKPILoadDataFromSQLQuery
    {
        public string ConnectionString { get; set; }
        public GenericKPILoadDataFromSQLQuery(string connectionString)
        {
            if (String.IsNullOrEmpty(connectionString))
                throw new ArgumentException("Invalid empty connection string");
            ConnectionString = connectionString;
        }

        public void LoadData(KPI theKPI)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    string spName = "[dbo].[" + theKPI.SP_Name + "]";

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(spName, conn);

                    foreach (var item in theKPI.GetAllSPParameters())
                    {
                        SqlParameter parameter = new SqlParameter();
                        parameter.ParameterName = item.ParameterName;
                        parameter.SqlDbType = item.ParameterType;
                        parameter.Value = item.ParameterValue;
                        cmd.Parameters.Add(parameter);
                    }

                    if(theKPI.SP_AutomaticFiltering)
                    {
                        SqlParameter parameter = new SqlParameter();
                        parameter.ParameterName = "@WhereClause";
                        parameter.SqlDbType = SqlDbType.NVarChar;
                        parameter.Value = theKPI.GetFilterWhereClause();
                        cmd.Parameters.Add(parameter);
                    }

                    //open connection
                    conn.Open();

                    //set the SqlCommand type to stored procedure and execute
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader dr = cmd.ExecuteReader();

                    //check if there are records
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            ProcessRow(dr, theKPI);
                        }
                    }
                    else
                    {
                        // No data found
                    }

                    //close data reader
                    dr.Close();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ProcessRow(SqlDataReader dr, KPI theKPI)
        {
            int yearColumnIndex = -1;
            int monthColumnIndex = -1;
            int dayColumnIndex = -1;
            int quarterColumnIndex = -1;
            int valueColumnIndex = -1;
            int groupColumnIndex = -1;

            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).ToLower() == "year")
                    yearColumnIndex = i;
                if (dr.GetName(i).ToLower() == "month")
                    monthColumnIndex = i;
                if (dr.GetName(i).ToLower() == "day")
                    dayColumnIndex = i;
                if (dr.GetName(i).ToLower() == "quarter")
                    quarterColumnIndex = i;
                if (dr.GetName(i).ToLower() == theKPI.SP_ValueFieldID.ToLower())
                    valueColumnIndex = i;
                if (theKPI.SP_GroupFieldID != null && dr.GetName(i).ToLower() == theKPI.SP_GroupFieldID.ToLower())
                    groupColumnIndex = i;
            }

            if(valueColumnIndex == -1)
                throw new Exception($"Results do not include a required value column with name {theKPI.SP_ValueFieldID}");
            if (yearColumnIndex == -1)
                throw new Exception("Results do not include a required year column");
            if(theKPI.PeriodType == KPIPeriodType.Month && monthColumnIndex == -1)
                throw new Exception("Results do not include a required month column");
            if (theKPI.PeriodType == KPIPeriodType.Day && monthColumnIndex == -1)
                throw new Exception("Results do not include a required month column");
            if (theKPI.PeriodType == KPIPeriodType.Day && dayColumnIndex == -1)
                throw new Exception("Results do not include a required day column");
            if (theKPI.PeriodType == KPIPeriodType.Quarter && yearColumnIndex == -1)
                throw new Exception("Results do not include a required year column");
            if (theKPI.PeriodType == KPIPeriodType.Quarter && quarterColumnIndex == -1)
                throw new Exception("Results do not include a required quarter column");

            int dayValue = 1;
            int monthValue = 1;
            int yearValue = dr.GetInt32(yearColumnIndex);
            int quarterValue = 1;
            string groupValue = "";
            double value = 0.00;

            switch (theKPI.PeriodType)
            {
                case KPIPeriodType.Day:
                    dayValue = dr.GetInt32(dayColumnIndex);
                    monthValue = dr.GetInt32(monthColumnIndex);
                    break;
                case KPIPeriodType.Month:
                    monthValue = dr.GetInt32(monthColumnIndex);
                    break;
                case KPIPeriodType.Year:
                    break;
                case KPIPeriodType.Quarter:
                    quarterValue = dr.GetInt32(quarterColumnIndex);
                    if (quarterValue < 1 || quarterValue > 4)
                        throw new Exception($"Invalid quarter value of {quarterValue}. Expected a number between 1 and 4");
                    dayValue = 1;
                    switch (quarterValue)
                    {
                        case 1:
                            monthValue = 1;
                            break;
                        case 2:
                            monthValue = 4;
                            break;
                        case 3:
                            monthValue = 7;
                            break;
                        case 4:
                            monthValue = 10;
                            break;
                        default:
                            throw new Exception($"Invalid quarter value of {quarterValue}. Expected a number between 1 and 4");
                    }
                    break;
                default:
                    break;
            }

            if(theKPI.Grouping != KPIGroupType.None && groupColumnIndex == -1)
                throw new Exception("Results do not include a required group column");

            if(theKPI.Grouping != KPIGroupType.None)
            {
                groupValue = dr.GetString(groupColumnIndex);
            }

            if (dr.GetValue(valueColumnIndex) is DBNull)
                value = 0;
            else
                value = Convert.ToDouble(dr.GetValue(valueColumnIndex));

            if (theKPI.Grouping == KPIGroupType.None)
                theKPI.AddData(new DateTime(yearValue, monthValue, dayValue), value);
            else
                theKPI.AddData(new DateTime(yearValue, monthValue, dayValue), theKPI.Grouping, groupValue, value);
        }
    }
}
