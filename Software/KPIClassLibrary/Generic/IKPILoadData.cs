﻿using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    public interface IKPILoadData
    {
        void LoadData(KPI kpi);
    }
}
