﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    [System.AttributeUsage(System.AttributeTargets.Class |
                           System.AttributeTargets.Struct,
                           AllowMultiple = true)  // Multiuse attribute.  
    ]
    public class KPIMetaData : System.Attribute
    {
        string group;
        string period;

        public KPIMetaData(string group, string period)
        {
            this.group = group;
            this.period = period;
        }

        public string GetGroup()
        {
            return group;
        }

        public string GetPeriod()
        {
            return period;
        }
    }
}
