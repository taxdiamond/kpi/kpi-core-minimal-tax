﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    public enum KPIResultType
    {
        Percent, Number, Money
    }

    public class KPICatalogArea
    {
        public string GroupID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Dimension { get; set; }
        public string Unit { get; set; }
        public string YAxisLabel { get; set; }
        public KPIResultType ResultType { get; set; }
        public string Formula { get; set; }
        public List<KPIType> Type { get; set; }
        public KPIDomain Domain { get; set; }

        public List<KPIVariable> Variables { get; set; }
        public List<KPICatalogItem> KPIs { get; set; }

        public KPICatalogArea()
        {
        }
    }
}
