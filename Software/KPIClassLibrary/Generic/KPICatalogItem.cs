﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KPIClassLibrary.KPI
{
    public class KPICatalogItem
    {
        public string ClassName { get; set; }
        public KPIPeriodType TimePeriod { get; set; }
        public KPIGroupType Group { get; set; }
        public List<KPICatalogParameter> Parameters { get; set; }
        public KPICatalogArea TheKPIArea { get; set; }
        public KPICatalogItem()
        {              
        }
    }
}
