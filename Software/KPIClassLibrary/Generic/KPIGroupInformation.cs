﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace KPIClassLibrary.KPI
{
    public class KPIVariable
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public KPIVariable()
        {

        }
    }

    public class KPIGroupInformation
    {
        public string GroupID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Dimension { get; set; }
        public string Unit { get; set; }
        public string YAxis { get; set; }
        public string ResultType { get; set; }
        public List<KPIVariable> Variables { get; set; }
        public string Formula { get; set; }
        public List<string> Type { get; set; }
        public string Domain { get; set; }

        public KPIGroupInformation()
        {
        }

        public static List<KPIGroupInformation> LoadKPIInformationFromJSONCOnfigurationFile(string jsonConfigurationFile)
        {
            List<KPIGroupInformation> information = new List<KPIGroupInformation>();

            if (!File.Exists(jsonConfigurationFile))
                throw new ArgumentException($"Unknown KPI Group Information JSON configration file {jsonConfigurationFile}");

            String JSONtxt = File.ReadAllText(jsonConfigurationFile);
            information = Newtonsoft.Json.JsonConvert.DeserializeObject<List<KPIGroupInformation>>(JSONtxt);

            if (information == null || information.Count == 0)
                throw new ArgumentException($"Empty KPI Group Information JSON configration file {jsonConfigurationFile}"); ;

            return information;
        }
    }
}
