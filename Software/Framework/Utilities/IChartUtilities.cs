﻿using KPIClassLibrary.KPI;
using System.Threading.Tasks;

namespace Framework.Utilities
{
    public interface IChartUtilities
    {
        KPIClassLibrary.KPI.KPI GetKPIFromCatalog(string kpiKey, string withFilter);
        string GetFlavor(KPI theKpi);

        Task CreateSampleByIndex(string userid, int idx, string domain);
        Task CreateSampleFirst(string userid, string dashboardType, string domain);
    }
}
