﻿using System;
using System.Threading.Tasks;

namespace Framework.Dashboard
{
    public interface IDashboardService
    {
        Task RowMoveUp(string dashboardid, Guid rowid);
        Task RowMoveDown(string dashboardid, Guid rowid);
    }
}
