﻿using Domain.Aggregate;
using Domain.ValueObjects.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Dashboard
{
    public class DashboardConfiguration : Entity<DashboardConfigurationGuid>
    {
        public Guid DashboardConfigurationId { get; set; }
        public Dashboard DashboardRef { get; set; }
        public List<DashboardRow> Rows { get; set; }
        
        public DashboardConfiguration() : base(null) {
            Rows = new List<DashboardRow>();
        }
        public DashboardConfiguration(Action<object> applier) : base(applier)
        {
            Rows = new List<DashboardRow>();
        }

        protected override void When(object @event)
        {
            switch(@event)
            {
                case Domain.Events.Dashboard.DashboardRowUpdate e:
                    Id = new DashboardConfigurationGuid(e.DashboardConfigurationId);
                    DashboardConfigurationId = Id.Value;
                    //DashboardRow row = Rows.First<DashboardRow>(o => o.DashboardRowId == e.DashboardRowId);
                    break;
            }
        }
    }
}
