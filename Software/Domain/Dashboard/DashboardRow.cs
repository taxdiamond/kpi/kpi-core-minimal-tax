﻿using Domain.Aggregate;
using Domain.Events.Dashboard;
using Domain.Indicators;
using Domain.ValueObjects.Dashboard;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Dashboard
{
    public class DashboardRow : Entity<DashboardRowGuid>
    {
        public Guid DashboardRowId { get; set; }
        public PositiveInteger RowNumber { get; set; }
        public List<Kpi> Kpis { get; set; }
        public DashboardConfiguration ConfigurationRef { get; set; }

        public DashboardRow() :base(null){ }
        public DashboardRow(Action<object> applier) : base(applier)
        {
            
        }
        protected override void When(object @event)
        {
            switch(@event)
            {
                case DashboardRowCreated e:
                    DashboardConfiguration config = new DashboardConfiguration()
                    {
                        DashboardConfigurationId = Guid.Parse(e.DashboardconfigurationId)
                    };
                    DashboardRowId = Guid.Parse(e.DashboardRowId);
                    ConfigurationRef = config;
                    break;
                case DashboardRowUpdate e:
                    RowNumber = e.RowNumber;
                    break;
            }
        }
    }

    public class SortByRowNumber : IComparer<DashboardRow>
    {
        public int Compare([AllowNull] DashboardRow x, [AllowNull] DashboardRow y)
        {
            if (x == null || y == null)
                return -1;

            return x.RowNumber.CompareTo(y.RowNumber);
        }
    }
}
