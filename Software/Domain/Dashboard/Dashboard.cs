﻿using Domain.Aggregate;
using Domain.Events.Dashboard;
using Domain.Exceptions;
using Domain.Security;
using Domain.ValueObjects.Dashboard;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Dashboard
{
    public class Dashboard : AggregateRoot<DashboardGuid>
    {
        public Guid DashboardId { get; set; }
        public Name250 Name { get; set; }
        public DashboardType Type { get; set; }
        public DashboardStatus Status { get; set; }
        public ApplicationUser Creator { get; set; }

        public DomainType Domain { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = DashboardId != null;
            switch(Status)
            {
                case DashboardStatus.DRAFT:
                    valid = valid &&
                        Name != null;
                    break;
                case DashboardStatus.PUBLISHED:
                case DashboardStatus.HIDDEN:
                    valid = valid &&
                        Name != null &&
                        Creator != null;
                    break;
            }
            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed in state {Status}");
        }

        protected override void When(object @event)
        {
            switch( @event)
            {
                case DashboardCreated e:
                    this.DashboardId = new DashboardGuid(e.DashboardId);
                    this.Name = new Name250(e.Name);
                    this.Type = e.Type;
                    this.Creator = e.Creator;
                    this.Status = DashboardStatus.DRAFT;
                    break;
            }
        }
    }
}
