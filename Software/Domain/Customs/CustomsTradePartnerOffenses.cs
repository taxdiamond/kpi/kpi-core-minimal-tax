﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsTradePartnerOffenses : AggregateRoot<IdInt>
    {
        public Int32 CustomsTradePartnerOffensesItemID { get; set; }
        public DateType CustomsTradePartnerOffensesItemDate { get; set; }
        public CustomsTradePartner.CustomsTradePartner CustomsTradePartnerID { get; set; }
        public PositiveInteger NumberOfViolations { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = CustomsTradePartnerOffensesItemDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsTradePartnerOffenses");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {

                case CustomsTradePartnerOffensesCreated e:
                    this.CustomsTradePartnerOffensesItemDate = DateType.FromDateTime(e.CustomsTradePartnerOffensesItemDate);
                    this.CustomsTradePartnerID.customsTradePartnerID = Id20.FromString(e.CustomsTradePartnerID);
                    this.NumberOfViolations = PositiveInteger.FromInt(e.NumberOfViolations);
                    break;
            }
        }
    }
}
