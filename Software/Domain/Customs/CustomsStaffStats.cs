﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsStaffStats : AggregateRoot<IdInt>
    {
        public Int32 CustomsStaffStatsItemID { get; set; }
        public DateType CustomsStaffStatsDate { get; set; }
        public CustomsStaff.CustomsStaff CustomsStaffID { get; set; }
        public PositiveInteger NumberOfDocumentaryInspections { get; set; }
        public PositiveInteger NumberOfDetectionsForDocumentaryInspections { get; set; }
        public PositiveInteger NumberOfPhysicalInspections { get; set; }
        public PositiveInteger NumberOfDetectionsForPhysicalInspections { get; set; }
        public PositiveInteger NumberOfRiskAlertsReceived { get; set; }
        public PositiveInteger NumberOfDetectionsForRiskAlerts { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = CustomsStaffStatsDate != null;

            valid = valid &&
               CustomsStaffID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsStaffStats");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {

                case CustomsStaffStatsCreated e:
                    this.CustomsStaffStatsDate = DateType.FromDateTime(e.CustomsStaffStatsDate);
                    this.CustomsStaffID.customsStaffID = Id20.FromString(e.CustomsStaffID); 
                    this.NumberOfDocumentaryInspections = PositiveInteger.FromInt(e.NumberOfDocumentaryInspections);
                    this.NumberOfDetectionsForDocumentaryInspections = PositiveInteger.FromInt(e.NumberOfDetectionsForDocumentaryInspections);
                    this.NumberOfPhysicalInspections = PositiveInteger.FromInt(e.NumberOfPhysicalInspections);
                    this.NumberOfDetectionsForPhysicalInspections = PositiveInteger.FromInt(e.NumberOfDetectionsForPhysicalInspections);
                    this.NumberOfRiskAlertsReceived = PositiveInteger.FromInt(e.NumberOfRiskAlertsReceived);
                    this.NumberOfDetectionsForRiskAlerts = PositiveInteger.FromInt(e.NumberOfDetectionsForRiskAlerts);
                    break;
            }
        }
    }
}
