﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsTradePartnerStats : AggregateRoot<IdInt>
    {
        public Int32 TradePartnerStatsItemID { get; set; }
        public DateType TradePartnerStatsItemDate { get; set; }
        public PositiveInteger NumberOfRegisteredImporters { get; set; }
        public PositiveInteger NumberOfRegisteredExporters { get; set; }
        public PositiveInteger NumberOfImporters70PercentRevenue { get; set; }
        public PositiveInteger NumberOfRegisteredBrokers { get; set; }
        public PositiveInteger NumberOfRegisteredCarriers { get; set; }
        public PositiveInteger NumberOfRegisteredWarehouses { get; set; }
        public PositiveInteger CancelledLicences { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = TradePartnerStatsItemDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsTradePartnerStats");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {

                case CustomsTradePartnerStatsCreated e:
                    this.TradePartnerStatsItemDate = DateType.FromDateTime(e.TradePartnerStatsItemDate);
                    this.NumberOfRegisteredImporters = PositiveInteger.FromInt(e.NumberOfRegisteredImporters);
                    this.NumberOfRegisteredExporters = PositiveInteger.FromInt(e.NumberOfRegisteredExporters);
                    this.NumberOfImporters70PercentRevenue = PositiveInteger.FromInt(e.NumberOfImporters70PercentRevenue);
                    this.NumberOfRegisteredBrokers = PositiveInteger.FromInt(e.NumberOfRegisteredBrokers);
                    this.NumberOfRegisteredCarriers = PositiveInteger.FromInt(e.NumberOfRegisteredCarriers);
                    this.NumberOfRegisteredWarehouses = PositiveInteger.FromInt(e.NumberOfRegisteredWarehouses);
                    this.CancelledLicences = PositiveInteger.FromInt(e.CancelledLicences);
                    break;
            }
        }
    }
}
