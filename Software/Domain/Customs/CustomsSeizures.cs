﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Customs
{
    public class CustomsSeizures : AggregateRoot<IdInt>
    {
        public Int32 CustomsSeizuresItemID { get; set; }
        public DateType CustomsSeizuresDate { get; set; }
        public CustomsPost.CustomsPost CustomsPostID { get; set; }
        public Name250 SeizureType { get; set; }
        public Name250 SeizureCategory { get; set; }
        public Name250 SeizureTransportMode { get; set; }
        public Name250 SeizureMode { get; set; }
        public PositiveDecimal SeizureValue { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = CustomsSeizuresDate != null &&
                CustomsPostID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsSeizures");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case CustomsSeizuresCreated e:
                    this.CustomsSeizuresDate = DateType.FromDateTime(e.CustomsSeizuresDate);
                    this.CustomsPostID.customsPostID = Id20.FromString(e.CustomsPostID);
                    this.SeizureType =  Name250.FromString(e.SeizureType);
                    this.SeizureCategory = Name250.FromString(e.SeizureCategory);
                    this.SeizureTransportMode = Name250.FromString(e.SeizureTransportMode);
                    this.SeizureMode = Name250.FromString(e.SeizureMode);
                    this.SeizureValue = PositiveDecimal.FromDecimal(e.SeizureValue);
                    break;
            }
        }
    }
}
