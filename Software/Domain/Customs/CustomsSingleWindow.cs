﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsSingleWindow : AggregateRoot<IdInt>
    {
        public Int32 CustomsSingleWindowItemID { get; set; }
        public DateType CustomsSingleWindowItemDate { get; set; }
        public PositiveInteger TotalCustomsProceduresProcessed { get; set; }
        public PositiveInteger TotalCustomsProceduresElectronic { get; set; }
        public PositiveInteger NumberCertificationsIssuance { get; set; }
        public PositiveInteger NumberOGARequirements { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = CustomsSingleWindowItemDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsSingleWindow");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {

                case CustomsSingleWindowCreated e:
                    this.CustomsSingleWindowItemDate = DateType.FromDateTime(e.CustomsSingleWindowItemDate);
                    this.TotalCustomsProceduresProcessed = PositiveInteger.FromInt(e.TotalCustomsProceduresProcessed);
                    this.TotalCustomsProceduresElectronic = PositiveInteger.FromInt(e.TotalCustomsProceduresElectronic);
                    this.NumberCertificationsIssuance = PositiveInteger.FromInt(e.NumberCertificationsIssuance);
                    this.NumberOGARequirements = PositiveInteger.FromInt(e.NumberOGARequirements);
                    break;
            }
        }
    }
}
