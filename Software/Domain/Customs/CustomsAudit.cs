﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsAudit : AggregateRoot<IdInt>
    {
        public Int32 CustomsAuditItemID { get; set; }
        public DateType CustomsAuditDate { get; set; }
        public PositiveDecimal YieldFromDeskReviewAudits { get; set; }
        public PositiveInteger NumberActiveDeskReviewAuditors { get; set; }
        public PositiveInteger NumberActiveFieldAuditors { get; set; }
        public PositiveInteger NumberAuditsMade { get; set; }
        public PositiveInteger NumberScheduledAudits { get; set; }
        public PositiveDecimal AverageNumberDeskAuditExaminationsByStaff { get; set; }
        public PositiveDecimal AverageNumberFieldAuditsByStaff { get; set; }
        public PositiveDecimal TotalAmountAssessedFromAudits { get; set; }
        public PositiveInteger NumberPenalDenunciations { get; set; }
        

        protected override void ValidateStatus()
        {
            bool valid = CustomsAuditDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsAudit");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {

                case CustomsAuditCreated e:
                    this.CustomsAuditDate = DateType.FromDateTime(e.CustomsAuditDate);
                    this.YieldFromDeskReviewAudits = PositiveDecimal.FromDecimal(e.YieldFromDeskReviewAudits);
                    this.NumberActiveDeskReviewAuditors = PositiveInteger.FromInt(e.NumberActiveDeskReviewAuditors);
                    this.NumberActiveFieldAuditors = PositiveInteger.FromInt(e.NumberActiveFieldAuditors);
                    this.NumberAuditsMade = PositiveInteger.FromInt(e.NumberAuditsMade);
                    this.NumberScheduledAudits = PositiveInteger.FromInt(e.NumberScheduledAudits);
                    this.AverageNumberDeskAuditExaminationsByStaff = PositiveDecimal.FromDecimal(e.AverageNumberDeskAuditExaminationsByStaff);
                    this.AverageNumberFieldAuditsByStaff = PositiveDecimal.FromDecimal(e.AverageNumberFieldAuditsByStaff);
                    this.TotalAmountAssessedFromAudits = PositiveDecimal.FromDecimal(e.TotalAmountAssessedFromAudits);
                    this.NumberPenalDenunciations = PositiveInteger.FromInt(e.NumberPenalDenunciations);
                    break;
            }
        }
    }
}
