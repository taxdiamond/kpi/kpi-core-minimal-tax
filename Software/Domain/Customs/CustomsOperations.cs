﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsOperations : AggregateRoot<IdInt>
    {
        public Int32 CustomsOperationsItemID { get; set; }
        public DateType CustomsOperationsDate { get; set; }
        public CustomsPost.CustomsPost CustomsPostID { get; set; }
        public PositiveInteger NumberOfCustomsDeclarations { get; set; }
        public PositiveInteger NumberOfImportDeclarations { get; set; }
        public PositiveInteger NumberOfExportDeclarations { get; set; }
        public PositiveInteger NumberOfArrivedTransitOperations { get; set; }
        public PositiveInteger NumberOfUnArrivedTransitOperations { get; set; }
        public PositiveInteger NumberOfDrawbackTransactions { get; set; }
        public PositiveInteger NumberOfImportDeclarationsPhysicallyExamined { get; set; }
        public PositiveInteger NumberOfDetectionsForPhysicalExaminationsImports { get; set; }
        public PositiveInteger NumberOfImportDeclarationsWithDocumentExamination { get; set; }
        public PositiveInteger NumberOfDetectionsForDocumentExaminationsImports { get; set; }
        public PositiveInteger NumberOfImportDeclarationsUnderAEOProgram { get; set; }
        public PositiveDecimal AverageTimeForDocumentaryCompliance { get; set; }
        public PositiveDecimal AverageTimeAtTerminalSinceArrival { get; set; }
        public PositiveInteger NumberBillsForOverstayedShipments { get; set; }
        public PositiveInteger NumberLabTestsForImports { get; set; }
        public PositiveInteger NumberPreArrivalClearancesForImports { get; set; }
        public PositiveInteger NumberReleasesPriorDeterminationAndPayments { get; set; }
        public PositiveInteger NumberSelfFilingOperationsForImports { get; set; }
        public PositiveInteger NumberOperationsUnderPreferentialTreatement { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = CustomsOperationsDate != null &&
                CustomsPostID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsOperations");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case CustomsOperationsCreated e:
                    this.CustomsOperationsDate = DateType.FromDateTime(e.CustomsOperationsDate);
                    this.CustomsPostID.customsPostID = Id20.FromString(e.CustomsPostID);
                    this.NumberOfCustomsDeclarations = PositiveInteger.FromInt(e.NumberOfCustomsDeclarations);
                    this.NumberOfImportDeclarations = PositiveInteger.FromInt(e.NumberOfImportDeclarations);
                    this.NumberOfExportDeclarations = PositiveInteger.FromInt(e.NumberOfExportDeclarations);
                    this.NumberOfArrivedTransitOperations = PositiveInteger.FromInt(e.NumberOfArrivedTransitOperations);
                    this.NumberOfUnArrivedTransitOperations = PositiveInteger.FromInt(e.NumberOfUnArrivedTransitOperations);
                    this.NumberOfDrawbackTransactions = PositiveInteger.FromInt(e.NumberOfDrawbackTransactions);
                    this.NumberOfImportDeclarationsPhysicallyExamined = PositiveInteger.FromInt(e.NumberOfImportDeclarationsPhysicallyExamined);
                    this.NumberOfDetectionsForPhysicalExaminationsImports = PositiveInteger.FromInt(e.NumberOfDetectionsForPhysicalExaminations);
                    this.NumberOfImportDeclarationsWithDocumentExamination = PositiveInteger.FromInt(e.NumberOfImportDeclarationsWithDocumentExamination);
                    this.NumberOfDetectionsForDocumentExaminationsImports = PositiveInteger.FromInt(e.NumberOfDetectionsForDocumentExaminations);
                    this.NumberOfImportDeclarationsUnderAEOProgram = PositiveInteger.FromInt(e.NumberOfImportDeclarationsUnderAEOProgram);
                    this.AverageTimeForDocumentaryCompliance = PositiveDecimal.FromDecimal(e.AverageTimeForDocumentaryCompliance);
                    this.AverageTimeAtTerminalSinceArrival = PositiveDecimal.FromDecimal(e.AverageTimeAtTerminalSinceArrival);
                    this.NumberBillsForOverstayedShipments = PositiveInteger.FromInt(e.NumberBillsForOverstayedShipments);
                    this.NumberLabTestsForImports = PositiveInteger.FromInt(e.NumberLabTestsForImports);
                    this.NumberPreArrivalClearancesForImports = PositiveInteger.FromInt(e.NumberPreArrivalClearancesForImports);
                    this.NumberReleasesPriorDeterminationAndPayments = PositiveInteger.FromInt(e.NumberReleasesPriorDeterminationAndPayments);
                    this.NumberSelfFilingOperationsForImports = PositiveInteger.FromInt(e.NumberSelfFilingOperationsForImports);
                    this.NumberOperationsUnderPreferentialTreatement = PositiveInteger.FromInt(e.NumberOperationsUnderPreferentialTreatement);
                    break;
            }
        }
    }
}
