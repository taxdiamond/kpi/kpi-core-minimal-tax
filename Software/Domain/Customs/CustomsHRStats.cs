﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsHRStats : AggregateRoot<IdInt>
    {
        public Int32 CustomsHRStatsItemID { get; set; }
        public DateType CustomsHRStatsItemDate { get; set; }
        public CustomsPost.CustomsPost CustomsPostID { get; set; }
        public PositiveInteger NumberFrontLineStaff { get; set; }
        public PositiveInteger NumberSpecializedEmployees { get; set; }
        public PositiveInteger NumberSpecializedEmployeesTrained { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = CustomsHRStatsItemDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsHRStats");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {

                case CustomsHRStatsCreated e:
                    this.CustomsHRStatsItemDate = DateType.FromDateTime(e.CustomsHRStatsItemDate);
                    this.NumberFrontLineStaff = PositiveInteger.FromInt(e.NumberFrontLineStaff);
                    this.NumberSpecializedEmployees = PositiveInteger.FromInt(e.NumberSpecializedEmployees);
                    this.NumberSpecializedEmployeesTrained = PositiveInteger.FromInt(e.NumberSpecializedEmployeesTrained);
                    break;
            }
        }
    }
}
