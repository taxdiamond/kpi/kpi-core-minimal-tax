﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsOperationsByTransportMode : AggregateRoot<IdInt>
    {
        public Int32 CustomsOperationID { get; set; }
        public DateType CustomsOperationDate { get; set; }
        public Name250 ModeOfTransport { get; set; }
        public PositiveDecimal TotalImportsAmount { get; set; }
        public PositiveDecimal TotalExportsAmount { get; set; }
        public PositiveDecimal TotalTemporaryImportsAmount { get; set; }
        public PositiveInteger FreeTradeZoneOperations { get; set; }
        public PositiveInteger PassengersProcessed { get; set; }
        public PositiveDecimal ClearanceTimeImports { get; set; }
        public PositiveDecimal ClearanceTimeImportsRedLane { get; set; }
        public PositiveDecimal ClearanceTimeImportsYellowLane { get; set; }
        public PositiveDecimal ClearanceTimeImportsGreenLane { get; set; }
        public PositiveInteger TotalCarriers { get; set; }
        public PositiveInteger TotalCarriersLodingAdvancedElectronicInfo { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = CustomsOperationDate != null &&
                ModeOfTransport != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsOperationsByTransportMode");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case CustomsOperationsByTransportModeCreated e:
                    this.CustomsOperationDate = DateType.FromDateTime(e.CustomsOperationDate);
                    this.ModeOfTransport.Value = Id20.FromString(e.ModeOfTransport);
                    this.TotalImportsAmount = PositiveDecimal.FromDecimal(e.TotalImportsAmount);
                    this.TotalExportsAmount = PositiveDecimal.FromDecimal(e.TotalExportsAmount);
                    this.TotalTemporaryImportsAmount = PositiveDecimal.FromDecimal(e.TotalTemporaryImportsAmount);
                    this.FreeTradeZoneOperations = PositiveInteger.FromInt(e.FreeTradeZoneOperations);
                    this.PassengersProcessed = PositiveInteger.FromInt(e.PassengersProcessed);
                    this.ClearanceTimeImports = PositiveDecimal.FromDecimal(e.ClearanceTimeImports);
                    this.ClearanceTimeImportsRedLane = PositiveDecimal.FromDecimal(e.ClearanceTimeImportsRedLane);
                    this.ClearanceTimeImportsYellowLane = PositiveDecimal.FromDecimal(e.ClearanceTimeImportsYellowLane);
                    this.ClearanceTimeImportsGreenLane = PositiveDecimal.FromDecimal(e.ClearanceTimeImportsGreenLane);
                    break;
            }
        }
    }
}
