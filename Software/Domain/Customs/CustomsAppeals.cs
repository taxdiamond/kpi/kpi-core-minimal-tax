﻿using Domain.Aggregate;
using Domain.Events.Customs;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Customs
{
    public class CustomsAppeals : AggregateRoot<IdInt>
    {
        public Int32 CustomsAppealsItemID { get; set; }
        public DateType CustomsAppealsDate { get; set; }
        public PositiveInteger NumberAppealsConsidered { get; set; }
        public PositiveInteger NumberAppealsResolved { get; set; }
        public PositiveInteger NumberAppealsUndergoLegalAction { get; set; }
        public PositiveInteger NumberJudicialDecissionsFavorableCustoms { get; set; }
        public PositiveInteger NumberLawsuitsFinished { get; set; }
        public PositiveDecimal AverageTimeToResolveAppeals { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = CustomsAppealsDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsAppeals");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case CustomsAppealsCreated e:
                    this.CustomsAppealsDate = DateType.FromDateTime(e.CustomsAppealsDate);
                    this.NumberAppealsConsidered = PositiveInteger.FromInt(e.NumberAppealsConsidered);
                    this.NumberAppealsResolved = PositiveInteger.FromInt(e.NumberAppealsResolved);
                    this.NumberAppealsUndergoLegalAction = PositiveInteger.FromInt(e.NumberAppealsUndergoLegalAction);
                    this.NumberJudicialDecissionsFavorableCustoms = PositiveInteger.FromInt(e.NumberJudicialDecissionsFavorableCustoms);
                    this.NumberLawsuitsFinished = PositiveInteger.FromInt(e.NumberLawsuitsFinished);
                    this.AverageTimeToResolveAppeals = PositiveDecimal.FromDecimal(e.AverageTimeToResolveAppeals);
                    break;
            }
        }
    }
}
