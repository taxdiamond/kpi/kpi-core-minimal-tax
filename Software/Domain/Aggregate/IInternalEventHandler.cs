﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Aggregate
{
    public interface IInternalEventHandler
    {
        void Handle(object @event);
    }
}
