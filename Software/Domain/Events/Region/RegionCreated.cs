﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Region
{
    public class RegionCreated
    {
        public string regionID { get; set; }
        public string regionName { get; set; }
    }
}
