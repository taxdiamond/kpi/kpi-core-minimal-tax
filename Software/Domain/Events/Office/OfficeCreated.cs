﻿namespace Domain.Events.Office
{
    public class OfficeCreated
    {
        public string officeID { get; set; }
        public string officeName { get; set; }
    }
}
