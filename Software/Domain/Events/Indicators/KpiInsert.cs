﻿using Domain.Enums.Indicators;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Indicators
{
    public class KpiInsert
    {
        public string Title { get; internal set; }
        public KpiType Type { get; internal set; }
        public KpiViewModelType ViewType { get; internal set; }

        public Guid DashboardId { get; set; }
        public Guid RowId { get; set; }
        public int CellNumber { get; set; }
    }
}
