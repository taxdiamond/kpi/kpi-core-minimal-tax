﻿using Domain.Indicators;
using Domain.Security;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Indicators
{
    public class DomainUserInsert
    {
        public string Domain { get; set; }
        public ApplicationUser User { get; set; }
    }
}
