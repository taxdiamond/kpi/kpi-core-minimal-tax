﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class TaxAdministrationExpenseCreated
    {
        public DateTime TaxAdministrationExpenseDate { get; set; }
        public string RegionID { get; set; }
        public string OfficeID { get; set; }
        public decimal CurrentExpense { get; set; }

    }
}
