﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class ImportCreated
    {
        public DateTime ImportDate { get; set; }
        public decimal TotalImports { get; set; }

    }
}
