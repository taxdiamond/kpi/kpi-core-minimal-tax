﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class FilingStatsCreated
    {
        public DateTime ReportDate { get; set; }
        public string TaxType { get; set; }
        public string SegmentID { get; set; }
        public int ExpectedFilers { get; set; }
        public int TotalFilings { get; set; }
        public int StopFilers { get; set; }
        public int EFilings { get; set; }
        public int TotalFilingsRequiringPayment { get; set; }
        public int FilingsPaidElectronically { get; set; }
        public int LateFilers { get; set; }
    }
}
