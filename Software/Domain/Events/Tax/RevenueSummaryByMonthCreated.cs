﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class RevenueSummaryByMonthCreated
    {
        public DateTime RevenueDate { get; set; }
        public decimal TotalRevenue { get; set; }
        public decimal TotalRevenuePaidInTIme { get; set; }
        public decimal TotalRevenueFromArrears { get; set; }
        public decimal TotalRevenueFromFines { get; set; }
        public decimal TotalRevenueFromAudits { get; set; }
        public decimal TotalRevenueFromEnforcementCollection { get; set; }
        public decimal TotalRevenueFromVoluntaryCompliance { get; set; }
        public decimal TotalRevenueFromTaxes { get; set; }
        public decimal TotalRevenueFromLitigation { get; set; }
        public string RegionID { get; set; }
        public string OfficeID { get; set; }
        public string SegmentID { get; set; }

    }
}
