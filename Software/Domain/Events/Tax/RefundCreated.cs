﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    class RefundCreated
    {
        public Int32 RefundItemID { get; set; }
        public DateTime RefundsDate { get; set; }
        public string RegionID { get; set; }
        public string OfficeID { get; set; }
        public string SegmentID { get; set; }
        public string Concept { get; set; }
        public string ConceptID { get; set; }
        public int NumberRefundClaimsRequested { get; set; }
        public int NumberRequestsClaimsProcessed { get; set; }
        public decimal RefundAmountRequested { get; set; }
        public decimal RefundAmountProcessed { get; set; }

    }
}
