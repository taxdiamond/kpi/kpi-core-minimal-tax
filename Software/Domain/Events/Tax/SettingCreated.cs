﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class SettingCreated 
    {
        public string ValueAddedTaxName { get; set; }
        public string CorporateIncomeTaxname { get; set; }
    }
}
