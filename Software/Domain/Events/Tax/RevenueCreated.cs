﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class RevenueCreated
    {
        public DateTime RevenueDate { get; set; }
        public string RegionID { get; set; }
        public string OfficeID { get; set; }
        public string SegmentID { get; set; }
        public string Concept { get; set; }
        public string ConceptID { get; set; }
        public decimal Amount { get; set; }
    }
}
