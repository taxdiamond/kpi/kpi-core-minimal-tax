﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class HumanResourceCreated
    {
        public DateTime HRItemReportDate { get; set; }
        public string RegionID { get; set; }
        public string OfficeID { get; set; }
        public int TotalHumanResources { get; set; }
        public int TotalHumanResourcesInTaxCollection { get; set; }
        public int TotalHumanResourcesInTaxPayerServices { get; set; }
        public int TotalHumanResourcesInTaxEnforcement { get; set; }
        public int TotalHumanResourcesInAudits { get; set; }
        public int TotalHumanResourcesInAppeals { get; set; }
        public int NumberPositionsFilledCompetitiveProcess { get; set; }
    }
}
