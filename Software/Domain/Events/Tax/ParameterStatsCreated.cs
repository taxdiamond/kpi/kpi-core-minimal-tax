﻿using System;

namespace Domain.Events.Tax
{
    public class ParameterStatsCreated
    {
        public string ParameterName { get; set; }
        public DateTime ValueDate { get; set; }
        public DateTime TimeStamp { get; set; }
        public decimal GeneratedValue { get; set; }

    }
}
