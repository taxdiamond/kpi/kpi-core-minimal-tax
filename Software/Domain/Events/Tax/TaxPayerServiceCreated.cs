﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class TaxPayerServiceCreated
    {
        public Int32 ServicesItemID { get; set; }
        public DateTime ServicesItemReportDate { get; set; }
        public int NumberOfServicesProvidedTaxpayers { get; set; }
        public int NumberOfElectronicServicesProvidedTaxpayers { get; set; }

    }
}
