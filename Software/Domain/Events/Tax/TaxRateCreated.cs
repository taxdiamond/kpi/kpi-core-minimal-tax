﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class TaxRateCreated
    {
        public DateTime TaxRateDate { get; set; }
        public string TaxName { get; set; }
        public decimal Rate { get; set; }
    }
}
