﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Tax
{
    public class AuditStatsCreated
    {
        public DateTime StatsDate { get; set; }
        public int NumberOfAuditsPerformed { get; set; }
        public int NumberOfAuditsResultedInAdditionalAssessments { get; set; }
        public decimal? TotalAdditionalAssessment { get; set; }
        public string RegionID { get; set; }
        public string OfficeID { get; set; }
        public string SegmentID { get; set; }

    }
}
