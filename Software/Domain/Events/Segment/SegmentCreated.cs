﻿namespace Domain.Events.Segment
{
    public class SegmentCreated
    {
        public string segmentID { get; set; }
        public string segmentName { get; set; }
    }
}
