﻿namespace Domain.Events.CustomsTradePartner
{
    class CustomsTradePartnerCreated
    {
        public string customsTradePartnerID { get; set; }
        public string tradePartnerType { get; set; }
        public string tradePartnerName { get; set; }
    }
}
