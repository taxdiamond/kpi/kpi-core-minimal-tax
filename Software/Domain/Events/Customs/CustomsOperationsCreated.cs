﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsOperationsCreated
    {
        public DateTime CustomsOperationsDate { get; set; }
        public string CustomsPostID { get; set; }
        public int NumberOfCustomsDeclarations { get; set; }
        public int NumberOfImportDeclarations { get; set; }
        public int NumberOfExportDeclarations { get; set; }
        public int NumberOfArrivedTransitOperations { get; set; }
        public int NumberOfUnArrivedTransitOperations { get; set; }
        public int NumberOfDrawbackTransactions { get; set; }
        public int NumberOfImportDeclarationsPhysicallyExamined { get; set; }
        public int NumberOfDetectionsForPhysicalExaminations { get; set; }
        public int NumberOfImportDeclarationsWithDocumentExamination { get; set; }
        public int NumberOfDetectionsForDocumentExaminations { get; set; }
        public int NumberOfImportDeclarationsUnderAEOProgram { get; set; }
        public decimal AverageTimeForDocumentaryCompliance { get; set; }
        public decimal AverageTimeAtTerminalSinceArrival { get; set; }
        public int NumberBillsForOverstayedShipments { get; set; }
        public int NumberLabTestsForImports { get; set; }
        public int NumberPreArrivalClearancesForImports { get; set; }
        public int NumberReleasesPriorDeterminationAndPayments { get; set; }
        public int NumberSelfFilingOperationsForImports { get; set; }
        public int NumberOperationsUnderPreferentialTreatement { get; set; }
    }
}
