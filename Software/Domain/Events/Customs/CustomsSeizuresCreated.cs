﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsSeizuresCreated
    {
        public DateTime CustomsSeizuresDate { get; set; }
        public string CustomsPostID { get; set; }
        public string SeizureType { get; set; }
        public string SeizureCategory { get; set; }
        public string SeizureTransportMode { get; set; }
        public string SeizureMode { get; set; }
        public decimal SeizureValue { get; set; }
    }
}
