﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsSatisfactionSurveyCreated
    {
        public Int32 SatisfactionSurveyItemID { get; set; }
        public DateTime SurveyItemReportDate { get; set; }
        public string SurveyArea { get; set; }
        public decimal SurveyAreaScore { get; set; }

    }
}
