﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsAEOCreated
    {
        public DateTime CustomsAEODate { get; set; }
        public string AEOEntityGroup { get; set; }
        public int NumberEnterprisesWithAEOCert { get; set; }
        public int NumberSMEEnterprisesWithAEOCert { get; set; }
        public decimal AccreditationAvergeTime { get; set; }
    }
}
