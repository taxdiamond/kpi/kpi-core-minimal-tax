﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsAuditCreated
    {
        public DateTime CustomsAuditDate { get; set; }
        public decimal YieldFromDeskReviewAudits { get; set; }
        public int NumberActiveDeskReviewAuditors { get; set; }
        public int NumberActiveFieldAuditors { get; set; }
        public int NumberAuditsMade { get; set; }
        public int NumberScheduledAudits { get; set; }
        public decimal AverageNumberDeskAuditExaminationsByStaff { get; set; }
        public decimal AverageNumberFieldAuditsByStaff { get; set; }
        public decimal TotalAmountAssessedFromAudits { get; set; }
        public int NumberPenalDenunciations { get; set; }
    }
}
