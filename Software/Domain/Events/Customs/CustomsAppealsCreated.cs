﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsAppealsCreated
    {
        public DateTime CustomsAppealsDate { get; set; }
        public int NumberAppealsConsidered { get; set; }
        public int NumberAppealsResolved { get; set; }
        public int NumberAppealsUndergoLegalAction { get; set; }
        public int NumberJudicialDecissionsFavorableCustoms { get; set; }
        public int NumberLawsuitsFinished { get; set; }
        public int AverageTimeToResolveAppeals { get; set; }
    }
}
