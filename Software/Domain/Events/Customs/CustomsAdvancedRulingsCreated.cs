﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsAdvancedRulingsCreated
    {
        public DateTime CustomsAdvancedRulingsDate { get; set; }
        public int NumberRulingRequestsOrigin { get; set; }
        public int NumberRulingRequestsTariff { get; set; }
        public int NumberRulingRequests { get; set; }
    }
}
