﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsAdministrationExpensesCreated
    {
        public DateTime CustomsAdministrationExpensesDate { get; set; }
        public string CustomsPostID { get; set; }
        public decimal CurrentExpense { get; set; }
    }
}
