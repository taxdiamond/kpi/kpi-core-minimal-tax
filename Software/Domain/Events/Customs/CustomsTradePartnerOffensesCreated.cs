﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsTradePartnerOffensesCreated
    {
        public DateTime CustomsTradePartnerOffensesItemDate { get; set; }
        public string CustomsTradePartnerID { get; set; }
        public int NumberOfViolations { get; set; }
    }
}
