﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsOperationsByTransportModeCreated
    {
        public DateTime CustomsOperationDate { get; set; }
        public string ModeOfTransport { get; set; }
        public decimal TotalImportsAmount { get; set; }
        public decimal TotalExportsAmount { get; set; }
        public int TotalTemporaryImportsAmount { get; set; }
        public int FreeTradeZoneOperations { get; set; }
        public int PassengersProcessed { get; set; }
        public decimal ClearanceTimeImports { get; set; }
        public decimal ClearanceTimeImportsRedLane { get; set; }
        public decimal ClearanceTimeImportsYellowLane { get; set; }
        public decimal ClearanceTimeImportsGreenLane { get; set; }
        public int TotalCarriers { get; set; }
        public int TotalCarriersLodingAdvancedElectronicInfo { get; set; }
    }
}
