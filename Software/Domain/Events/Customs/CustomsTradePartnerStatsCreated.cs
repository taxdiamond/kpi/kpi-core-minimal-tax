﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Customs
{
    public class CustomsTradePartnerStatsCreated
    {
        public DateTime TradePartnerStatsItemDate { get; set; }
        public int NumberOfRegisteredImporters { get; set; }
        public int NumberOfRegisteredExporters { get; set; }
        public int NumberOfImporters70PercentRevenue { get; set; }
        public int NumberOfRegisteredBrokers { get; set; }
        public int NumberOfRegisteredCarriers { get; set; }
        public int NumberOfRegisteredWarehouses { get; set; }
        public int CancelledLicences { get; set; }

    }
}
