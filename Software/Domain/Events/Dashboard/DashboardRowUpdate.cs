﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Dashboard
{
    public class DashboardRowUpdate
    {
        public Guid DashboardRowId { get; set; }
        public Guid DashboardConfigurationId { get; set; }
        public int RowNumber { get; set; }
    }
}
