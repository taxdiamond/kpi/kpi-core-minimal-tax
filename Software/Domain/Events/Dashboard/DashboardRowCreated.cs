﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Events.Dashboard
{
    public class DashboardRowCreated
    {
        public string DashboardconfigurationId { get; set; }
        public string DashboardRowId { get; set; }
        public int RowNumber { get; set; }
    }
}
