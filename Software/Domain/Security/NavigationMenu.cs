﻿using System.Collections.Generic;

namespace Domain.Security
{
    public class NavigationMenu
    {
        public string Menu { get; set; }
        public string Url { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string ResourceItem { get; set; }
        public bool Public { get; set; }
        public string Claim { get; set; }
        public string Icon { get; set; }

        public List<NavigationMenu> submenus { get; set; }

        public NavigationMenu()
        {
            submenus = new List<NavigationMenu>();
        }
    }
}
