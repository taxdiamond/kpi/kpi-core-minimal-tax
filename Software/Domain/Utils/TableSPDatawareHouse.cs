﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Utils
{
    
    public class TableSPDatawareHouse
    {
        public string TableName { get; set; }
        public string SpNameInsert { get; set; }
        public string SpNameGetLastDate { get; set; }
        public string ColumnDate { get; set; }
        public List<ParameterSQLType> Parameters { get; set; }

        public TableSPDatawareHouse()
        {

        }
    }

    public class ParameterSQLType
    {
        public string Name { get; set; }
        public string Type { get; set; }

        public ParameterSQLType()
        {

        }
    }
}
