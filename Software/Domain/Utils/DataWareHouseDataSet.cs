﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Utils
{
    public class DataWareHouseDataSet
    {
        public DataSet DataSetDW { get; set; }
        public TableSPDatawareHouse tableSpDW { get; set; }
        public DataWareHouseDataSet()
        {
            
        }
        public DataWareHouseDataSet(TableSPDatawareHouse table,DataSet ds)
        {
            tableSpDW = table;
            DataSetDW = ds;
        }
    }
}
