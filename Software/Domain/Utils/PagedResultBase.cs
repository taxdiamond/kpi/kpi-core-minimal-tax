﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Utils
{
    public class PagedResultBase<T>
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalRows { get; set; }

        public List<T> ResultList { get; set; }

        public int FirstRowOnPage
        {

            get { return (CurrentPage - 1) * PageSize + 1; }
        }

        public int LastRowOnPage
        {
            get { return Math.Min(CurrentPage * PageSize, TotalRows); }
        }

        public int PageCount 
        {
            get
            {
                return (int)Math.Ceiling(TotalRows / (double)PageSize);
            }
        }

        public PagedResultBase()
        {
            this.ResultList = new List<T>();
        }
    }
}
