﻿using Domain.DataWarehouse;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IDataWarehouseRepository
    {
        Task<List<DataWarehouseRecord>> GetDataWarehouseRecords();        
    }
}
