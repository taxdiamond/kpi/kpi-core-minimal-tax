﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IDashboardUserRepository
    {
        Task<bool> Exists(Guid dashboardGuid, Guid userId);
        Task<Dashboard.DashboardUser> Create(Dashboard.DashboardUser obj);
        Task Remove(Guid id);
        Task Remove(Guid dashboardGuid, Guid userId);
        Task<Dashboard.DashboardUser> FindById(Guid id);
        Task<List<Dashboard.DashboardUser>> FindByDashboardId(Guid id);
        Task<List<Dashboard.DashboardUser>> FindByUserId(Guid id);

    }
}
