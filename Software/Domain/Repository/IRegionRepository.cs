﻿using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IRegionRepository
    {
        Task<bool> Exists(string regionID);
        Task<Region.Region> Create(Region.Region obj);
        Region.Region Update(Region.Region obj);
        Task Remove(string id);
        Task<Region.Region> FindById(string id);
        List<Region.Region> FindAll(string query);
        Task<PagedResultBase<Domain.Region.Region>> FindAll(int pageSize, int currentPage);

    }
}
