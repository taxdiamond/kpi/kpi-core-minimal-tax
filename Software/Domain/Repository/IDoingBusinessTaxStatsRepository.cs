﻿using Domain.DoingBusinessStats;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IDoingBusinessTaxStatsRepository
    {
        public Task<PagedResultBase<DoingBusinessTaxStats>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
