﻿using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IRevenueSummaryByMonthRepository
    {
        Task<bool> Exists(int id);
        Task<RevenueSummaryByMonth> Create(RevenueSummaryByMonth obj);
        RevenueSummaryByMonth Update(RevenueSummaryByMonth obj);
        Task Remove(int id);
        Task<RevenueSummaryByMonth> FindById(int id);
        List<RevenueSummaryByMonth> FindAll(string query);
        Task<PagedResultBase<RevenueSummaryByMonth>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
