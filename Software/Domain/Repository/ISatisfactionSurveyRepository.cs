﻿using Domain.Tax;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repository
{
    public interface ISatisfactionSurveyRepository : IRepository<SatisfactionSurvey,int>
    {
    }
}
