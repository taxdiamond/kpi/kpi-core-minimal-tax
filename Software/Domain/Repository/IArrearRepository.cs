﻿using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IArrearRepository
    {
        Task<bool> Exists(int id);
        Task<Arrear> Create(Arrear obj);
        Arrear Update(Arrear obj);
        Task Remove(int id);
        Task<Arrear> FindById(int id);
        List<Arrear> FindAll(string query);
        public Task<PagedResultBase<Arrear>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
