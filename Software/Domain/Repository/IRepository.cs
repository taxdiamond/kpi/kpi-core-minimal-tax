﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IRepository<T,Id>
    {
        Task<bool> Exists(Id id);
        Task<T> Create(T obj);
        T Update(T obj);
        Task Remove(Id id);
        Task<T> FindById(Id id);
        List<T> FindAll(string query);
    }
}
