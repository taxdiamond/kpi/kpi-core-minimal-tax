﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repository
{
    public interface ICustomsPostRepository : IRepository<CustomsPost.CustomsPost,string>
    {
    }
}
