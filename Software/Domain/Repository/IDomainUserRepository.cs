﻿using Domain.Indicators;
using System;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IDomainUserRepository : IRepository<Domain.Indicators.DomainUser,Guid>
    {
        Task<DomainUser> FindDomainTypeByUserId(string userId);
    }
}
