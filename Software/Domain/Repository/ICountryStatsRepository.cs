﻿using Domain.CountryStats;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface ICountryStatsRepository
    {
        Task<bool> Exists(int id);
        Task<CountryStats.CountryStats> Create(CountryStats.CountryStats obj);
        CountryStats.CountryStats Update(CountryStats.CountryStats obj);
        Task Remove(int id);
        Task<CountryStats.CountryStats> FindById(int id);
        List<CountryStats.CountryStats> FindAll(string query);
        Task<PagedResultBase<CountryStats.CountryStats>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
