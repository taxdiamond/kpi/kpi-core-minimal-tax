﻿using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IOfficeRepository
    {
        Task<bool> Exists(string OfficeID);
        Task<Office.Office> Create(Office.Office obj);
        Office.Office Update(Office.Office obj);
        Task Remove(string id);
        Task<Office.Office> FindById(string id);
        List<Office.Office> FindAll(string query);
        Task<PagedResultBase<Office.Office>> FindAll(int pageSize, int currentPage);


    }
}
