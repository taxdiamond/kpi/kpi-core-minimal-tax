﻿using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface ITaxPayerServiceRepository : IRepository<TaxPayerService,int>
    {

        Task<PagedResultBase<TaxPayerService>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
