﻿using Domain.Utils;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface ISegmentRepository
    {
        Task<bool> Exists(string SegmentID);
        Task<Segment.Segment> Create(Segment.Segment obj);
        Segment.Segment Update(Segment.Segment obj);
        Task Remove(string id);
        Task<Segment.Segment> FindById(string id);
        List<Segment.Segment> FindAll(string query);
        Task<PagedResultBase<Segment.Segment>> FindAll(int pageSize, int currentPage);
    }
}
