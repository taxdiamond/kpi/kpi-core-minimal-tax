﻿using Domain.ValueObjects.Indicators;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IKpiRepository
    {
        Task<Domain.Indicators.Kpi> Load(string id, bool noTracking);
        Task Add(Domain.Indicators.Kpi entity);
        Task Delete(KpiGuid id, Domain.Indicators.Kpi kpiToDelete);
        void Update(Domain.Indicators.Kpi entity);
        Task<bool> Exists(KpiGuid id);
        List<Domain.Indicators.Kpi> FindByRow(Guid rowId);
    }
}
