﻿using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IRevenueRepository
    {
        Task<bool> Exists(int id);
        Task<Revenue> Create(Revenue obj);
        Revenue Update(Revenue obj);
        Task Remove(int id);
        Task<Revenue> FindById(int id);
        List<Revenue> FindAll(string query);
        Task<PagedResultBase<Revenue>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
