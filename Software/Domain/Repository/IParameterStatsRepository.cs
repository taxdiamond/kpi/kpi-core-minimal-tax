﻿using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IParameterStatsRepository
    {
        Task<bool> Exists(string regionID);
        Task<ParameterStats> Create(ParameterStats obj);
        ParameterStats Update(ParameterStats obj);
        Task Remove(int id);
        Task<ParameterStats> FindById(int id);
        List<ParameterStats> FindAll(string query);
    }
}
