﻿using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface IRevenueForecastRepository
    {
        Task<bool> Exists(int id);
        Task<RevenueForecast> Create(RevenueForecast obj);
        RevenueForecast Update(RevenueForecast obj);
        Task Remove(int id);
        Task<RevenueForecast> FindById(int id);
        List<RevenueForecast> FindAll(string query);
        Task<PagedResultBase<RevenueForecast>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
