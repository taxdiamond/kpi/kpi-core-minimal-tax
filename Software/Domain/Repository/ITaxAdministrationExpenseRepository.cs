﻿using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface ITaxAdministrationExpenseRepository
    {
        Task<bool> Exists(int id);
        Task<TaxAdministrationExpense> Create(TaxAdministrationExpense obj);
        TaxAdministrationExpense Update(TaxAdministrationExpense obj);
        Task Remove(int id);
        Task<TaxAdministrationExpense> FindById(int id);
        List<TaxAdministrationExpense> FindAll(string query);
        Task<PagedResultBase<TaxAdministrationExpense>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo);
    }
}
