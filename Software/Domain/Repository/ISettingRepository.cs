﻿using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repository
{
    public interface ISettingRepository
    {
        Task<bool> Exists(int id);
        Task<Setting> Create(Setting obj);
        Setting Update(Setting obj);
        Task Remove(int id);
        Task<Setting> FindById(int id);
        List<Setting> FindAll(string query);
    }
}
