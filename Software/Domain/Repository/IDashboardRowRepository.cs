﻿using Domain.Dashboard;
using Domain.ValueObjects.Dashboard;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repository
{
    public interface IDashboardRowRepository : IRepository<DashboardRow, DashboardRowGuid>
    {
        List<DashboardRow> FindByDashboard(Guid guid);

        void Remove(DashboardRow obj);
    }
}
