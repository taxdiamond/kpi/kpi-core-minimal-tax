﻿using Domain.Aggregate;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DoingBusinessStats
{
    public class DoingBusinessTaxStats : AggregateRoot<IdInt>
    {

        public Int32 DoingBusinessStatsID { get; set; }
        public DateType StatsDate { get; set; }
        public decimal? Time { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = Time >= 0;

            valid = valid &&
                StatsDate != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for DoingBusinessTaxStats");
        }

        protected override void When(object @event)
        {

        }
    }
}
