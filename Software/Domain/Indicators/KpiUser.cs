﻿using Domain.Aggregate;
using Domain.Enums.Indicators;
using Domain.Exceptions;
using Domain.Security;
using Domain.ValueObjects.Indicators;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Indicators
{
    public class KpiUser : AggregateRoot<KpiUserGuid>
    {
        public Guid KpiUserId { get; set; }
        public Kpi KpiRef { get; set; }
        public ApplicationUser UserRef { get; set; }
        public KpiViewModelType ChartType { get; set; }
        public PositiveInteger NumberOfPeriods { get; set; }
        public string FilterRegions { get; set; }
        public string FilterOffices { get; set; }
        public string FilterTaxPayerSegments { get; set; }
        public KpiUser()
        {
            NumberOfPeriods = 5;
            FilterRegions = "";
            FilterOffices = "";
            FilterTaxPayerSegments = "";
        }

        protected override void ValidateStatus()
        {
            bool valid = KpiUserId != null;
            valid = valid &&
                KpiRef != null &&
                NumberOfPeriods != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post-checks failed in state of Kpi");
        }
        protected override void When(object @event)
        {
            switch (@event)
            {
                case Events.Indicators.KpiUserInsert e:
                    KpiUserId = Guid.NewGuid();
                    KpiRef = new Kpi();
                    KpiRef.KpiId = e.KpiId;
                    ChartType = e.ChartType;
                    NumberOfPeriods = e.NumberOfPeriods;
                    UserRef = new ApplicationUser();
                    UserRef.Id = e.UserId.ToString();
                    FilterOffices = e.FilterOffices == null ? "" : e.FilterOffices;
                    FilterRegions = e.FilterRegions == null ? "" : e.FilterRegions;
                    FilterTaxPayerSegments = e.FilterTaxPayerSegments == null ? "" : e.FilterTaxPayerSegments;

                    break;
                default:
                    break;
            }
        }
    }
}
