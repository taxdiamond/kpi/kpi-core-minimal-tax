﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Shared
{
    public class Id20 : Value<Id20>
    {
        protected Id20() { }
        public string Value { get; set; }
        internal Id20(string title) => Value = title;
        public static Id20 FromString(string title)
        {
            CheckValidity(title);
            return new Id20(title);
        }

        private static void CheckValidity(string title)
        {
            if (string.IsNullOrWhiteSpace(title))
                throw new ArgumentException("Id cannot be empty", nameof(title));
            if (title.Length > 20)
                throw new ArgumentOutOfRangeException(nameof(title), "Id cannot have more than 200 chars");
        }

        public static implicit operator string(Id20 title) => title.Value;
        public static implicit operator Id20(string title) => new Id20(title);
    }
}
