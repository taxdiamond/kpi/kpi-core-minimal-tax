﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Shared
{
    public class Name250 : Value<Name250>
    {
        protected Name250() { }
        public string Value { get; set; }
        internal Name250(string title) => Value = title;
        public static Name250 FromString(string title)
        {
            CheckValidity(title);
            return new Name250(title);
        }

        private static void CheckValidity(string title)
        {
            if (string.IsNullOrWhiteSpace(title))
                throw new ArgumentException("Name cannot be empty", nameof(title));
            if (title.Length > 250)
                throw new ArgumentOutOfRangeException(nameof(title), "Name cannot have more than 200 chars");
        }

        public static implicit operator string(Name250 title) => title.Value;
        public static implicit operator Name250(string title) => new Name250(title);
    }
}
