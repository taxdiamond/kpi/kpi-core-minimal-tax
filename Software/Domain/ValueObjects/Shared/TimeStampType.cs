﻿using System;

namespace Domain.ValueObjects.Shared
{
    public class TimeStampType : Value<TimeStampType>, IEquatable<TimeStampType>
    {
        public DateTime Value { get; internal set; }

        protected TimeStampType() { }

        internal TimeStampType(DateTime dt) => Value = dt;

        public static TimeStampType FromDateTime(DateTime dt)
        {
            CheckValidity(dt);
            return new TimeStampType(dt.Date);
        }

        public static TimeStampType Now()
            => new TimeStampType(DateTime.Today);

        public static void CheckValidity(DateTime dt)
        {
            if (dt == null)
                throw new ArgumentException("The date cannot be a null", nameof(dt));
            if (dt.CompareTo(new DateTime(1900, 1, 1)) < 0)
                throw new ArgumentException("The date cannot be before 01/01/1900", nameof(dt));
        }

        public bool Equals(TimeStampType other) => this.Value.Equals(other.Value);

        public static implicit operator TimeStampType(DateTime dt)
            => new TimeStampType(dt);
        public static bool operator ==(TimeStampType d1, TimeStampType d2)
            => d1.Value == d2.Value;
        public static bool operator !=(TimeStampType d1, TimeStampType d2)
            => d1.Value != d2.Value;
        public static TimeStampType MinValue => new TimeStampType(new DateTime(1900, 1, 1));

        public override bool Equals(object obj)
        {
            if (obj is TimeStampType)
                return Equals((TimeStampType)obj);

            return false;
        }

        public override int GetHashCode() => Value.GetHashCode();
    }
}
