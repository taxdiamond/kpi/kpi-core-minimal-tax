﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Shared
{
    public class PositiveDecimal : Value<PositiveDecimal>
    {
        protected PositiveDecimal() { }
        public decimal Value { get; set; }
        internal PositiveDecimal(decimal positive) => Value = positive;
        public static PositiveDecimal FromDecimal(decimal aNumber)
        {
            CheckValidity(aNumber);
            return new PositiveDecimal(aNumber);
        }

        private static void CheckValidity(decimal aNumber)
        {
            if (aNumber <= 0)
                throw new ArgumentException("A positive decimal must be positive", nameof(aNumber));            
        }

        public static implicit operator decimal(PositiveDecimal number) => number == null ? 0 : number.Value;
        public static implicit operator PositiveDecimal(decimal number) => FromDecimal(number);
    }
}
