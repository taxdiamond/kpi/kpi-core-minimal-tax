﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Indicators
{
    public class KpiGuid : Value<KpiGuid>
    {
        public Guid Value { get; set; }

        protected KpiGuid() { }
        public KpiGuid(Guid id)
        {
            if (id == default)
                throw new ArgumentException("The id cannot be the default value", nameof(id));
            Value = id;
        }

        public static implicit operator Guid(KpiGuid id) => id.Value;

        public static implicit operator KpiGuid(string id)
            => new KpiGuid(Guid.Parse(id));

        public static implicit operator KpiGuid(Guid id)
            => new KpiGuid(id);
        public static bool operator ==(KpiGuid o1, KpiGuid o2)
            => o1.Value.Equals(o2.Value);
        public static bool operator !=(KpiGuid o1, KpiGuid o2)
            => !o1.Value.Equals(o2.Value);

        public override int GetHashCode() => base.GetHashCode();
        public override bool Equals(object other) => base.Equals(other);
    }
}
