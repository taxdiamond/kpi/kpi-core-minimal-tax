﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Indicators
{
    public class KpiUserGuid : Value<KpiUserGuid>
    {
        public Guid Value { get; set; }

        protected KpiUserGuid() { }
        public KpiUserGuid(Guid id)
        {
            if (id == default)
                throw new ArgumentException("The id cannot be the default value", nameof(id));
            Value = id;
        }

        public static implicit operator Guid(KpiUserGuid id) => id.Value;

        public static implicit operator KpiUserGuid(string id)
            => new KpiUserGuid(Guid.Parse(id));

        public static implicit operator KpiUserGuid(Guid id)
            => new KpiUserGuid(id);
        public static bool operator ==(KpiUserGuid o1, KpiUserGuid o2)
            => o1.Value.Equals(o2.Value);
        public static bool operator !=(KpiUserGuid o1, KpiUserGuid o2)
            => !o1.Value.Equals(o2.Value);

        public override int GetHashCode() => base.GetHashCode();
        public override bool Equals(object other) => base.Equals(other);
    }
}
