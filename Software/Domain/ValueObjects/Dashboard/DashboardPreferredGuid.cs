﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Dashboard
{
    public class DashboardPreferredGuid : Value<DashboardPreferredGuid>
    {
        public Guid Value { get; set; }

        protected DashboardPreferredGuid() { }
        public DashboardPreferredGuid(Guid id)
        {
            if (id == default)
                throw new ArgumentException("The id cannot be the default value", nameof(id));
            Value = id;
        }

        public static implicit operator Guid(DashboardPreferredGuid id) => id.Value;

        public static implicit operator DashboardPreferredGuid(string id)
            => new DashboardPreferredGuid(Guid.Parse(id));

        public static implicit operator DashboardPreferredGuid(Guid id)
            => new DashboardPreferredGuid(id);
        public static bool operator ==(DashboardPreferredGuid o1, DashboardPreferredGuid o2)
            => o1.Value.Equals(o2.Value);
        public static bool operator !=(DashboardPreferredGuid o1, DashboardPreferredGuid o2)
            => !o1.Value.Equals(o2.Value);
        public override int GetHashCode() => base.GetHashCode();
        public override bool Equals(object other) => base.Equals(other);
    }
}
