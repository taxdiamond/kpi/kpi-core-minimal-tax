﻿using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Dashboard
{
    public class DivSizeHeight : PositiveInteger
    {
        protected DivSizeHeight() { }
        internal DivSizeHeight(int positive) => Value = positive;
        public static new DivSizeHeight FromInt(int aNumber)
        {
            CheckValidity(aNumber);
            return new DivSizeHeight(aNumber);
        }

        public static implicit operator int(DivSizeHeight number) => number.Value;
        public static implicit operator DivSizeHeight(int number) => FromInt(number);

        private static void CheckValidity(int aNumber)
        {
            if (!(aNumber >= 1 && aNumber <= 2))
                throw new ArgumentException("Positive between 1 and 2 for height", nameof(aNumber));
        }
    }
}
