﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Dashboard
{
    public class DashboardUserGuid : Value<DashboardUserGuid>
    {
        public Guid Value { get; set; }
        protected DashboardUserGuid() { }
        public DashboardUserGuid(Guid id)
        {
            if (id == default)
                throw new ArgumentException("The id cannot be the default value", nameof(id));
            Value = id;
        }

        public static implicit operator Guid(DashboardUserGuid id) => id.Value;

        public static implicit operator DashboardUserGuid(string id)
            => new DashboardUserGuid(Guid.Parse(id));

        public static implicit operator DashboardUserGuid(Guid id)
            => new DashboardUserGuid(id);
        public static bool operator ==(DashboardUserGuid o1, DashboardUserGuid o2)
            => o1.Value.Equals(o2.Value);
        public static bool operator !=(DashboardUserGuid o1, DashboardUserGuid o2)
            => !o1.Value.Equals(o2.Value);
        public override int GetHashCode() => base.GetHashCode();
        public override bool Equals(object other) => base.Equals(other);
    }
}
