﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Dashboard
{
    public enum DashboardStatus
    {
        DRAFT, PUBLISHED, HIDDEN
    }
}
