﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ValueObjects.Dashboard
{
    public class DashboardGuid : Value<DashboardGuid>
    {
        public Guid Value { get; set; }

        protected DashboardGuid() { }
        public DashboardGuid(Guid id)
        {
            if (id == default)
                throw new ArgumentException("The id cannot be the default value", nameof(id));
            Value = id;
        }

        public static implicit operator Guid(DashboardGuid id) => id.Value;

        public static implicit operator DashboardGuid(string  id)
            => new DashboardGuid(Guid.Parse(id));

        public static implicit operator DashboardGuid(Guid id) 
            => new DashboardGuid(id);
        public static bool operator ==(DashboardGuid o1, DashboardGuid o2) 
            => o1.Value.Equals(o2.Value);
        public static bool operator !=(DashboardGuid o1, DashboardGuid o2)
            => !o1.Value.Equals(o2.Value);
        public override int GetHashCode() => base.GetHashCode();
        public override bool Equals(object other) => base.Equals(other);
    }
}
