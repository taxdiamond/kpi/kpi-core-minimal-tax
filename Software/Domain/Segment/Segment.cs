﻿using Domain.Aggregate;
using Domain.Events.Segment;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;

namespace Domain.Segment
{
    public class Segment : AggregateRoot<Id20>
    {
        public Id20 segmentID { get; set; }
        public Name250 segmentName { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = segmentID != null;

            valid = valid && segmentName != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for Segment");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case SegmentCreated e:
                    this.segmentID = new Id20(e.segmentID);
                    this.segmentName = new Name250(e.segmentName);
                    break;
            }
        }
    }
}
