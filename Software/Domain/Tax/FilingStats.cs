﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    public class FilingStats : AggregateRoot<IdInt>
    {

        public Int32 FilingItemID { get; set; }
        public DateType ReportDate { get; set; }
        public Name250 TaxType { get; set; }
        public Segment.Segment SegmentID { get; set; }        
        public PositiveInteger ExpectedFilers { get; set; }
        public PositiveInteger TotalFilings { get; set; }
        public PositiveInteger StopFilers { get; set; }
        public PositiveInteger EFilings { get; set; }
        public PositiveInteger TotalFilingsRequiringPayment { get; set; }
        public PositiveInteger FilingsPaidElectronically { get; set; }
        public PositiveInteger LateFilers { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = FilingItemID >= 0;

            valid = valid &&
                ReportDate != null &&
                SegmentID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for FilingStats");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case FilingStatsCreated e:
                    this.ReportDate = DateType.FromDateTime(e.ReportDate);
                    this.SegmentID.segmentID = Id20.FromString(e.SegmentID);

                    this.EFilings = e.EFilings;
                    this.ExpectedFilers = e.ExpectedFilers;
                    this.FilingsPaidElectronically = e.FilingsPaidElectronically;
                    this.LateFilers = e.LateFilers;
                    this.StopFilers = e.StopFilers;
                    this.TotalFilings = e.TotalFilings;
                    this.TotalFilingsRequiringPayment = e.TotalFilingsRequiringPayment;
                    break;
            }
        }
    }
}
