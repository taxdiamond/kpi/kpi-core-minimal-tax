﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    public class TaxRate : AggregateRoot<IdInt>
    {
        public Int32 TaxRateId { get; set; }
        public DateType TaxRateDate { get; set; }
        public Name250 TaxName { get; set; }
        public decimal Rate { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = TaxRateId >= 0;

            valid = valid &&
                TaxRateDate != null &&
                TaxName != null &&
                Rate > decimal.MinValue && Rate < decimal.MaxValue;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for TaxRate");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case TaxRateCreated e:
                    this.TaxRateId = IdInt.NoIdYet;
                    this.TaxName = Name250.FromString(e.TaxName);
                    this.TaxRateDate = DateType.FromDateTime(e.TaxRateDate);
                    this.Rate = e.Rate;
                    break;
            }
        }
    }
}
