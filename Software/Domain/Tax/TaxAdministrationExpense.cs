﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Tax
{
    public class TaxAdministrationExpense : AggregateRoot<IdInt>
    {
        public Int32 TaxAdministrationExpenseItemID { get; set; }
        public DateType TaxAdministrationExpenseDate { get; set; }
        public Region.Region RegionID { get; set; }
        public Office.Office OfficeID { get; set; }
        public PositiveDecimal CurrentExpense { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = TaxAdministrationExpenseItemID >= 0;

            valid = valid &&
                TaxAdministrationExpenseDate != null &&
                RegionID != null &&
                OfficeID != null &&
                CurrentExpense != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for TaxAdministrationExpense TaxAdministrationExpense");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case TaxAdministrationExpenseCreated e:
                    this.TaxAdministrationExpenseDate = DateType.FromDateTime(e.TaxAdministrationExpenseDate);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.OfficeID.officeID = Id20.FromString(e.OfficeID);
                    this.CurrentExpense = PositiveDecimal.FromDecimal(e.CurrentExpense);
                    break;
            }
        }
    }
}
