﻿using Domain.Aggregate;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    public class Appeal : AggregateRoot<IdInt>
    {

        public Int32 AppealsItemID { get; set; }
        public DateType AppealsDate { get; set; }
        public Region.Region RegionID { get; set; }
        public Office.Office OfficeID { get; set; }
        public Segment.Segment SegmentID { get; set; }
        public Name250 Concept { get; set; }
        public Name250 ConceptID { get; set; }
        public PositiveInteger NumberAppealsCasesPending { get; set; }
        public PositiveDecimal AmountAppealed { get; set; }
        public PositiveInteger NumberCasesResolved { get; set; }
        public PositiveInteger NumberCasesResolvedFavorTaxPayer { get; set; }
        public PositiveInteger NumberCasesResolvedFavorTaxAdministration { get; set; }
        public PositiveInteger AverageDaysToResolveCases { get; set; }
        public PositiveDecimal TotalAmountResolvedFavorTaxPayer { get; set; }
        public PositiveDecimal TotalAmountResolvedFavorTaxAdministration { get; set; }
        public PositiveInteger NumberOfAppealCasesSubmitted { get; set; }
        public PositiveDecimal ValueOfAppealCasesSubmitted { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = AppealsItemID >= 0;

            valid = valid &&
                AppealsDate != null &&
                RegionID != null &&
                OfficeID != null &&
                SegmentID != null &&
                Concept != null &&
                ConceptID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for Appeal Appeal");
        }

        protected override void When(object @event)
        {
            /*switch (@event)
            {
                case AppealCreated e:
                    this.AppealsDate = DateType.FromDateTime(e.AppealDate);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.OfficeID.officeID = Id20.FromString(e.OfficeID);
                    this.SegmentID.segmentID = Id20.FromString(e.SegmentID);
                    this.Concept = e.Concept;
                    this.ConceptID = e.ConceptID;
                    this.Amount = PositiveDecimal.FromDecimal(e.Amount);
                    break;
            }*/
        }
    }
}
