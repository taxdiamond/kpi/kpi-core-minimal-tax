﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Tax
{
    public class Arrear : AggregateRoot<IdInt>
    {
        public Int32 ArrearItemID { get; set; }
        public DateType ArrearDate { get; set; }
        public Region.Region RegionID { get; set; }
        public Office.Office OfficeID { get; set; }
        public Segment.Segment SegmentID { get; set; }
        public Name250 Concept { get; set; }
        public Name250 ConceptID { get; set; }
        public PositiveInteger NumberOfArrearCasesPending { get; set; }
        public PositiveDecimal Amount { get; set; }   // This is the ValueOfArrearCasesPending
        public PositiveInteger NumberOfArrearCasesGenerated { get; set; }
        public PositiveDecimal ValueOfArrearCasesGenerated { get; set; }
        public PositiveInteger NumberOfArrearCasesResolved { get; set; }
        public PositiveDecimal ValueOfArrearCasesResolved { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = ArrearItemID >= 0;

            valid = valid &&
                ArrearDate != null &&
                RegionID != null &&
                OfficeID != null &&
                SegmentID != null &&
                Concept != null &&
                ConceptID != null &&
                Amount != null &&
                NumberOfArrearCasesPending != null &&
                NumberOfArrearCasesGenerated != null &&
                ValueOfArrearCasesGenerated != null &&
                NumberOfArrearCasesResolved != null &&
                ValueOfArrearCasesResolved != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for Arrear Arrear");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                // TODO: Add NumberOfArrearCasesPending, NumberOfArrearCasesGenerated, ValueOfArrearCasesGenerated, NumberOfArrearCasesResolved, and ValueOfArrearCasesResolved to the ArrearCreated event
                case ArrearCreated e:
                    this.ArrearDate = DateType.FromDateTime(e.ArrearDate);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.OfficeID.officeID = Id20.FromString(e.OfficeID);
                    this.SegmentID.segmentID = Id20.FromString(e.SegmentID);
                    this.Concept = e.Concept;
                    this.ConceptID = e.ConceptID;
                    this.Amount = PositiveDecimal.FromDecimal(e.Amount);
                    break;
            }
        }
    }
}
