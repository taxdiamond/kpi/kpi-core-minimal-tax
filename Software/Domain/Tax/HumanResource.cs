﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    /// <summary>
    /// </summary>
    public class HumanResource : AggregateRoot<IdInt>
    {
        public Int32 HRItemID { get; set; }
        public DateType HRItemReportDate { get; set; }
        public Region.Region RegionID { get; set; }
        public Office.Office OfficeID { get; set; }
        public PositiveInteger TotalHumanResources { get; set; }
        public PositiveInteger TotalHumanResourcesInTaxCollection { get; set; }
        public PositiveInteger TotalHumanResourcesInTaxPayerServices { get; set; }
        public PositiveInteger TotalHumanResourcesInTaxEnforcement { get; set; }
        public PositiveInteger TotalHumanResourcesInAudits { get; set; }
        public PositiveInteger TotalHumanResourcesInAppeals { get; set; }
        public PositiveInteger NumberPositionsFilledCompetitiveProcess { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = HRItemID >= 0;

            valid = valid &&
                HRItemReportDate != null &&
                RegionID != null &&
                OfficeID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for Human Resource");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case HumanResourceCreated e:
                    this.HRItemReportDate = DateType.FromDateTime(e.HRItemReportDate);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.OfficeID.officeID = Id20.FromString(e.OfficeID);
                    
                    this.TotalHumanResources = e.TotalHumanResources;
                    this.TotalHumanResourcesInAppeals = e.TotalHumanResourcesInAppeals;
                    this.TotalHumanResourcesInAudits = e.TotalHumanResourcesInAudits;
                    this.TotalHumanResourcesInTaxCollection = e.TotalHumanResourcesInTaxCollection;
                    this.TotalHumanResourcesInTaxEnforcement = e.TotalHumanResourcesInTaxEnforcement;
                    this.TotalHumanResourcesInTaxPayerServices = e.TotalHumanResourcesInTaxPayerServices;
                    this.NumberPositionsFilledCompetitiveProcess = e.NumberPositionsFilledCompetitiveProcess;
                    break;
            }
        }
    }
}
