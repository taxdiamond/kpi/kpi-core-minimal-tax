﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    public class Import : AggregateRoot<IdInt>
    {
        public Int32 ImportID { get; set; }
        public DateType ImportDate { get; set; }
        public PositiveDecimal TotalImports { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = ImportID >= 0;

            valid = valid &&
                ImportDate != null &&
                TotalImports != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for Revenue Summary by month");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case ImportCreated e:
                    this.ImportDate = DateType.FromDateTime(e.ImportDate);
                    this.TotalImports = e.TotalImports;
                    break;
            }
        }
    }
}
