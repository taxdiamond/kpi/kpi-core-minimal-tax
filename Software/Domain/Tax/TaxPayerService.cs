﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    public class TaxPayerService : AggregateRoot<IdInt>
    {
        public Int32 ServicesItemID { get; set; }
        public DateType ServicesItemReportDate { get; set; }
        public PositiveInteger NumberOfServicesProvidedTaxpayers { get; set; }
        public PositiveInteger NumberOfElectronicServicesProvidedTaxpayers { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = ServicesItemID >= 0;

            valid = valid &&
                ServicesItemReportDate != null &&
                NumberOfElectronicServicesProvidedTaxpayers != null &&
                NumberOfServicesProvidedTaxpayers != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for TaxPayerService TaxPayerService");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case TaxPayerServiceCreated e:
                    this.ServicesItemReportDate = DateType.FromDateTime(e.ServicesItemReportDate);
                    this.NumberOfServicesProvidedTaxpayers = PositiveInteger.FromInt(e.NumberOfServicesProvidedTaxpayers);
                    this.NumberOfElectronicServicesProvidedTaxpayers = PositiveInteger.FromInt(e.NumberOfElectronicServicesProvidedTaxpayers);
                    break;
            }
        }
    }
}
