﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    public class SatisfactionSurvey : AggregateRoot<IdInt>
    {
        public Int32 SatisfactionSurveyItemID { get; set; }
        public DateType SurveyItemReportDate { get; set; }
        public Region.Region RegionID { get; set; }
        public Office.Office OfficeID { get; set; }
        public Name250 SurveyArea { get; set; }
        public PositiveDecimal SurveyAreaScore { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = SatisfactionSurveyItemID >= 0;

            valid = valid &&
                SurveyItemReportDate != null &&
                RegionID != null &&
                OfficeID != null &&
                SurveyArea != null &&
                SurveyAreaScore != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for Satisfaction Survey");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case SatisfactionSurveyCreated e:
                    this.SurveyItemReportDate = DateType.FromDateTime(e.SurveyItemReportDate);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.OfficeID.officeID = Id20.FromString(e.OfficeID);
                    this.SurveyArea = e.SurveyArea;
                    this.SurveyAreaScore = e.SurveyAreaScore;
                    break;
            }
        }
    }
}
