﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    public class RevenueSummaryByMonth : AggregateRoot<IdInt>
    {
        public Int32 RevenueSummaryID { get; set; }
        public DateType RevenueDate { get; set; }
        public PositiveDecimal TotalRevenue { get; set; }
        public PositiveDecimal TotalRevenuePaidInTIme { get; set; }
        public PositiveDecimal TotalRevenueFromArrears { get; set; }
        public PositiveDecimal TotalRevenueFromFines { get; set; }
        public PositiveDecimal TotalRevenueFromAudits { get; set; }
        public PositiveDecimal TotalRevenueFromEnforcementCollection { get; set; }
        public PositiveDecimal TotalRevenueFromVoluntaryCompliance { get; set; }
        public PositiveDecimal TotalRevenueFromTaxes { get; set; }
        public PositiveDecimal TotalRevenueFromLitigation { get; set; }
        public Region.Region RegionID { get; set; }
        public Office.Office OfficeID { get; set; }
        public Segment.Segment SegmentID { get; set; }

        protected override void ValidateStatus()
        {
            bool valid = RevenueSummaryID >= 0;

            valid = valid &&
                RevenueDate != null &&
                RegionID != null &&
                OfficeID != null &&
                SegmentID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for Revenue Summary by month");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case RevenueSummaryByMonthCreated e:
                    this.RevenueDate = DateType.FromDateTime(e.RevenueDate);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.OfficeID.officeID = Id20.FromString(e.OfficeID);
                    this.SegmentID.segmentID = Id20.FromString(e.SegmentID);
                    
                    this.TotalRevenue = e.TotalRevenue;
                    this.TotalRevenueFromArrears = e.TotalRevenueFromArrears;
                    this.TotalRevenueFromAudits = e.TotalRevenueFromAudits;
                    this.TotalRevenueFromEnforcementCollection = e.TotalRevenueFromEnforcementCollection;
                    this.TotalRevenueFromFines = e.TotalRevenueFromFines;
                    this.TotalRevenueFromLitigation = e.TotalRevenueFromLitigation;
                    this.TotalRevenueFromTaxes = e.TotalRevenueFromTaxes;
                    this.TotalRevenueFromVoluntaryCompliance = e.TotalRevenueFromVoluntaryCompliance;
                    this.TotalRevenuePaidInTIme = e.TotalRevenuePaidInTIme;
                    break;
            }
        }
    }
}
