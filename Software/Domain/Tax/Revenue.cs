﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Tax
{
    public class Revenue : AggregateRoot<IdInt>
    {
        public Int32 RevenueItemID { get; set; }
        public DateType RevenueDate { get; set; }
        public Region.Region RegionID { get; set; }
        public Office.Office OfficeID { get; set; }
        public Segment.Segment SegmentID { get; set; }
        public Name250 Concept { get; set; }
        public Name250 ConceptID { get; set; }
        public PositiveDecimal Amount { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = RevenueItemID >= 0;

            valid = valid &&
                RevenueDate != null &&
                RegionID != null &&
                OfficeID != null &&
                SegmentID != null &&
                Concept != null &&
                ConceptID != null &&
                Amount != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for Revenue Revenue");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case RevenueCreated e:
                    this.RevenueDate = DateType.FromDateTime(e.RevenueDate);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.OfficeID.officeID = Id20.FromString(e.OfficeID);
                    this.SegmentID.segmentID = Id20.FromString(e.SegmentID);
                    this.Concept = e.Concept;
                    this.ConceptID = e.ConceptID;
                    this.Amount = PositiveDecimal.FromDecimal(e.Amount);
                    break;
            }
        }
    }
}
