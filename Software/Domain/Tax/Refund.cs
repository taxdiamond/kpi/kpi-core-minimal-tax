﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Tax
{
    public class Refund : AggregateRoot<IdInt>
    {
        public Int32 RefundItemID { get; set; }
        public DateType RefundsDate { get; set; }
        public Region.Region RegionID { get; set; }
        public Office.Office OfficeID { get; set; }
        public Segment.Segment SegmentID { get; set; }
        public Name250 Concept { get; set; }
        public Name250 ConceptID { get; set; }
        public PositiveInteger NumberRefundClaimsRequested { get; set; }
        public PositiveInteger NumberRequestsClaimsProcessed { get; set; }
        public PositiveDecimal RefundAmountRequested { get; set; }
        public PositiveDecimal RefundAmountProcessed { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = RefundItemID >= 0;

            valid = valid &&
                RefundsDate != null &&
                RegionID != null &&
                OfficeID != null &&
                SegmentID != null &&
                Concept != null &&
                ConceptID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for Refund Refund");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case RefundCreated e:
                    this.RefundsDate = DateType.FromDateTime(e.RefundsDate);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.OfficeID.officeID = Id20.FromString(e.OfficeID);
                    this.SegmentID.segmentID = Id20.FromString(e.SegmentID);
                    this.Concept = e.Concept;
                    this.ConceptID = e.ConceptID;
                    this.NumberRefundClaimsRequested = PositiveInteger.FromInt(e.NumberRefundClaimsRequested);
                    this.NumberRequestsClaimsProcessed = PositiveInteger.FromInt(e.NumberRequestsClaimsProcessed);
                    this.RefundAmountRequested = PositiveDecimal.FromDecimal(e.RefundAmountRequested);
                    this.RefundAmountProcessed = PositiveDecimal.FromDecimal(e.RefundAmountProcessed);
                    break;
            }
        }
    }
}
