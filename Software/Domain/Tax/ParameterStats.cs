﻿using Domain.Aggregate;
using Domain.Events.Tax;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;

namespace Domain.Tax
{
    public class ParameterStats : AggregateRoot<IdInt>
    {

        public Int32 ParameterStatsID { get; set; }
        public Name250 ParameterName { get; set; }
        public DateType ValueDate { get; set; }
        public TimeStampType TimeStamp { get; set; }
        public decimal GeneratedValue { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = ParameterStatsID >= 0;

            valid = valid &&
                ValueDate != null &&
                TimeStamp != null &&
                ParameterName != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for TaxPayerStats");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case ParameterStatsCreated e:
                    this.ParameterName = Name250.FromString(e.ParameterName);
                    this.ValueDate = DateType.FromDateTime(e.ValueDate);
                    this.TimeStamp = TimeStampType.FromDateTime(e.TimeStamp);
                    this.GeneratedValue = e.GeneratedValue;

                    break;
            }
        }
    }
}
