﻿using Domain.Aggregate;
using Domain.Events.Region;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Region
{
    public class RegionStats : AggregateRoot<IdInt>
    {
        public Int32 RegionStatsId { get; set; }
        public DateType StatsDate { get; set; } 
        public Region RegionID { get; set; }
        public decimal? GdpPerCapitaAtDate { get; set; }
        public decimal? NominalGdpAtDate { get; set; }
        public PositiveInteger PopulationAtDate { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = RegionStatsId >= 0;

            valid = valid &&
                StatsDate != null &&
                RegionID != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for RegionStats");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case RegionStatsCreated e:
                    this.StatsDate = DateType.FromDateTime(e.StatsDate);
                    this.RegionID.regionID = Id20.FromString(e.RegionID);
                    this.GdpPerCapitaAtDate = e.GdpPerCapitaAndDate;
                    this.NominalGdpAtDate = e.NominalGdpAtDate;
                    this.PopulationAtDate = PositiveInteger.FromInt(e.PopulationAtDate);
                    break;
            }
        }
    }
}
