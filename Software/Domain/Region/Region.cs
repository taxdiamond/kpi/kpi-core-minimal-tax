﻿using Domain.Aggregate;
using Domain.Events.Region;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Region
{
    public class Region : AggregateRoot<Id20>
    {
        public Id20 regionID { get; set; }
        public Name250 regionName { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = regionID != null;

            valid = valid && regionName != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for region");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case RegionCreated e:
                    this.regionID = new Id20(e.regionID);
                    this.regionName = new Name250(e.regionName);
                    break;
            }
        }
    }
}
