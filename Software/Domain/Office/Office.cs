﻿using Domain.Aggregate;
using Domain.Events.Office;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;

namespace Domain.Office
{
    public class Office : AggregateRoot<Id20>
    {
        public Id20 officeID { get; set; }
        public Name250 officeName { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = officeID != null;

            valid = valid && officeName != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for Office");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case OfficeCreated e:
                    this.officeID = new Id20(e.officeID);
                    this.officeName = new Name250(e.officeName);
                    break;
            }
        }
    }
}
