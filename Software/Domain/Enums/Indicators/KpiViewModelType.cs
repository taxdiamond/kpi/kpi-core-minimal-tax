﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums.Indicators
{
    public enum KpiViewModelType
    {
        None,
        SingleValue_SingleSeries,
        SingleValue_Multiseries,
        SingleValue_SingleSeries_Sparkline,
        Bar_SingleSeries,
        Bar_MultiSeries,
        StackedBar_MultiSeries,
        Donut_MultiSeries,
        Column_SingleSeries,
        Column_Multiseries,
        StackedColumn_Multiseries,
        Line_SingleSeries,
        Line_MultiSeries
    }
}
