﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums.DataWareHouse
{
    //Do not alter order, always add last this number use in SP UTIL_DeleteKPIDWByDate
    public enum DataWareHouseType
    {        
        APPEALS = 0,
        ARREARS,
        COUNTRY_STATS,
        COURSES,
        FILING_STATS,
        HUMAN_RESOURCES,
        IMPORTS,
        KPIS,
        OFFICE,
        PARAMETER_STATS,
        REFUNDS,
        REGION,
        REGION_STATS,
        REVENUE,
        REVENUE_FORECAST,
        REVENUE_SUMMARY_BY_MONTH,
        SATISFACTION_SURVEYS,
        SEGMENT,
        SETTINGS,
        TAX_ADMINISTRATION_EXPENSES,
        TAX_PAYER_SERVICES,
        TAX_PAYER_STATS,
        AUDITSTATS,
        TAX_RATES,
        DOINGBUSINESSTAXSTATS,
        SIMULATION_STATES
    }
}
