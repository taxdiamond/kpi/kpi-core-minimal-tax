﻿using Domain.Aggregate;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using Domain.Events.CustomsTradePartner;

namespace Domain.CustomsTradePartner
{
    public class CustomsTradePartner : AggregateRoot<Id20>
    {
        public Id20 customsTradePartnerID { get; set; }
        public Name250 tradePartnerType { get; set; }
        public Name250 tradePartnerName { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = customsTradePartnerID != null;

            valid = valid && tradePartnerType != null;
            valid = valid && tradePartnerName != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsTradePartner");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case CustomsTradePartnerCreated e:
                    this.customsTradePartnerID = new Id20(e.customsTradePartnerID);
                    this.tradePartnerType = new Name250(e.tradePartnerType);
                    this.tradePartnerName = new Name250(e.tradePartnerName);
                    break;
            }
        }
    }
}
