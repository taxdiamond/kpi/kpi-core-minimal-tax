﻿using Domain.Aggregate;
using Domain.Exceptions;
using Domain.ValueObjects.Shared;
using Domain.Events.CustomsPost;

namespace Domain.CustomsPost
{
    public class CustomsPost : AggregateRoot<Id20>
    {
        public Id20 customsPostID { get; set; }
        public Name250 customsPostName { get; set; }
        protected override void ValidateStatus()
        {
            bool valid = customsPostID != null;

            valid = valid && customsPostName != null;

            if (!valid)
                throw new InvalidEntityException(this,
                    $"Post checks failed for CustomsPost");
        }

        protected override void When(object @event)
        {
            switch (@event)
            {
                case CustomsPostCreated e:
                    this.customsPostID = new Id20(e.customsPostID);
                    this.customsPostName = new Name250(e.customsPostName);
                    break;
            }
        }
    }
}
