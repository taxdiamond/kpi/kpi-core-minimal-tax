DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearlyByTypeOfTax]
GO

DROP PROCEDURE IF EXISTS[dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearlyByOffice]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 2021
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearlyByTypeOfTax]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	-- We expect the data to have segmented data per month.  So, 
	-- we have taxpayer data for region 1, office 1, segment 1, region 1, office 1, segment 2, etc.
	-- all of this data should be ADDED to aget the total number of taxpayers in month n.
	--
	-- After that, when we are computing the yearly data, we should choose for year 1 the last data point
	-- we have for that year.  So if month 12 of year 2020 was X, then we will use X as the data for year 2020
	
	declare @taxpayerstats as table(year int, month int, numberOfRegisteredTaxPayers int, conceptID nvarchar(250))

	insert into @taxpayerstats
	select DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month,
		sum([numberOfRegisteredTaxPayers]) as numberOfRegisteredTaxPayers,
		conceptID
	from [dbo].[TaxPayerStats]
	where DATEPART(year, [statsDate]) > @startYear 
		and DATEPART(year, [statsDate]) <= @limitYear
	group by conceptID, DATEPART(year, [statsDate]), DATEPART(month, [statsDate])

	-- Use only the last month for every year
	select year, numberOfRegisteredTaxPayers, conceptID from (
		select row_number() over(partition by year, conceptID order by year desc, month desc, conceptID desc) as nrow, 
			year, 
			month, 
			numberOfRegisteredTaxPayers as numberOfRegisteredTaxPayers,
			conceptID 
		from @taxpayerstats
	) as x where x.nrow = 1
	order by year, conceptID

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 2021
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearlyByOffice]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	-- We expect the data to have segmented data per month.  So, 
	-- we have taxpayer data for region 1, office 1, segment 1, region 1, office 1, segment 2, etc.
	-- all of this data should be ADDED to aget the total number of taxpayers in month n.
	--
	-- After that, when we are computing the yearly data, we should choose for year 1 the last data point
	-- we have for that year.  So if month 12 of year 2020 was X, then we will use X as the data for year 2020
	
	declare @taxpayerstats as table(year int, month int, numberOfRegisteredTaxPayers int, officeID nvarchar(20))

	insert into @taxpayerstats
	select DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month,
		sum([numberOfRegisteredTaxPayers]) as numberOfRegisteredTaxPayers,
		officeID
	from [dbo].[TaxPayerStats]
	where DATEPART(year, [statsDate]) > @startYear 
		and DATEPART(year, [statsDate]) <= @limitYear
	group by officeID, DATEPART(year, [statsDate]), DATEPART(month, [statsDate])

	-- Use only the last month for every year
	select year, numberOfRegisteredTaxPayers, officeID from (
		select row_number() over(partition by year, officeID order by year desc, month desc, officeID desc) as nrow, 
			year, 
			month, 
			numberOfRegisteredTaxPayers as numberOfRegisteredTaxPayers,
			officeID 
		from @taxpayerstats
	) as x where x.nrow = 1
	order by year, officeID

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[TAXPAYER_ReportTaxpayerData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@numberOfRegisteredTaxpayers int,
	@numberOfActiveTaxPayers int,
    @numberOfTaxPayersFiled int,
    @numberTaxPayersFiledInTime int,
    @numberOfTaxPayers70PercentRevenue int,
	@conceptID nvarchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@numberOfRegisteredTaxpayers = 0)
		set @numberOfRegisteredTaxpayers = null

	if(@numberOfActiveTaxPayers = 0)
		set @numberOfActiveTaxPayers = null

	if(@numberOfTaxPayersFiled = 0)
		set @numberOfTaxPayersFiled = null

	if(@numberTaxPayersFiledInTime = 0)
		set @numberTaxPayersFiledInTime = null

	if(@numberOfTaxPayers70PercentRevenue = 0)
		set @numberOfTaxPayers70PercentRevenue = null

	INSERT INTO [dbo].[TaxPayerStats]
           ([statsDate]
           ,[numberOfRegisteredTaxPayers]
           ,[numberOfActiveTaxPayers]
           ,[numberOfTaxPayersFiled]
           ,[numberTaxPayersFiledInTime]
           ,[numberOfTaxPayers70PercentRevenue]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
		   ,[conceptID])
	VALUES
           (@dateReported
           ,@numberOfRegisteredTaxpayers
           ,@numberOfActiveTaxPayers
           ,@numberOfTaxPayersFiled
           ,@numberTaxPayersFiledInTime
           ,@numberOfTaxPayers70PercentRevenue
           ,@regionID
           ,@officeID
           ,@segmentID
		   ,@conceptID)
END
GO
