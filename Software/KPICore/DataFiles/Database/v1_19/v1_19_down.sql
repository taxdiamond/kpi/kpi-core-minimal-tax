SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 29 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT year([revenueDate]) as year, month([revenueDate]) as month, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startdate
	group by year([revenueDate]), month([revenueDate])
	order by year([revenueDate]) asc, month([revenueDate]) asc
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 5 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthlyByTaxType]
	@NumberOfMonths INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT year([revenueDate]) as year, month([revenueDate]) as month, [conceptID] as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startdate and conceptID is not null and conceptID != ''
	group by year([revenueDate]), month([revenueDate]), conceptID
	order by year([revenueDate]) asc, month([revenueDate]), conceptID  asc
END
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 29 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear and DATEPART(year, [revenueDate]) <= @limitYear
	group by DATEPART(year, [revenueDate])
	order by DATEPART(year, [revenueDate]) asc
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 29 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)
	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear) +'
	    and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+'
		and '+@WhereClause+'  
	group by DATEPART(year, [revenueDate])
	order by DATEPART(year, [revenueDate]) asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 2 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByTaxType]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	SELECT DATEPART(year, [revenueDate]) as year, [conceptID] as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear and DATEPART(year, [revenueDate]) <= @limitYear
		and conceptID is not null and conceptID != ''
	group by DATEPART(year, [revenueDate]), conceptID 
	order by DATEPART(year, [revenueDate]) asc, conceptID  asc
END
GO

