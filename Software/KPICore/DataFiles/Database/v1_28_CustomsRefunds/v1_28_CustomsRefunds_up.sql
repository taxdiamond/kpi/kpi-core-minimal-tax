SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
ALTER PROCEDURE [dbo].[REFUND_ReportRefundsData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@numberRefundClaimsRequested int,
	@numberRequestsClaimsProcessed int,
	@refundAmountRequested money,
	@refundAmountProcessed money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @conceptID = NULL


	INSERT INTO [dbo].[Refunds]
           ([refundsDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[numberRefundClaimsRequested]
		   ,[numberRequestsClaimsProcessed]
		   ,[refundAmountRequested]
		   ,[refundAmountProcessed])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@numberRefundClaimsRequested
		   ,@numberRequestsClaimsProcessed
		   ,@refundAmountRequested
		   ,@refundAmountProcessed)

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 8 2020
-- =============================================
ALTER PROCEDURE [dbo].[APPEALS_ReportAppealsData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@numberAppealsCasesPending int,
	@amountAppealed money,
	@numberCasesResolved int,
	@numberCasesResolvedFavorTaxPayer int,
	@numberCasesResolvedFavorTaxAdministration int,
	@averageDaysToResolveCases int,
	@totalAmountResolvedFavorTaxPayer money,
	@totalAmountResolvedFavorTaxAdministration money,
	@numberOfAppealCasesSubmitted int,
	@valueOfAppealCasesSubmitted decimal(18,2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @conceptID = NULL

	INSERT INTO [dbo].[Appeals]
           ([appealsDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[numberAppealsCasesPending]
           ,[amountAppealed]
           ,[numberCasesResolved]
           ,[numberCasesResolvedFavorTaxPayer]
           ,[numberCasesResolvedFavorTaxAdministration]
           ,[averageDaysToResolveCases]
           ,[totalAmountResolvedFavorTaxPayer]
           ,[totalAmountResolvedFavorTaxAdministration]
		   ,[numberOfAppealCasesSubmitted]
		   ,[valueOfAppealCasesSubmitted])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@numberAppealsCasesPending
           ,@amountAppealed
           ,@numberCasesResolved
           ,@numberCasesResolvedFavorTaxPayer
           ,@numberCasesResolvedFavorTaxAdministration
           ,@averageDaysToResolveCases
           ,@totalAmountResolvedFavorTaxPayer
           ,@totalAmountResolvedFavorTaxAdministration
		   ,@numberOfAppealCasesSubmitted
		   ,@valueOfAppealCasesSubmitted)

END
Go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
ALTER PROCEDURE [dbo].[ARREARS_ReportArrearsData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@numberOfArrearCasesPending int,
	@amount money,    -- This is the ValueOfArrearCasesPending
	@numberOfArrearCasesGenerated int,
	@valueOfArrearCasesGenerated money,
	@numberOfArrearCasesResolved int,
	@valueOfArrearCasesResolved money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @conceptID = NULL


	INSERT INTO [dbo].[Arrears]
           ([arrearDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[amount]
		   ,[valueOfArrearCasesGenerated]
		   ,[valueOfArrearCasesResolved]
		   ,[numberOfArrearCasesGenerated]
		   ,[numberOfArrearCasesPending]
		   ,[numberOfArrearCasesResolved])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@amount
		   ,@valueOfArrearCasesGenerated
		   ,@valueOfArrearCasesResolved
		   ,@numberOfArrearCasesGenerated
		   ,@numberOfArrearCasesPending
		   ,@numberOfArrearCasesResolved)
END
GO




DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSREFUND_ReportRefundsData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 24 2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSREFUND_ReportRefundsData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@numberRefundClaimsRequested int,
	@numberRequestsClaimsProcessed int,
	@refundAmountRequested money,
	@refundAmountProcessed money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @conceptID = NULL

	INSERT INTO [dbo].[CustomsRefund]
           ([customsRefundsDate]
           ,[customsPostID]
           ,[concept]
           ,[conceptID]
           ,[numberRefundClaimsRequested]
           ,[numberRequestsClaimsProcessed]
           ,[refundAmountRequested]
           ,[refundAmountProcessed])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@concept
           ,@conceptID
           ,@numberRefundClaimsRequested
           ,@numberRequestsClaimsProcessed
           ,@refundAmountRequested
           ,@refundAmountProcessed)
END
Go

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSREFUND_GetLastDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSREFUND_GetLastDate]
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsRefundsDate]) as LastDate
	FROM [dbo].[CustomsRefund]

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
ALTER PROCEDURE [dbo].[FORECAST_ReportForecastData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@amount money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @conceptID = NULL


	INSERT INTO [dbo].[RevenueForecast]
           ([forecastDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[amount])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@amount)

END
GO

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSFORECAST_ReportForecastData]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSFORECAST_ReportForecastData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@amount money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @conceptID = NULL

	INSERT INTO [dbo].[CustomsRevenueForecast]
           ([customsRevenueForecastDate]
           ,[customsPostID]
           ,[concept]
           ,[conceptID]
           ,[amount])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@concept
           ,@conceptID
           ,@amount)

END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSFORECAST_GetLastDate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSFORECAST_GetLastDate]
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsRevenueForecastDate]) as LastDate
	FROM [dbo].[CustomsRevenueForecast]

END
GO
