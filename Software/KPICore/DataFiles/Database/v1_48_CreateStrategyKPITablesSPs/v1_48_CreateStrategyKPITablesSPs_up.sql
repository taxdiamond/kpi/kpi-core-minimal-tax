DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSADMINEXPENSES_GetLastDate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSADMINEXPENSES_GetLastDate]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsAdministrationExpensesDate]) as LastDate
	FROM [dbo].[CustomsAdministrationExpenses]
END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSADMINEXPENSES_ReportCustomsAdminExpensesData]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSADMINEXPENSES_ReportCustomsAdminExpensesData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@currentExpense money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = null

	INSERT INTO [dbo].[CustomsAdministrationExpenses]
			   ([customsAdministrationExpensesDate]
			   ,[customsPostID]
			   ,[currentExpense])
	 VALUES
			   (@dateReported
			   ,@customsPostID
			   ,@currentExpense)
END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSHR_GetLastDate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSHR_GetLastDate]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsHRStatsItemDate]) as LastDate
	FROM [dbo].[CustomsHRStats]
END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSHR_ReportCustomsHRData]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSHR_ReportCustomsHRData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@numberFrontLineStaff int,
	@numberSpecializedEmployees int,
	@numberSpecializedEmployeesTrained int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = null

	INSERT INTO [dbo].[CustomsHRStats]
           ([customsHRStatsItemDate]
           ,[customsPostID]
           ,[numberFrontLineStaff]
           ,[numberSpecializedEmployees]
           ,[numberSpecializedEmployeesTrained])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@numberFrontLineStaff
           ,@numberSpecializedEmployees
           ,@numberSpecializedEmployeesTrained)

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsByTransportModeData]
	@dateReported date,
	@modeOfTransport nvarchar(250),
	@totalImportsAmount money,
	@totalExportsAmount money,
	@totalTemporaryImportsAmount int,
	@freeTradeZoneOperations int,
	@passengersProcessed int,
	@clearanceTimeImports int,
	@clearanceTimeImportsRedLane decimal(18,3),
	@clearanceTimeImportsYellowLane decimal(18,3),
	@clearanceTimeImportsGreenLane decimal(18,3),
	@totalCarriers int,
	@totalCarriersLodingAdvancedElectronicInfo int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@modeOfTransport = '')
		set @modeOfTransport = NULL

	INSERT INTO [dbo].[CustomsOperationsByTransportMode]
           ([customsOperationsDate]
           ,[modeOfTransport]
           ,[totalImportsAmount]
           ,[totalExportsAmount]
           ,[totalTemporaryImportsAmount]
           ,[freeTradeZoneOperations]
           ,[passengersProcessed]
           ,[clearanceTimeImports]
           ,[clearanceTimeImportsRedLane]
           ,[clearanceTimeImportsYellowLane]
           ,[clearanceTimeImportsGreenLane]
		   ,[totalCarriers]
		   ,[totalCarriersLodingAdvancedElectronicInfo])
     VALUES
           (@dateReported
           ,@modeOfTransport
           ,@totalImportsAmount
           ,@totalExportsAmount
           ,@totalTemporaryImportsAmount
           ,@freeTradeZoneOperations
           ,@passengersProcessed
           ,@clearanceTimeImports
           ,@clearanceTimeImportsRedLane
           ,@clearanceTimeImportsYellowLane
           ,@clearanceTimeImportsGreenLane
		   ,@totalCarriers
		   ,@totalCarriersLodingAdvancedElectronicInfo)

END
GO

DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSSEIZURES_GetLastDate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSSEIZURES_GetLastDate]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsSeizuresDate]) as LastDate
	FROM [dbo].[CustomsSeizures]
END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSSEIZURES_ReportCustomsSeizuresData]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSSEIZURES_ReportCustomsSeizuresData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@seizureType nvarchar(250),
	@seizureCategory nvarchar(250),
	@seizureTransportMode nvarchar(250),
	@seizureMode nvarchar(250),
	@seizureValue money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = null

	INSERT INTO [dbo].[CustomsSeizures]
        ([customsSeizuresDate]
        ,[customsPostID]
        ,[seizureType]
        ,[seizureCategory]
        ,[seizureTransportMode]
        ,[seizureMode]
        ,[seizureValue])
	VALUES
        (@dateReported
        ,@customsPostID
        ,@seizureType
        ,@seizureCategory
        ,@seizureTransportMode
        ,@seizureMode
        ,@seizureValue)

END
GO



DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSINGLEWINDOW_GetLastDate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSINGLEWINDOW_GetLastDate]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsSingleWindowItemDate]) as LastDate
	FROM [dbo].[CustomsSingleWindow]
END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSINGLEWINDOW_ReportCustomsSingleWindowData]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSINGLEWINDOW_ReportCustomsSingleWindowData]
	@dateReported date,
	@totalCustomsProceduresProcessed int,
	@totalCustomsProceduresElectronic int,
	@numberCertificationsIssuance int,
	@numberOGARequirements int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsSingleWindow]
           ([customsSingleWindowItemDate]
           ,[totalCustomsProceduresProcessed]
           ,[totalCustomsProceduresElectronic]
           ,[numberCertificationsIssuance]
           ,[numberOGARequirements])
     VALUES
           (@dateReported
           ,@totalCustomsProceduresProcessed
           ,@totalCustomsProceduresElectronic
           ,@numberCertificationsIssuance
           ,@numberOGARequirements)

END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSTRADEPARTNERS_GetLastDate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSTRADEPARTNERS_GetLastDate]
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([tradePartnerStatsItemDate]) as LastDate
	FROM [dbo].[CustomsTradePartnerStats]
END
GO


DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSTRADEPARTNERS_ReportCustomsTradePartnerStatsData]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSTRADEPARTNERS_ReportCustomsTradePartnerStatsData]
	@dateReported date,
	@numberOfRegisteredImporters int,
	@numberOfRegisteredExporters int,
	@numberOfImporters70PercentRevenue int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsTradePartnerStats]
           ([tradePartnerStatsItemDate]
           ,[numberOfRegisteredImporters]
           ,[numberOfRegisteredExporters]
           ,[numberOfImporters70PercentRevenue])
     VALUES
           (@dateReported
           ,@numberOfRegisteredImporters
           ,@numberOfRegisteredExporters
           ,@numberOfImporters70PercentRevenue)

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ImportersWhoAccountFor70PercentOfTotalRevenuesQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_ImportersWhoAccountFor70PercentOfTotalRevenuesQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, amount money)

	select DATEPART(year, [tradePartnerStatsItemDate]) as [year], 
		DATEPART(quarter, [tradePartnerStatsItemDate]) as [quarter],
		avg([numberOfImporters70PercentRevenue]) as importers
	from [dbo].[CustomsTradePartnerStats]
	where DATEPART(year, [tradePartnerStatsItemDate]) > @startYear 
		and DATEPART(year, [tradePartnerStatsItemDate]) <= @limitYear
	group by DATEPART(year, [tradePartnerStatsItemDate]),  DATEPART(quarter, [tradePartnerStatsItemDate])
	order by DATEPART(year, [tradePartnerStatsItemDate]) asc,  DATEPART(quarter, [tradePartnerStatsItemDate]) asc
END
go


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ImportersWhoAccountFor70PercentOfTotalRevenuesMonthly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_ImportersWhoAccountFor70PercentOfTotalRevenuesMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select DATEPART(year, [tradePartnerStatsItemDate]) as [year], 
		DATEPART(month, [tradePartnerStatsItemDate]) as [month],
		avg([numberOfImporters70PercentRevenue]) as importers
	from [dbo].[CustomsTradePartnerStats]
	where [tradePartnerStatsItemDate] >= @startDate
	group by DATEPART(year, [tradePartnerStatsItemDate]),  DATEPART(month, [tradePartnerStatsItemDate])
	order by DATEPART(year, [tradePartnerStatsItemDate]) asc, DATEPART(month, [tradePartnerStatsItemDate]) asc
END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CostOfCollectionRatioQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CostOfCollectionRatioQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @revenue as table([year] int, [quarter] int, amount money)
	declare @cost as table([year] int, [quarter] int, cost money)

	insert into @revenue
	select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(quarter, [customsRevenueDate]) as [quarter],
		sum([amount]) as revenue
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(quarter, [customsRevenueDate])

	insert into @cost
	select DATEPART(year, [customsAdministrationExpensesDate]) as [year], 
		DATEPART(quarter, [customsAdministrationExpensesDate]) as [quarter],
		sum([currentExpense]) as revenue
	from [dbo].[CustomsAdministrationExpenses]
	where DATEPART(year, [customsAdministrationExpensesDate]) > @startYear 
		and DATEPART(year, [customsAdministrationExpensesDate]) <= @limitYear
	group by DATEPART(year, [customsAdministrationExpensesDate]),  DATEPART(quarter, [customsAdministrationExpensesDate])

	select 
		r.year, 
		r.quarter, 
		case when isnull(r.amount,0.00) = 0.00 then 0.00 else isnull(c.cost,0.00) / r.amount * 100.00 end as costRatio
	from @revenue as r
	join @cost as c on r.year = c.year and r.quarter = c.quarter
	order by r.year asc, r.quarter asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_NarcoticsSeizuresMonthlyGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_NarcoticsSeizuresMonthlyGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select DATEPART(year, [customsSeizuresDate]) as [year], 
		DATEPART(month, [customsSeizuresDate]) as [month],
		sum([seizureValue]) / 1000 as seizureValue
	from [dbo].[CustomsSeizures]
	where [customsSeizuresDate] >= @startDate
		and [seizureType] = 'Narcotics'
	group by DATEPART(year, [customsSeizuresDate]),  DATEPART(month, [customsSeizuresDate])
	order by DATEPART(year, [customsSeizuresDate]) asc, DATEPART(month, [customsSeizuresDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_NarcoticsSeizuresMonthlyByType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_NarcoticsSeizuresMonthlyByType]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select DATEPART(year, [customsSeizuresDate]) as [year], 
		DATEPART(month, [customsSeizuresDate]) as [month],
		sum([seizureValue]) / 1000 as seizureValue,
		[seizureCategory]
	from [dbo].[CustomsSeizures]
	where [customsSeizuresDate] >= @startDate
		and [seizureType] = 'Narcotics'
	group by DATEPART(year, [customsSeizuresDate]),  DATEPART(month, [customsSeizuresDate]), [seizureCategory]
	order by DATEPART(year, [customsSeizuresDate]) asc, DATEPART(month, [customsSeizuresDate]) asc, [seizureCategory]

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PrecursorsSeizuresMonthlyGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PrecursorsSeizuresMonthlyGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select DATEPART(year, [customsSeizuresDate]) as [year], 
		DATEPART(month, [customsSeizuresDate]) as [month],
		sum([seizureValue]) / 1000 as seizureValue
	from [dbo].[CustomsSeizures]
	where [customsSeizuresDate] >= @startDate
		and [seizureType] = 'Precursors'
	group by DATEPART(year, [customsSeizuresDate]),  DATEPART(month, [customsSeizuresDate])
	order by DATEPART(year, [customsSeizuresDate]) asc, DATEPART(month, [customsSeizuresDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ContrabandSeizuresMonthlyGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_ContrabandSeizuresMonthlyGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select DATEPART(year, [customsSeizuresDate]) as [year], 
		DATEPART(month, [customsSeizuresDate]) as [month],
		sum([seizureValue]) / 1000 as seizureValue
	from [dbo].[CustomsSeizures]
	where [customsSeizuresDate] >= @startDate
		and [seizureMode] = 'Contraband'
	group by DATEPART(year, [customsSeizuresDate]),  DATEPART(month, [customsSeizuresDate])
	order by DATEPART(year, [customsSeizuresDate]) asc, DATEPART(month, [customsSeizuresDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ProgressToInstitutionalElectronicIssuanceOfCustomsProceduresQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_ProgressToInstitutionalElectronicIssuanceOfCustomsProceduresQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	select DATEPART(year, [customsSingleWindowItemDate]) as [year], 
		DATEPART(quarter, [customsSingleWindowItemDate]) as [quarter],
		case 
			when isnull(avg([totalCustomsProceduresProcessed]),0) = 0 then 0.00 else 
			convert(decimal(18,3), avg([totalCustomsProceduresElectronic])) / 
			convert(decimal(18,3), avg([totalCustomsProceduresProcessed])) * 100
		end as ratio
	from [dbo].[CustomsSingleWindow]
	where DATEPART(year, [customsSingleWindowItemDate]) > @startYear 
		and DATEPART(year, [customsSingleWindowItemDate]) <= @limitYear
	group by DATEPART(year, [customsSingleWindowItemDate]),  DATEPART(quarter, [customsSingleWindowItemDate])
	order by DATEPART(year, [customsSingleWindowItemDate]) asc,  DATEPART(quarter, [customsSingleWindowItemDate]) asc

END
go



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ProgressSWIOGAElectronicIssuanceProceduresQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_ProgressSWIOGAElectronicIssuanceProceduresQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	select DATEPART(year, [customsSingleWindowItemDate]) as [year], 
		DATEPART(quarter, [customsSingleWindowItemDate]) as [quarter],
		case 
			when isnull(avg([numberOGARequirements]),0) = 0 then 0.00 else 
			convert(decimal(18,3), avg([numberCertificationsIssuance])) / 
			convert(decimal(18,3), avg([numberOGARequirements])) * 100
		end as ratio
	from [dbo].[CustomsSingleWindow]
	where DATEPART(year, [customsSingleWindowItemDate]) > @startYear 
		and DATEPART(year, [customsSingleWindowItemDate]) <= @limitYear
	group by DATEPART(year, [customsSingleWindowItemDate]),  DATEPART(quarter, [customsSingleWindowItemDate])
	order by DATEPART(year, [customsSingleWindowItemDate]) asc,  DATEPART(quarter, [customsSingleWindowItemDate]) asc

END
go


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CarriersLodgingAdvanceElectronicInformationBillsQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CarriersLodgingAdvanceElectronicInformationBillsQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	select DATEPART(year, [customsOperationsDate]) as [year], 
		DATEPART(quarter, [customsOperationsDate]) as [quarter],
		case 
			when isnull(avg([totalCarriers]),0) = 0 then 0.00 else 
			convert(decimal(18,3), avg([totalCarriersLodingAdvancedElectronicInfo])) / 
			convert(decimal(18,3), avg([totalCarriers])) * 100
		end as ratio
	from [dbo].[CustomsOperationsByTransportMode]
	where DATEPART(year, [customsOperationsDate]) > @startYear 
		and DATEPART(year, [customsOperationsDate]) <= @limitYear
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(quarter, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(quarter, [customsOperationsDate]) asc

END
go


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CustomsAverageYieldInPostClearanceAuditsGlobalMonthly]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CustomsAverageYieldInPostClearanceAuditsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsAuditDate]) as year, 
		DATEPART(month, [customsAuditDate]) as month,
		case when sum([numberAuditsMade]) > 0 then sum([totalAmountAssessedFromAudits]) / sum([numberAuditsMade]) else 0.00 end as amountPerAudit
	FROM [dbo].[CustomsAudit]
	where [customsAuditDate] >= @startDate
	group by DATEPART(year, [customsAuditDate]), DATEPART(month, [customsAuditDate])
	order by DATEPART(year, [customsAuditDate]) asc, DATEPART(month, [customsAuditDate]) asc


END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CollectionGrowthRateFromPostClearanceAuditsGlobalMonthly]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CollectionGrowthRateFromPostClearanceAuditsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @tab as table([year] int, [month] int, amount money)

	insert into @tab
	SELECT DATEPART(year, [customsAuditDate]) as year, 
		DATEPART(month, [customsAuditDate]) as month,
		sum([totalAmountAssessedFromAudits])
	FROM [dbo].[CustomsAudit]
	where [customsAuditDate] >= @startDate
	group by DATEPART(year, [customsAuditDate]), DATEPART(month, [customsAuditDate])

	select t1.year, t1.month, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as collectionGrowth
	from @tab as t1
	join @tab as t2 on t1.year*12+t1.month-1 = t2.year*12+t2.month 
	order by t1.[year] asc , t1.[month] asc


END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromCustomsControlsQuarterly]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromCustomsControlsQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @controlRevenue as table([year] int, [quarter] int, amount money)
	declare @totalRevenue as table([year] int, [quarter] int, amount money)

	insert into @controlRevenue
	SELECT DATEPART(year, [customsRevenueDate]) as year, 
		DATEPART(quarter, [customsRevenueDate]) as month,
		sum([amount])
	FROM [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
		and concept='Control'
	group by DATEPART(year, [customsRevenueDate]), DATEPART(quarter, [customsRevenueDate])

	insert into @totalRevenue
	SELECT DATEPART(year, [customsRevenueDate]) as year, 
		DATEPART(quarter, [customsRevenueDate]) as month,
		sum([amount])
	FROM [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
	group by DATEPART(year, [customsRevenueDate]), DATEPART(quarter, [customsRevenueDate])

	select t1.year, t1.quarter, 
	case when t1.amount > 0 then t2.amount / t1.amount * 100.00 else 0.00 end as ratio
	from @totalRevenue as t1
	join @controlRevenue as t2 on t1.year = t2.year and t1.quarter = t2.quarter
	order by t1.year asc, t1.quarter asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromPostClearanceAuditsQuarterly]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromPostClearanceAuditsQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @auditRevenue as table([year] int, [quarter] int, amount money)
	declare @totalRevenue as table([year] int, [quarter] int, amount money)

	insert into @auditRevenue
	SELECT DATEPART(year, [customsRevenueDate]) as year, 
		DATEPART(quarter, [customsRevenueDate]) as month,
		sum([amount])
	FROM [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
		and concept='Audit'
	group by DATEPART(year, [customsRevenueDate]), DATEPART(quarter, [customsRevenueDate])

	insert into @totalRevenue
	SELECT DATEPART(year, [customsRevenueDate]) as year, 
		DATEPART(quarter, [customsRevenueDate]) as month,
		sum([amount])
	FROM [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
	group by DATEPART(year, [customsRevenueDate]), DATEPART(quarter, [customsRevenueDate])

	select t1.year, t1.quarter, 
	case when t1.amount > 0 then t2.amount / t1.amount * 100.00 else 0.00 end as ratio
	from @totalRevenue as t1
	join @auditRevenue as t2 on t1.year = t2.year and t1.quarter = t2.quarter
	order by t1.year asc, t1.quarter asc

END
GO




