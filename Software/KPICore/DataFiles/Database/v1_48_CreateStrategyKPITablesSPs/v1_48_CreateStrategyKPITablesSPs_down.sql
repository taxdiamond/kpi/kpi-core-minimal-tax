DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSADMINEXPENSES_GetLastDate]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSADMINEXPENSES_ReportCustomsAdminExpensesData]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSHR_GetLastDate]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSHR_ReportCustomsHRData]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSSEIZURES_GetLastDate]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSSEIZURES_ReportCustomsSeizuresData]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSINGLEWINDOW_GetLastDate]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSINGLEWINDOW_ReportCustomsSingleWindowData]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSTRADEPARTNERS_GetLastDate]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSTRADEPARTNERS_ReportCustomsTradePartnerStatsData]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ImportersWhoAccountFor70PercentOfTotalRevenuesQuarterly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ImportersWhoAccountFor70PercentOfTotalRevenuesMonthly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CostOfCollectionRatioQuarterly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_NarcoticsSeizuresMonthlyGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_NarcoticsSeizuresMonthlyByType]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PrecursorsSeizuresMonthlyGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ContrabandSeizuresMonthlyGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ProgressToInstitutionalElectronicIssuanceOfCustomsProceduresQuarterly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_ProgressSWIOGAElectronicIssuanceProceduresQuarterly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CarriersLodgingAdvanceElectronicInformationBillsQuarterly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CustomsAverageYieldInPostClearanceAuditsGlobalMonthly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CollectionGrowthRateFromPostClearanceAuditsGlobalMonthly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromCustomsControlsQuarterly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromPostClearanceAuditsQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsByTransportModeData]
	@dateReported date,
	@modeOfTransport nvarchar(250),
	@totalImportsAmount money,
	@totalExportsAmount money,
	@totalTemporaryImportsAmount int,
	@freeTradeZoneOperations int,
	@passengersProcessed int,
	@clearanceTimeImports int,
	@clearanceTimeImportsRedLane decimal(18,3),
	@clearanceTimeImportsYellowLane decimal(18,3),
	@clearanceTimeImportsGreenLane decimal(18,3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@modeOfTransport = '')
		set @modeOfTransport = NULL

	INSERT INTO [dbo].[CustomsOperationsByTransportMode]
           ([customsOperationsDate]
           ,[modeOfTransport]
           ,[totalImportsAmount]
           ,[totalExportsAmount]
           ,[totalTemporaryImportsAmount]
           ,[freeTradeZoneOperations]
           ,[passengersProcessed]
           ,[clearanceTimeImports]
           ,[clearanceTimeImportsRedLane]
           ,[clearanceTimeImportsYellowLane]
           ,[clearanceTimeImportsGreenLane])
     VALUES
           (@dateReported
           ,@modeOfTransport
           ,@totalImportsAmount
           ,@totalExportsAmount
           ,@totalTemporaryImportsAmount
           ,@freeTradeZoneOperations
           ,@passengersProcessed
           ,@clearanceTimeImports
           ,@clearanceTimeImportsRedLane
           ,@clearanceTimeImportsYellowLane
           ,@clearanceTimeImportsGreenLane)

END
GO

