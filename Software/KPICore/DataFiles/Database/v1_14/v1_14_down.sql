USE [KPICoreDB]
GO

/****** Object:  StoredProcedure [dbo].[REVENUE_ReportRevenueData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[REVENUE_ReportRevenueData]
GO

/****** Object:  StoredProcedure [dbo].[ARREARS_ReportArrearsData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[ARREARS_ReportArrearsData]
GO

/****** Object:  StoredProcedure [dbo].[STAT_InsertParameterStats]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[STAT_InsertParameterStats]
GO

/****** Object:  StoredProcedure [dbo].[COUNTRY_ReportCountryData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[COUNTRY_ReportCountryData]
GO

/****** Object:  StoredProcedure [dbo].[REGION_ReportRegionData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[REGION_ReportRegionData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthly]
GO

/****** Object:  StoredProcedure [dbo].[FORECAST_ReportForecastData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[FORECAST_ReportForecastData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthlyByTaxType]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthlyByTaxType]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByTaxType]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByTaxType]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[TAXPAYER_ReportTaxpayerData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[TAXPAYER_ReportTaxpayerData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByTaxPayerSegment]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByOffice]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByOffice]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByOffice]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByOffice]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxToGDPRatioByRegion]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxToGDPRatioByRegion]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxToGDPRatioGlobal]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxToGDPRatioGlobal]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByTaxPayerSegment]
GO

/****** Object:  StoredProcedure [dbo].[IMPORTS_ReportImportData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[IMPORTS_ReportImportData]
GO

/****** Object:  StoredProcedure [dbo].[TAXRATE_ReportTaxRateData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[TAXRATE_ReportTaxRateData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_VATTaxToGDPRatioByRegion]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_VATTaxToGDPRatioByRegion]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_VATTaxToGDPRatioGlobal]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_VATTaxToGDPRatioGlobal]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_VATGrossComplianceRatio]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_VATGrossComplianceRatio]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_CorporateIncomeTaxProductivity]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CorporateIncomeTaxProductivity]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_CorporateIncomeTaxProductivityByRegion]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CorporateIncomeTaxProductivityByRegion]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTime]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_PercentageTaxPayersFiledInTime]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[REVENUE_ReportSummaryRevenueData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[REVENUE_ReportSummaryRevenueData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[AUDIT_ReportAuditData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[AUDIT_ReportAuditData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditCoverageYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditCoverageYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditCoverageMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditCoverageMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByTaxType]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByTaxType]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByTaxType]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByTaxType]
GO

/****** Object:  StoredProcedure [dbo].[EXPENSES_ReportTaxAdministrationExpensesData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[EXPENSES_ReportTaxAdministrationExpensesData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPICostCollectionRatioYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPICostCollectionRatioYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPICostCollectionRatioMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPICostCollectionRatioMonthly]
GO

/****** Object:  StoredProcedure [dbo].[UTILITY_GetTableSchema]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[UTILITY_GetTableSchema]
GO

/****** Object:  StoredProcedure [dbo].[HR_ReportHRData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[HR_ReportHRData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldMonthlyByRegion]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldMonthlyByRegion]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldYearlyByRegion]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldYearlyByRegion]
GO

/****** Object:  StoredProcedure [dbo].[SERVICES_ReportTaxPayerServices]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[SERVICES_ReportTaxPayerServices]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICoverageElectronicTaxServicesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPICoverageElectronicTaxServicesYearly]
GO

/****** Object:  StoredProcedure [dbo].[COURSES_ReportCouresData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[COURSES_ReportCouresData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCoursesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfCoursesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCoursesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfCoursesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCourseMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfCourseMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCourseMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfCourseMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[SATISFACTION_SURVEYS_ReportSurveyData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[SATISFACTION_SURVEYS_ReportSurveyData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPISatisfactionSurveysMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPISatisfactionSurveysYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPISatisfactionSurveysMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPISatisfactionSurveysYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINewRegistrantsYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINewRegistrantsYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINewRegistrantsMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINewRegistrantsMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingMonthly]
GO

/****** Object:  StoredProcedure [dbo].[FILING_ReportFilingData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[FILING_ReportFilingData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[REFUND_ReportRefundsData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[REFUND_ReportRefundsData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditAdditionalAssessmentYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditAdditionalAssessmentYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByTaxPayerSegment]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyTaxPayerSegmentByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyTaxPayerSegmentByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByTaxPayerSegment]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyTaxPayerSegmentByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyTaxPayerSegmentByFilter]
GO

/****** Object:  StoredProcedure [dbo].[APPEALS_ReportAppealsData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[APPEALS_ReportAppealsData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearlyByFilter]
GO
