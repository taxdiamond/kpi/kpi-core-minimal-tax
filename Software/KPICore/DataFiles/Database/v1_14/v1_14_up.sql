/****** Object:  StoredProcedure [dbo].[REVENUE_ReportRevenueData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[REVENUE_ReportRevenueData]
GO

/****** Object:  StoredProcedure [dbo].[ARREARS_ReportArrearsData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[ARREARS_ReportArrearsData]
GO

/****** Object:  StoredProcedure [dbo].[STAT_InsertParameterStats]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[STAT_InsertParameterStats]
GO

/****** Object:  StoredProcedure [dbo].[COUNTRY_ReportCountryData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[COUNTRY_ReportCountryData]
GO

/****** Object:  StoredProcedure [dbo].[REGION_ReportRegionData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[REGION_ReportRegionData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthly]
GO

/****** Object:  StoredProcedure [dbo].[FORECAST_ReportForecastData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[FORECAST_ReportForecastData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthlyByTaxType]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthlyByTaxType]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByTaxType]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByTaxType]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[TAXPAYER_ReportTaxpayerData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[TAXPAYER_ReportTaxpayerData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByTaxPayerSegment]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByOffice]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByOffice]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByOffice]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByOffice]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxToGDPRatioByRegion]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxToGDPRatioByRegion]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxToGDPRatioGlobal]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxToGDPRatioGlobal]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByTaxPayerSegment]
GO

/****** Object:  StoredProcedure [dbo].[IMPORTS_ReportImportData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[IMPORTS_ReportImportData]
GO

/****** Object:  StoredProcedure [dbo].[TAXRATE_ReportTaxRateData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[TAXRATE_ReportTaxRateData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_VATTaxToGDPRatioByRegion]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_VATTaxToGDPRatioByRegion]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_VATTaxToGDPRatioGlobal]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_VATTaxToGDPRatioGlobal]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_VATGrossComplianceRatio]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_VATGrossComplianceRatio]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_CorporateIncomeTaxProductivity]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CorporateIncomeTaxProductivity]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_CorporateIncomeTaxProductivityByRegion]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CorporateIncomeTaxProductivityByRegion]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTime]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_PercentageTaxPayersFiledInTime]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[REVENUE_ReportSummaryRevenueData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[REVENUE_ReportSummaryRevenueData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[AUDIT_ReportAuditData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[AUDIT_ReportAuditData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditCoverageYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditCoverageYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditCoverageMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditCoverageMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByTaxType]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByTaxType]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByTaxType]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByTaxType]
GO

/****** Object:  StoredProcedure [dbo].[EXPENSES_ReportTaxAdministrationExpensesData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[EXPENSES_ReportTaxAdministrationExpensesData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPICostCollectionRatioYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPICostCollectionRatioYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPICostCollectionRatioMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPICostCollectionRatioMonthly]
GO

/****** Object:  StoredProcedure [dbo].[UTILITY_GetTableSchema]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[UTILITY_GetTableSchema]
GO

/****** Object:  StoredProcedure [dbo].[HR_ReportHRData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[HR_ReportHRData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldMonthlyByRegion]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldMonthlyByRegion]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldYearlyByRegion]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditYieldYearlyByRegion]
GO

/****** Object:  StoredProcedure [dbo].[SERVICES_ReportTaxPayerServices]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[SERVICES_ReportTaxPayerServices]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICoverageElectronicTaxServicesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPICoverageElectronicTaxServicesYearly]
GO

/****** Object:  StoredProcedure [dbo].[COURSES_ReportCouresData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[COURSES_ReportCouresData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCoursesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfCoursesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCoursesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfCoursesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCourseMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfCourseMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCourseMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfCourseMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[SATISFACTION_SURVEYS_ReportSurveyData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[SATISFACTION_SURVEYS_ReportSurveyData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPISatisfactionSurveysMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPISatisfactionSurveysYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPISatisfactionSurveysMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPISatisfactionSurveysYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINewRegistrantsYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINewRegistrantsYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINewRegistrantsMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINewRegistrantsMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingMonthly]
GO

/****** Object:  StoredProcedure [dbo].[FILING_ReportFilingData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[FILING_ReportFilingData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[REFUND_ReportRefundsData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[REFUND_ReportRefundsData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditAdditionalAssessmentYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditAdditionalAssessmentYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByTaxPayerSegment]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyTaxPayerSegmentByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyTaxPayerSegmentByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByTaxPayerSegment]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyTaxPayerSegmentByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyTaxPayerSegmentByFilter]
GO

/****** Object:  StoredProcedure [dbo].[APPEALS_ReportAppealsData]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[APPEALS_ReportAppealsData]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearly]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearlyByFilter]
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([numberCasesResolved]) as appealsResolved
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc'
	
	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([numberCasesResolved]) as appealsResolved
	from [dbo].[Appeals]
	where concept = ''Tax'' and
			DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		sum([numberCasesResolved]) as appealsResolved
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxPayerSegmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([numberCasesResolved]) as appealsResolved
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([numberCasesResolved]) as appealsResolved
	from [dbo].[Appeals]
	where concept = 'Tax' and
			DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfTaxAppealsCasesResolvedYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		sum([numberCasesResolved]) as appealsResolved
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		s.segmentName as segmentName,
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, segmentName asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals]
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate])
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		[conceptID] as taxType,
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals]
	where concept = ''Tax'' and
			[appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, [conceptID] asc'
	
	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals]
	where concept = ''Tax'' and
			DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc' 
	
	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		s.segmentName as segmentName,
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, segmentName asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		[conceptID] as taxType,
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals]
	where concept = 'Tax' and
			[appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals]
	where [appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate])
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals]
	where concept = 'Tax' and
			DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxAppealsCasesSubmittedYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		sum([numberOfAppealCasesSubmitted]) as appealsSubmitted
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc

END
GO

/****** Object:  StoredProcedure [dbo].[APPEALS_ReportAppealsData]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 8 2020
-- =============================================
CREATE PROCEDURE [dbo].[APPEALS_ReportAppealsData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@numberAppealsCasesPending int,
	@ammountAppealed money,
	@numberCasesResolved int,
	@numberCasesResolvedFavorTaxPayer int,
	@numberCasesResolvedFavorTaxAdministration int,
	@averageDaysToResolveCases int,
	@totalAmountResolvedFavorTaxPayer money,
	@totalAmountResolvedFavorTaxAdministration money,
	@numberOfAppealCasesSubmitted int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @regionID = NULL

	INSERT INTO [dbo].[Appeals]
           ([appealsDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[numberAppealsCasesPending]
           ,[amountAppealed]
           ,[numberCasesResolved]
           ,[numberCasesResolvedFavorTaxPayer]
           ,[numberCasesResolvedFavorTaxAdministration]
           ,[averageDaysToResolveCases]
           ,[totalAmountResolvedFavorTaxPayer]
           ,[totalAmountResolvedFavorTaxAdministration]
		   ,[numberOfAppealCasesSubmitted])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@numberAppealsCasesPending
           ,@ammountAppealed
           ,@numberCasesResolved
           ,@numberCasesResolvedFavorTaxPayer
           ,@numberCasesResolvedFavorTaxAdministration
           ,@averageDaysToResolveCases
           ,@totalAmountResolvedFavorTaxPayer
           ,@totalAmountResolvedFavorTaxAdministration
		   ,@numberOfAppealCasesSubmitted)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyTaxPayerSegmentByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyTaxPayerSegmentByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month
	
	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) + 1 as year, 
		s.segmentName,
		sum([ammountAppealed]) as appealsStock
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [appealsDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), s.segmentName
	order by datepart(year, [appealsDate]) asc, s.segmentName asc' 

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByTaxPayerSegment]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) + 1 as year, 
		s.segmentName,
		sum([amountAppealed]) as appealsStock
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > (@startYear -1)  
			and DATEPART(year, [appealsDate]) < @limitYear
	group by datepart(year, [appealsDate]), s.segmentName
	order by datepart(year, [appealsDate]) asc, s.segmentName asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate])  + 1 as year, 
		sum([ammountAppealed]) as appealsStock
	from [dbo].[Appeals]
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [appealsDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForAppealCasesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) + 1 as year, 
		sum([amountAppealed]) as appealsStock
	from [dbo].[Appeals]
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > (@startYear -1) 
			and DATEPART(year, [appealsDate]) < @limitYear
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyTaxPayerSegmentByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyTaxPayerSegmentByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- IT IS POSSIBLE that we need to review this SP since this is measuring the number of tax appeals 
	-- for January and the number of appeals for January First may have been reported on the Dec/31st 
	-- of the previous month
	
	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) + 1 as year, 
		s.segmentName,
		sum([numberAppealsCasesPending]) as appealsStock
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [appealsDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), s.segmentName
	order by datepart(year, [appealsDate]) asc, s.segmentName asc' 

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByTaxPayerSegment]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) + 1 as year, 
		s.segmentName,
		sum([numberAppealsCasesPending]) as appealsStock
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > (@startYear -1)  
			and DATEPART(year, [appealsDate]) < @limitYear
	group by datepart(year, [appealsDate]), s.segmentName
	order by datepart(year, [appealsDate]) asc, s.segmentName asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate])  + 1 as year, 
		sum([numberAppealsCasesPending]) as appealsStock
	from [dbo].[Appeals]
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [appealsDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInNumberOfAppealCasesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) + 1 as year, 
		sum([numberAppealsCasesPending]) as appealsStock
	from [dbo].[Appeals]
	where datepart(month,[appealsDate]) = 12
		and DATEPART(year, [appealsDate]) > (@startYear -1) 
			and DATEPART(year, [appealsDate]) < @limitYear
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
create PROCEDURE [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [statsDate]) as year, 
		s.segmentName,
		sum([totalAdditionalAssessment]) as additionalAssessment
		from [dbo].[AuditStats] as a
		join [dbo].[Segment] as s on a.segmentID = s.segmentID
		where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [statsDate]), s.segmentName
	order by DATEPART(year, [statsDate]) asc, s.segmentName asc' 

	execute(@query)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
create PROCEDURE [dbo].[KPI_DATA_AuditAdditionalAssessmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [statsDate]) as year, 
		sum([totalAdditionalAssessment]) as additionalAssessment
		from [dbo].[AuditStats]
		where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
create PROCEDURE [dbo].[KPI_DATA_AuditAdditionalAssessmentByTaxPayerSegmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [statsDate]) as year, 
		s.segmentName,
		sum([totalAdditionalAssessment]) as additionalAssessment
		from [dbo].[AuditStats] as a
		join [dbo].[Segment] as s on a.segmentID = s.segmentID
		where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]), s.segmentName
	order by DATEPART(year, [statsDate]) asc, s.segmentName asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditAdditionalAssessmentYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
create PROCEDURE [dbo].[KPI_DATA_AuditAdditionalAssessmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [statsDate]) as year, 
		sum([totalAdditionalAssessment]) as additionalAssessment
		from [dbo].[AuditStats]
		where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [refundsDate]) as year, DATEPART(month, [refundsDate]) as month,
		[conceptID] as taxType,
		sum([refundAmountProcessed]) as refundAmountProcessed
	from [dbo].[Refunds]
	where [concept] = ''Tax'' 
		and [refundsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [refundsDate]), DATEPART(month, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, DATEPART(month, [refundsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	select DATEPART(year, [refundsDate]) as year, 
		DATEPART(month, [refundsDate]) as month,
		[conceptID] as taxType,
		sum([refundAmountProcessed]) as refundAmountProcessed
	from [dbo].[Refunds]
	where [concept] = 'Tax' 
		and [refundsDate] >= @startDate  
	group by DATEPART(year, [refundsDate]), DATEPART(month, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, DATEPART(month, [refundsDate]) asc, [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [refundsDate]) as year,
		[conceptID] as taxType,
		sum([refundAmountProcessed]) as refundAmountProcessed
	from [dbo].[Refunds]
	where [concept] = ''Tax''
		and DATEPART(year, [refundsDate]) > '+convert(varchar, @startYear) + '  
		and DATEPART(year, [refundsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundAmountProcessedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [refundsDate]) as year,
		[conceptID] as taxType,
		sum([refundAmountProcessed]) as refundAmountProcessed
	from [dbo].[Refunds]
	where [concept] = 'Tax' 
		and DATEPART(year, [refundsDate]) > @startYear  
		and DATEPART(year, [refundsDate]) <= @limitYear
	group by DATEPART(year, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [refundsDate]) as year, DATEPART(month, [refundsDate]) as month,
		[conceptID] as taxType,
		case when sum([numberRefundClaimsRequested]) = 0 then 
			0.0
		else 
			convert(decimal(18,3), sum([numberRequestsClaimsProcessed])) / 
			convert(decimal(18,3), sum([numberRefundClaimsRequested])) * 100.00 
		end as refundClaimsProcessedRatio
	from [dbo].[Refunds]
	where [concept] = ''Tax'' 
		and [refundsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [refundsDate]), DATEPART(month, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, DATEPART(month, [refundsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	select DATEPART(year, [refundsDate]) as year, 
		DATEPART(month, [refundsDate]) as month,
		[conceptID] as taxType,
		case when sum([numberRefundClaimsRequested]) = 0 then 
			0.0
		else 
			convert(decimal(18,3), sum([numberRequestsClaimsProcessed])) / 
			convert(decimal(18,3), sum([numberRefundClaimsRequested])) * 100.00 
		end as refundClaimsProcessedRatio
	from [dbo].[Refunds]
	where [concept] = 'Tax' 
		and [refundsDate] >= @startDate  
	group by DATEPART(year, [refundsDate]), DATEPART(month, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, DATEPART(month, [refundsDate]) asc, [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [refundsDate]) as year,
		[conceptID] as taxType,
		case when sum([numberRefundClaimsRequested]) = 0 then 
			0.0
		else 
			convert(decimal(18,3), sum([numberRequestsClaimsProcessed])) / 
			convert(decimal(18,3), sum([numberRefundClaimsRequested])) * 100.00 
		end as refundClaimsProcessedRatio
	from [dbo].[Refunds]
	where [concept] = ''Tax''
		and DATEPART(year, [refundsDate]) > '+convert(varchar, @startYear) + '  
		and DATEPART(year, [refundsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundClaimsProcessedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [refundsDate]) as year,
		[conceptID] as taxType,
		case when sum([numberRefundClaimsRequested]) = 0 then 
			0.0
		else 
			convert(decimal(18,3), sum([numberRequestsClaimsProcessed])) / 
			convert(decimal(18,3), sum([numberRefundClaimsRequested])) * 100.00 
		end as refundClaimsProcessedRatio
	from [dbo].[Refunds]
	where [concept] = 'Tax' 
		and DATEPART(year, [refundsDate]) > @startYear  
		and DATEPART(year, [refundsDate]) <= @limitYear
	group by DATEPART(year, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [refundsDate]) as year, DATEPART(month, [refundsDate]) as month,
		[conceptID] as taxType,
		sum([numberRefundClaimsRequested]) as totalRefundClaimsRequested
	from [dbo].[Refunds]
	where [concept] = ''Tax'' 
		and [refundsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [refundsDate]), DATEPART(month, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, DATEPART(month, [refundsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	select DATEPART(year, [refundsDate]) as year, DATEPART(month, [refundsDate]) as month,
		[conceptID] as taxType,
		sum([numberRefundClaimsRequested]) as totalRefundClaimsRequested
	from [dbo].[Refunds]
	where [concept] = 'Tax' 
		and [refundsDate] >= @startDate  
	group by DATEPART(year, [refundsDate]), DATEPART(month, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, DATEPART(month, [refundsDate]) asc, [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [refundsDate]) as year,
		[conceptID] as taxType,
		sum([numberRefundClaimsRequested]) as totalRefundClaimsRequested
	from [dbo].[Refunds]
	where [concept] = ''Tax''
		and DATEPART(year, [refundsDate]) > '+convert(varchar, @startYear) + '  
		and DATEPART(year, [refundsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRefundClaimsRequestedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [refundsDate]) as year,
		[conceptID] as taxType,
		sum([numberRefundClaimsRequested]) as totalRefundClaimsRequested
	from [dbo].[Refunds]
	where [concept] = 'Tax' 
		and DATEPART(year, [refundsDate]) > @startYear  
		and DATEPART(year, [refundsDate]) <= @limitYear
	group by DATEPART(year, [refundsDate]), [conceptID]
	order by DATEPART(year, [refundsDate]) asc, [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[REFUND_ReportRefundsData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
CREATE PROCEDURE [dbo].[REFUND_ReportRefundsData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@numberRefundClaimsRequested int,
	@numberRequestsClaimsProcessed int,
	@refundAmountRequested money,
	@refundAmountProcessed money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @regionID = NULL


	INSERT INTO [dbo].[Refunds]
           ([refundsDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[numberRefundClaimsRequested]
		   ,[numberRequestsClaimsProcessed]
		   ,[refundAmountRequested]
		   ,[refundAmountProcessed])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@numberRefundClaimsRequested
		   ,@numberRequestsClaimsProcessed
		   ,@refundAmountRequested
		   ,@refundAmountProcessed)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month,
		s.segmentName as segmentName,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue] as r
	join [dbo].[Segment] as s on r.segmentID = s.segmentID
	where [concept] = ''Tax'' 
		and [revenueDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), s.segmentName
	order by DATEPART(year, [revenueDate]) asc, DATEPART(month, [revenueDate]) asc, s.segmentName asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [revenueDate]) as year,
		s.segmentName as segmentName,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue] as r
	join [dbo].[Segment] as s on r.segmentID = s.segmentID
	where [concept] = ''Tax'' 
		and DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear) + ' 
		and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [revenueDate]), s.segmentName
	order by DATEPART(year, [revenueDate]) asc, s.segmentName asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	select DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month,
		s.segmentName as segmentName,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue] as r
	join [dbo].[Segment] as s on r.segmentID = s.segmentID
	where [concept] = 'Tax' 
		and [revenueDate] >= @startDate  
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), s.segmentName
	order by DATEPART(year, [revenueDate]) asc, DATEPART(month, [revenueDate]) asc, s.segmentName asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxPayerSegmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [revenueDate]) as year,
		s.segmentName as segmentName,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue] as r
	join [dbo].[Segment] as s on r.segmentID = s.segmentID
	where [concept] = 'Tax' 
		and DATEPART(year, [revenueDate]) > @startYear  
		and DATEPART(year, [revenueDate]) <= @limitYear
	group by DATEPART(year, [revenueDate]), s.segmentName
	order by DATEPART(year, [revenueDate]) asc, s.segmentName asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [revenueDate]) as year,
		[conceptID] as taxType,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue]
	where [concept] = ''Tax'' 
		and DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear) + ' 
		and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [revenueDate]), [conceptID]
	order by DATEPART(year, [revenueDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [reportDate]) as year,
		[taxType],
		sum([totalFilings]) as returnsFiled
	from [dbo].[FilingStats]
	where DATEPART(year, [reportDate]) > '+convert(varchar, @startYear) + ' 
		and DATEPART(year, [reportDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [reportDate]), [taxType]
	order by DATEPART(year, [reportDate]) asc, [taxType] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month,
		[conceptID] as taxType,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue]
	where [concept] = ''Tax'' 
		and [revenueDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), [conceptID]
	order by DATEPART(year, [revenueDate]) asc, DATEPART(month, [revenueDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	-- We expect the data to have segmented data per month.  So, 
	-- we have taxpayer data for region 1, office 1, segment 1, region 1, office 1, segment 2, etc.
	-- all of this data should be ADDED to aget the total number of taxpayers in month n.

	select @query = 'select DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month,
		sum([numberOfRegisteredTaxPayers]) as numberOfRegisteredTaxPayers
	from [dbo].[TaxPayerStats]
	where [statsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])
	order by DATEPART(year, [statsDate]) asc, DATEPART(month, [statsDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	select DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month,
		[conceptID] as taxType,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue]
	where [concept] = 'Tax' 
		and [revenueDate] >= @startDate  
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), [conceptID]
	order by DATEPART(year, [revenueDate]) asc, DATEPART(month, [revenueDate]) asc, [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxCollectedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [revenueDate]) as year,
		[conceptID] as taxType,
		sum([amount]) as totalTaxCollected
	from [dbo].[Revenue]
	where [concept] = 'Tax' 
		and DATEPART(year, [revenueDate]) > @startYear  
		and DATEPART(year, [revenueDate]) <= @limitYear
	group by DATEPART(year, [revenueDate]), [conceptID]
	order by DATEPART(year, [revenueDate]) asc, [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPINumberOfReturnsFiledByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [reportDate]) as year,
		[taxType],
		sum([totalFilings]) as returnsFiled
	from [dbo].[FilingStats]
	where DATEPART(year, [reportDate]) > @startYear  
		and DATEPART(year, [reportDate]) <= @limitYear
	group by DATEPART(year, [reportDate]), [taxType]
	order by DATEPART(year, [reportDate]) asc, [taxType] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [reportDate]) as year, 
		[taxType],
		case when avg([totalFilingsRequiringPayment]) = 0 then 
			0.0 
		else 
			convert(decimal(18,3), avg([filingsPaidElectronically])) / convert(decimal(18,3), avg([totalFilingsRequiringPayment]))*100 
		end as ePaymentRatio
	from [dbo].[FilingStats]
	where DATEPART(year, [reportDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [reportDate]) <= '+convert(varchar, @limitYear)+'
			and '+@WhereClause +'
	group by DATEPART(year, [reportDate]), taxType
	order by DATEPART(year, [reportDate]) asc, taxType asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIEPaymentsByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [reportDate]) as year, 
		[taxType],
		case when avg([totalFilingsRequiringPayment]) = 0 then 
			0.0 
		else 
			convert(decimal(18,3), avg([filingsPaidElectronically])) / convert(decimal(18,3), avg([totalFilingsRequiringPayment]))*100 
		end as ePaymentRatio
	from [dbo].[FilingStats]
	where DATEPART(year, [reportDate]) > @startYear  
			and DATEPART(year, [reportDate]) <= @limitYear
	group by DATEPART(year, [reportDate]), taxType
	order by DATEPART(year, [reportDate]) asc, taxType asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [reportDate]) as year, 
		[taxType],
		case when avg([totalFilings]) = 0 then 
			0.0 
		else 
			convert(decimal(18,3), avg([eFilings])) / convert(decimal(18,3), avg([totalFilings]))*100 
		end as eFilingRatio
	from [dbo].[FilingStats]
	where DATEPART(year, [reportDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [reportDate]) <= '+convert(varchar, @limitYear)+'
			and '+@WhereClause +'
	group by DATEPART(year, [reportDate]), taxType
	order by DATEPART(year, [reportDate]) asc, taxType asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIEfilingsByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [reportDate]) as year, 
		[taxType],
		case when avg([totalFilings]) = 0 then 
			0.0 
		else 
			convert(decimal(18,3), avg([eFilings])) / convert(decimal(18,3), avg([totalFilings]))*100 
		end as eFilingRatio
	from [dbo].[FilingStats]
	where DATEPART(year, [reportDate]) > @startYear  
			and DATEPART(year, [reportDate]) <= @limitYear
	group by DATEPART(year, [reportDate]), taxType
	order by DATEPART(year, [reportDate]) asc, taxType asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [reportDate]) as year, 
		[taxType],
		case when avg([expectedFilers]) = 0 then 
			0.0 
		else 
			convert(decimal(18,3), avg([totalFilings])) / convert(decimal(18,3), avg([expectedFilers]))*100 
		end as filersRatio
	from [dbo].[FilingStats]
	where DATEPART(year, [reportDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [reportDate]) <= '+convert(varchar, @limitYear)+'
			and '+@WhereClause +'
	group by DATEPART(year, [reportDate]), taxType
	order by DATEPART(year, [reportDate]) asc, taxType asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxPayersFilingByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [reportDate]) as year, 
		[taxType],
		case when avg([expectedFilers]) = 0 then 
			0.0 
		else 
			convert(decimal(18,3), avg([totalFilings])) / convert(decimal(18,3), avg([expectedFilers]))*100 
		end as filersRatio
	from [dbo].[FilingStats]
	where DATEPART(year, [reportDate]) > @startYear  
			and DATEPART(year, [reportDate]) <= @limitYear
	group by DATEPART(year, [reportDate]), taxType
	order by DATEPART(year, [reportDate]) asc, taxType asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [reportDate]) as year, 
		[taxType],
		case when avg([expectedFilers]) = 0 then 
			0.0 
		else 
			convert(decimal(18,3), avg([stopFilers])) / convert(decimal(18,3), avg([expectedFilers]))*100 
		end as stopFilerRatio
	from [dbo].[FilingStats]
	where DATEPART(year, [reportDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [reportDate]) <= '+convert(varchar, @limitYear)+'
			and '+@WhereClause +'
	group by DATEPART(year, [reportDate]), taxType
	order by DATEPART(year, [reportDate]) asc, taxType asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIStopFilersByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [reportDate]) as year, 
		[taxType],
		case when avg([expectedFilers]) = 0 then 
			0.0 
		else 
			convert(decimal(18,3), avg([stopFilers])) / convert(decimal(18,3), avg([expectedFilers]))*100 
		end as stopFilerRatio
	from [dbo].[FilingStats]
	where DATEPART(year, [reportDate]) > @startYear  
			and DATEPART(year, [reportDate]) <= @limitYear
	group by DATEPART(year, [reportDate]), taxType
	order by DATEPART(year, [reportDate]) asc, taxType asc

END
GO

/****** Object:  StoredProcedure [dbo].[FILING_ReportFilingData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 16 2020
-- =============================================
CREATE PROCEDURE [dbo].[FILING_ReportFilingData]
	 @reportDate date
	,@taxType nvarchar(200)
    ,@segmentID nvarchar(10)
    ,@expectedFilers int
    ,@totalFilings int
    ,@stopFilers int
    ,@eFilings int
	,@totalFilingsRequiringPayment int
    ,@filingsPaidElectronically int
    ,@lateFilers int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[FilingStats]
           ([reportDate]
           ,[taxType]
           ,[segmentID]
           ,[expectedFilers]
           ,[totalFilings]
           ,[stopFilers]
           ,[eFilings]
           ,[totalFilingsRequiringPayment]
           ,[filingsPaidElectronically]
           ,[lateFilers])
     VALUES
           (@reportDate
           ,@taxType
           ,@segmentID
           ,@expectedFilers
           ,@totalFilings
           ,@stopFilers
           ,@eFilings
           ,@totalFilingsRequiringPayment
           ,@filingsPaidElectronically
           ,@lateFilers)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)

	select DATEPART(year, [statsDate]) as year, DATEPART(month, [statsDate]) as month, s.segmentName,
		case when sum([numberOfActiveTaxPayers]) > 0 then 
			sum(convert(decimal(18,3), [numberOfTaxPayersFiled])) / convert(decimal(18,3), sum([numberOfActiveTaxPayers])) * 100.00 else 0.00 end as  percentTaxPayersFiling
	from [dbo].[TaxPayerStats] as ts
	join [dbo].[Segment] as s on ts.segmentID = s.segmentID
	where [statsDate] >= @startdate
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate]), s.segmentName
	order by DATEPART(year, [statsDate]) asc, DATEPART(month, [statsDate]) asc, s.segmentName
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIPercentActiveTaxpayersFilingYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [statsDate]) as year, s.segmentName,
		case when sum([numberOfActiveTaxPayers]) > 0 then 
			sum(convert(decimal(18,3), [numberOfTaxPayersFiled])) / convert(decimal(18,3), sum([numberOfActiveTaxPayers])) * 100.00 else 0.00 end as  percentTaxPayersFiling
	from [dbo].[TaxPayerStats] as ts
	join [dbo].[Segment] as s on ts.segmentID = s.segmentID
	where DATEPART(year, [statsDate]) > @startYear  
		and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]), s.segmentName
	order by DATEPART(year, [statsDate]) asc, s.segmentName
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 15 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPINewRegistrantsMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	declare @registeredTaxpayers as table(year int, month int, numberOfRegisteredTaxpayers int, sequenceNumber int)

	insert into @registeredTaxpayers
	select DATEPART(year, [statsDate]), 
		DATEPART(month, [statsDate]), 
		sum([numberOfRegisteredTaxPayers]), 
		DATEPART(year, [statsDate])*12+DATEPART(month, [statsDate])
	from [dbo].[TaxPayerStats]
	where [statsDate] >= @startDate and 
		-- Don't show this month
		not (DATEPART(month, [statsDate]) = DATEPART(month, getdate()) and DATEPART(year, [statsDate]) = DATEPART(year, getdate()))
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])

	select r1.year, r1.month, 
		convert(decimal(18,3), r1.numberOfRegisteredTaxpayers - r2.numberOfRegisteredTaxpayers) / 
		Convert(decimal(18,3), r2.numberOfRegisteredTaxpayers) * 100.00 as increaseInRegisteredTaxPayers
	from @registeredTaxpayers as r1
	join @registeredTaxpayers as r2 on r2.sequenceNumber = r1.sequenceNumber-1 
	order by r1.year asc, r1.month asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 15 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPINewRegistrantsMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		DATEPART(month, dateadd(month, -1*(@NumberOfMonths +1), GETDATE())),
		1)  

	declare @registeredTaxpayers as table(year int, month int, numberOfRegisteredTaxpayers int, sequenceNumber int)

	declare @query as nvarchar(MAX)
	select @query = 'select DATEPART(year, [statsDate]), 
		DATEPART(month, [statsDate]), 
		sum([numberOfRegisteredTaxPayers]), 
		DATEPART(year, [statsDate])*12+DATEPART(month, [statsDate])
	from [dbo].[TaxPayerStats]
	where [statsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and
		not (DATEPART(month, [statsDate]) = DATEPART(month, getdate()) and DATEPART(year, [statsDate]) = DATEPART(year, getdate()))
		and '+@WhereClause +' 
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])'

	insert into @registeredTaxpayers
	execute(@query)

	select r1.year, r1.month, 
		convert(decimal(18,3), r1.numberOfRegisteredTaxpayers - r2.numberOfRegisteredTaxpayers) / 
		Convert(decimal(18,3), r2.numberOfRegisteredTaxpayers) * 100.00 as increaseInRegisteredTaxPayers
	from @registeredTaxpayers as r1
	join @registeredTaxpayers as r2 on r2.sequenceNumber = r1.sequenceNumber-1 
	order by r1.year asc, r1.month asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[KPI_DATA_KPINewRegistrantsYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears - 1

	declare @registeredTaxpayers as table(year int, numberOfRegisteredTaxpayers int)

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [statsDate]), sum([numberOfRegisteredTaxPayers]) 
	from [dbo].[TaxPayerStats]
	where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc'

	insert into @registeredTaxpayers
	execute(@query)

	select r1.year, convert(decimal(18,3), r1.numberOfRegisteredTaxpayers - r2.numberOfRegisteredTaxpayers) / 
		Convert(decimal(18,3), r2.numberOfRegisteredTaxpayers) * 100.00 as increaseInRegisteredTaxPayers
	from @registeredTaxpayers as r1
	join @registeredTaxpayers as r2 on r2.year = r1.year-1
	order by r1.year asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPINewRegistrantsYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 15 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPINewRegistrantsYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears - 1

	declare @registeredTaxpayers as table(year int, numberOfRegisteredTaxpayers int)

	insert into @registeredTaxpayers
	select DATEPART(year, [statsDate]), sum([numberOfRegisteredTaxPayers]) 
	from [dbo].[TaxPayerStats]
	where DATEPART(year, [statsDate]) > @startYear  
		and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc

	select r1.year, convert(decimal(18,3), r1.numberOfRegisteredTaxpayers - r2.numberOfRegisteredTaxpayers) / 
		Convert(decimal(18,3), r2.numberOfRegisteredTaxpayers) * 100.00 as increaseInRegisteredTaxPayers
	from @registeredTaxpayers as r1
	join @registeredTaxpayers as r2 on r2.year = r1.year-1
	order by r1.year asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPISatisfactionSurveysYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears + 1

	declare @startDate date
	set @startDate = convert(varchar, @startYear)+'-1-1'
	declare @endDate date
	set @endDate = convert(varchar, @LimitYear)+'-12-31'

	declare @monthsTable as table(year int, month int)
	insert into @monthsTable
	SELECT   year(DATEADD(MONTH, x.number, @StartDate)), Month(DATEADD(MONTH, x.number, @StartDate)) AS MonthId
	FROM    master.dbo.spt_values x		
	WHERE   x.type = 'P'          
	AND     x.number <= DATEDIFF(MONTH, @StartDate, @EndDate)  

	declare @surveyAreas as table(surveyArea nvarchar(200))
	insert into @surveyAreas
	select distinct [surveyArea]
	from [dbo].[SatisfactionSurveys]
	where [surveyItemReportDate] >= @startDate
		and [surveyItemReportDate] <= @endDate

	declare @partialResults as table(year int, month int, surveyArea nvarchar(200), averageScore decimal(18,3))

	declare @year int, @month int, @surveyArea nvarchar(200)  

	DECLARE date_cursor CURSOR FOR   
	SELECT year, month  
	FROM @monthsTable
	ORDER BY year asc, month asc  
  
	OPEN date_cursor  
  
	FETCH NEXT FROM date_cursor   
	INTO @year, @month  
  
	WHILE @@FETCH_STATUS = 0  
	BEGIN  

		DECLARE area_cursor CURSOR FOR   
		SELECT surveyArea 
		FROM @surveyAreas
		order by surveyArea
  
		OPEN area_cursor  
		FETCH NEXT FROM area_cursor INTO @surveyArea  
  
		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			if(DATEFROMPARTS(@year, @month, 1) <= getdate()) begin
				
				-- Only add those where the dates are before today.  We are not going to return future months with zeros

				declare @score as decimal(18,3)

				select @score = avg([surveyAreaScore])
				from [dbo].[SatisfactionSurveys]
				where datepart(year, [surveyItemReportDate]) = @year and 
					datepart(month, [surveyItemReportDate]) = @month and
					[surveyArea] = @surveyArea

				if(@score is null)
					set @score = 0

				insert into @partialResults values (@year, @month, @surveyArea, @score)
			end 

			FETCH NEXT FROM area_cursor INTO @surveyArea  
        END  
		CLOSE area_cursor  
		DEALLOCATE area_cursor  

		FETCH NEXT FROM date_cursor   
		INTO @year, @month  
	END   
	CLOSE date_cursor;  
	DEALLOCATE date_cursor;  

	select year, surveyArea, avg(averageScore) as averageScore
	from @partialResults
	group by year, surveyArea
	order by year, surveyArea

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPISatisfactionSurveysMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date
	set @startDate = DATEADD(month, -1*@NumberOfMonths, getdate())
	set @startDate = DATEFROMPARTS(datepart(year, @startDate), datepart(month, @startDate), 1)

	declare @endDate as date
	set @endDate = DATEFROMPARTS(datepart(year, getdate()), datepart(month, getdate()), 1)

	set @endDate = DATEADD(month, -1, @endDate)

	declare @monthsTable as table(year int, month int)
	insert into @monthsTable
	SELECT   year(DATEADD(MONTH, x.number, @StartDate)), Month(DATEADD(MONTH, x.number, @StartDate)) AS MonthId
	FROM    master.dbo.spt_values x		
	WHERE   x.type = 'P'          
	AND     x.number <= DATEDIFF(MONTH, @StartDate, @EndDate)  

	declare @query as nvarchar(MAX)

	declare @surveyAreas as table(surveyArea nvarchar(200))

	select @query = 'select distinct [surveyArea]
	from [dbo].[SatisfactionSurveys]
	where [surveyItemReportDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
		and [surveyItemReportDate] <= ''' + convert(varchar,@endDate, 126) + ''' 
		and '+@WhereClause 

	insert into @surveyAreas
	execute(@query)

	declare @partialResults as table(year int, month int, surveyArea nvarchar(200), averageScore decimal(18,3))

	declare @year int, @month int, @surveyArea nvarchar(200)  

	DECLARE date_cursor CURSOR FOR   
	SELECT year, month  
	FROM @monthsTable
	ORDER BY year asc, month asc  
  
	OPEN date_cursor  
  
	FETCH NEXT FROM date_cursor   
	INTO @year, @month  
  
	WHILE @@FETCH_STATUS = 0  
	BEGIN  

		DECLARE area_cursor CURSOR FOR   
		SELECT surveyArea 
		FROM @surveyAreas
		order by surveyArea
  
		OPEN area_cursor  
		FETCH NEXT FROM area_cursor INTO @surveyArea  
  
		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			if(DATEFROMPARTS(@year, @month, 1) <= getdate()) begin
				
				-- Only add those where the dates are before today.  We are not going to return future months with zeros

				declare @score as decimal(18,3)
				declare @scoreTable as table(score decimal(18,3))

				set @query = 'select avg([surveyAreaScore])
				from [dbo].[SatisfactionSurveys]
				where datepart(year, [surveyItemReportDate]) = '+Convert(varchar, @year)+' and 
					datepart(month, [surveyItemReportDate]) = '+convert(varchar, @month)+' and
					[surveyArea] = '''+@surveyArea+''' and '+@WhereClause 

				insert into @scoreTable
				execute(@query)

				select @score = MAX(score) from @scoreTable

				if(@score is null)
					set @score = 0

				insert into @partialResults values (@year, @month, @surveyArea, @score)

				delete from @scoreTable
			end 

			FETCH NEXT FROM area_cursor INTO @surveyArea  
        END  
		CLOSE area_cursor  
		DEALLOCATE area_cursor  

		FETCH NEXT FROM date_cursor   
		INTO @year, @month  
	END   
	CLOSE date_cursor;  
	DEALLOCATE date_cursor;  

	select year, month, surveyArea, avg(averageScore) as averageScore
	from @partialResults
	group by year, month, surveyArea
	order by year, month, surveyArea
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPISatisfactionSurveysYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears + 1

	declare @startDate date
	set @startDate = convert(varchar, @startYear)+'-1-1'
	declare @endDate date
	set @endDate = convert(varchar, @LimitYear)+'-12-31'

	declare @monthsTable as table(year int, month int)
	insert into @monthsTable
	SELECT   year(DATEADD(MONTH, x.number, @StartDate)), Month(DATEADD(MONTH, x.number, @StartDate)) AS MonthId
	FROM    master.dbo.spt_values x		
	WHERE   x.type = 'P'          
	AND     x.number <= DATEDIFF(MONTH, @StartDate, @EndDate)  

	declare @surveyAreas as table(surveyArea nvarchar(200))

	declare @query as nvarchar(MAX)

	select @query = 'select distinct [surveyArea]
	from [dbo].[SatisfactionSurveys]
	where [surveyItemReportDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
		and [surveyItemReportDate] <= ''' + convert(varchar,@endDate, 126) + ''' 
		and '+@WhereClause 

	insert into @surveyAreas
	execute(@query)

	declare @partialResults as table(year int, month int, surveyArea nvarchar(200), averageScore decimal(18,3))

	declare @year int, @month int, @surveyArea nvarchar(200)  

	DECLARE date_cursor CURSOR FOR   
	SELECT year, month  
	FROM @monthsTable
	ORDER BY year asc, month asc  
  
	OPEN date_cursor  
  
	FETCH NEXT FROM date_cursor   
	INTO @year, @month  
  
	WHILE @@FETCH_STATUS = 0  
	BEGIN  

		DECLARE area_cursor CURSOR FOR   
		SELECT surveyArea 
		FROM @surveyAreas
		order by surveyArea
  
		OPEN area_cursor  
		FETCH NEXT FROM area_cursor INTO @surveyArea  
  
		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			if(DATEFROMPARTS(@year, @month, 1) <= getdate()) begin
				
				-- Only add those where the dates are before today.  We are not going to return future months with zeros

				declare @score as decimal(18,3)
				declare @scoreTable as table(score decimal(18,3))
				
				set @query = 'select avg([surveyAreaScore])
				from [dbo].[SatisfactionSurveys]
				where datepart(year, [surveyItemReportDate]) = '+Convert(varchar, @year)+' and 
					datepart(month, [surveyItemReportDate]) = '+convert(varchar, @month)+' and
					[surveyArea] = '''+@surveyArea+''' and '+@WhereClause 

				insert into @scoreTable
				execute(@query)

				select @score = MAX(score) from @scoreTable

				if(@score is null)
					set @score = 0

				insert into @partialResults values (@year, @month, @surveyArea, @score)

				delete from @scoreTable
			end 

			FETCH NEXT FROM area_cursor INTO @surveyArea  
        END  
		CLOSE area_cursor  
		DEALLOCATE area_cursor  

		FETCH NEXT FROM date_cursor   
		INTO @year, @month  
	END   
	CLOSE date_cursor;  
	DEALLOCATE date_cursor;  

	select year, surveyArea, avg(averageScore) as averageScore
	from @partialResults
	group by year, surveyArea
	order by year, surveyArea

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPISatisfactionSurveysMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPISatisfactionSurveysMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date
	set @startDate = DATEADD(month, -1*@NumberOfMonths, getdate())
	set @startDate = DATEFROMPARTS(datepart(year, @startDate), datepart(month, @startDate), 1)

	declare @endDate as date
	set @endDate = DATEFROMPARTS(datepart(year, getdate()), datepart(month, getdate()), 1)

	set @endDate = DATEADD(month, -1, @endDate)

	declare @monthsTable as table(year int, month int)
	insert into @monthsTable
	SELECT   year(DATEADD(MONTH, x.number, @StartDate)), Month(DATEADD(MONTH, x.number, @StartDate)) AS MonthId
	FROM    master.dbo.spt_values x		
	WHERE   x.type = 'P'          
	AND     x.number <= DATEDIFF(MONTH, @StartDate, @EndDate)  

	declare @surveyAreas as table(surveyArea nvarchar(200))
	insert into @surveyAreas
	select distinct [surveyArea]
	from [dbo].[SatisfactionSurveys]
	where [surveyItemReportDate] >= @startDate
		and [surveyItemReportDate] <= @endDate

	declare @partialResults as table(year int, month int, surveyArea nvarchar(200), averageScore decimal(18,3))

	declare @year int, @month int, @surveyArea nvarchar(200)  

	DECLARE date_cursor CURSOR FOR   
	SELECT year, month  
	FROM @monthsTable
	ORDER BY year asc, month asc  
  
	OPEN date_cursor  
  
	FETCH NEXT FROM date_cursor   
	INTO @year, @month  
  
	WHILE @@FETCH_STATUS = 0  
	BEGIN  

		DECLARE area_cursor CURSOR FOR   
		SELECT surveyArea 
		FROM @surveyAreas
		order by surveyArea
  
		OPEN area_cursor  
		FETCH NEXT FROM area_cursor INTO @surveyArea  
  
		WHILE @@FETCH_STATUS = 0  
		BEGIN  
			if(DATEFROMPARTS(@year, @month, 1) <= getdate()) begin
				
				-- Only add those where the dates are before today.  We are not going to return future months with zeros

				declare @score as decimal(18,3)

				select @score = avg([surveyAreaScore])
				from [dbo].[SatisfactionSurveys]
				where datepart(year, [surveyItemReportDate]) = @year and 
					datepart(month, [surveyItemReportDate]) = @month and
					[surveyArea] = @surveyArea

				if(@score is null)
					set @score = 0

				insert into @partialResults values (@year, @month, @surveyArea, @score)
			end 

			FETCH NEXT FROM area_cursor INTO @surveyArea  
        END  
		CLOSE area_cursor  
		DEALLOCATE area_cursor  

		FETCH NEXT FROM date_cursor   
		INTO @year, @month  
	END   
	CLOSE date_cursor;  
	DEALLOCATE date_cursor;  

	select year, month, surveyArea, avg(averageScore) as averageScore
	from @partialResults
	group by year, month, surveyArea
	order by year, month, surveyArea

END
GO

/****** Object:  StoredProcedure [dbo].[SATISFACTION_SURVEYS_ReportSurveyData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 14 2020
-- =============================================
CREATE PROCEDURE [dbo].[SATISFACTION_SURVEYS_ReportSurveyData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@surveyArea nvarchar(200),
	@surveyAreaScore decimal(18,3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	INSERT INTO [dbo].[SatisfactionSurveys]
           ([surveyItemReportDate]
           ,[regionID]
           ,[officeID]
           ,[surveyArea]
           ,[surveyAreaScore])
    VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@surveyArea
           ,@surveyAreaScore)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCourseMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 13 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfCourseMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [trainingCoursesReportDate]) as year, 
		DATEPART(month, [trainingCoursesReportDate]) as month,	
		c.[courseArea],
		sum([numberOfCoursesOffered]) as coursesOffered
	from [dbo].[Courses] as c 
	join [dbo].[Region] as r on r.regionID = c.[regionID]
	where [trainingCoursesReportDate] >= ''' + convert(varchar,@startDate, 126) + '''
		and '+@WhereClause+' 
	group by DATEPART(year, [trainingCoursesReportDate]), DATEPART(month, [trainingCoursesReportDate]), c.[courseArea]
	order by DATEPART(year, [trainingCoursesReportDate]) asc, DATEPART(month, [trainingCoursesReportDate]) asc, c.[courseArea] asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCourseMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 13 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfCourseMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select DATEPART(year, [trainingCoursesReportDate]) as year, 
		DATEPART(month, [trainingCoursesReportDate]) as month,	
		c.[courseArea],
		sum([numberOfCoursesOffered]) as coursesOffered
	from [dbo].[Courses] as c 
	join [dbo].[Region] as r on r.regionID = c.[regionID]
	where [trainingCoursesReportDate] >= @startDate
	group by DATEPART(year, [trainingCoursesReportDate]), DATEPART(month, [trainingCoursesReportDate]), c.[courseArea]
	order by DATEPART(year, [trainingCoursesReportDate]) asc, DATEPART(month, [trainingCoursesReportDate]) asc, c.[courseArea] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCoursesYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 13 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfCoursesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,	
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [trainingCoursesReportDate]) as year, 	
		c.[courseArea],
		sum([numberOfCoursesOffered]) as coursesOffered
	from [dbo].[Courses] as c 
	join [dbo].[Region] as r on r.regionID = c.[regionID]
	where DATEPART(year, [trainingCoursesReportDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [trainingCoursesReportDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
	group by DATEPART(year, [trainingCoursesReportDate]), c.[courseArea]'

	execute(@query)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfCoursesYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 13 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfCoursesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	
	select DATEPART(year, [trainingCoursesReportDate]) as year, 	
		c.[courseArea],
		sum([numberOfCoursesOffered]) as coursesOffered
	from [dbo].[Courses] as c 
	join [dbo].[Region] as r on r.regionID = c.[regionID]
	where DATEPART(year, [trainingCoursesReportDate]) > @startYear 
		and DATEPART(year, [trainingCoursesReportDate]) <= @limitYear
	group by DATEPART(year, [trainingCoursesReportDate]), c.[courseArea]

END
GO

/****** Object:  StoredProcedure [dbo].[COURSES_ReportCouresData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 12 2020
-- =============================================
CREATE PROCEDURE [dbo].[COURSES_ReportCouresData]
	@dateReported date,
	@regionID nvarchar(10),
	@courseArea nvarchar(250),
	@numberOfCoursesOffered int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	INSERT INTO [dbo].[Courses]
           ([trainingCoursesReportDate]
           ,[regionID]
           ,[courseArea]
           ,[numberOfCoursesOffered])
     VALUES
           (@dateReported
           ,@regionID
           ,@courseArea
           ,@numberOfCoursesOffered)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICoverageElectronicTaxServicesYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPICoverageElectronicTaxServicesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	SELECT DATEPART(year, [servicesItemReportDate]) as year, 
		case when sum([numberOfServicesProvidedTaxpayers]) = 0 then 0 else 
			Convert(decimal(18,3), sum([numberOfElectronicServicesProvidedTaxpayers]))
			/Convert(decimal(18,3), sum([numberOfServicesProvidedTaxpayers]))*100.00 end as electronicCoverge
	FROM [dbo].[TaxPayerServices]
	where DATEPART(year, [servicesItemReportDate]) > @startYear  
		and DATEPART(year, [servicesItemReportDate]) <= @limitYear
	group by DATEPART(year, [servicesItemReportDate])
	order by DATEPART(year, [servicesItemReportDate]) asc
END
GO

/****** Object:  StoredProcedure [dbo].[SERVICES_ReportTaxPayerServices]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 12 2020
-- =============================================
CREATE PROCEDURE [dbo].[SERVICES_ReportTaxPayerServices]
	@dateReported date,
	@numberOfServicesProvidedTaxpayers int,
	@numberOfElectronicServicesProvidedTaxpayers int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[TaxPayerServices]
           ([servicesItemReportDate]
           ,[numberOfServicesProvidedTaxpayers]
           ,[numberOfElectronicServicesProvidedTaxpayers])
     VALUES
           (@dateReported
           ,@numberOfServicesProvidedTaxpayers
           ,@numberOfElectronicServicesProvidedTaxpayers)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldYearlyByRegion]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditYieldYearlyByRegion]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [statsDate]) as year, 
		[regionName],
		case when sum([numberOfAuditsPerformed]) > 0 then
			convert(decimal(18,3),sum([numberOfAuditsResultedInAdditionalAssessments])) /
			convert(decimal(18,3), sum([numberOfAuditsPerformed])) * 100 
		else 0.00 end as auditYield
	from [dbo].[AuditStats] as s
	join [dbo].[Region] as r on r.[regionID] = s.[regionID]
		where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]), [regionName]
	order by DATEPART(year, [statsDate]) asc, [regionName] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldMonthlyByRegion]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditYieldMonthlyByRegion]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	select DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month, 
		[regionName],
		case when sum([numberOfAuditsPerformed]) > 0 then
			convert(decimal(18,3),sum([numberOfAuditsResultedInAdditionalAssessments])) /
			convert(decimal(18,3), sum([numberOfAuditsPerformed])) * 100 
		else 0.00 end as auditYield
	from [dbo].[AuditStats] as s
	join [dbo].[Region] as r on r.[regionID] = s.[regionID]
	where [statsDate] >= @startDate  
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate]), [regionName]
	order by  DATEPART(year, [statsDate]) asc, DATEPART(month, [statsDate]) asc, [regionName]

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditYieldMonthlyByFilter]
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month, 
		case when sum([numberOfAuditsPerformed]) > 0 then
			convert(decimal(18,3),sum([numberOfAuditsResultedInAdditionalAssessments])) /
			convert(decimal(18,3), sum([numberOfAuditsPerformed])) * 100 
		else 0.00 end as auditYield
		from [dbo].[AuditStats]
		where [statsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
			and '+@WhereClause +' 
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])
	order by  DATEPART(year, [statsDate]) asc, DATEPART(month, [statsDate]) asc' 

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditYieldMonthly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	select DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month, 
		case when sum([numberOfAuditsPerformed]) > 0 then
			convert(decimal(18,3),sum([numberOfAuditsResultedInAdditionalAssessments])) /
			convert(decimal(18,3), sum([numberOfAuditsPerformed])) * 100 
		else 0.00 end as auditYield
		from [dbo].[AuditStats]
		where [statsDate] >= @startDate  
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])
	order by  DATEPART(year, [statsDate]) asc, DATEPART(month, [statsDate]) asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Jun 11 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [hrItemReportDate]) as year, 
		DATEPART(month, [hrItemReportDate]) as month,
		case when sum([totalHumanResources]) > 0 then
			Convert(decimal(18,3), sum([totalHumanResourcesInTaxCollection] +
				[totalHumanResourcesInTaxPayerServices] +
				[totalHumanResourcesInTaxEnforcement] +
				[totalHumanResourcesInAudits] +
				[totalHumanResourcesInAppeals])) / Convert(decimal(18,3), sum([totalHumanResources]))*100 else 0.00 end as allocation
	from [dbo].[HumanResources]
	where [hrItemReportDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
			and '+@WhereClause +' 
	group by DATEPART(year, [hrItemReportDate]), DATEPART(month, [hrItemReportDate])
	order by DATEPART(year, [hrItemReportDate]) asc, DATEPART(month, [hrItemReportDate]) asc'

	execute(@query)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Jun 11 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AllocationStaffKeyFunctionsMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select DATEPART(year, [hrItemReportDate]) as year, 
		DATEPART(month, [hrItemReportDate]) as month,
		case when sum([totalHumanResources]) > 0 then
			Convert(decimal(18,3), sum([totalHumanResourcesInTaxCollection] +
				[totalHumanResourcesInTaxPayerServices] +
				[totalHumanResourcesInTaxEnforcement] +
				[totalHumanResourcesInAudits] +
				[totalHumanResourcesInAppeals])) / Convert(decimal(18,3), sum([totalHumanResources]))*100 else 0.00 end as allocation
	from [dbo].[HumanResources]
	where [hrItemReportDate] >= @startdate
	group by DATEPART(year, [hrItemReportDate]), DATEPART(month, [hrItemReportDate])
	order by DATEPART(year, [hrItemReportDate]) asc, DATEPART(month, [hrItemReportDate]) asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Jun 11 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [hrItemReportDate]) as year, 
		case when sum([totalHumanResources]) > 0 then
			Convert(decimal(18,3), sum([totalHumanResourcesInTaxCollection] +
				[totalHumanResourcesInTaxPayerServices] +
				[totalHumanResourcesInTaxEnforcement] +
				[totalHumanResourcesInAudits] +
				[totalHumanResourcesInAppeals])) / Convert(decimal(18,3), sum([totalHumanResources]))*100 else 0.00 end as allocation
	from [dbo].[HumanResources]
	where DATEPART(year, [hrItemReportDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [hrItemReportDate])<= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
	group by DATEPART(year, [hrItemReportDate])'

	execute(@query)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Jun 11 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [hrItemReportDate]) as year, 
		case when sum([totalHumanResources]) > 0 then
			Convert(decimal(18,3), sum([totalHumanResourcesInTaxCollection] +
				[totalHumanResourcesInTaxPayerServices] +
				[totalHumanResourcesInTaxEnforcement] +
				[totalHumanResourcesInAudits] +
				[totalHumanResourcesInAppeals])) / Convert(decimal(18,3), sum([totalHumanResources]))*100 else 0.00 end as allocation
	from [dbo].[HumanResources]
	where DATEPART(year, [hrItemReportDate]) > @startYear 
		and DATEPART(year, [hrItemReportDate]) <= @limitYear
	group by DATEPART(year, [hrItemReportDate])

END
GO

/****** Object:  StoredProcedure [dbo].[HR_ReportHRData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 11 2020
-- =============================================
CREATE PROCEDURE [dbo].[HR_ReportHRData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@totalHumanResources int,
	@totalHumanResourcesInTaxCollection int,
	@totalHumanResourcesInTaxPayerServices int,
	@totalHumanResourcesInTaxEnforcement int,
	@totalHumanResourcesInAudits int,
	@totalHumanResourcesInAppeals int,
	@numberPositionsFilledCompetitiveProcess int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	INSERT INTO [dbo].[HumanResources]
           ([hrItemReportDate]
		   ,[regionID]
           ,[officeID]
           ,[totalHumanResources]
           ,[totalHumanResourcesInTaxCollection]
           ,[totalHumanResourcesInTaxPayerServices]
           ,[totalHumanResourcesInTaxEnforcement]
           ,[totalHumanResourcesInAudits]
           ,[totalHumanResourcesInAppeals]
           ,[numberPositionsFilledCompetitiveProcess])
     VALUES
           (@dateReported
		   ,@regionID
           ,@officeID
           ,@totalHumanResources
           ,@totalHumanResourcesInTaxCollection
           ,@totalHumanResourcesInTaxPayerServices
           ,@totalHumanResourcesInTaxEnforcement
           ,@totalHumanResourcesInAudits
           ,@totalHumanResourcesInAppeals
           ,@numberPositionsFilledCompetitiveProcess)
END
GO

/****** Object:  StoredProcedure [dbo].[UTILITY_GetTableSchema]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[UTILITY_GetTableSchema]
	@tableName varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select td.[No] as 'No'
		, td.ColumnName as 'Column'
		, td.DataType as 'Type'
		, td.MaxLength as 'Max. Length'
		, td.IsNullable as 'Nullable'
		, sep.Description as 'Description'
	from  
		(
			SELECT
				ORDINAL_POSITION as 'No', 
				COLUMN_NAME 'ColumnName', 
				DATA_TYPE as 'DataType', 
				CHARACTER_MAXIMUM_LENGTH as 'MaxLength',
				IS_NULLABLE 'IsNullable'		
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = @tableName
		) as td 
	join 
		(
 			select 
				 sc.name 'ColumnName'
				,sep.value [Description]
				,sc.column_id
				,sc.system_type_id
			from sys.tables st
			inner join sys.columns sc on st.object_id = sc.object_id
			left join sys.extended_properties sep on st.object_id = sep.major_id
												 and sc.column_id = sep.minor_id
												 and sep.name = 'MS_Description'
			where st.name = @tableName
		) as sep on td.ColumnName = sep.ColumnName
	order by [No]
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPICostCollectionRatioMonthly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @expenseTable as table([year] int, [month] int, expense money)
	declare @revenueTable as table([year] int, [month] int, revenue money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, 
		DATEPART(month, [revenueDate]) as month,
		sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startDate
	group by DATEPART(year, [revenueDate]),  DATEPART(month, [revenueDate])
	
	insert into @expenseTable
	SELECT DATEPART(year, [adminExpensesDate]) as year, 
		DATEPART(month, [adminExpensesDate]) as month, 
		sum([currentExpense]) as expense
	FROM  [dbo].[TaxAdministrationExpenses]
	where [adminExpensesDate] >= @startDate
	group by DATEPART(year, [adminExpensesDate]),  DATEPART(month, [adminExpensesDate])

	select r.year, r.month, 
		case when revenue > 0 then expense/revenue*100 else 0.00 end as costofcollectionratio
	from @revenueTable as r 
	join @expenseTable as f on f.year = r.year and f.month = r.month
	order by r.year asc, r.month asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPICostCollectionRatioMonthlyByFilter]
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @query as nvarchar(MAX)

	declare @expenseTable as table([year] int, [month] int, expense money)
	declare @revenueTable as table([year] int, [month] int, revenue money)

	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, 
		DATEPART(month, [revenueDate]) as month,
		sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= ''' + convert(varchar,@startDate, 126) + '''
		and '+@WhereClause+' 
	group by DATEPART(year, [revenueDate]),  DATEPART(month, [revenueDate])'
	
	insert into @revenueTable
	execute(@query)

	select @query = 'SELECT DATEPART(year, [adminExpensesDate]) as year, 
		DATEPART(month, [adminExpensesDate]) as month, 
		sum([currentExpense]) as expense
	FROM  [dbo].[TaxAdministrationExpenses]
	where [adminExpensesDate] >= ''' + convert(varchar,@startDate, 126) + '''
		and '+@WhereClause+' 
	group by DATEPART(year, [adminExpensesDate]),  DATEPART(month, [adminExpensesDate])' 

    print @query
	insert into @expenseTable
	execute(@query)

	select r.year, r.month,
		case when revenue > 0 then expense/revenue*100 else 0.00 end as costofcollectionratio
	from @revenueTable as r 
	join @expenseTable as f on f.year = r.year and f.month = r.month
	order by r.year asc, r.month asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPICostCollectionRatioYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	declare @expenseTable as table([year] int, expense money)
	declare @revenueTable as table([year] int, revenue money)

	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
	group by DATEPART(year, [revenueDate])'
	
	insert into @revenueTable
	execute(@query)

	select @query = 'SELECT DATEPART(year, [adminExpensesDate]) as year, sum([currentExpense]) as expense
	FROM  [dbo].[TaxAdministrationExpenses]
	where DATEPART(year, [adminExpensesDate]) > '+convert(varchar, @startYear) + '
		 and DATEPART(year, [adminExpensesDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
	group by DATEPART(year, [adminExpensesDate])' 

	insert into @expenseTable
	execute(@query)

	select r.year, 
		case when revenue > 0 then expense/revenue*100 else 0.00 end as costofcollectionratio
	from @revenueTable as r 
	join @expenseTable as f on f.year = r.year
	order by r.year asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPICostCollectionRatioYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPICostCollectionRatioYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @expenseTable as table([year] int, expense money)
	declare @revenueTable as table([year] int, revenue money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear  
		and DATEPART(year, [revenueDate]) <= @limitYear
	group by DATEPART(year, [revenueDate])
	
	insert into @expenseTable
	SELECT DATEPART(year, [adminExpensesDate]) as year, sum([currentExpense]) as expense
	FROM  [dbo].[TaxAdministrationExpenses]
	where DATEPART(year, [adminExpensesDate]) > @startYear
		 and DATEPART(year, [adminExpensesDate]) <= @limitYear
	group by DATEPART(year, [adminExpensesDate])

	select r.year, 
		case when revenue > 0 then expense/revenue*100 else 0.00 end as costofcollectionratio
	from @revenueTable as r 
	join @expenseTable as f on f.year = r.year
	order by r.year asc
END
GO

/****** Object:  StoredProcedure [dbo].[EXPENSES_ReportTaxAdministrationExpensesData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 8 2020
-- =============================================
CREATE PROCEDURE [dbo].[EXPENSES_ReportTaxAdministrationExpensesData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@currentExpense money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	INSERT INTO [dbo].[TaxAdministrationExpenses]
           ([adminExpensesDate]
           ,[regionID]
           ,[officeID]
           ,[currentExpense])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@currentExpense)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByTaxType]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByTaxType]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [appealsDate]) as year, 
		DATEPART(month, [appealsDate]) as month,
		case when sum(numberCasesResolved) = 0 then 0 else
		sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases,
		[conceptID] as taxType
	FROM [KPIDW].[dbo].[Appeals]
	where [appealsDate] >= @startDate
		and concept = 'Tax' and [conceptID] is not null
	group by DATEPART(year, [appealsDate]), DATEPART(month, [appealsDate]),  [conceptID]
	order by DATEPART(year, [appealsDate]) asc, DATEPART(month, [appealsDate]) asc,  [conceptID] asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByTaxType]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByTaxType]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, arrears money)

	SELECT DATEPART(year, [appealsDate]) as year, 
		case when sum(numberCasesResolved) = 0 then 0 else 
			sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases,
		[conceptID] as taxType
	FROM [KPIDW].[dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > @startYear  
		and DATEPART(year, [appealsDate]) <= @limitYear
		and concept = 'Tax' and [conceptID] is not null
	group by DATEPART(year, [appealsDate]), [conceptID]
	order by DATEPART(year, [appealsDate]) asc, conceptID asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 6 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,	
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, arrears money)

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [appealsDate]) as year, 
		case when sum(numberCasesResolved) = 0 then 0 else 
			sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases
	FROM [KPIDW].[dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + ' 
		and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
	group by DATEPART(year, [appealsDate])
	order by DATEPART(year, [appealsDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [appealsDate]) as year, 
		DATEPART(month, [appealsDate]) as month,
		case when sum(numberCasesResolved) = 0 then 0 else
		sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases
	FROM [KPIDW].[dbo].[Appeals]
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
	and '+@WhereClause+'  
	group by DATEPART(year, [appealsDate]), DATEPART(month, [appealsDate])
	order by DATEPART(year, [appealsDate]) asc, DATEPART(month, [appealsDate]) asc'

	execute(@query)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [appealsDate]) as year, 
		DATEPART(month, [appealsDate]) as month,
		case when sum(numberCasesResolved) = 0 then 0 else
		sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases
	FROM [KPIDW].[dbo].[Appeals]
	where [appealsDate] >= @startDate
	group by DATEPART(year, [appealsDate]), DATEPART(month, [appealsDate])
	order by DATEPART(year, [appealsDate]) asc, DATEPART(month, [appealsDate]) asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIAverageDaysResolveTaxAppealCasesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, arrears money)

	SELECT DATEPART(year, [appealsDate]) as year, 
		case when sum(numberCasesResolved) = 0 then 0 else 
			sum([averageDaysToResolveCases]*[numberCasesResolved])/sum(numberCasesResolved) end as averageDaysToResolveTaxAppealCases
	FROM [KPIDW].[dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > @startYear  
		and DATEPART(year, [appealsDate]) <= @limitYear
	group by DATEPART(year, [appealsDate])
	order by DATEPART(year, [appealsDate]) asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditYieldYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [statsDate]) as year, convert(decimal(18,3),sum([numberOfAuditsResultedInAdditionalAssessments])) /
		convert(decimal(18,3), sum([numberOfAuditsPerformed])) * 100 as auditYield
		from [dbo].[AuditStats]
		where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+'
			and '+@WhereClause +'
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditYieldYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditYieldYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [statsDate]) as year, convert(decimal(18,3),sum([numberOfAuditsResultedInAdditionalAssessments])) /
		convert(decimal(18,3), sum([numberOfAuditsPerformed])) * 100 as auditYield
		from [dbo].[AuditStats]
		where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 25 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditCoverageMonthlyByFilter]
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @query as nvarchar(MAX)

	select @query = 'select a.year, a.month, convert(decimal(18,5), numberOfAuditsPerformed)/convert(decimal(18,5), numberOfRegisteredTaxPayers)*100.00 as auditcoverage
	from (
		select DATEPART(year, [statsDate]) as year, 
			DATEPART(month, [statsDate]) as month, 
			sum([numberOfRegisteredTaxPayers]) as numberOfRegisteredTaxPayers
		from [dbo].[TaxPayerStats]
		where [statsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
			and '+@WhereClause +' 
		group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate]) 
	) as a
	join 
	(
		select DATEPART(year, [statsDate]) as year,
			DATEPART(month, [statsDate]) as month, 
			sum([numberOfAuditsPerformed]) as numberOfAuditsPerformed
		from [dbo].[AuditStats]
		where [statsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
			and '+@WhereClause +' 
		group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])
	) as b on a.year = b.year and a.month = b.month
	order by a.year asc, a.month asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditCoverageMonthly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	select a.year, a.month, convert(decimal(18,5), numberOfAuditsPerformed)/convert(decimal(18,5), numberOfRegisteredTaxPayers)*100.00 as auditcoverage
	from (
		select DATEPART(year, [statsDate]) as year, 
			DATEPART(month, [statsDate]) as month, 
			sum([numberOfRegisteredTaxPayers]) as numberOfRegisteredTaxPayers
		from [dbo].[TaxPayerStats]
		where [statsDate] >= @startDate
		group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate]) 
	) as a
	join 
	(
		select DATEPART(year, [statsDate]) as year,
			DATEPART(month, [statsDate]) as month, 
			sum([numberOfAuditsPerformed]) as numberOfAuditsPerformed
		from [dbo].[AuditStats]
		where [statsDate] >= @startDate
		group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])
	) as b on a.year = b.year and a.month = b.month
	order by a.year asc, a.month asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditCoverageYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select a.year, convert(decimal(18,5), 
		numberOfAuditsPerformed)/convert(decimal(18,5), numberOfRegisteredTaxPayers)*100.00 as auditcoverage
	from (
		select DATEPART(year, [statsDate]) as year, sum([numberOfRegisteredTaxPayers]) as numberOfRegisteredTaxPayers
		from [dbo].[TaxPayerStats]
		where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
		group by DATEPART(year, [statsDate])
	) as a
	join 
	(
		select DATEPART(year, [statsDate]) as year, sum([numberOfAuditsPerformed]) as numberOfAuditsPerformed
		from [dbo].[AuditStats]
		where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
		group by DATEPART(year, [statsDate])
	) as b on a.year = b.year
	order by a.year asc'

	execute(@query)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_AuditCoverageYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_AuditCoverageYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select a.year, convert(decimal(18,5), numberOfAuditsPerformed)/convert(decimal(18,5), numberOfRegisteredTaxPayers)*100.00 as auditcoverage
	from (
		select DATEPART(year, [statsDate]) as year, sum([numberOfRegisteredTaxPayers]) as numberOfRegisteredTaxPayers
		from [dbo].[TaxPayerStats]
		where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
		group by DATEPART(year, [statsDate])
	) as a
	join 
	(
		select DATEPART(year, [statsDate]) as year, sum([numberOfAuditsPerformed]) as numberOfAuditsPerformed
		from [dbo].[AuditStats]
		where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
		group by DATEPART(year, [statsDate])
	) as b on a.year = b.year
	order by a.year asc

END
GO

/****** Object:  StoredProcedure [dbo].[AUDIT_ReportAuditData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
CREATE PROCEDURE [dbo].[AUDIT_ReportAuditData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@numberOfAuditsPerformed int,
	@numberOfAuditsResultedInAdditionalAssessments int,
	@totalAdditionalAssessment money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@numberOfAuditsPerformed is null)
		set @numberOfAuditsPerformed = 0

	if(@numberOfAuditsResultedInAdditionalAssessments is null)
		set @numberOfAuditsResultedInAdditionalAssessments = 0

	if(@totalAdditionalAssessment is null)
		set @totalAdditionalAssessment = 0

	INSERT INTO [dbo].[AuditStats]
           ([statsDate]
           ,[numberOfAuditsPerformed]
           ,[numberOfAuditsResultedInAdditionalAssessments]
           ,[totalAdditionalAssessment]
           ,[regionID]
           ,[officeID]
           ,[segmentID])
	VALUES
           (@dateReported
           ,@numberOfAuditsPerformed
           ,@numberOfAuditsResultedInAdditionalAssessments
           ,@totalAdditionalAssessment
           ,@regionID
           ,@officeID
           ,@segmentID)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select year, 
		case when sumTaxes = 0 then 0.00 else sumVoluntary/sumTaxes*100.00 end as voluntaryCompliance
	from  (
		select DATEPART(year, [revenueDate]) as year,
			sum([totalRevenueFromTaxes]) as sumTaxes,
			sum([totalRevenueFromVoluntaryCompliance]) as sumVoluntary
		from [dbo].[RevenueSummaryByMonth]
		where DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
		group by DATEPART(year, [revenueDate])
	) AS X
	order by year ASC'

	execute(@query)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TimelyPaymentOfTaxesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select year, 
		case when sumTaxes = 0 then 0.00 else sumVoluntary/sumTaxes*100.00 end as voluntaryCompliance
	from  (
		select DATEPART(year, [revenueDate]) as year,
			sum([totalRevenueFromTaxes]) as sumTaxes,
			sum([totalRevenueFromVoluntaryCompliance]) as sumVoluntary
		from [dbo].[RevenueSummaryByMonth]
		where DATEPART(year, [revenueDate]) > @startYear  
			and DATEPART(year, [revenueDate]) <= @limitYear
		group by DATEPART(year, [revenueDate])
	) AS X
	order by year ASC

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 25 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthlyByFilter]
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @query as nvarchar(MAX)

	select @query = 'select year, 
	month,
	case when sumTaxes = 0 then 0.00 else sumVoluntary/sumTaxes*100.00 end as voluntaryCompliance
	from  (
		select DATEPART(year, [revenueDate]) as year,
			DATEPART(month, [revenueDate]) as month,
			sum([totalRevenueFromTaxes]) as sumTaxes,
			sum([totalRevenueFromVoluntaryCompliance]) as sumVoluntary
		from [dbo].[RevenueSummaryByMonth]
		where [revenueDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
			and '+@WhereClause +' 
		group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate])
	) AS X
	order by year ASC, month asc' 

	execute(@query)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 25 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TimelyPaymentOfTaxesMonthly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

		select year, 
		month,
		case when sumTaxes = 0 then 0.00 else sumVoluntary/sumTaxes*100.00 end as voluntaryCompliance
	from  (
		select DATEPART(year, [revenueDate]) as year,
			DATEPART(month, [revenueDate]) as month,
			sum([totalRevenueFromTaxes]) as sumTaxes,
			sum([totalRevenueFromVoluntaryCompliance]) as sumVoluntary
		from [dbo].[RevenueSummaryByMonth]
		where [revenueDate] >= @startDate
		group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate])
	) AS X
	order by year ASC, month asc

END
GO

/****** Object:  StoredProcedure [dbo].[REVENUE_ReportSummaryRevenueData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
CREATE PROCEDURE [dbo].[REVENUE_ReportSummaryRevenueData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@totalRevenue money,
	@totalRevenuePaidInTime money,
	@totalRevenueFromArrears money,
	@totalRevenueFromFines money,
	@totalRevenueFromAudits money,
	@totalRevenueFromEnforcementCollection money,
	@totalRevenueFromVoluntaryCompliance money,
	@totalRevenueFromTaxes money,
	@totalRevenueFromLitigation money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@totalRevenue is null)
		set @totalRevenue = 0

	if(@totalRevenuePaidInTime is null)
		set @totalRevenuePaidInTime = 0

	if(@totalRevenueFromArrears is null)
		set @totalRevenueFromArrears = 0

	if(@totalRevenueFromFines is null)
		set @totalRevenueFromFines = 0

	if(@totalRevenueFromAudits is null)
		set @totalRevenueFromAudits = 0

	if(@totalRevenueFromVoluntaryCompliance is null)
		set @totalRevenueFromVoluntaryCompliance = 0

	if(@totalRevenueFromEnforcementCollection is null)
		set @totalRevenueFromEnforcementCollection = 0

	if(@totalRevenueFromVoluntaryCompliance is null)
		set @totalRevenueFromVoluntaryCompliance = 0

	if(@totalRevenueFromTaxes is null)
		set @totalRevenueFromTaxes = 0

	if(@totalRevenueFromLitigation is null)
		set @totalRevenueFromLitigation = 0

	INSERT INTO [dbo].[RevenueSummaryByMonth]
           ([revenueDate]
		   ,[totalRevenue]
           ,[totalRevenuePaidInTIme]
           ,[totalRevenueFromArrears]
           ,[totalRevenueFromFines]
           ,[totalRevenueFromAudits]
           ,[totalRevenueFromEnforcementCollection]
           ,[totalRevenueFromVoluntaryCompliance]
           ,[totalRevenueFromTaxes]
           ,[totalRevenueFromLitigation]
           ,[regionID]
           ,[officeID]
           ,[segmentID])
	VALUES
           (@dateReported
		   ,@totalRevenue
           ,@totalRevenuePaidInTime
           ,@totalRevenueFromArrears
           ,@totalRevenueFromFines
           ,@totalRevenueFromAudits
           ,@totalRevenueFromEnforcementCollection
           ,@totalRevenueFromVoluntaryCompliance
           ,@totalRevenueFromTaxes
           ,@totalRevenueFromLitigation
           ,@regionID
           ,@officeID
           ,@segmentID)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthlyByFilter]
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	-- We expect the data to have segmented data per month.  So, 
	-- we have taxpayer data for region 1, office 1, segment 1, region 1, office 1, segment 2, etc.
	-- all of this data should be ADDED to aget the total number of taxpayers in month n.
	--
	-- Yearly data is computed by adding all monthly data

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [statsDate]) as year, DATEPART(month, [statsDate]) as month,
		convert(decimal(18,3), sum([numberTaxPayersFiledInTime])) / 
		convert(decimal(18,3), sum([numberOfTaxPayersFiled])) * 100.00 as percentageFiledInTime
	FROM [dbo].[TaxPayerStats]
	where [statsDate] >= '''+convert(varchar, @startDate, 126)+ ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])
	order by DATEPART(year, [statsDate]) asc, DATEPART(month, [statsDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeMonthly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	-- We expect the data to have segmented data per month.  So, 
	-- we have taxpayer data for region 1, office 1, segment 1, region 1, office 1, segment 2, etc.
	-- all of this data should be ADDED to aget the total number of taxpayers in month n.
	--
	-- Yearly data is computed by adding all monthly data

	SELECT DATEPART(year, [statsDate]) as year, DATEPART(month, [statsDate]) as month,
		convert(decimal(18,3), sum([numberTaxPayersFiledInTime])) / 
		convert(decimal(18,3), sum([numberOfTaxPayersFiled])) * 100.00 as percentageFiledInTime
	FROM [dbo].[TaxPayerStats]
	where [statsDate] >= @startDate
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])
	order by DATEPART(year, [statsDate]) asc, DATEPART(month, [statsDate]) asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_PercentageTaxPayersFiledInTimeByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	-- We expect the data to have segmented data per month.  So, 
	-- we have taxpayer data for region 1, office 1, segment 1, region 1, office 1, segment 2, etc.
	-- all of this data should be ADDED to aget the total number of taxpayers in month n.
	--
	-- Yearly data is computed by adding all monthly data

	SELECT @query = 'SELECT DATEPART(year, [statsDate]) as year, 
		convert(decimal(18,3), sum([numberTaxPayersFiledInTime])) / 
		convert(decimal(18,3), sum([numberOfTaxPayersFiled])) * 100.00 as percentageFiledInTime
	FROM [dbo].[TaxPayerStats]
	where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear)+ '
		and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+ 
		' and '+@WhereClause +' 
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_PercentageTaxPayersFiledInTime]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_PercentageTaxPayersFiledInTime]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	-- We expect the data to have segmented data per month.  So, 
	-- we have taxpayer data for region 1, office 1, segment 1, region 1, office 1, segment 2, etc.
	-- all of this data should be ADDED to aget the total number of taxpayers in month n.
	--
	-- Yearly data is computed by adding all monthly data

	SELECT DATEPART(year, [statsDate]) as year, 
		convert(decimal(18,3), sum([numberTaxPayersFiledInTime])) / 
		convert(decimal(18,3), sum([numberOfTaxPayersFiled])) * 100.00 as percentageFiledInTime
	FROM [dbo].[TaxPayerStats]
	where DATEPART(year, [statsDate]) > @startYear 
		and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,	
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	-- We expect the data to have segmented data per month.  So, 
	-- we have taxpayer data for region 1, office 1, segment 1, region 1, office 1, segment 2, etc.
	-- all of this data should be ADDED to aget the total number of taxpayers in month n.
	--
	-- After that, when we are computing the yearly data, we should choose for year 1 the last data point
	-- we have for that year.  So if month 12 of year 2020 was X, then we will use X as the data for year 2020
	
	declare @taxpayerstats as table(year int, month int, numberOfRegisteredTaxPayers int)

	select @query = 'select DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month,
		sum([numberOfRegisteredTaxPayers]) as numberOfRegisteredTaxPayers
	from [dbo].[TaxPayerStats]
	where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])'

	insert into @taxpayerstats
	execute(@query)

	-- Use only the last month for every year
	select year, numberOfRegisteredTaxPayers from (
		select row_number() over(partition by year order by year desc, month desc) as nrow, 
			year, 
			month, 
			numberOfRegisteredTaxPayers as numberOfRegisteredTaxPayers
		from @taxpayerstats
	) as x where x.nrow = 1
	order by year

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	-- We expect the data to have segmented data per month.  So, 
	-- we have taxpayer data for region 1, office 1, segment 1, region 1, office 1, segment 2, etc.
	-- all of this data should be ADDED to aget the total number of taxpayers in month n.
	--
	-- After that, when we are computing the yearly data, we should choose for year 1 the last data point
	-- we have for that year.  So if month 12 of year 2020 was X, then we will use X as the data for year 2020
	
	declare @taxpayerstats as table(year int, month int, numberOfRegisteredTaxPayers int)

	insert into @taxpayerstats
	select DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month,
		sum([numberOfRegisteredTaxPayers]) as numberOfRegisteredTaxPayers
	from [dbo].[TaxPayerStats]
	where DATEPART(year, [statsDate]) > @startYear 
		and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])

	-- Use only the last month for every year
	select year, numberOfRegisteredTaxPayers from (
		select row_number() over(partition by year order by year desc, month desc) as nrow, 
			year, 
			month, 
			numberOfRegisteredTaxPayers as numberOfRegisteredTaxPayers
		from @taxpayerstats
	) as x where x.nrow = 1
	order by year

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberOfRegisteredTaxPayersMontly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	-- We expect the data to have segmented data per month.  So, 
	-- we have taxpayer data for region 1, office 1, segment 1, region 1, office 1, segment 2, etc.
	-- all of this data should be ADDED to aget the total number of taxpayers in month n.

	select DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month,
		sum([numberOfRegisteredTaxPayers]) as numberOfRegisteredTaxPayers
	from [dbo].[TaxPayerStats]
	where [statsDate] >= @startDate
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])
	order by DATEPART(year, [statsDate]) asc, DATEPART(month, [statsDate]) asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_CorporateIncomeTaxProductivityByRegion]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CorporateIncomeTaxProductivityByRegion]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @citTaxName nvarchar(200)
	select top(1) @citTaxName = [corporateIncomeTaxname]
		from [dbo].[Settings]

	declare @citTaxRate as table([year] int, taxrate decimal(18,3))
	declare @gdp as table([year] int, regionID nvarchar(10), [nominalGdpAtDate] money)
	declare @citRevenue as table([year] int, regionID nvarchar(10), amount decimal(18,3))

	insert into @citTaxRate
	SELECT DATEPART(year, [taxRateDate]) as year, AVG([taxRate])
	FROM [dbo].[TaxRates] 
	where [taxName] = @citTaxName
		and DATEPART(year, [taxRateDate]) > @startYear 
		and DATEPART(year, [taxRateDate]) <= @limitYear
	group by DATEPART(year, [taxRateDate]) 
	
	insert into @gdp
	select DATEPART(year, [statsDate]) as year, [regionID], AVG([nominalGdpAtDate])
	FROM  [dbo].[RegionStats]
	where DATEPART(year, [statsDate]) > @startYear 
		and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]), regionID

	insert into @citRevenue
	select DATEPART(year, [revenueDate]) as year, regionID, SUM([amount])
	from [dbo].[Revenue]
	where DATEPART(year, [revenueDate]) > @startYear 
		and DATEPART(year, [revenueDate]) <= @limitYear
		and [concept] = 'tax'
		and [conceptID] = @citTaxName
	group by DATEPART(year, [revenueDate]), regionID
	
	select c.year, re.regionName, c.amount / g.nominalGdpAtDate / t.taxrate as CorporateIncomeTaxProductivity
	from @citRevenue as c 
	join @gdp as g on c.year = g.year and c.regionID = g.regionID
	join @citTaxRate as t on t.year = c.year
	join [dbo].[Region] as re on c.regionID = re.regionID
	order by c.year asc, regionName
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_CorporateIncomeTaxProductivity]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CorporateIncomeTaxProductivity]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @citTaxName nvarchar(200)
	select top(1) @citTaxName = [corporateIncomeTaxname]
		from [dbo].[Settings]

	declare @citTaxRate as table([year] int, taxrate decimal(18,3))
	declare @gdp as table([year] int, [nominalGdpAtDate] money)
	declare @citRevenue as table([year] int, amount decimal(18,3))

	insert into @citTaxRate
	SELECT DATEPART(year, [taxRateDate]) as year, AVG([taxRate])
	FROM [dbo].[TaxRates] 
	where [taxName] = @citTaxName
		and DATEPART(year, [taxRateDate]) > @startYear 
		and DATEPART(year, [taxRateDate]) <= @limitYear
	group by DATEPART(year, [taxRateDate]) 
	
	insert into @gdp
	select DATEPART(year, [statsDate]) as year, AVG([nominalGdpAtDate])
	FROM  [dbo].[CountryStats]
	where DATEPART(year, [statsDate]) > @startYear 
		and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]) 

	insert into @citRevenue
	select DATEPART(year, [revenueDate]) as year, SUM([amount])
	from [dbo].[Revenue]
	where DATEPART(year, [revenueDate]) > @startYear 
		and DATEPART(year, [revenueDate]) <= @limitYear
		and [concept] = 'tax'
		and [conceptID] = @citTaxName
	group by DATEPART(year, [revenueDate]) 
	
	select c.year, c.amount / g.nominalGdpAtDate / t.taxrate as CorporateIncomeTaxProductivity
	from @citTaxRate as t
	join @gdp as g on t.year = g.year
	join @citRevenue as c on t.year = c.year
	order by c.year asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_VATGrossComplianceRatio]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_VATGrossComplianceRatio]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @vatTaxName nvarchar(200)
	select top(1) @vatTaxName = [valueAddedTaxName]
		from [dbo].[Settings]

	declare @vatTaxRate as table([year] int, taxrate decimal(18,3))
	declare @gdp as table([year] int, [nominalGdpAtDate] money)
	declare @vatRevenue as table([year] int, amount decimal(18,3))
	declare @imports as table([year] int, [totalImports] decimal(18,3))

	insert into @vatTaxRate
	SELECT DATEPART(year, [taxRateDate]) as year, AVG([taxRate])
	FROM [dbo].[TaxRates] 
	where [taxName] = @vatTaxName
		and DATEPART(year, [taxRateDate]) > @startYear 
		and DATEPART(year, [taxRateDate]) <= @limitYear
	group by DATEPART(year, [taxRateDate]) 
	
	insert into @gdp
	select DATEPART(year, [statsDate]) as year, AVG([nominalGdpAtDate])
	FROM  [dbo].[CountryStats]
	where DATEPART(year, [statsDate]) > @startYear 
		and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]) 

	insert into @vatRevenue
	select DATEPART(year, [revenueDate]) as year, SUM([amount])
	from [dbo].[Revenue]
	where DATEPART(year, [revenueDate]) > @startYear 
		and DATEPART(year, [revenueDate]) <= @limitYear
		and [concept] = 'tax' and [conceptID] = @vatTaxName
	group by DATEPART(year, [revenueDate]) 
	
	insert into @imports
	select DATEPART(year, [importDate]) as year, SUM([totalImports])
	from [dbo].[Imports]
	where DATEPART(year, [importDate]) > @startYear 
		and DATEPART(year, [importDate]) <= @limitYear
	group by DATEPART(year, [importDate]) 

	select i.year, v.amount / (g.nominalGdpAtDate - i.totalImports)/t.taxrate as VATGrossComplianceRatio
	from @vatTaxRate as t
	join @gdp as g on t.year = g.year
	join @vatRevenue as v on t.year = v.year
	join @imports as i on t.year = i.year
	order by i.year asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_VATTaxToGDPRatioGlobal]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_VATTaxToGDPRatioGlobal]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @vatTaxName nvarchar(200)
	select top(1) @vatTaxName = [valueAddedTaxName]
		from [dbo].[Settings]

	declare @revenueResults as table(yearStats int, revenue money)
	declare @gdpResults as table(yearStats int, gdp money)

	insert into @revenueResults
	select DATEPART(year, [revenueDate]), sum([amount]) as amount
	from [dbo].[Revenue]
	where DATEPART(year, [revenueDate]) > @startYear and DATEPART(year, [revenueDate]) <= @limitYear
		and concept = 'tax' and conceptID=@vatTaxName
	group by DATEPART(year, [revenueDate]) 
	order by DATEPART(year, [revenueDate]) asc

	insert into @gdpResults
	select  DATEPART(year, [statsDate]), avg([nominalGdpAtDate])
	from [dbo].[CountryStats]
	where DATEPART(year, [statsDate]) > @startYear and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]) 
	order by DATEPART(year, [statsDate]) asc

	select r.yearStats as [year], 
		case when gdp > 0 then revenue/gdp*100 else 0.00 end as TaxToGPDRatio
	from @revenueResults as r
	join  @gdpResults as g on r.yearStats = g.yearStats
	order by [year] asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_VATTaxToGDPRatioByRegion]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_VATTaxToGDPRatioByRegion]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @vatTaxName nvarchar(200)
	select top(1) @vatTaxName = [valueAddedTaxName]
		from [dbo].[Settings]

	declare @revenueRegionResults as table(yearStats int, regionID nvarchar(10), revenue money)
	declare @gdpRegionResults as table(yearStats int, regionID nvarchar(10), gdp money)

	insert into @revenueRegionResults
	select DATEPART(year, [revenueDate]), regionID, sum([amount]) as amount
	from [dbo].[Revenue]
	where DATEPART(year, [revenueDate]) > @startYear and DATEPART(year, [revenueDate]) <= @limitYear
		and concept = 'tax' and conceptID=@vatTaxName
	group by regionID, DATEPART(year, [revenueDate])
	order by DATEPART(year, [revenueDate]) asc

	insert into @gdpRegionResults
	select  DATEPART(year, [statsDate]), regionID, avg([nominalGdpAtDate])
	from [dbo].[RegionStats]
	where DATEPART(year, [statsDate]) > @startYear and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]), [regionID]
	order by DATEPART(year, [statsDate]) asc

	select r.yearStats as [year], reg.regionName, 
		case when gdp > 0 then revenue/gdp*100 else 0.00 end as TaxToGPDRatio
	from @revenueRegionResults as r
	join  @gdpRegionResults as g on r.yearStats = g.yearStats and r.regionID = g.regionID
	join [dbo].[Region] as reg on r.regionID = reg.regionID
	order by r.regionID asc, [year] asc
END
GO

/****** Object:  StoredProcedure [dbo].[TAXRATE_ReportTaxRateData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 21 2020
-- =============================================
CREATE PROCEDURE [dbo].[TAXRATE_ReportTaxRateData]
	@reportDate date,
	@taxName nvarchar(200),
	@taxRate decimal(18,3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[TaxRates]
           ([taxRateDate]
           ,[taxName]
           ,[taxRate])
	VALUES
           (@reportDate
           ,@taxName
           ,@taxRate)
END
GO

/****** Object:  StoredProcedure [dbo].[IMPORTS_ReportImportData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: IMPORTS_ReportImportData
-- =============================================
CREATE PROCEDURE [dbo].[IMPORTS_ReportImportData]
	@reportDate date,
	@totalImports money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[Imports]
           ([importDate]
           ,[totalImports])
     VALUES
           (@reportDate
           ,@totalImports)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 14 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByTaxPayerSegment]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	SELECT DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month, 
		sum([numberOfTaxPayers70PercentRevenue]) as numberOfTaxpayers,
		segmentName
	FROM [dbo].[TaxPayerStats] as t
	join [dbo].[Segment] as s on s.segmentID = t.segmentID
	where [statsDate] >= @startDate  
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate]), segmentName
	order by DATEPART(year, [statsDate]) asc, DATEPART(month, [statsDate]) asc, segmentName asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 14 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthlyByFilter]
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @query as nvarchar(MAX)

	SELECT @query = 'SELECT DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month, 
		sum([numberOfTaxPayers70PercentRevenue]) as numberOfTaxpayers
	FROM [dbo].[TaxPayerStats]
	where [statsDate] >= '''+convert(varchar, @startDate, 126)+ ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])  
	order by DATEPART(year, [statsDate]) asc, DATEPART(month, [statsDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 14 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesMonthly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	SELECT DATEPART(year, [statsDate]) as year, 
		DATEPART(month, [statsDate]) as month, 
		sum([numberOfTaxPayers70PercentRevenue]) as numberOfTaxpayers
	FROM [dbo].[TaxPayerStats]
	where [statsDate] >= @startDate  
	group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])  
	order by DATEPART(year, [statsDate]) asc, DATEPART(month, [statsDate]) asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxToGDPRatioGlobal]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxToGDPRatioGlobal]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueResults as table(yearStats int, revenue money)
	declare @gdpResults as table(yearStats int, gdp money)

	insert into @revenueResults
	select DATEPART(year, [revenueDate]), sum([amount]) as amount
	from [dbo].[Revenue]
	where DATEPART(year, [revenueDate]) > @startYear and DATEPART(year, [revenueDate]) <= @limitYear
	group by DATEPART(year, [revenueDate]) 
	order by DATEPART(year, [revenueDate]) asc

	insert into @gdpResults
	select  DATEPART(year, [statsDate]), avg([nominalGdpAtDate])
	from [dbo].[CountryStats]
	where DATEPART(year, [statsDate]) > @startYear and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]) 
	order by DATEPART(year, [statsDate]) asc

	select r.yearStats as [year], 
		case when gdp > 0 then revenue/gdp*100 else 0.00 end as TaxToGPDRatio
	from @revenueResults as r
	join  @gdpResults as g on r.yearStats = g.yearStats
	order by r.yearStats asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPITaxToGDPRatioByRegion]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPITaxToGDPRatioByRegion]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueRegionResults as table(yearStats int, regionID nvarchar(10), revenue money)
	declare @gdpRegionResults as table(yearStats int, regionID nvarchar(10), gdp money)

	insert into @revenueRegionResults
	select DATEPART(year, [revenueDate]), regionID, sum([amount]) as amount
	from [dbo].[Revenue]
	where DATEPART(year, [revenueDate]) > @startYear and DATEPART(year, [revenueDate]) <= @limitYear
	group by regionID, DATEPART(year, [revenueDate])
	order by DATEPART(year, [revenueDate]) asc

	insert into @gdpRegionResults
	select  DATEPART(year, [statsDate]), regionID, avg([nominalGdpAtDate])
	from [dbo].[RegionStats]
	where DATEPART(year, [statsDate]) > @startYear and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]), [regionID]
	order by DATEPART(year, [statsDate]) asc

	select r.yearStats as [year], reg.regionName, 
		case when gdp > 0 then revenue/gdp*100 else 0.00 end as TaxToGPDRatio
	from @revenueRegionResults as r
	join  @gdpRegionResults as g on r.yearStats = g.yearStats and r.regionID = g.regionID
	join [dbo].[Region] as reg on r.regionID = reg.regionID
	order by r.regionID asc, r.yearStats asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, taxtype nvarchar(200), revenue money)
	declare @arrearsTable as table([year] int, taxtype nvarchar(200), arrears money)

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, conceptID as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear)+ '  
		and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+ ' 
		and [concept] = ''Tax'' and [conceptID] is not null and '+@WhereClause+' 
	group by DATEPART(year, [revenueDate]), conceptID' 
	
	insert into @revenueTable
	execute(@query)

	select @query = 'SELECT DATEPART(year, [arrearDate]) as year, conceptID as taxtype, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear)+ ' 
		 and DATEPART(year, [arrearDate]) <= '+convert(varchar, @limitYear)+ '
		 and [concept] = ''Arrears'' and [conceptID] is not null and '+@WhereClause+'
	group by DATEPART(year, [arrearDate]), conceptID'

	insert into @arrearsTable
	execute(@query)

	select r.year, r.taxtype, 
		case when revenue > 0 then arrears/revenue*100 else 0.00 end as arrearsRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.taxtype = r.taxtype
	order by year asc

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, taxtype nvarchar(200), revenue money)
	declare @arrearsTable as table([year] int, taxtype nvarchar(200), arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, conceptID as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear  
		and DATEPART(year, [revenueDate]) <= @limitYear
		and [concept] = 'Tax' and [conceptID] is not null 
	group by DATEPART(year, [revenueDate]), conceptID 
	
	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, conceptID as taxtype, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > @startYear
		 and DATEPART(year, [arrearDate]) <= @limitYear
		 and [concept] = 'Arrears' and [conceptID] is not null 
	group by DATEPART(year, [arrearDate]), conceptID

	select r.year, r.taxtype, 
		case when revenue > 0 then arrears/revenue*100 else 0.00 end as arrearsRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.taxtype = r.taxtype
	order by year asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthlyByFilter]
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @revenueTable as table([year] int, [month] int, taxtype nvarchar(200), revenue money)
	declare @arrearsTable as table([year] int, [month] int, taxtype nvarchar(200), arrears money)

	declare @query as nvarchar(MAX)

	SELECT @query = 'SELECT DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month, conceptID as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= '''+convert(varchar, @startDate, 126)+ ''' 
		and [concept] = ''Tax'' and [conceptID] is not null 
		and '+@WhereClause +' 
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), conceptID'

	insert into @revenueTable
	execute(@query)

	SELECT @query = 'SELECT DATEPART(year, [arrearDate]) as year, DATEPART(month, [arrearDate]) as month, conceptID as taxtype, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where [arrearDate] >= '''+convert(varchar, @startDate, 126)+ '''  
		 and [concept] = ''Arrears'' and [conceptID] is not null and '+@WhereClause +' 
	group by DATEPART(year, [arrearDate]), DATEPART(month, [arrearDate]), conceptID'

	insert into @arrearsTable
	execute(@query)

	select r.year, r.month,  r.taxtype, 
		case when revenue > 0 then arrears/revenue*100 else 0.00 end as arrearsRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.month = r.month and f.taxtype = r.taxtype
	order by year asc, month asc, r.taxtype asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @revenueTable as table([year] int, [month] int, taxtype nvarchar(200), revenue money)
	declare @arrearsTable as table([year] int, [month] int, taxtype nvarchar(200), arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month, conceptID as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startDate and [concept] = 'Tax' and [conceptID] is not null
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), conceptID

	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, DATEPART(month, [arrearDate]) as month, conceptID as taxtype, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where [arrearDate] >= @startDate
		 and [concept] = 'Arrears' and [conceptID] is not null 
	group by DATEPART(year, [arrearDate]), DATEPART(month, [arrearDate]), conceptID

	select r.year, r.month, r.taxtype, 
		case when revenue > 0 then arrears/revenue*100 else 0.00 end as arrearsRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.month = r.month and f.taxtype = r.taxtype
	order by year asc, month asc, r.taxtype asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByOffice]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByOffice]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, [officeID] nvarchar(10), arrearsRecovery money)
	declare @arrearsTable as table([year] int, [officeID] nvarchar(10), arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, [officeID], sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear  
		and DATEPART(year, [revenueDate]) <= @limitYear
		and [concept] = 'Arrears Recovered'  
	group by DATEPART(year, [revenueDate]), [officeID]
	
	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, [officeID], sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > @startYear
		 and DATEPART(year, [arrearDate]) <= @limitYear
		 and [concept] = 'Arrears' 
	group by DATEPART(year, [arrearDate]), [officeID]

	select r.year, o.officeName, 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.officeID = r.officeID
	join [dbo].[Office] as o on f.officeID = o.officeID and r.officeID = o.officeID
	order by year asc, o.officeName asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	declare @revenueTable as table([year] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, arrears money)

	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
		and [concept] = ''Arrears Recovered''  
	group by DATEPART(year, [revenueDate])'

	insert into @revenueTable
	execute(@query)
	
	select @query = 'SELECT DATEPART(year, [arrearDate]) as year, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear) + '
		 and DATEPART(year, [arrearDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
		 and [concept] = ''Arrears'' 
	group by DATEPART(year, [arrearDate])'

	insert into @arrearsTable
	execute(@query)

	select r.year, 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year
	order by year asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear  
		and DATEPART(year, [revenueDate]) <= @limitYear
		and [concept] = 'Arrears Recovered'  
	group by DATEPART(year, [revenueDate])
	
	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > @startYear
		 and DATEPART(year, [arrearDate]) <= @limitYear
		 and [concept] = 'Arrears' 
	group by DATEPART(year, [arrearDate])

	select r.year, 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year
	order by year asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByFilter]
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @query as nvarchar(MAX)

	declare @revenueTable as table([year] int, [month] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, [month] int, arrears money)

	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, 
		DATEPART(month, [revenueDate]) as month, 
		sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where [revenueDate] >= ''' + convert(varchar,@startDate, 126) + '''
		and '+@WhereClause+' 
		and [concept] = ''Arrears Recovered''  
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate])'
	
	insert into @revenueTable
	execute(@query)

	select @query = 'SELECT DATEPART(year, [arrearDate]) as year, 
		DATEPART(month, [arrearDate]) as month, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where [arrearDate] >= ''' + convert(varchar,@startDate, 126) + '''
		and '+@WhereClause+' 
		 and [concept] = ''Arrears'' 
	group by DATEPART(year, [arrearDate]), DATEPART(month, [arrearDate])'

	insert into @arrearsTable
	execute(@query)

	select r.year, r.month, 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.month = r.month
	order by year asc, month asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @revenueTable as table([year] int, [month] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, [month] int, arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, 
		DATEPART(month, [revenueDate]) as month, 
		sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startDate
		and [concept] = 'Arrears Recovered'  
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate])
	
	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, 
		DATEPART(month, [arrearDate]) as month, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where [arrearDate] >= @startDate
		 and [concept] = 'Arrears' 
	group by DATEPART(year, [arrearDate]), DATEPART(month, [arrearDate])

	select r.year, r.month, 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.month = r.month
	order by year asc, month asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, revenue money)
	declare @finesTable as table([year] int, fines money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear and DATEPART(year, [revenueDate]) <= @limitYear
	group by DATEPART(year, [revenueDate])

	insert into @finesTable
	SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear and DATEPART(year, [revenueDate]) <= @limitYear
		and [concept] = 'Fines'
	group by DATEPART(year, [revenueDate])

	select r.[year], 
		case when revenue > 0 then fines/revenue*100 else 0.00 end as ratio
	from @revenueTable r 
	join @finesTable f on r.year = f.year
	order by r.year
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[KPI_DATA_KPIProportionOfFinesAndPenaltiesFromRevenueMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is N months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @revenueTable as table([year] int, [month] int, revenue money)
	declare @finesTable as table([year] int, [month] int, fines money)

	insert into @revenueTable
	SELECT year([revenueDate]) as year, month([revenueDate]) as month, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startdate
	group by year([revenueDate]), month([revenueDate])

	insert into @finesTable
	SELECT year([revenueDate]) as year, month([revenueDate]) as month, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startdate
		and [concept] = 'Fines'
	group by year([revenueDate]), month([revenueDate])

	select r.[year], r.[month], 
		case when revenue > 0 then fines/revenue*100 else 0.00 end as ratio
	from @revenueTable r 
	join @finesTable f on r.year = f.year and r.month = f.month
	order by r.year, r.month
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,	
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, revenue money)
	declare @forecastTable as table([year] int, forecast money)

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear) + ' 
		and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
	group by DATEPART(year, [revenueDate])'

	insert into @revenueTable
	execute(@query)

	select @query = 'SELECT DATEPART(year, [forecastDate]) as year, sum([amount]) as forecastOfRevenue
	FROM  [dbo].[RevenueForecast]
	where DATEPART(year, [forecastDate]) > '+convert(varchar, @startYear) + '
		 and DATEPART(year, [forecastDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
	group by DATEPART(year, [forecastDate])'

	insert into @forecastTable
	execute(@query)

	select r.year, 
		case when forecast > 0 then revenue/forecast*100 else 0.00 end as revenueForecastDeviation
	from @revenueTable as r 
	join @forecastTable as f on f.year = r.year
	order by year asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, revenue money)
	declare @forecastTable as table([year] int, forecast money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear and DATEPART(year, [revenueDate]) <= @limitYear
	group by DATEPART(year, [revenueDate])

	insert into @forecastTable
	SELECT DATEPART(year, [forecastDate]) as year, sum([amount]) as forecastOfRevenue
	FROM  [dbo].[RevenueForecast]
	where DATEPART(year, [forecastDate]) > @startYear and DATEPART(year, [forecastDate]) <= @limitYear
	group by DATEPART(year, [forecastDate])

	select r.year, 
		case when forecast > 0 then revenue/forecast*100 else 0.00 end as revenueForecastDeviation
	from @revenueTable as r 
	join @forecastTable as f on f.year = r.year
	order by year asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 30 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthlyByFilter]
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @revenueTable as table([year] int, [month] int, revenue money)
	declare @forecastTable as table([year] int, [month] int, forecast money)

	declare @query as nvarchar(MAX)
	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]), sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
	and '+@WhereClause+'  
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate])'

	insert into @revenueTable
	execute(@query)

	select @query = 'SELECT DATEPART(year, [forecastDate]) as year, DATEPART(month, [forecastDate]), sum([amount]) as forecastOfRevenue
	FROM  [dbo].[RevenueForecast]
	where [forecastDate] >= ''' + convert(varchar,@startDate, 126) + '''
	and '+@WhereClause+'  
	group by DATEPART(year, [forecastDate]), DATEPART(month, [forecastDate])'
	
	insert into @forecastTable
	execute(@query)

	select r.year, r.month, 
		case when forecast > 0 then revenue/forecast*100 else 0.00 end as revenueForecastDeviation
	from @revenueTable as r 
	join @forecastTable as f on f.year = r.year and f.month = r.month
	order by year asc, month asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 30 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_DeviationBetweenRevenueForecastedAndCollectedMonthly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @revenueTable as table([year] int, [month] int, revenue money)
	declare @forecastTable as table([year] int, [month] int, forecast money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]), sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startDate
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate])

	insert into @forecastTable
	SELECT DATEPART(year, [forecastDate]) as year, DATEPART(month, [forecastDate]), sum([amount]) as forecastOfRevenue
	FROM  [dbo].[RevenueForecast]
	where [forecastDate] >= @startDate 
	group by DATEPART(year, [forecastDate]), DATEPART(month, [forecastDate])

	select r.year, r.month,	
		case when forecast > 0 then revenue/forecast*100 else 0.00 end as revenueForecastDeviation
	from @revenueTable as r 
	join @forecastTable as f on f.year = r.year and f.month = r.month
	order by year asc, month asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByOffice]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByOffice]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @revenueTable as table([year] int, [month] int, [officeID] nvarchar(10), arrearsRecovery money)
	declare @arrearsTable as table([year] int, [month] int, [officeID] nvarchar(10), arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, 
		DATEPART(month, [revenueDate]) as month, 
		[officeID],
		sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startDate
		and [concept] = 'Arrears Recovered'  
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), [officeID] 
	
	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, 
		DATEPART(month, [arrearDate]) as month, 
		[officeID], 
		sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where [arrearDate] >= @startDate
		 and [concept] = 'Arrears' 
	group by DATEPART(year, [arrearDate]), DATEPART(month, [arrearDate]), [officeID]

	select r.year, r.month, o.[officeName], 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.month = r.month and r.officeID = f.officeID
	join [dbo].[Office] as o on f.officeID = o.officeID and r.officeID = o.officeID
	order by year asc, month asc, officeName asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByTaxPayerSegment]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 14 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByTaxPayerSegment]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select year, AVG(numberOfTaxpayers) as numberOfTaxpayers, segmentName from (
		SELECT DATEPART(year, [statsDate]) as year, 
			DATEPART(month, [statsDate]) as month,
			[segmentID],
			sum([numberOfTaxPayers70PercentRevenue]) as numberOfTaxpayers
		FROM [dbo].[TaxPayerStats]
		where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
		group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate]), segmentID
	) as x
	join [dbo].[Segment] as s on x.segmentID = s.segmentID
	group by year, segmentName
	order by year asc, segmentName asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 14 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @tempResults as table([year] int, [month] int, numberOfTaxpayers int)
	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [statsDate]) as year, 
			DATEPART(month, [statsDate]) as month, 
			sum([numberOfTaxPayers70PercentRevenue]) as numberOfTaxpayers
		FROM [dbo].[TaxPayerStats]
		where DATEPART(year, [statsDate]) > '+convert(varchar, @startYear)+ '
			and DATEPART(year, [statsDate]) <= '+convert(varchar, @limitYear)+ 
			' and '+@WhereClause +' 
		group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])'

	insert into @tempResults
	execute(@query)

	select [year], AVG(numberOfTaxpayers) as numberOfTaxpayers from (
		select [year], [month], numberOfTaxpayers
		from @tempResults
	) as x
	group by year
	order by year asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 14 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_TaxPayersAccount70PercentRevenuesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select year, AVG(numberOfTaxpayers) as numberOfTaxpayers from (
		SELECT DATEPART(year, [statsDate]) as year, 
			DATEPART(month, [statsDate]) as month, sum([numberOfTaxPayers70PercentRevenue]) as numberOfTaxpayers
		FROM [dbo].[TaxPayerStats]
		where DATEPART(year, [statsDate]) > @startYear  
			and DATEPART(year, [statsDate]) <= @limitYear
		group by DATEPART(year, [statsDate]), DATEPART(month, [statsDate])  
	) as x
	group by year
	order by year asc
END
GO

/****** Object:  StoredProcedure [dbo].[TAXPAYER_ReportTaxpayerData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 9 2020
-- =============================================
CREATE PROCEDURE [dbo].[TAXPAYER_ReportTaxpayerData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@numberOfRegisteredTaxpayers int,
	@numberOfActiveTaxPayers int,
    @numberOfTaxPayersFiled int,
    @numberTaxPayersFiledInTime int,
    @numberOfTaxPayers70PercentRevenue int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@numberOfRegisteredTaxpayers = 0)
		set @numberOfRegisteredTaxpayers = null

	if(@numberOfActiveTaxPayers = 0)
		set @numberOfActiveTaxPayers = null

	if(@numberOfTaxPayersFiled = 0)
		set @numberOfTaxPayersFiled = null

	if(@numberTaxPayersFiledInTime = 0)
		set @numberTaxPayersFiledInTime = null

	if(@numberOfTaxPayers70PercentRevenue = 0)
		set @numberOfTaxPayers70PercentRevenue = null

	INSERT INTO [dbo].[TaxPayerStats]
           ([statsDate]
           ,[numberOfRegisteredTaxPayers]
           ,[numberOfActiveTaxPayers]
           ,[numberOfTaxPayersFiled]
           ,[numberTaxPayersFiledInTime]
           ,[numberOfTaxPayers70PercentRevenue]
           ,[regionID]
           ,[officeID]
           ,[segmentID])
	VALUES
           (@dateReported
           ,@numberOfRegisteredTaxpayers
           ,@numberOfActiveTaxPayers
           ,@numberOfTaxPayersFiled
           ,@numberTaxPayersFiledInTime
           ,@numberOfTaxPayers70PercentRevenue
           ,@regionID
           ,@officeID
           ,@segmentID)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByFilter]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 29 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)
	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear) +'
	    and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+'
		and '+@WhereClause+'  
	group by DATEPART(year, [revenueDate])
	order by DATEPART(year, [revenueDate]) asc'

	execute(@query)
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByTaxType]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 2 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearlyByTaxType]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	SELECT DATEPART(year, [revenueDate]) as year, [conceptID] as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear and DATEPART(year, [revenueDate]) <= @limitYear
		and conceptID is not null and conceptID != ''
	group by DATEPART(year, [revenueDate]), conceptID 
	order by DATEPART(year, [revenueDate]) asc, conceptID  asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 29 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear and DATEPART(year, [revenueDate]) <= @limitYear
	group by DATEPART(year, [revenueDate])
	order by DATEPART(year, [revenueDate]) asc
END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthlyByTaxType]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 5 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthlyByTaxType]
	@NumberOfMonths INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT year([revenueDate]) as year, month([revenueDate]) as month, [conceptID] as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startdate and conceptID is not null and conceptID != ''
	group by year([revenueDate]), month([revenueDate]), conceptID
	order by year([revenueDate]) asc, month([revenueDate]), conceptID  asc
END
GO

/****** Object:  StoredProcedure [dbo].[FORECAST_ReportForecastData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
CREATE PROCEDURE [dbo].[FORECAST_ReportForecastData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@amount money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @regionID = NULL


	INSERT INTO [dbo].[RevenueForecast]
           ([forecastDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[amount])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@amount)

END
GO

/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthly]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 29 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_KPIEvolutionOfRevenueCollectionMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month 12 months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT year([revenueDate]) as year, month([revenueDate]) as month, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startdate
	group by year([revenueDate]), month([revenueDate])
	order by year([revenueDate]) asc, month([revenueDate]) asc
END
GO

/****** Object:  StoredProcedure [dbo].[REGION_ReportRegionData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: APril 26 2020
-- =============================================
CREATE PROCEDURE [dbo].[REGION_ReportRegionData]
	@dateReported date,
	@regionID nvarchar(10),
	@gdpPerCapitaAtDate money,
	@nominalGdpAtDate money,
	@populationAtDate int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@gdpPerCapitaAtDate = 0)
		set @gdpPerCapitaAtDate = NULL

	if(@nominalGdpAtDate = 0)
		set @nominalGdpAtDate = NULL

	if(@populationAtDate = 0)
		set @populationAtDate = NULL

	INSERT INTO [dbo].[RegionStats]
           ([statsDate]
           ,[regionID]
		   ,[gdpPerCapitaAtDate]
           ,[nominalGdpAtDate]
           ,[populationAtDate])
     VALUES
           (@dateReported
           ,@regionID
		   ,@gdpPerCapitaAtDate
           ,@nominalGdpAtDate
           ,@populationAtDate)

END
GO

/****** Object:  StoredProcedure [dbo].[COUNTRY_ReportCountryData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: APril 26 2020
-- =============================================
CREATE PROCEDURE [dbo].[COUNTRY_ReportCountryData]
	@dateReported date,
	@gdpPerCapitaAtDate money,
	@nominalGdpAtDate money,
	@populationAtDate int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@gdpPerCapitaAtDate = 0)
		set @gdpPerCapitaAtDate = NULL

	if(@nominalGdpAtDate = 0)
		set @nominalGdpAtDate = NULL

	if(@populationAtDate = 0)
		set @populationAtDate = NULL

	INSERT INTO [dbo].[CountryStats]
           ([statsDate]
           ,[gdpPerCapitaAtDate]
           ,[nominalGdpAtDate]
           ,[populationAtDate])
     VALUES
           (@dateReported
           ,@gdpPerCapitaAtDate
           ,@nominalGdpAtDate
           ,@populationAtDate)

END
GO

/****** Object:  StoredProcedure [dbo].[STAT_InsertParameterStats]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
CREATE PROCEDURE [dbo].[STAT_InsertParameterStats]
	@parameterDate date,
	@parameterName nvarchar(100),
	@generatedValue decimal(21,3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[ParameterStats]
           ([parameterName]
           ,[timeStamp]
		   ,[valueDate]
           ,[generatedValue])
    VALUES
           (@parameterName
		   ,GETDATE()
           ,@parameterDate
           ,@generatedValue)

END
GO

/****** Object:  StoredProcedure [dbo].[ARREARS_ReportArrearsData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
CREATE PROCEDURE [dbo].[ARREARS_ReportArrearsData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@amount money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @regionID = NULL


	INSERT INTO [dbo].[Arrears]
           ([arrearDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[amount])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@amount)

END
GO

/****** Object:  StoredProcedure [dbo].[REVENUE_ReportRevenueData]    Script Date: 6/23/2020 4:45:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
CREATE PROCEDURE [dbo].[REVENUE_ReportRevenueData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@amount money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @regionID = NULL


	INSERT INTO [dbo].[Revenue]
           ([revenueDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[amount])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@amount)

END
GO

