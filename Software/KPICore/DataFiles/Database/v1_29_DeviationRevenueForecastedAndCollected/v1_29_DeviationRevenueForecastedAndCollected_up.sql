DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_DeviationRevenueForecastedAndCollected]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_DeviationRevenueForecastedAndCollected]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @revenue as table([year] int, [quarter] int, amount money)
	declare @refunds as table([year] int, [quarter] int, amount money)
	declare @forecast as table([year] int, [quarter] int, amount money)

	insert into @revenue
	select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(quarter, [customsRevenueDate]) as [quarter],
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(quarter, [customsRevenueDate])

	insert into @refunds 
	select DATEPART(year, [customsRefundsDate]) as [year], 
		DATEPART(quarter, [customsRefundsDate]) as [quarter],
		sum([refundAmountProcessed]) as amount
	from [dbo].[CustomsRefund]
	where DATEPART(year, [customsRefundsDate]) > @startYear 
		and DATEPART(year, [customsRefundsDate]) <= @limitYear
	group by DATEPART(year, [customsRefundsDate]),  DATEPART(quarter, [customsRefundsDate])

	insert into @forecast
	select DATEPART(year, [customsRevenueForecastDate]) as [year], 
		DATEPART(quarter, [customsRevenueForecastDate]) as [quarter],
		sum([amount]) as amount
	from [dbo].[CustomsRevenueForecast]
	where DATEPART(year, [customsRevenueForecastDate]) > @startYear 
		and DATEPART(year, [customsRevenueForecastDate]) <= @limitYear
	group by DATEPART(year, [customsRevenueForecastDate]),  DATEPART(quarter, [customsRevenueForecastDate])

	select 	revenue.[year], revenue.[quarter], 
		case when forecast.amount is null or forecast.amount = 0 then 0.00 else
		(revenue.amount - refunds.amount - forecast.amount)/ forecast.amount * 100 end as deviation
	from @revenue as revenue
	join @refunds as refunds on revenue.[quarter] = refunds.[quarter] and revenue.[year] = refunds.[year]
	join @forecast as forecast on revenue.[quarter] = forecast.[quarter] and revenue.[year] = forecast.[year]
	order by revenue.[year] asc , revenue.[quarter] asc

END
Go

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_DeviationRevenueForecastedAndCollectedByFilter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_DeviationRevenueForecastedAndCollectedByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @revenue as table([year] int, [quarter] int, amount money)
	declare @refunds as table([year] int, [quarter] int, amount money)
	declare @forecast as table([year] int, [quarter] int, amount money)

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(quarter, [customsRevenueDate]) as [quarter],
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [customsRevenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(quarter, [customsRevenueDate])' 

	insert into @revenue
	execute(@query)

	
	select @query = 'select DATEPART(year, [customsRefundsDate]) as [year], 
		DATEPART(quarter, [customsRefundsDate]) as [quarter],
		sum([refundAmountProcessed]) as amount
	from [dbo].[CustomsRefund]
	where DATEPART(year, [customsRefundsDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [customsRefundsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [customsRefundsDate]),  DATEPART(quarter, [customsRefundsDate])'
	
	insert into @refunds 
	execute(@query)

	select @query = 'select DATEPART(year, [customsRevenueForecastDate]) as [year], 
		DATEPART(quarter, [customsRevenueForecastDate]) as [quarter],
		sum([amount]) as amount
	from [dbo].[CustomsRevenueForecast]
	where DATEPART(year, [customsRevenueForecastDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [customsRevenueForecastDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by DATEPART(year, [customsRevenueForecastDate]),  DATEPART(quarter, [customsRevenueForecastDate])'

	insert into @forecast
	execute(@query)

	select 	revenue.[year], revenue.[quarter], 
		case when forecast.amount is null or forecast.amount = 0 then 0.00 else
		(revenue.amount - refunds.amount - forecast.amount)/ forecast.amount * 100 end as deviation
	from @revenue as revenue
	join @refunds as refunds on revenue.[quarter] = refunds.[quarter] and revenue.[year] = refunds.[year]
	join @forecast as forecast on revenue.[quarter] = forecast.[quarter] and revenue.[year] = forecast.[year]
	order by revenue.[year] asc , revenue.[quarter] asc

END
GO
