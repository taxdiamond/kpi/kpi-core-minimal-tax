DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsData]
DROP PROCEDURE IF EXISTS [dbo].[CUSTOMSOPERATIONS_GetLastDate]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TopCustomsPortsOfEntryByCargoVolumeGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfUnArrivedTransitsGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfUnArrivedTransitsByCustomsPost]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfDrawbackTransactionsGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TotalNumberOfDrawbackTransactionsByCustomsPost]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentOfImportsPhysicallyExaminedByCustomsPost]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentOfImportsSubmittedToDocumentaryInspectionByCustomsPost]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_EffectivenessInPhysicalExaminationsByCustomsPost]
GO





