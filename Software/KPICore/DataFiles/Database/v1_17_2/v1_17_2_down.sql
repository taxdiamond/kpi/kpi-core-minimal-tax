SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
ALTER PROCEDURE [dbo].[ARREARS_ReportArrearsData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@amount money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @regionID = NULL


	INSERT INTO [dbo].[Arrears]
           ([arrearDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[amount])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@amount)

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearlyByTaxPayerSegment]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearlyTaxPayerSegmentByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfArrearCasesYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfArrearCasesYearlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfArrearCasesYearlyByTaxPayerSegment]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_InitialStockInNumberOfArrearCasesYearlyTaxPayerSegmentByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedYearlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedMonthly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedMonthlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxTypeYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxTypeYearlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxTypeMonthly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxTypeMonthlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxPayerSegmentYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxPayerSegmentYearlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxPayerSegmentMonthly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxPayerSegmentMonthlyByFilter]
GO

