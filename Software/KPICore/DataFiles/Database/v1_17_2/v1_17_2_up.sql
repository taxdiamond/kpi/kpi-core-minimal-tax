SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
ALTER PROCEDURE [dbo].[ARREARS_ReportArrearsData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@numberOfArrearCasesPending int,
	@amount money,    -- This is the ValueOfArrearCasesPending
	@numberOfArrearCasesGenerated int,
	@valueOfArrearCasesGenerated money,
	@numberOfArrearCasesResolved int,
	@valueOfArrearCasesResolved money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @regionID = NULL


	INSERT INTO [dbo].[Arrears]
           ([arrearDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[amount]
		   ,[valueOfArrearCasesGenerated]
		   ,[valueOfArrearCasesResolved]
		   ,[numberOfArrearCasesGenerated]
		   ,[numberOfArrearCasesPending]
		   ,[numberOfArrearCasesResolved])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@amount
		   ,@valueOfArrearCasesGenerated
		   ,@valueOfArrearCasesResolved
		   ,@numberOfArrearCasesGenerated
		   ,@numberOfArrearCasesPending
		   ,@numberOfArrearCasesResolved)
END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [arrearDate]) + 1 as year, 
		sum([amount]) as arrearStock
	from [dbo].[Arrears]
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > (@startYear -1) 
			and DATEPART(year, [arrearDate]) < @limitYear
	group by datepart(year, [arrearDate]) 
	order by datepart(year, [arrearDate]) asc
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [arrearDate]) + 1 as year, 
		sum([amount]) as arrearStock
	from [dbo].[Arrears]
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [arrearDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [arrearDate]) 
	order by datepart(year, [arrearDate]) asc'
	
	execute(@query)
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearlyByTaxPayerSegment]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [arrearDate]) + 1 as year, 
		s.segmentName,
		sum([amount]) as arrearStock
	from [dbo].[Arrears] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > (@startYear -1)  
			and DATEPART(year, [arrearDate]) < @limitYear
	group by datepart(year, [arrearDate]), s.segmentName
	order by datepart(year, [arrearDate]) asc, s.segmentName asc
END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearlyTaxPayerSegmentByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month
	
	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select
		datepart(year, [arrearDate]) + 1 as year, 
		s.segmentName,
		sum([amount]) as arrearStock
	from [dbo].[Arrears] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [arrearDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [arrearDate]), s.segmentName
	order by datepart(year, [arrearDate]) asc, s.segmentName asc'
	
	execute(@query)
END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInNumberOfArrearCasesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [arrearDate]) + 1 as year, 
		sum([numberOfArrearCasesPending]) as arrearStock
	from [dbo].[Arrears]
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > (@startYear -1) 
			and DATEPART(year, [arrearDate]) < @limitYear
	group by datepart(year, [arrearDate]) 
	order by datepart(year, [arrearDate]) asc
END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInNumberOfArrearCasesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [arrearDate]) + 1 as year, 
		sum([numberOfArrearCasesPending]) as arrearStock
	from [dbo].[Arrears]
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [arrearDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [arrearDate]) 
	order by datepart(year, [arrearDate]) asc'
	
	execute(@query)
END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInNumberOfArrearCasesYearlyByTaxPayerSegment]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [arrearDate]) + 1 as year, 
		s.segmentName,
		sum([numberOfArrearCasesPending]) as arrearStock
	from [dbo].[Arrears] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > (@startYear -1)  
			and DATEPART(year, [arrearDate]) < @limitYear
	group by datepart(year, [arrearDate]), s.segmentName
	order by datepart(year, [arrearDate]) asc, s.segmentName asc
END
GO




SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_InitialStockInNumberOfArrearCasesYearlyTaxPayerSegmentByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- IT IS POSSIBLE that we need to review this SP since this is measuring the number of tax appeals 
	-- for January and the number of appeals for January First may have been reported on the Dec/31st 
	-- of the previous month
	
	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [arrearDate]) + 1 as year, 
		s.segmentName,
		sum([numberOfArrearCasesPending]) as arrearStock
	from [dbo].[Arrears] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [arrearDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [arrearDate]), s.segmentName
	order by datepart(year, [arrearDate]) asc, s.segmentName asc'

	execute(@query)
END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [arrearDate]) as year, 
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > (@startYear) 
			and DATEPART(year, [arrearDate]) <= @limitYear
	group by datepart(year, [arrearDate]) 
	order by datepart(year, [arrearDate]) asc

END
GO




SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [arrearDate]) as year, 
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [arrearDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [arrearDate]) 
	order by datepart(year, [arrearDate]) asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [arrearDate]) as year, 
		datepart(month, [arrearDate]) as month, 
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears]
	where [arrearDate] >= @startdate
	group by datepart(year, [arrearDate]), datepart(month, [arrearDate])
	order by datepart(year, [arrearDate]) asc, datepart(month, [arrearDate]) asc

END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [arrearDate]) as year, 
		datepart(month, [arrearDate]) as month, 
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears]
	where [arrearDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [arrearDate]), datepart(month, [arrearDate])
	order by datepart(year, [arrearDate]) asc, datepart(month, [arrearDate]) asc'
	
	execute(@query)
END
GO




SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [arrearDate]) as year, 
		[conceptID] as taxType,
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears]
	where concept = 'Tax' and
			DATEPART(year, [arrearDate]) > (@startYear) 
			and DATEPART(year, [arrearDate]) <= @limitYear
	group by datepart(year, [arrearDate]), [conceptID]
	order by datepart(year, [arrearDate]) asc, [conceptID] asc

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [arrearDate]) as year, 
		[conceptID] as taxType,
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears]
	where concept = ''Tax'' and
			DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [arrearDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [arrearDate]), [conceptID]
	order by datepart(year, [arrearDate]) asc, [conceptID] asc'

	execute(@query)
END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [arrearDate]) as year, 
		datepart(month, [arrearDate]) as month, 
		[conceptID] as taxType,
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears]
	where concept = 'Tax' and
			[arrearDate] >= @startdate
	group by datepart(year, [arrearDate]), datepart(month, [arrearDate]), [conceptID]
	order by datepart(year, [arrearDate]) asc, datepart(month, [arrearDate]) asc, [conceptID] asc

END
GO




SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [arrearDate]) as year, 
		datepart(month, [arrearDate]) as month, 
		[conceptID] as taxType,
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears]
	where concept = ''Tax'' and
			[arrearDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [arrearDate]), datepart(month, [arrearDate]), [conceptID]
	order by datepart(year, [arrearDate]) asc, datepart(month, [arrearDate]) asc, [conceptID] asc' 
	
	execute(@query)
END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxPayerSegmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [arrearDate]) as year, 
		s.segmentName as segmentName,
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [arrearDate]) > (@startYear) 
			and DATEPART(year, [arrearDate]) <= @limitYear
	group by datepart(year, [arrearDate]), segmentName
	order by datepart(year, [arrearDate]) asc, segmentName asc

END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxPayerSegmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [arrearDate]) as year, 
		s.segmentName as segmentName,
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [arrearDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [arrearDate]), segmentName
	order by datepart(year, [arrearDate]) asc, segmentName asc'
	
	execute(@query)
END
GO




SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxPayerSegmentMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [arrearDate]) as year, 
		datepart(month, [arrearDate]) as month, 
		s.segmentName as segmentName,
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [arrearDate] >= @startdate
	group by datepart(year, [arrearDate]), datepart(month, [arrearDate]), segmentName
	order by datepart(year, [arrearDate]) asc, datepart(month, [arrearDate]) asc, segmentName asc

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_NumberIOfTaxArrearsCasesGeneratedByTaxPayerSegmentMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [arrearDate]) as year, 
		datepart(month, [arrearDate]) as month, 
		s.segmentName as segmentName,
		sum([numberOfArrearCasesGenerated]) as arrearsGenerated
	from [dbo].[Arrears] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [arrearDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [arrearDate]), datepart(month, [arrearDate]), segmentName
	order by datepart(year, [arrearDate]) asc, datepart(month, [arrearDate]) asc, segmentName asc'

	execute(@query)
END
GO



