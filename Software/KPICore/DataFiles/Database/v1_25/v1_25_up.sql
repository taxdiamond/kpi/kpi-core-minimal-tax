
/****** Object:  StoredProcedure [dbo].[UTIL_ResetKPIDW]    Script Date: 3/8/2020 17:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 24 2020
-- Description:	Deletes all Data Warehouse data from the Tax Administration.  This SP does not 
-- delete other databases that are used in the KPI addlication, such as user tables, dashboards, etc.
-- =============================================
ALTER PROCEDURE [dbo].[UTIL_ResetKPIDW] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	delete from [dbo].[Appeals]
	delete from [dbo].[Arrears]
	delete from [dbo].[CountryStats]
	delete from [dbo].[AuditStats]
	delete from [dbo].[Courses]
	delete from [dbo].[FilingStats]
	delete from [dbo].[HumanResources]
	delete from [dbo].[Imports]
	delete from [dbo].[ParameterStats]
	delete from [dbo].[Refunds]
	delete from [dbo].[RegionStats]
	delete from [dbo].[Revenue]
	delete from [dbo].[RevenueForecast]
	delete from [dbo].[RevenueSummaryByMonth]
	delete from [dbo].[SatisfactionSurveys]
	delete from [dbo].[TaxAdministrationExpenses]
	delete from [dbo].[TaxPayerServices]
	delete from [dbo].[TaxPayerStats]
	delete from [dbo].[TaxRates]
	delete from [dbo].[DoingBusinessTaxStats]
	delete from [dbo].[SimulationStates]

END
GO
--==============================================================================================

/****** Object:  StoredProcedure [dbo].[UTIL_ResetKPIDW]    Script Date: 3/8/2020 17:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 24 2020
-- Description:	Deletes all Data Warehouse data from the Tax Administration.  This SP does not 
-- delete other databases that are used in the KPI addlication, such as user tables, dashboards, etc.
-- =============================================
CREATE PROCEDURE [dbo].[UTIL_DeleteKPIDWByDate] 
	@enumDW int,
	@fromDate datetime,
	@untilDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @enumDW = 0
		DELETE FROM [dbo].[Appeals] WHERE appealsDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 1
		DELETE FROM [dbo].[Arrears] WHERE arrearDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 2
		DELETE FROM [dbo].[CountryStats] WHERE statsDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 22
		DELETE FROM [dbo].[AuditStats] WHERE statsDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 3
		DELETE FROM [dbo].[Courses] WHERE trainingCoursesReportDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 4
		DELETE FROM [dbo].[FilingStats] WHERE reportDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 5
		DELETE FROM [dbo].[HumanResources] WHERE hrItemReportDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 6
		DELETE FROM [dbo].[Imports] WHERE importDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 9
		DELETE FROM [dbo].[ParameterStats] WHERE timeStamp BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 10
		DELETE FROM [dbo].[Refunds] WHERE RefundsDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 12
		DELETE FROM [dbo].[RegionStats] WHERE statsDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 13
		DELETE FROM [dbo].[Revenue] WHERE revenueDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 14
		DELETE FROM [dbo].[RevenueForecast] WHERE forecastDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 15
		DELETE FROM [dbo].[RevenueSummaryByMonth] WHERE revenueDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 16
		DELETE FROM [dbo].[SatisfactionSurveys] WHERE surveyItemReportDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 19
		DELETE FROM [dbo].[TaxAdministrationExpenses] WHERE adminExpensesDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 20
		DELETE FROM [dbo].[TaxPayerServices] WHERE servicesItemReportDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 21
		DELETE FROM [dbo].[TaxPayerStats] WHERE statsDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 23
		DELETE FROM [dbo].[TaxRates] WHERE taxRateDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 24
		DELETE FROM [dbo].[DoingBusinessTaxStats] WHERE statsDate BETWEEN @fromDate AND @untilDate
	ELSE IF @enumDW = 25
		DELETE FROM [dbo].[SimulationStates] WHERE LastDateProcessed BETWEEN @fromDate AND @untilDate

END
GO
--==============================================================================================
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 03-08-2020
-- Description:	Deletes all Classifier Data Warehouse data from the Tax Administration.
-- =============================================
CREATE PROCEDURE [dbo].[UTIL_ResetClassifierKPIDW] 
AS
BEGIN
	
	delete from [dbo].[Segment]
	delete from [dbo].[Office]
	delete from [dbo].[Region]
END
GO

--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE APPEALS_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([appealsDate]) as LastDate
	FROM [KPICoreDB].[dbo].[Appeals]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE ARREARS_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(arrearDate) as LastDate
	FROM [KPICoreDB].[dbo].[Arrears]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE AUDIT_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(statsDate) as LastDate
	FROM [KPICoreDB].[dbo].[AuditStats]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE COUNTRY_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(statsDate) as LastDate
	FROM [KPICoreDB].[dbo].[CountryStats]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE COURSES_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(trainingCoursesReportDate) as LastDate
	FROM [KPICoreDB].[dbo].[Courses]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE FILING_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(reportDate) as LastDate
	FROM [KPICoreDB].[dbo].[FilingStats]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE HR_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(hrItemReportDate) as LastDate
	FROM [KPICoreDB].[dbo].[HumanResources]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE IMPORTS_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(importDate) as LastDate
	FROM [KPICoreDB].[dbo].[Imports]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE REFUND_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(RefundsDate) as LastDate
	FROM [KPICoreDB].[dbo].[Refunds]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE REGION_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(statsDate) as LastDate
	FROM [KPICoreDB].[dbo].[RegionStats]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE REVENUE_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(revenueDate) as LastDate
	FROM [KPICoreDB].[dbo].[Revenue]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE FORECAST_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(forecastDate) as LastDate
	FROM [KPICoreDB].[dbo].[RevenueForecast]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE REVENUE_SUMMARY_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(revenueDate) as LastDate
	FROM [KPICoreDB].[dbo].[RevenueSummaryByMonth]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE SATISFACTION_SURVEYS_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(surveyItemReportDate) as LastDate
	FROM [KPICoreDB].[dbo].[SatisfactionSurveys]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE EXPENSES_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(adminExpensesDate) as LastDate
	FROM [KPICoreDB].[dbo].[TaxAdministrationExpenses]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE SERVICES_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(servicesItemReportDate) as LastDate
	FROM [KPICoreDB].[dbo].[TaxPayerServices]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE TAXPAYER_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(statsDate) as LastDate
	FROM [KPICoreDB].[dbo].[TaxPayerStats]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE TAXRATE_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(taxRateDate) as LastDate
	FROM [KPICoreDB].[dbo].[TaxRates]

END
GO
--=======================================================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE DOINGBUSINESS_GetLastDate
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX(statsDate) as LastDate
	FROM [KPICoreDB].[dbo].[DoingBusinessTaxStats]

END
GO
--=======================================================================================================================

/****** Object:  StoredProcedure [dbo].[APPEALS_ReportAppealsData]    Script Date: 8/8/2020 13:40:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 8 2020
-- =============================================
ALTER PROCEDURE [dbo].[APPEALS_ReportAppealsData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@numberAppealsCasesPending int,
	@amountAppealed money,
	@numberCasesResolved int,
	@numberCasesResolvedFavorTaxPayer int,
	@numberCasesResolvedFavorTaxAdministration int,
	@averageDaysToResolveCases int,
	@totalAmountResolvedFavorTaxPayer money,
	@totalAmountResolvedFavorTaxAdministration money,
	@numberOfAppealCasesSubmitted int,
	@valueOfAppealCasesSubmitted decimal(18,2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @regionID = NULL

	INSERT INTO [dbo].[Appeals]
           ([appealsDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[numberAppealsCasesPending]
           ,[amountAppealed]
           ,[numberCasesResolved]
           ,[numberCasesResolvedFavorTaxPayer]
           ,[numberCasesResolvedFavorTaxAdministration]
           ,[averageDaysToResolveCases]
           ,[totalAmountResolvedFavorTaxPayer]
           ,[totalAmountResolvedFavorTaxAdministration]
		   ,[numberOfAppealCasesSubmitted]
		   ,[valueOfAppealCasesSubmitted])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@numberAppealsCasesPending
           ,@amountAppealed
           ,@numberCasesResolved
           ,@numberCasesResolvedFavorTaxPayer
           ,@numberCasesResolvedFavorTaxAdministration
           ,@averageDaysToResolveCases
           ,@totalAmountResolvedFavorTaxPayer
           ,@totalAmountResolvedFavorTaxAdministration
		   ,@numberOfAppealCasesSubmitted
		   ,@valueOfAppealCasesSubmitted)

END
GO