USE [KPICoreDB]
GO
/****** Object:  StoredProcedure [dbo].[UTIL_ResetKPIDW]    Script Date: 3/8/2020 17:52:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 24 2020
-- Description:	Deletes all Data Warehouse data from the Tax Administration.  This SP does not 
-- delete other databases that are used in the KPI addlication, such as user tables, dashboards, etc.
-- =============================================
ALTER PROCEDURE [dbo].[UTIL_ResetKPIDW] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	delete from [dbo].[Appeals]
	delete from [dbo].[Arrears]
	delete from [dbo].[CountryStats]
	delete from [dbo].[AuditStats]
	delete from [dbo].[Courses]
	delete from [dbo].[FilingStats]
	delete from [dbo].[HumanResources]
	delete from [dbo].[Imports]
	delete from [dbo].[ParameterStats]
	delete from [dbo].[Refunds]
	delete from [dbo].[RegionStats]
	delete from [dbo].[Revenue]
	delete from [dbo].[RevenueForecast]
	delete from [dbo].[RevenueSummaryByMonth]
	delete from [dbo].[SatisfactionSurveys]
	delete from [dbo].[TaxAdministrationExpenses]
	delete from [dbo].[TaxPayerServices]
	delete from [dbo].[TaxPayerStats]
	delete from [dbo].[TaxRates]
	delete from [dbo].[DoingBusinessTaxStats]
	delete from [dbo].[SimulationStates]

	delete from [dbo].[Segment]
	delete from [dbo].[Office]
	delete from [dbo].[Region]
END
GO
--==============================================================================================
DROP PROCEDURE IF EXISTS [dbo].[UTIL_ResetClassifierKPIDW] 
DROP PROCEDURE IF EXISTS [dbo].[UTIL_DeleteKPIDWByDate] 

DROP PROCEDURE IF EXISTS [dbo].[APPEALS_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[ARREARS_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[AUDIT_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[COUNTRY_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[COURSES_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[FILING_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[HR_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[IMPORTS_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[REFUND_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[REGION_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[REVENUE_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[FORECAST_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[REVENUE_SUMMARY_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[SATISFACTION_SURVEYS_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[EXPENSES_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[SERVICES_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[TAXPAYER_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[TAXRATE_GetLastDate] 
DROP PROCEDURE IF EXISTS [dbo].[DOINGBUSINESS_GetLastDate] 
