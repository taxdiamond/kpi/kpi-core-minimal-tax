SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 9 2020
-- =============================================
ALTER PROCEDURE [dbo].[TAXPAYER_ReportTaxpayerData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@numberOfRegisteredTaxpayers int,
	@numberOfActiveTaxPayers int,
    @numberOfTaxPayersFiled int,
    @numberTaxPayersFiledInTime int,
    @numberOfTaxPayers70PercentRevenue int,
	@conceptID nvarchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@numberOfRegisteredTaxpayers = 0)
		set @numberOfRegisteredTaxpayers = null

	if(@numberOfActiveTaxPayers = 0)
		set @numberOfActiveTaxPayers = null

	if(@numberOfTaxPayersFiled = 0)
		set @numberOfTaxPayersFiled = null

	if(@numberTaxPayersFiledInTime = 0)
		set @numberTaxPayersFiledInTime = null

	if(@numberOfTaxPayers70PercentRevenue = 0)
		set @numberOfTaxPayers70PercentRevenue = null

	INSERT INTO [dbo].[TaxPayerStats]
           ([statsDate]
           ,[numberOfRegisteredTaxPayers]
           ,[numberOfActiveTaxPayers]
           ,[numberOfTaxPayersFiled]
           ,[numberTaxPayersFiledInTime]
           ,[numberOfTaxPayers70PercentRevenue]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
		   ,[conceptID])
	VALUES
           (@dateReported
           ,@numberOfRegisteredTaxpayers
           ,@numberOfActiveTaxPayers
           ,@numberOfTaxPayersFiled
           ,@numberTaxPayersFiledInTime
           ,@numberOfTaxPayers70PercentRevenue
           ,@regionID
           ,@officeID
           ,@segmentID
		   ,@conceptID)
END
GO
