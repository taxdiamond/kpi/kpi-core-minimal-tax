SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Jun 11 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select DATEPART(year, [hrItemReportDate]) as year, 
		case when sum([totalHumanResources]) > 0 then
			Convert(decimal(18,3), sum([totalHumanResourcesInTaxCollection] +
				[totalHumanResourcesInTaxPayerServices] +
				[totalHumanResourcesInTaxEnforcement] +
				[totalHumanResourcesInAudits] +
				[totalHumanResourcesInAppeals])) / Convert(decimal(18,3), sum([totalHumanResources]))*100 else 0.00 end as allocation
	from [dbo].[HumanResources]
	where DATEPART(year, [hrItemReportDate]) > @startYear 
		and DATEPART(year, [hrItemReportDate]) <= @limitYear
	group by DATEPART(year, [hrItemReportDate])
	order by DATEPART(year, [hrItemReportDate]) asc
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Jun 11 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_AllocationStaffKeyFunctionsYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [hrItemReportDate]) as year, 
		case when sum([totalHumanResources]) > 0 then
			Convert(decimal(18,3), sum([totalHumanResourcesInTaxCollection] +
				[totalHumanResourcesInTaxPayerServices] +
				[totalHumanResourcesInTaxEnforcement] +
				[totalHumanResourcesInAudits] +
				[totalHumanResourcesInAppeals])) / Convert(decimal(18,3), sum([totalHumanResources]))*100 else 0.00 end as allocation
	from [dbo].[HumanResources]
	where DATEPART(year, [hrItemReportDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [hrItemReportDate])<= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
	group by DATEPART(year, [hrItemReportDate])
	order by DATEPART(year, [hrItemReportDate]) asc'

	execute(@query)

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [arrearDate]) + 1 as year, 
		sum([amount]) / 1000000 as arrearStock
	from [dbo].[Arrears]
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > (@startYear -1) 
			and DATEPART(year, [arrearDate]) < @limitYear
	group by datepart(year, [arrearDate]) 
	order by datepart(year, [arrearDate]) asc
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [arrearDate]) + 1 as year, 
		sum([amount]) / 1000000 as arrearStock
	from [dbo].[Arrears]
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [arrearDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [arrearDate]) 
	order by datepart(year, [arrearDate]) asc'
	
	execute(@query)
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearlyByTaxPayerSegment]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [arrearDate]) + 1 as year, 
		s.segmentName,
		sum([amount]) / 1000000 as arrearStock
	from [dbo].[Arrears] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > (@startYear -1)  
			and DATEPART(year, [arrearDate]) < @limitYear
	group by datepart(year, [arrearDate]), s.segmentName
	order by datepart(year, [arrearDate]) asc, s.segmentName asc
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_InitialStockInAmountForArrearCasesYearlyTaxPayerSegmentByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- The number of appeals for January First is the same as what is reported on the Dec/31st 
	-- of the previous month
	
	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select
		datepart(year, [arrearDate]) + 1 as year, 
		s.segmentName,
		sum([amount]) / 1000000 as arrearStock
	from [dbo].[Arrears] as a
	join [dbo].[Segment] as s on a.segmentID = s.segmentID
	where datepart(month,[arrearDate]) = 12
		and DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear -1) + '
			and DATEPART(year, [arrearDate]) < '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [arrearDate]), s.segmentName
	order by datepart(year, [arrearDate]) asc, s.segmentName asc'
	
	execute(@query)
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear  
		and DATEPART(year, [revenueDate]) <= @limitYear
		and [concept] = 'Arrears Recovered'  
	group by DATEPART(year, [revenueDate])
	
	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > @startYear
		 and DATEPART(year, [arrearDate]) <= @limitYear
		 and [concept] = 'Tax' 
	group by DATEPART(year, [arrearDate])

	select r.year, 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year
	order by year asc
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	declare @revenueTable as table([year] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, arrears money)

	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +' 
		and [concept] = ''Arrears Recovered''  
	group by DATEPART(year, [revenueDate])'

	insert into @revenueTable
	execute(@query)
	
	select @query = 'SELECT DATEPART(year, [arrearDate]) as year, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear) + '
		 and DATEPART(year, [arrearDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
		 and [concept] = ''Tax'' 
	group by DATEPART(year, [arrearDate])'

	insert into @arrearsTable
	execute(@query)

	select r.year, 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year
	order by year asc
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @revenueTable as table([year] int, [month] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, [month] int, arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, 
		DATEPART(month, [revenueDate]) as month, 
		sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startDate
		and [concept] = 'Arrears Recovered'  
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate])
	
	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, 
		DATEPART(month, [arrearDate]) as month, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where [arrearDate] >= @startDate
		 and [concept] = 'Tax' 
	group by DATEPART(year, [arrearDate]), DATEPART(month, [arrearDate])

	select r.year, r.month, 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.month = r.month
	order by year asc, month asc
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByFilter]
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @query as nvarchar(MAX)

	declare @revenueTable as table([year] int, [month] int, arrearsRecovery money)
	declare @arrearsTable as table([year] int, [month] int, arrears money)

	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, 
		DATEPART(month, [revenueDate]) as month, 
		sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where [revenueDate] >= ''' + convert(varchar,@startDate, 126) + '''
		and '+@WhereClause+' 
		and [concept] = ''Arrears Recovered''  
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate])'
	
	insert into @revenueTable
	execute(@query)

	select @query = 'SELECT DATEPART(year, [arrearDate]) as year, 
		DATEPART(month, [arrearDate]) as month, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where [arrearDate] >= ''' + convert(varchar,@startDate, 126) + '''
		and '+@WhereClause+' 
		 and [concept] = ''Tax'' 
	group by DATEPART(year, [arrearDate]), DATEPART(month, [arrearDate])'

	insert into @arrearsTable
	execute(@query)

	select r.year, r.month, 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.month = r.month
	order by year asc, month asc
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsMonthlyByOffice]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @revenueTable as table([year] int, [month] int, [officeID] nvarchar(10), arrearsRecovery money)
	declare @arrearsTable as table([year] int, [month] int, [officeID] nvarchar(10), arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, 
		DATEPART(month, [revenueDate]) as month, 
		[officeID],
		sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startDate
		and [concept] = 'Arrears Recovered'  
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), [officeID] 
	
	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, 
		DATEPART(month, [arrearDate]) as month, 
		[officeID], 
		sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where [arrearDate] >= @startDate
		 and [concept] = 'Tax' 
	group by DATEPART(year, [arrearDate]), DATEPART(month, [arrearDate]), [officeID]

	select r.year, r.month, o.[officeName], 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.month = r.month and r.officeID = f.officeID
	join [dbo].[Office] as o on f.officeID = o.officeID and r.officeID = o.officeID
	order by year asc, month asc, officeName asc
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIRecoveryTaxArrearsYearlyByOffice]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, [officeID] nvarchar(10), arrearsRecovery money)
	declare @arrearsTable as table([year] int, [officeID] nvarchar(10), arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, [officeID], sum([amount]) as arrearsRecovery
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear  
		and DATEPART(year, [revenueDate]) <= @limitYear
		and [concept] = 'Arrears Recovered'  
	group by DATEPART(year, [revenueDate]), [officeID]
	
	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, [officeID], sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > @startYear
		 and DATEPART(year, [arrearDate]) <= @limitYear
		 and [concept] = 'Tax' 
	group by DATEPART(year, [arrearDate]), [officeID]

	select r.year, o.officeName, 
		case when arrears > 0 then arrearsRecovery/arrears*100 else 0.00 end as recoveryRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.officeID = r.officeID
	join [dbo].[Office] as o on f.officeID = o.officeID and r.officeID = o.officeID
	order by year asc, o.officeName asc
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthly]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @revenueTable as table([year] int, [month] int, taxtype nvarchar(200), revenue money)
	declare @arrearsTable as table([year] int, [month] int, taxtype nvarchar(200), arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month, conceptID as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= @startDate and [concept] = 'Tax' and [conceptID] is not null
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), conceptID

	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, DATEPART(month, [arrearDate]) as month, conceptID as taxtype, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where [arrearDate] >= @startDate
		 and [concept] = 'Tax' and [conceptID] is not null 
	group by DATEPART(year, [arrearDate]), DATEPART(month, [arrearDate]), conceptID

	select r.year, r.month, r.taxtype, 
		case when revenue > 0 then arrears/revenue*100 else 0.00 end as arrearsRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.month = r.month and f.taxtype = r.taxtype
	order by year asc, month asc, r.taxtype asc
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedMonthlyByFilter]
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startDate date 
	select @startDate = DATEFROMPARTS( DATEPART(year, GETDATE()), 1, 1)

	declare @revenueTable as table([year] int, [month] int, taxtype nvarchar(200), revenue money)
	declare @arrearsTable as table([year] int, [month] int, taxtype nvarchar(200), arrears money)

	declare @query as nvarchar(MAX)

	SELECT @query = 'SELECT DATEPART(year, [revenueDate]) as year, DATEPART(month, [revenueDate]) as month, conceptID as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where [revenueDate] >= '''+convert(varchar, @startDate, 126)+ ''' 
		and [concept] = ''Tax'' and [conceptID] is not null 
		and '+@WhereClause +' 
	group by DATEPART(year, [revenueDate]), DATEPART(month, [revenueDate]), conceptID'

	insert into @revenueTable
	execute(@query)

	SELECT @query = 'SELECT DATEPART(year, [arrearDate]) as year, DATEPART(month, [arrearDate]) as month, conceptID as taxtype, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where [arrearDate] >= '''+convert(varchar, @startDate, 126)+ '''  
		 and [concept] = ''Tax'' and [conceptID] is not null and '+@WhereClause +' 
	group by DATEPART(year, [arrearDate]), DATEPART(month, [arrearDate]), conceptID'

	insert into @arrearsTable
	execute(@query)

	select r.year, r.month,  r.taxtype, 
		case when revenue > 0 then arrears/revenue*100 else 0.00 end as arrearsRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.month = r.month and f.taxtype = r.taxtype
	order by year asc, month asc, r.taxtype asc
END
GO


/****** Object:  StoredProcedure [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearly]    Script Date: 7/15/2020 9:40:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, taxtype nvarchar(200), revenue money)
	declare @arrearsTable as table([year] int, taxtype nvarchar(200), arrears money)

	insert into @revenueTable
	SELECT DATEPART(year, [revenueDate]) as year, conceptID as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > @startYear  
		and DATEPART(year, [revenueDate]) <= @limitYear
		and [concept] = 'Tax' and [conceptID] is not null 
	group by DATEPART(year, [revenueDate]), conceptID 
	
	insert into @arrearsTable
	SELECT DATEPART(year, [arrearDate]) as year, conceptID as taxtype, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > @startYear
		 and DATEPART(year, [arrearDate]) <= @limitYear
		 and [concept] = 'Tax' and [conceptID] is not null 
	group by DATEPART(year, [arrearDate]), conceptID

	select r.year, r.taxtype, 
		case when revenue > 0 then arrears/revenue*100 else 0.00 end as arrearsRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.taxtype = r.taxtype
	order by year asc
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: May 3 2020
-- =============================================
ALTER PROCEDURE [dbo].[KPI_DATA_KPIStockOfTaxArrearsAsPercentageOfTotalNetTaxRevenueCollectedYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTable as table([year] int, taxtype nvarchar(200), revenue money)
	declare @arrearsTable as table([year] int, taxtype nvarchar(200), arrears money)

	declare @query as nvarchar(MAX)

	select @query = 'SELECT DATEPART(year, [revenueDate]) as year, conceptID as taxtype, sum([amount]) as revenue
	FROM [dbo].[Revenue] 
	where DATEPART(year, [revenueDate]) > '+convert(varchar, @startYear)+ '  
		and DATEPART(year, [revenueDate]) <= '+convert(varchar, @limitYear)+ ' 
		and [concept] = ''Tax'' and [conceptID] is not null and '+@WhereClause+' 
	group by DATEPART(year, [revenueDate]), conceptID' 
	
	insert into @revenueTable
	execute(@query)

	select @query = 'SELECT DATEPART(year, [arrearDate]) as year, conceptID as taxtype, sum([amount]) as arrears
	FROM  [dbo].[Arrears]
	where DATEPART(year, [arrearDate]) > '+convert(varchar, @startYear)+ ' 
		 and DATEPART(year, [arrearDate]) <= '+convert(varchar, @limitYear)+ '
		 and [concept] = ''Tax'' and [conceptID] is not null and '+@WhereClause+'
	group by DATEPART(year, [arrearDate]), conceptID'

	insert into @arrearsTable
	execute(@query)

	select r.year, r.taxtype, 
		case when revenue > 0 then arrears/revenue*100 else 0.00 end as arrearsRate
	from @revenueTable as r 
	join @arrearsTable as f on f.year = r.year and f.taxtype = r.taxtype
	order by year asc

END
GO

