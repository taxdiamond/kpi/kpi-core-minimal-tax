SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSREVENUE_ReportRevenueData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@amount money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = NULL

	if(@conceptID = '')
		set @conceptID = NULL

	INSERT INTO [dbo].[CustomsRevenue]
           ([customsRevenueDate]
           ,[customsPostID]
           ,[concept]
           ,[conceptID]
           ,[amount])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@concept
           ,@conceptID
           ,@amount)

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 23 2020
-- =============================================
ALTER PROCEDURE [dbo].[REVENUE_ReportRevenueData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@amount money
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @conceptID = NULL

	INSERT INTO [dbo].[Revenue]
           ([revenueDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[amount])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@amount)

END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Takunori Kasai
-- Create date: 07/08/2020
-- =============================================
CREATE PROCEDURE [dbo].[CUSTOMSREVENUE_GetLastDate]
	
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT MAX([customsRevenueDate]) as LastDate
	FROM [dbo].[CustomsRevenue]

END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RevenueToGDPRatioGlobal]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueResults as table(yearStats int, revenue money)
	declare @gdpResults as table(yearStats int, gdp money)

	insert into @revenueResults
	select DATEPART(year, [customsRevenueDate]), sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear and DATEPART(year, [customsRevenueDate]) <= @limitYear
	group by DATEPART(year, [customsRevenueDate]) 
	order by DATEPART(year, [customsRevenueDate]) asc

	insert into @gdpResults
	select  DATEPART(year, [statsDate]), avg([nominalGdpAtDate])
	from [dbo].[CountryStats]
	where DATEPART(year, [statsDate]) > @startYear and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate]) 
	order by DATEPART(year, [statsDate]) asc

	select r.yearStats as [year], 
		case when gdp > 0 then revenue/gdp*100 else 0.00 end as RevenueToGPDRatio
	from @revenueResults as r
	join  @gdpResults as g on r.yearStats = g.yearStats
	order by r.yearStats asc
END
GO



SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RevenueToGDPRatioByCustomsRevenueType]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @revenueTypeResults as table(yearStats int, revenueType nvarchar(250), revenue money)
	declare @gdpResults as table(yearStats int, gdp money)

	insert into @revenueTypeResults
	select DATEPART(year, [customsRevenueDate]), conceptID, sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where  [concept] = 'Tax' and
		DATEPART(year, [customsRevenueDate]) > @startYear and 
		DATEPART(year, [customsRevenueDate]) <= @limitYear
	group by [conceptID], DATEPART(year, [customsRevenueDate])
	order by DATEPART(year, [customsRevenueDate]) asc

	insert into @gdpResults
	select  DATEPART(year, [statsDate]), avg([nominalGdpAtDate])
	from [dbo].[CountryStats]
	where DATEPART(year, [statsDate]) > @startYear and DATEPART(year, [statsDate]) <= @limitYear
	group by DATEPART(year, [statsDate])
	order by DATEPART(year, [statsDate]) asc
	
	select r.yearStats as [year], r.revenueType, 
	case when gdp > 0 then revenue*100/gdp else 0.00 end as RevenueToGPDRatio
	from @revenueTypeResults as r
	join  @gdpResults as g on r.yearStats = g.yearStats
	order by r.yearStats asc, r.revenueType asc

END
GO

