DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_GrowthOfCustomsDutiesCollectedQuarterly]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_GrowthOfCustomsDutiesCollectedQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, amount money)

	insert into @tab
	select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(quarter, [customsRevenueDate]) as [quarter],
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
		and [conceptID] = 'Duties'
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(quarter, [customsRevenueDate])

	select t1.year, t1.quarter, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as evolutionOfRevenue
	from @tab as t1
	join @tab as t2 on (t1.year -1) = t2.year and t1.quarter = t2.quarter
	order by t1.[year] asc , t1.[quarter] asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_GrowthOfCustomsDutiesCollectedQuarterlyByFilter]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_GrowthOfCustomsDutiesCollectedQuarterlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, amount money)

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(quarter, [customsRevenueDate]) as [quarter],
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [customsRevenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
		and [conceptID] = ''Duties''
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(quarter, [customsRevenueDate])'

	insert into @tab
	execute(@query)

	select t1.year, t1.quarter, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as evolutionOfRevenue
	from @tab as t1
	join @tab as t2 on (t1.year -1) = t2.year and t1.quarter = t2.quarter
	order by t1.[year] asc , t1.[quarter] asc
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_GrowthOfImportVATCollectedQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_GrowthOfImportVATCollectedQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, amount money)

	insert into @tab
	select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(quarter, [customsRevenueDate]) as [quarter],
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
		and [conceptID] = 'Import VAT'
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(quarter, [customsRevenueDate])

	select t1.year, t1.quarter, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as evolutionOfRevenue
	from @tab as t1
	join @tab as t2 on (t1.year -1) = t2.year and t1.quarter = t2.quarter
	order by t1.[year] asc , t1.[quarter] asc

END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_GrowthOfImportVATCollectedQuarterlyByFilter]
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_GrowthOfImportVATCollectedQuarterlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, amount money)

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(quarter, [customsRevenueDate]) as [quarter],
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [customsRevenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
		and [conceptID] = ''Import VAT''
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(quarter, [customsRevenueDate])'

	insert into @tab
	execute(@query)

	select t1.year, t1.quarter, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as evolutionOfRevenue
	from @tab as t1
	join @tab as t2 on (t1.year -1) = t2.year and t1.quarter = t2.quarter
	order by t1.[year] asc , t1.[quarter] asc
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_GrowthOfExciseCollectedQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_GrowthOfExciseCollectedQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, amount money)

	insert into @tab
	select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(quarter, [customsRevenueDate]) as [quarter],
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
		and [conceptID] = 'Import VAT'
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(quarter, [customsRevenueDate])

	select t1.year, t1.quarter, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as evolutionOfRevenue
	from @tab as t1
	join @tab as t2 on (t1.year -1) = t2.year and t1.quarter = t2.quarter
	order by t1.[year] asc , t1.[quarter] asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_GrowthOfExciseCollectedQuarterlyByFilter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_GrowthOfExciseCollectedQuarterlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, amount money)

	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(quarter, [customsRevenueDate]) as [quarter],
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > '+convert(varchar, @startYear) + '
		and DATEPART(year, [customsRevenueDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
		and [conceptID] = ''Excise''
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(quarter, [customsRevenueDate])'

	insert into @tab
	execute(@query)

	select t1.year, t1.quarter, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as evolutionOfRevenue
	from @tab as t1
	join @tab as t2 on (t1.year -1) = t2.year and t1.quarter = t2.quarter
	order by t1.[year] asc , t1.[quarter] asc
END
GO


