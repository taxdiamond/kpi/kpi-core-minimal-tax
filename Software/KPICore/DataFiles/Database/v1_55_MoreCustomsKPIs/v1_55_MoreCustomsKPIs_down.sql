DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_NumberOfRegisteredEntitiesMonthlyGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_NumberOfCancelledLicensesMonthlyGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TopBrokersWithMostPenaltiesOrOffenses]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TopCarriersWithMostPenaltiesOrOffenses]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TopWarehousesWithMostPenaltiesOrOffenses]
GO

