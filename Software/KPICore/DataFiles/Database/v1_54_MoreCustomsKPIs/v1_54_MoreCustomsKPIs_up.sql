SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSAPPEALS_ReportCustomsAppealsData]
	@dateReported date,
	@numberAppealsConsidered int,
	@numberAppealsResolved int,
	@numberAppealsUndergoLegalAction int,
	@numberJudicialDecissionsFavorableCustoms int,
	@numberLawsuitsFinished int,
	@averageTimeToResolveAppeals decimal(18,3)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsAppeals]
			   ([customsAppealsDate]
			   ,[numberAppealsConsidered]
			   ,[numberAppealsResolved]
			   ,[numberAppealsUndergoLegalAction]
			   ,[numberJudicialDecissionsFavorableCustoms]
			   ,[numberLawsuitsFinished]
			   ,[averageTimeToResolveAppeals])
	 VALUES
			   (@dateReported
			   ,@numberAppealsConsidered
			   ,@numberAppealsResolved
			   ,@numberAppealsUndergoLegalAction
			   ,@numberJudicialDecissionsFavorableCustoms
			   ,@numberLawsuitsFinished
			   ,@averageTimeToResolveAppeals)

END
GO


drop procedure IF EXISTS [dbo].[KPI_DATA_CUSTOMS_IPRSeizuresMonthlyGlobal]
go

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sêpt 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_IPRSeizuresMonthlyGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select DATEPART(year, [customsSeizuresDate]) as [year], 
		DATEPART(month, [customsSeizuresDate]) as [month],
		sum([seizureValue]) / 1000 as seizureValue
	from [dbo].[CustomsSeizures]
	where [customsSeizuresDate] >= @startDate
		and [seizureType] = 'Intellectual Property Rights'
	group by DATEPART(year, [customsSeizuresDate]),  DATEPART(month, [customsSeizuresDate])
	order by DATEPART(year, [customsSeizuresDate]) asc, DATEPART(month, [customsSeizuresDate]) asc

END
GO


drop procedure IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TimeForSolvingAppealsThroughAdministrativeActionQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_TimeForSolvingAppealsThroughAdministrativeActionQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @controlRevenue as table([year] int, [quarter] int, amount money)
	declare @totalRevenue as table([year] int, [quarter] int, amount money)

	SELECT DATEPART(year, [customsAppealsDate]) as year, 
		DATEPART(quarter, [customsAppealsDate]) as quarter,
		avg([averageTimeToResolveAppeals]) timeToResolve
	FROM [dbo].[CustomsAppeals]
	where DATEPART(year, [customsAppealsDate]) > @startYear 
		and DATEPART(year, [customsAppealsDate]) <= @limitYear
	group by DATEPART(year, [customsAppealsDate]), DATEPART(quarter, [customsAppealsDate])
	order by DATEPART(year, [customsAppealsDate]) asc, DATEPART(quarter, [customsAppealsDate]) asc

END
GO


drop procedure IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AdministrativeAppealResolutionRateQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AdministrativeAppealResolutionRateQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @controlRevenue as table([year] int, [quarter] int, amount money)
	declare @totalRevenue as table([year] int, [quarter] int, amount money)

	SELECT DATEPART(year, [customsAppealsDate]) as year, 
		DATEPART(quarter, [customsAppealsDate]) as quarter,
		case when isnull(sum([numberAppealsConsidered]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberAppealsResolved])) / 
			convert(decimal(18,3), sum([numberAppealsConsidered])) * 100 
		end as appealsResolved
	FROM [dbo].[CustomsAppeals]
	where DATEPART(year, [customsAppealsDate]) > @startYear 
		and DATEPART(year, [customsAppealsDate]) <= @limitYear
	group by DATEPART(year, [customsAppealsDate]), DATEPART(quarter, [customsAppealsDate])
	order by DATEPART(year, [customsAppealsDate]) asc, DATEPART(quarter, [customsAppealsDate]) asc

END
GO


drop procedure IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RatioOfAppealsThatUndergoLegalActionQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RatioOfAppealsThatUndergoLegalActionQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @controlRevenue as table([year] int, [quarter] int, amount money)
	declare @totalRevenue as table([year] int, [quarter] int, amount money)

	SELECT DATEPART(year, [customsAppealsDate]) as year, 
		DATEPART(quarter, [customsAppealsDate]) as quarter,
		case when isnull(sum([numberAppealsConsidered]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberAppealsUndergoLegalAction])) / 
			convert(decimal(18,3), sum([numberAppealsConsidered])) * 100 
		end as appealsUdergoLegalAction
	FROM [dbo].[CustomsAppeals]
	where DATEPART(year, [customsAppealsDate]) > @startYear 
		and DATEPART(year, [customsAppealsDate]) <= @limitYear
	group by DATEPART(year, [customsAppealsDate]), DATEPART(quarter, [customsAppealsDate])
	order by DATEPART(year, [customsAppealsDate]) asc, DATEPART(quarter, [customsAppealsDate]) asc

END
GO


drop procedure IF EXISTS [dbo].[KPI_DATA_CUSTOMS_JudicialDecisionInFavorOfCustomsAdministrationQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_JudicialDecisionInFavorOfCustomsAdministrationQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @controlRevenue as table([year] int, [quarter] int, amount money)
	declare @totalRevenue as table([year] int, [quarter] int, amount money)

	SELECT DATEPART(year, [customsAppealsDate]) as year, 
		DATEPART(quarter, [customsAppealsDate]) as quarter,
		case when isnull(sum([numberLawsuitsFinished]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberJudicialDecissionsFavorableCustoms])) / 
			convert(decimal(18,3), sum([numberLawsuitsFinished])) * 100 
		end as favorableToCustoms
	FROM [dbo].[CustomsAppeals]
	where DATEPART(year, [customsAppealsDate]) > @startYear 
		and DATEPART(year, [customsAppealsDate]) <= @limitYear
	group by DATEPART(year, [customsAppealsDate]), DATEPART(quarter, [customsAppealsDate])
	order by DATEPART(year, [customsAppealsDate]) asc, DATEPART(quarter, [customsAppealsDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AdvanceRulingRequestOnOriginMonthly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AdvanceRulingRequestOnOriginMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsAdvancedRulingsDate]) as year, 
		DATEPART(month, [customsAdvancedRulingsDate]) as month,
		CASE WHEN ISNULL(SUM([numberRulingRequests]),0) = 0 THEN 0.00 ELSE
			CONVERT(DECIMAL(18,3), SUM([numberRulingRequestsOrigin])) / 
			CONVERT(DECIMAL(18,3), SUM([numberRulingRequests])) * 100.00
		END AS originRequests
	from [dbo].[CustomsAdvancedRulings]
	where [customsAdvancedRulingsDate] > @startDate
	group by DATEPART(year, [customsAdvancedRulingsDate]),  DATEPART(month, [customsAdvancedRulingsDate])
	order by DATEPART(year, [customsAdvancedRulingsDate]) asc,  DATEPART(month, [customsAdvancedRulingsDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AdvanceRulingRequestOnClassificationMonthly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AdvanceRulingRequestOnClassificationMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsAdvancedRulingsDate]) as year, 
		DATEPART(month, [customsAdvancedRulingsDate]) as month,
		CASE WHEN ISNULL(SUM([numberRulingRequests]),0) = 0 THEN 0.00 ELSE
			CONVERT(DECIMAL(18,3), SUM([numberRulingRequestsTariff])) / 
			CONVERT(DECIMAL(18,3), SUM([numberRulingRequests])) * 100.00
		END AS classificationRequests
	from [dbo].[CustomsAdvancedRulings]
	where [customsAdvancedRulingsDate] > @startDate
	group by DATEPART(year, [customsAdvancedRulingsDate]),  DATEPART(month, [customsAdvancedRulingsDate])
	order by DATEPART(year, [customsAdvancedRulingsDate]) asc,  DATEPART(month, [customsAdvancedRulingsDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_IncreaseInAEOCertificationsQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_IncreaseInAEOCertificationsQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, number int)

	insert into @tab
	select DATEPART(year, [customsAEODate]) as [year], 
		DATEPART(quarter, [customsAEODate]) as [quarter],
		sum([numberEnterprisesWithAEOCert])+sum([numberSMEEnterprisesWithAEOCert])
	from [dbo].[CustomsAEO]
	where DATEPART(year, [customsAEODate]) > @startYear 
		and DATEPART(year, [customsAEODate]) <= @limitYear
	group by DATEPART(year, [customsAEODate]),  DATEPART(quarter, [customsAEODate])

	select t1.year, t1.quarter, case when t2.number is null or t2.number = 0 then 0.00 else
		convert(decimal(18,3), (t1.number - t2.number))/ convert(decimal(18,3), t2.number)*100.00 end as increase
	from @tab as t1
	join @tab as t2 on (t1.year -1) = t2.year and t1.quarter = t2.quarter
	order by t1.[year] asc , t1.[quarter] asc

END
Go


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_IncreaseInAEOCertificationsByEntityGroupQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_IncreaseInAEOCertificationsByEntityGroupQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, [AEOEntityGroup] nvarchar(250), number int)

	insert into @tab
	select DATEPART(year, [customsAEODate]) as [year], 
		DATEPART(quarter, [customsAEODate]) as [quarter],
		[AEOEntityGroup],
		sum([numberEnterprisesWithAEOCert])+sum([numberSMEEnterprisesWithAEOCert])
	from [dbo].[CustomsAEO]
	where DATEPART(year, [customsAEODate]) > @startYear 
		and DATEPART(year, [customsAEODate]) <= @limitYear
	group by DATEPART(year, [customsAEODate]),  DATEPART(quarter, [customsAEODate]), [AEOEntityGroup]

	select t1.year, t1.quarter, case when t2.number is null or t2.number = 0 then 0.00 else
		convert(decimal(18,3), (t1.number - t2.number))/ convert(decimal(18,3), t2.number)*100.00 end as increase,
		t1.AEOEntityGroup
	from @tab as t1
	join @tab as t2 on (t1.year -1) = t2.year and t1.quarter = t2.quarter and t1.AEOEntityGroup = t2.AEOEntityGroup
	order by t1.[year] asc , t1.[quarter] asc

END
Go



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_SMEInTheTotalNumberOfAEOsQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_SMEInTheTotalNumberOfAEOsQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, [AEOEntityGroup] nvarchar(250), number int)

	select DATEPART(year, [customsAEODate]) as [year], 
		DATEPART(quarter, [customsAEODate]) as [quarter],
		case when isnull(sum([numberEnterprisesWithAEOCert]),0) = 0 then 0.00 else 
			convert(decimal(18,3), sum([numberSMEEnterprisesWithAEOCert])) / 
			convert(decimal(18,3), sum([numberEnterprisesWithAEOCert]))  * 100
		end as ratio
	from [dbo].[CustomsAEO]
	where DATEPART(year, [customsAEODate]) > @startYear 
		and DATEPART(year, [customsAEODate]) <= @limitYear
	group by DATEPART(year, [customsAEODate]),  DATEPART(quarter, [customsAEODate])
	order by DATEPART(year, [customsAEODate]) asc,  DATEPART(quarter, [customsAEODate]) asc

END
Go



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AverageTimeToObtainAEOCertificationQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_AverageTimeToObtainAEOCertificationQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, [AEOEntityGroup] nvarchar(250), number int)

	select DATEPART(year, [customsAEODate]) as [year], 
		DATEPART(quarter, [customsAEODate]) as [quarter],
		avg([accreditationAvergeTime]) as accreditationAvergeTime
	from [dbo].[CustomsAEO]
	where DATEPART(year, [customsAEODate]) > @startYear 
		and DATEPART(year, [customsAEODate]) <= @limitYear
	group by DATEPART(year, [customsAEODate]),  DATEPART(quarter, [customsAEODate])
	order by DATEPART(year, [customsAEODate]) asc,  DATEPART(quarter, [customsAEODate]) asc

END
Go



