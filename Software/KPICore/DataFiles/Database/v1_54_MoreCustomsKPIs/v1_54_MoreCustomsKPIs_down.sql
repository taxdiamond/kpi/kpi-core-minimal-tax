SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSAPPEALS_ReportCustomsAppealsData]
	@dateReported date,
	@numberAppealsConsidered int,
	@numberAppealsResolved int,
	@numberAppealsUndergoLegalAction int,
	@numberJudicialDecissionsFavorableCustoms int,
	@numberLawsuitsFinished int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsAppeals]
			   ([customsAppealsDate]
			   ,[numberAppealsConsidered]
			   ,[numberAppealsResolved]
			   ,[numberAppealsUndergoLegalAction]
			   ,[numberJudicialDecissionsFavorableCustoms]
			   ,[numberLawsuitsFinished])
	 VALUES
			   (@dateReported
			   ,@numberAppealsConsidered
			   ,@numberAppealsResolved
			   ,@numberAppealsUndergoLegalAction
			   ,@numberJudicialDecissionsFavorableCustoms
			   ,@numberLawsuitsFinished)

END
GO


drop procedure IF EXISTS [dbo].[KPI_DATA_CUSTOMS_IPRSeizuresMonthlyGlobal]
drop procedure IF EXISTS [dbo].[KPI_DATA_CUSTOMS_TimeForSolvingAppealsThroughAdministrativeActionQuarterly]
drop procedure IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AdministrativeAppealResolutionRateQuarterly]
drop procedure IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RatioOfAppealsThatUndergoLegalActionQuarterly]
drop procedure IF EXISTS [dbo].[KPI_DATA_CUSTOMS_JudicialDecisionInFavorOfCustomsAdministrationQuarterly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AdvanceRulingRequestOnOriginMonthly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AdvanceRulingRequestOnClassificationMonthly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_IncreaseInAEOCertificationsQuarterly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_IncreaseInAEOCertificationsByEntityGroupQuarterly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_SMEInTheTotalNumberOfAEOsQuarterly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_AverageTimeToObtainAEOCertificationQuarterly]
GO
