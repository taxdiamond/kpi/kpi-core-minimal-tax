SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 8 2020
-- =============================================
ALTER PROCEDURE [dbo].[APPEALS_ReportAppealsData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@numberAppealsCasesPending int,
	@ammountAppealed money,
	@numberCasesResolved int,
	@numberCasesResolvedFavorTaxPayer int,
	@numberCasesResolvedFavorTaxAdministration int,
	@averageDaysToResolveCases int,
	@totalAmountResolvedFavorTaxPayer money,
	@totalAmountResolvedFavorTaxAdministration money,
	@numberOfAppealCasesSubmitted int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @regionID = NULL

	INSERT INTO [dbo].[Appeals]
           ([appealsDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[numberAppealsCasesPending]
           ,[amountAppealed]
           ,[numberCasesResolved]
           ,[numberCasesResolvedFavorTaxPayer]
           ,[numberCasesResolvedFavorTaxAdministration]
           ,[averageDaysToResolveCases]
           ,[totalAmountResolvedFavorTaxPayer]
           ,[totalAmountResolvedFavorTaxAdministration]
		   ,[numberOfAppealCasesSubmitted])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@numberAppealsCasesPending
           ,@ammountAppealed
           ,@numberCasesResolved
           ,@numberCasesResolvedFavorTaxPayer
           ,@numberCasesResolvedFavorTaxAdministration
           ,@averageDaysToResolveCases
           ,@totalAmountResolvedFavorTaxPayer
           ,@totalAmountResolvedFavorTaxAdministration
		   ,@numberOfAppealCasesSubmitted)
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedYearlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedMonthlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedMonthly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeYearlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeMonthlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeMonthly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedYearlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedMonthlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedMonthly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeYearlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeMonthlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeMonthly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentYearlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentYearly]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentMonthlyByFilter]
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentMonthly]
GO
