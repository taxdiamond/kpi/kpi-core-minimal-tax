SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 8 2020
-- =============================================
ALTER PROCEDURE [dbo].[APPEALS_ReportAppealsData]
	@dateReported date,
	@regionID nvarchar(10),
	@officeID nvarchar(10),
	@segmentID nvarchar(10),
	@concept nvarchar(200),
	@conceptID nvarchar(200),
	@numberAppealsCasesPending int,
	@ammountAppealed money,
	@numberCasesResolved int,
	@numberCasesResolvedFavorTaxPayer int,
	@numberCasesResolvedFavorTaxAdministration int,
	@averageDaysToResolveCases int,
	@totalAmountResolvedFavorTaxPayer money,
	@totalAmountResolvedFavorTaxAdministration money,
	@numberOfAppealCasesSubmitted int,
	@valueOfAppealCasesSubmitted decimal(18,2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@regionID = '')
		set @regionID = NULL

	if(@officeID = '')
		set @officeID = NULL

	if(@segmentID = '')
		set @segmentID = NULL

	if(@concept = '')
		set @concept = NULL

	if(@conceptID = '')
		set @regionID = NULL

	INSERT INTO [dbo].[Appeals]
           ([appealsDate]
           ,[regionID]
           ,[officeID]
           ,[segmentID]
           ,[concept]
           ,[conceptID]
           ,[numberAppealsCasesPending]
           ,[amountAppealed]
           ,[numberCasesResolved]
           ,[numberCasesResolvedFavorTaxPayer]
           ,[numberCasesResolvedFavorTaxAdministration]
           ,[averageDaysToResolveCases]
           ,[totalAmountResolvedFavorTaxPayer]
           ,[totalAmountResolvedFavorTaxAdministration]
		   ,[numberOfAppealCasesSubmitted]
		   ,[valueOfAppealCasesSubmitted])
     VALUES
           (@dateReported
           ,@regionID
           ,@officeID
           ,@segmentID
           ,@concept
           ,@conceptID
           ,@numberAppealsCasesPending
           ,@ammountAppealed
           ,@numberCasesResolved
           ,@numberCasesResolvedFavorTaxPayer
           ,@numberCasesResolvedFavorTaxAdministration
           ,@averageDaysToResolveCases
           ,@totalAmountResolvedFavorTaxPayer
           ,@totalAmountResolvedFavorTaxAdministration
		   ,@numberOfAppealCasesSubmitted
		   ,@valueOfAppealCasesSubmitted)

END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc' 
	
	execute(@query)
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate])
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc'

	execute(@query)
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where [appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate])
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where concept = ''Tax'' and
			DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where concept = 'Tax' and
			DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		[conceptID] as taxType,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where concept = ''Tax'' and
			[appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, [conceptID] asc'
	
	execute(@query)
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		[conceptID] as taxType,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals]
	where concept = 'Tax' and
			[appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, [conceptID] asc

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		s.segmentName as segmentName,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, segmentName asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesSubmittedByTaxPayerSegmentMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		s.segmentName as segmentName,
		sum([valueOfAppealCasesSubmitted]) as valueAppealsSubmitted
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, segmentName asc

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc' 
	
	execute(@query)
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]) 
	order by datepart(year, [appealsDate]) asc

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate])
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc'

	execute(@query)
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where [appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate])
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where concept = ''Tax'' and
			DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc'

	execute(@query)
END
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		[conceptID] as taxType,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where concept = 'Tax' and
			DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, [conceptID] asc

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		[conceptID] as taxType,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where concept = ''Tax'' and
			[appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, [conceptID] asc'
	
	execute(@query)
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxTypeMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		[conceptID] as taxType,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals]
	where concept = 'Tax' and
			[appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), [conceptID]
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, [conceptID] asc

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentYearlyByFilter]
	@LimitYear int,
	@NumberOfYears int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > '+convert(varchar, @startYear) + '
			and DATEPART(year, [appealsDate]) <= '+convert(varchar, @limitYear)+' and '+@WhereClause +'
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentYearly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	select 
		datepart(year, [appealsDate]) as year, 
		s.segmentName as segmentName,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where DATEPART(year, [appealsDate]) > (@startYear) 
			and DATEPART(year, [appealsDate]) <= @limitYear
	group by datepart(year, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, segmentName asc

END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentMonthlyByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @query as nvarchar(MAX)

	select @query = 'select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		s.segmentName as segmentName,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [appealsDate] >= ''' + convert(varchar,@startDate, 126) + ''' and '+@WhereClause+'  
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, segmentName asc'

	execute(@query)
END
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: June 20 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_ValueOfTaxAppealsCasesResolvedByTaxPayerSegmentMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- StartDate is X months back... starting from the first of the month X months ago
	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	select 
		datepart(year, [appealsDate]) as year, 
		datepart(month, [appealsDate]) as month, 
		s.segmentName as segmentName,
		sum([totalAmountResolvedFavorTaxPayer]+[totalAmountResolvedFavorTaxAdministration]) as valueAppealsResolved
	from [dbo].[Appeals] as a
	join [dbo].[Segment] as s on s.segmentID = a.segmentID
	where [appealsDate] >= @startdate
	group by datepart(year, [appealsDate]), datepart(month, [appealsDate]), segmentName
	order by datepart(year, [appealsDate]) asc, datepart(month, [appealsDate]) asc, segmentName asc

END
GO


