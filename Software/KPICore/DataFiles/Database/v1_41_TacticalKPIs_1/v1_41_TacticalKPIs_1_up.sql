DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerDocumentaryGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerDocumentaryGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsStaffStatsDate]) as year, 
		DATEPART(month, [customsStaffStatsDate]) as month,
		case 
			when sum([numberOfDocumentaryInspections]) = 0 then 0 
			else sum(convert(decimal(18,3), [numberOfDetectionsForDocumentaryInspections])) / 
				sum(convert(decimal(18,3),[numberOfDocumentaryInspections])) * 100 
		end as effectiveness
	from [dbo].[CustomsStaffStats]
	where [customsStaffStatsDate] > @startDate
	group by DATEPART(year, [customsStaffStatsDate]),  DATEPART(month, [customsStaffStatsDate])
	order by DATEPART(year, [customsStaffStatsDate]) asc,  DATEPART(month, [customsStaffStatsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerDocumentaryByFilter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerDocumentaryByFilter]
	@NumberOfMonths int,
 	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [customsStaffStatsDate]) as year, 
		DATEPART(month, [customsStaffStatsDate]) as month,
		case 
			when sum([numberOfDocumentaryInspections]) = 0 then 0 
			else sum(convert(decimal(18,3), [numberOfDetectionsForDocumentaryInspections])) / 
				sum(convert(decimal(18,3),[numberOfDocumentaryInspections])) * 100 
		end as effectiveness
	from [dbo].[CustomsStaffStats]
	where [customsStaffStatsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
	and '+@WhereClause+'  
	group by DATEPART(year, [customsStaffStatsDate]),  DATEPART(month, [customsStaffStatsDate])
	order by DATEPART(year, [customsStaffStatsDate]) asc,  DATEPART(month, [customsStaffStatsDate]) asc'

	exec(@query)
END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerPhysicalGlobal]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerPhysicalGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsStaffStatsDate]) as year, 
		DATEPART(month, [customsStaffStatsDate]) as month,
		case 
			when sum([numberOfPhysicalInspections]) = 0 then 0 
			else sum(convert(decimal(18,3), [numberOfDetectionsForPhysicalInspections])) / 
				sum(convert(decimal(18,3),[numberOfPhysicalInspections])) * 100 
		end as effectiveness
	from [dbo].[CustomsStaffStats]
	where [customsStaffStatsDate] > @startDate
	group by DATEPART(year, [customsStaffStatsDate]),  DATEPART(month, [customsStaffStatsDate])
	order by DATEPART(year, [customsStaffStatsDate]) asc,  DATEPART(month, [customsStaffStatsDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerPhysicalByFilter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerPhysicalByFilter]
	@NumberOfMonths int,
 	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [customsStaffStatsDate]) as year, 
		DATEPART(month, [customsStaffStatsDate]) as month,
		case 
			when sum([numberOfPhysicalInspections]) = 0 then 0 
			else sum(convert(decimal(18,3), [numberOfDetectionsForPhysicalInspections])) / 
				sum(convert(decimal(18,3),[numberOfPhysicalInspections])) * 100 
		end as effectiveness
	from [dbo].[CustomsStaffStats]
	where [customsStaffStatsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
	and '+@WhereClause+'  
	group by DATEPART(year, [customsStaffStatsDate]),  DATEPART(month, [customsStaffStatsDate])
	order by DATEPART(year, [customsStaffStatsDate]) asc,  DATEPART(month, [customsStaffStatsDate]) asc'

	exec(@query)
END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByRiskAnalystGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByRiskAnalystGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsStaffStatsDate]) as year, 
		DATEPART(month, [customsStaffStatsDate]) as month,
		case 
			when sum([numberOfRiskAlertsReceived]) = 0 then 0 
			else sum(convert(decimal(18,3), [numberOfDetectionsForRiskAlerts])) / 
				sum(convert(decimal(18,3),[numberOfRiskAlertsReceived])) * 100 
		end as effectiveness
	from [dbo].[CustomsStaffStats]
	where [customsStaffStatsDate] > @startDate
	group by DATEPART(year, [customsStaffStatsDate]),  DATEPART(month, [customsStaffStatsDate])
	order by DATEPART(year, [customsStaffStatsDate]) asc,  DATEPART(month, [customsStaffStatsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByRiskAnalystByFilter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByRiskAnalystByFilter]
	@NumberOfMonths int,
 	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [customsStaffStatsDate]) as year, 
		DATEPART(month, [customsStaffStatsDate]) as month,
		case 
			when sum([numberOfRiskAlertsReceived]) = 0 then 0 
			else sum(convert(decimal(18,3), [numberOfDetectionsForRiskAlerts])) / 
				sum(convert(decimal(18,3),[numberOfRiskAlertsReceived])) * 100 
		end as effectiveness
	from [dbo].[CustomsStaffStats]
	where [customsStaffStatsDate] >= ''' + convert(varchar,@startDate, 126) + ''' 
	and '+@WhereClause+'  
	group by DATEPART(year, [customsStaffStatsDate]),  DATEPART(month, [customsStaffStatsDate])
	order by DATEPART(year, [customsStaffStatsDate]) asc,  DATEPART(month, [customsStaffStatsDate]) asc'

	exec(@query)
END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RedFlaggedOperationsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RedFlaggedOperationsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @cop as table(year int, month int, [numberOfImportDeclarations] int, 
		[numberOfExportDeclarations] int)

	insert into @cop
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		sum([numberOfImportDeclarations]),
		sum([numberOfExportDeclarations])
	from [dbo].[CustomsOperations]
	where [customsOperationsDate] > @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])

	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		CASE WHEN ISNULL(SUM(c.numberOfExportDeclarations) + SUM(c.numberOfImportDeclarations), 0) = 0 THEN
			0.00
		ELSE 
			CONVERT(DECIMAL(18,3), SUM([numberRedFlaggedOperations]))/
			( 
		      CONVERT(DECIMAL(18,3),SUM(c.numberOfExportDeclarations)) + 
			  CONVERT(DECIMAL(18,3),SUM(c.numberOfImportDeclarations)) 
			) * 100.00
		END AS ratio
	from [dbo].[CustomsInspections] as i
	join @cop as c on c.month = DATEPART(month, i.[customsInspectionsDate])
		and c.year = DATEPART(year, i.[customsInspectionsDate])
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_YellowFlaggedOperationsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_YellowFlaggedOperationsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @cop as table(year int, month int, [numberOfImportDeclarations] int, 
		[numberOfExportDeclarations] int)

	insert into @cop
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		sum([numberOfImportDeclarations]),
		sum([numberOfExportDeclarations])
	from [dbo].[CustomsOperations]
	where [customsOperationsDate] > @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])

	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		CASE WHEN ISNULL(SUM(c.numberOfExportDeclarations) + SUM(c.numberOfImportDeclarations), 0) = 0 THEN
			0.00
		ELSE 
			CONVERT(DECIMAL(18,3), SUM([numberYellowFlaggedOperations]))/
			( 
		      CONVERT(DECIMAL(18,3),SUM(c.numberOfExportDeclarations)) + 
			  CONVERT(DECIMAL(18,3),SUM(c.numberOfImportDeclarations)) 
			) * 100.00
		END AS ratio
	from [dbo].[CustomsInspections] as i
	join @cop as c on c.month = DATEPART(month, i.[customsInspectionsDate])
		and c.year = DATEPART(year, i.[customsInspectionsDate])
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate]) asc

END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_EffectivenesOfRedFlaggedOperationsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EffectivenesOfRedFlaggedOperationsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		CASE WHEN ISNULL(SUM([numberRedFlaggedOperations]), 0) = 0 THEN
			0.00
		ELSE 
			CONVERT(DECIMAL(18,3), SUM([numberOfDetectionsRedFlaggedOperations]))/
			CONVERT(DECIMAL(18,3),SUM([numberRedFlaggedOperations])) * 100.00
		END AS ratio
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_EffectivenesOfYellowFlaggedOperationsGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EffectivenesOfYellowFlaggedOperationsGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		CASE WHEN ISNULL(SUM([numberYellowFlaggedOperations]), 0) = 0 THEN
			0.00
		ELSE 
			CONVERT(DECIMAL(18,3), SUM([numberOfDetectionsYellowFlaggedOperations]))/
			CONVERT(DECIMAL(18,3),SUM([numberYellowFlaggedOperations])) * 100.00
		END AS ratio
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate]) asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_FalsePositivesGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_FalsePositivesGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsInspectionsDate]) as year, 
		DATEPART(month, [customsInspectionsDate]) as month,
		CASE WHEN ISNULL(SUM([numberFalsePositivesForAllInspections]) + SUM([numberTrueNegativesForAllInspections]), 0) = 0 THEN
			0.00
		ELSE 
			CONVERT(DECIMAL(18,3), SUM([numberFalsePositivesForAllInspections])) /
			CONVERT(DECIMAL(18,3),(SUM([numberFalsePositivesForAllInspections]) + SUM([numberTrueNegativesForAllInspections]))) * 100.00
		END AS ratio
	from [dbo].[CustomsInspections]
	where [customsInspectionsDate] > @startDate
	group by DATEPART(year, [customsInspectionsDate]),  DATEPART(month, [customsInspectionsDate])
	order by DATEPART(year, [customsInspectionsDate]) asc,  DATEPART(month, [customsInspectionsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_DocumentaryInspectionsByComplianceOfficerGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_DocumentaryInspectionsByComplianceOfficerGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsStaffStatsDate]) as year, 
		DATEPART(month, [customsStaffStatsDate]) as month,
		sum([numberOfDocumentaryInspections]) as inspections
	from [dbo].[CustomsStaffStats]
	where [customsStaffStatsDate] > @startDate
	group by DATEPART(year, [customsStaffStatsDate]),  DATEPART(month, [customsStaffStatsDate])
	order by DATEPART(year, [customsStaffStatsDate]) asc,  DATEPART(month, [customsStaffStatsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_DocumentaryInspectionsByComplianceOfficerByFilter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_DocumentaryInspectionsByComplianceOfficerByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [customsStaffStatsDate]) as year, 
		DATEPART(month, [customsStaffStatsDate]) as month,
		sum([numberOfDocumentaryInspections]) as inspections
	from [dbo].[CustomsStaffStats]
	where [customsStaffStatsDate] > ''' + convert(varchar,@startDate, 126) + ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [customsStaffStatsDate]),  DATEPART(month, [customsStaffStatsDate])
	order by DATEPART(year, [customsStaffStatsDate]) asc,  DATEPART(month, [customsStaffStatsDate]) asc'

	execute(@query)
END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PhysicalInspectionsByComplianceOfficerGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PhysicalInspectionsByComplianceOfficerGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsStaffStatsDate]) as year, 
		DATEPART(month, [customsStaffStatsDate]) as month,
		sum([numberOfPhysicalInspections]) as inspections
	from [dbo].[CustomsStaffStats]
	where [customsStaffStatsDate] > @startDate
	group by DATEPART(year, [customsStaffStatsDate]),  DATEPART(month, [customsStaffStatsDate])
	order by DATEPART(year, [customsStaffStatsDate]) asc,  DATEPART(month, [customsStaffStatsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PhysicalInspectionsByComplianceOfficerByFilter]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PhysicalInspectionsByComplianceOfficerByFilter]
	@NumberOfMonths int,
	@WhereClause nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @query as nvarchar(MAX)

	select @query = 'select DATEPART(year, [customsStaffStatsDate]) as year, 
		DATEPART(month, [customsStaffStatsDate]) as month,
		sum([numberOfPhysicalInspections]) as inspections
	from [dbo].[CustomsStaffStats]
	where [customsStaffStatsDate] > ''' + convert(varchar,@startDate, 126) + ''' 
		and '+@WhereClause +' 
	group by DATEPART(year, [customsStaffStatsDate]),  DATEPART(month, [customsStaffStatsDate])
	order by DATEPART(year, [customsStaffStatsDate]) asc,  DATEPART(month, [customsStaffStatsDate]) asc'

	execute(@query)
END
GO


