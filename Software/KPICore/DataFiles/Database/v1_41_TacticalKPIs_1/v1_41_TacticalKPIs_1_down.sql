DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerDocumentaryGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerDocumentaryByFilter]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerPhysicalGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByCompliancePortOfficerPhysicalByFilter]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByRiskAnalystGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PercentEffectivenessByRiskAnalystByFilter]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RedFlaggedOperationsGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_YellowFlaggedOperationsGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_EffectivenesOfRedFlaggedOperationsGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_EffectivenesOfYellowFlaggedOperationsGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_FalsePositivesGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_DocumentaryInspectionsByComplianceOfficerGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_DocumentaryInspectionsByComplianceOfficerByFilter]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PhysicalInspectionsByComplianceOfficerGlobal]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PhysicalInspectionsByComplianceOfficerByFilter]
GO








