DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CustomsAverageYieldInPostClearanceAuditsGlobalMonthly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CollectionGrowthRateFromPostClearanceAuditsGlobalMonthly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromCustomsControlsQuarterly]
DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromPostClearanceAuditsQuarterly]

