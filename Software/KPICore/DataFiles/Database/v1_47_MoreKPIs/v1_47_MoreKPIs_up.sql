DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CustomsAverageYieldInPostClearanceAuditsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CustomsAverageYieldInPostClearanceAuditsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	SELECT DATEPART(year, [customsAuditDate]) as year, 
		DATEPART(month, [customsAuditDate]) as month,
		case when sum([numberAuditsMade]) > 0 then sum([totalAmountAssessedFromAudits]) / sum([numberAuditsMade]) else 0.00 end as amountPerAudit
	FROM [dbo].[CustomsAudit]
	where [customsAuditDate] >= @startDate
	group by DATEPART(year, [customsAuditDate]), DATEPART(month, [customsAuditDate])
	order by DATEPART(year, [customsAuditDate]) asc, DATEPART(month, [customsAuditDate]) asc


END
GO



DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CollectionGrowthRateFromPostClearanceAuditsGlobalMonthly]
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CollectionGrowthRateFromPostClearanceAuditsGlobalMonthly]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  

	declare @tab as table([year] int, [month] int, amount money)

	insert into @tab
	SELECT DATEPART(year, [customsAuditDate]) as year, 
		DATEPART(month, [customsAuditDate]) as month,
		sum([totalAmountAssessedFromAudits])
	FROM [dbo].[CustomsAudit]
	where [customsAuditDate] >= @startDate
	group by DATEPART(year, [customsAuditDate]), DATEPART(month, [customsAuditDate])

	select t1.year, t1.month, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as collectionGrowth
	from @tab as t1
	join @tab as t2 on t1.year*12+t1.month-1 = t2.year*12+t2.month 
	order by t1.[year] asc , t1.[month] asc


END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromCustomsControlsQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromCustomsControlsQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @controlRevenue as table([year] int, [quarter] int, amount money)
	declare @totalRevenue as table([year] int, [quarter] int, amount money)

	insert into @controlRevenue
	SELECT DATEPART(year, [customsRevenueDate]), 
		DATEPART(quarter, [customsRevenueDate]),
		sum([amount])
	FROM [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
		and concept='Control'
	group by DATEPART(year, [customsRevenueDate]), DATEPART(quarter, [customsRevenueDate])

	insert into @totalRevenue
	SELECT DATEPART(year, [customsRevenueDate]), 
		DATEPART(quarter, [customsRevenueDate]),
		sum([amount])
	FROM [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
	group by DATEPART(year, [customsRevenueDate]), DATEPART(quarter, [customsRevenueDate])

	select t1.year, t1.quarter, 
	case when t1.amount > 0 then t2.amount / t1.amount * 100.00 else 0.00 end as ratio
	from @totalRevenue as t1
	join @controlRevenue as t2 on t1.year = t2.year and t1.quarter = t2.quarter
	order by t1.year asc, t1.quarter asc

END
GO




DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromPostClearanceAuditsQuarterly]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: Sept 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_RateOfCollectionFromPostClearanceAuditsQuarterly]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears

	declare @auditRevenue as table([year] int, [quarter] int, amount money)
	declare @totalRevenue as table([year] int, [quarter] int, amount money)

	insert into @auditRevenue
	SELECT DATEPART(year, [customsRevenueDate]) as year, 
		DATEPART(quarter, [customsRevenueDate]) as month,
		sum([amount])
	FROM [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
		and concept='Audit'
	group by DATEPART(year, [customsRevenueDate]), DATEPART(quarter, [customsRevenueDate])

	insert into @totalRevenue
	SELECT DATEPART(year, [customsRevenueDate]) as year, 
		DATEPART(quarter, [customsRevenueDate]) as month,
		sum([amount])
	FROM [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
	group by DATEPART(year, [customsRevenueDate]), DATEPART(quarter, [customsRevenueDate])

	select t1.year, t1.quarter, 
	case when t1.amount > 0 then t2.amount / t1.amount * 100.00 else 0.00 end as ratio
	from @totalRevenue as t1
	join @auditRevenue as t2 on t1.year = t2.year and t1.quarter = t2.quarter
	order by t1.year asc, t1.quarter asc

END
GO