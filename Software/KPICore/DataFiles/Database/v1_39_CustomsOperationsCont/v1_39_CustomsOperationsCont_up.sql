DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_FreeTradeZoneTransactionsGrowthRateGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_FreeTradeZoneTransactionsGrowthRateGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @tab as table([year] int, [month] int, operations int)

	insert into @tab
	select DATEPART(year, [customsOperationsDate]), 
		DATEPART(month, [customsOperationsDate]),
		sum([freeTradeZoneOperations]) as operations
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc

	select t1.year, t1.month, case when isnull(t2.operations,0) = 0 then 0.00 else
		convert(decimal(18,3), (t1.operations - t2.operations))/convert(decimal(18,3), t2.operations)*100.00 end as growth
	from @tab as t1
	join @tab as t2 on t1.year*12+t1.month-1 = t2.year*12+t2.month 
	order by t1.[year] asc , t1.month asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_FreeTradeZoneTransactionsGrowthByTransportMode]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_FreeTradeZoneTransactionsGrowthByTransportMode]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @tab as table([year] int, [month] int, modeOfTransport varchar(250), operations int)

	insert into @tab
	select DATEPART(year, [customsOperationsDate]), 
		DATEPART(month, [customsOperationsDate]),
		[modeOfTransport],
		sum([freeTradeZoneOperations]) as operations
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate]), modeOfTransport
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc, modeOfTransport asc

	select t1.year, t1.month, case when isnull(t2.operations,0) = 0 then 0.00 else
		convert(decimal(18,3), (t1.operations - t2.operations))/convert(decimal(18,3), t2.operations)*100.00 end as growth, 
		t1.modeOfTransport
	from @tab as t1
	join @tab as t2 on t1.year*12+t1.month-1 = t2.year*12+t2.month and t1.modeOfTransport = t2.modeOfTransport
	order by t1.[year] asc , t1.month asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PassengersGrowthRateGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PassengersGrowthRateGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @tab as table([year] int, [month] int, operations int)

	insert into @tab
	select DATEPART(year, [customsOperationsDate]), 
		DATEPART(month, [customsOperationsDate]),
		sum([passengersProcessed]) as operations
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc

	select t1.year, t1.month, case when isnull(t2.operations,0) = 0 then 0.00 else
		convert(decimal(18,3), (t1.operations - t2.operations))/convert(decimal(18,3), t2.operations)*100.00 end as growth
	from @tab as t1
	join @tab as t2 on t1.year*12+t1.month-1 = t2.year*12+t2.month 
	order by t1.[year] asc , t1.month asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_PassengersGrowthRateByTransportMode]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_PassengersGrowthRateByTransportMode]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	declare @tab as table([year] int, [month] int, modeOfTransport varchar(250), operations int)

	insert into @tab
	select DATEPART(year, [customsOperationsDate]), 
		DATEPART(month, [customsOperationsDate]),
		[modeOfTransport],
		sum([passengersProcessed]) as operations
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] >= @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate]), modeOfTransport
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc, modeOfTransport asc

	select t1.year, t1.month, case when isnull(t2.operations,0) = 0 then 0.00 else
		convert(decimal(18,3), (t1.operations - t2.operations))/convert(decimal(18,3), t2.operations)*100.00 end as growth, 
		t1.modeOfTransport
	from @tab as t1
	join @tab as t2 on t1.year*12+t1.month-1 = t2.year*12+t2.month and t1.modeOfTransport = t2.modeOfTransport
	order by t1.[year] asc , t1.month asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		AVG([clearanceTimeImports]) as dwellTime
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] > @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeByTransportMode]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeByTransportMode]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[modeOfTransport],
		avg([clearanceTimeImports]) as dwellTime
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] > @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate]), modeOfTransport
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc, modeOfTransport asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeRedLaneGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeRedLaneGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		AVG([clearanceTimeImportsRedLane]) as dwellTime
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] > @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeRedLaneByTransportMode]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeRedLaneByTransportMode]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[modeOfTransport],
		avg([clearanceTimeImportsRedLane]) as dwellTime
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] > @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate]), modeOfTransport
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc, modeOfTransport asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeYellowLaneGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeYellowLaneGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		AVG([clearanceTimeImportsYellowLane]) as dwellTime
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] > @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeYellowLaneByTransportMode]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeYellowLaneByTransportMode]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[modeOfTransport],
		avg([clearanceTimeImportsYellowLane]) as dwellTime
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] > @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate]), modeOfTransport
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc, modeOfTransport asc

END
GO


DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeGreenLaneGlobal]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeGreenLaneGlobal]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		AVG([clearanceTimeImportsGreenLane]) as dwellTime
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] > @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate])
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc

END
GO

DROP PROCEDURE IF EXISTS [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeGreenLaneByTransportMode]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: August 2020 
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_CargoDwellTimeGreenLaneByTransportMode]
	@NumberOfMonths int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startdate as date
	set @startdate = DATEFROMPARTS (
		DATEPART(year, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		DATEPART(month, dateadd(month, -1*@NumberOfMonths, GETDATE())),
		1)  
	
	select DATEPART(year, [customsOperationsDate]) as year, 
		DATEPART(month, [customsOperationsDate]) as month,
		[modeOfTransport],
		avg([clearanceTimeImportsGreenLane]) as dwellTime
	from [dbo].[CustomsOperationsByTransportMode]
	where [customsOperationsDate] > @startDate
	group by DATEPART(year, [customsOperationsDate]),  DATEPART(month, [customsOperationsDate]), modeOfTransport
	order by DATEPART(year, [customsOperationsDate]) asc,  DATEPART(month, [customsOperationsDate]) asc, modeOfTransport asc
END
GO


