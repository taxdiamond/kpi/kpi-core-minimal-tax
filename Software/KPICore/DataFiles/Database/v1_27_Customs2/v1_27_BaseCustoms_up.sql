SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EvolutionOfRevenueCollectionGlobal]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, amount money)

	insert into @tab
	select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(quarter, [customsRevenueDate]) as [quarter],
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
	group by DATEPART(year, [customsRevenueDate]),  DATEPART(quarter, [customsRevenueDate])

	select t1.year, t1.quarter, case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as evolutionOfRevenue
	from @tab as t1
	join @tab as t2 on (t1.year -1) = t2.year and t1.quarter = t2.quarter
	order by t1.[year] asc , t1.[quarter] asc

END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Ivan Krsul
-- Create date: April 2020
-- =============================================
CREATE PROCEDURE [dbo].[KPI_DATA_CUSTOMS_EvolutionOfRevenueCollectionByCustomsRevenueType]
	@LimitYear int,
	@NumberOfYears int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @startYear int
	select @startYear = @limitYear - @NumberOfYears
	
	declare @tab as table([year] int, [quarter] int, revenueType nvarchar(250), amount money)

	insert into @tab
	select DATEPART(year, [customsRevenueDate]) as [year], 
		DATEPART(quarter, [customsRevenueDate]) as [quarter],
		conceptID,
		sum([amount]) as amount
	from [dbo].[CustomsRevenue]
	where [concept] = 'Tax' 
		and DATEPART(year, [customsRevenueDate]) > @startYear 
		and DATEPART(year, [customsRevenueDate]) <= @limitYear
	group by [conceptID], 
		DATEPART(year, [customsRevenueDate]),  
		DATEPART(quarter, [customsRevenueDate])
	
	select t1.year, t1.quarter, t1.revenueType, 
		case when t2.amount is null or t2.amount = 0 then 0.00 else
		(t1.amount - t2.amount)/t2.amount*100 end as evolutionOfRevenue
	from @tab as t1
	join @tab as t2 on (t1.year -1) = t2.year and t1.quarter = t2.quarter and t1.revenueType = t2.revenueType

END
GO



