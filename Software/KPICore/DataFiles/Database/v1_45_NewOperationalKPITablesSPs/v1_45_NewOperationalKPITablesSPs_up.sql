SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 08/27/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSOPERATIONS_ReportCustomsOperationsData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@numberOfCustomsDeclarations int,
	@numberOfImportDeclarations int,
	@numberOfExportDeclarations int,
	@numberOfArrivedTransitOperations int, 
	@numberOfUnArrivedTransitOperations int,
	@numberOfDrawbackTransactions int,
	@numberOfImportDeclarationsPhysicallyExamined int,
	@numberOfDetectionsForPhysicalExaminationsImports int,
	@numberOfImportDeclarationsWithDocumentExamination int,
	@numberOfDetectionsForDocumentExaminationsImports int,
	@numberOfImportDeclarationsUnderAEOProgram int,
	@averageTimeAtTerminalSinceArrival decimal(18,3),
	@averageTimeForDocumentaryCompliance decimal(18,3),
	@numberBillsForOverstayedShipments int,
	@numberLabTestsForImports int,
	@numberPreArrivalClearancesForImports int,
	@numberReleasesPriorDeterminationAndPayments int,
	@numberSelfFilingOperationsForImports int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@customsPostID = '')
		set @customsPostID = NULL


	INSERT INTO [dbo].[CustomsOperations]
           ([customsOperationsDate]
           ,[customsPostID]
           ,[numberOfCustomsDeclarations]
           ,[numberOfImportDeclarations]
           ,[numberOfExportDeclarations]
           ,[numberOfArrivedTransitOperations]
           ,[numberOfUnArrivedTransitOperations]
           ,[numberOfDrawbackTransactions]
           ,[numberOfImportDeclarationsPhysicallyExamined]
           ,[numberOfDetectionsForPhysicalExaminationsImports]
           ,[numberOfImportDeclarationsWithDocumentExamination]
           ,[numberOfDetectionsForDocumentExaminationsImports]
           ,[numberOfImportDeclarationsUnderAEOProgram]
           ,[averageTimeAtTerminalSinceArrival]
           ,[averageTimeForDocumentaryCompliance]
           ,[numberBillsForOverstayedShipments]
           ,[numberLabTestsForImports]
           ,[numberPreArrivalClearancesForImports]
           ,[numberReleasesPriorDeterminationAndPayments]
           ,[numberSelfFilingOperationsForImports])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@numberOfCustomsDeclarations
           ,@numberOfImportDeclarations
           ,@numberOfExportDeclarations
           ,@numberOfArrivedTransitOperations
           ,@numberOfUnArrivedTransitOperations
           ,@numberOfDrawbackTransactions
           ,@numberOfImportDeclarationsPhysicallyExamined
           ,@numberOfDetectionsForPhysicalExaminationsImports
           ,@numberOfImportDeclarationsWithDocumentExamination
           ,@numberOfDetectionsForDocumentExaminationsImports
           ,@numberOfImportDeclarationsUnderAEOProgram
           ,@averageTimeAtTerminalSinceArrival
           ,@averageTimeForDocumentaryCompliance
           ,@numberBillsForOverstayedShipments
           ,@numberLabTestsForImports
           ,@numberPreArrivalClearancesForImports
           ,@numberReleasesPriorDeterminationAndPayments
           ,@numberSelfFilingOperationsForImports)

END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ivan Krsul
-- Create date: 09/03/2020
-- =============================================
ALTER PROCEDURE [dbo].[CUSTOMSINSPECTIONS_ReportCustomsInspectionsData]
	@dateReported date,
	@customsPostID nvarchar(20),
	@numberDocumentaryInspections int,
	@numberPhysicalInspections int,
	@numberRedFlaggedOperations int,
	@numberYellowFlaggedOperations int,
	@numberOfDetections int,
	@numberOfDetectionsRedFlaggedOperations int,
	@numberOfDetectionsYellowFlaggedOperations int,
	@numberFalsePositivesForAllInspections int,
	@numberTrueNegativesForAllInspections int,
	@averageDocumentaryInspectionsByComplianceOfficer decimal(18, 2),
	@averagePhysicalInspectionsByComplianceOfficer decimal(18, 2),
	@documentaryExaminationsBasedRiskSelectivity int,
	@physicalExaminationsBasedRiskSelectivity int,
	@documentaryExaminationsBasedReferralsRiskAnalysisUnit int,
	@physicalExaminationsBasedReferralsRiskAnalysisUnit int,
	@documentaryExaminationsBasedTipOffs int,
	@physicalExaminationsBasedTipOffs int,
	@alertsSentFromRiskAnalysisUnit int,
	@customsValueAlerts int,
	@contrabandAlerts int,
	@IPRAlerts int,
	@narcoticsAlerts int,
	@weaponsAlerts int,
	@cashAlerts int,
	@numberBorderSecurityDetections int,
	@numberCustomsValueDetections int,
	@numberIPRDetections int,
	@numberIncorrectOriginDetections int,
	@numberMisclassificationDetections int,
	@numberNonInstrusiveInspectionsDetections int,
	@numberOfDetectionsWithValueGreaterThanX int, 
	@numberRegulatoryNonComplianceDetections int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [dbo].[CustomsInspections]
           ([customsInspectionsDate]
           ,[customsPostID]
           ,[numberDocumentaryInspections]
           ,[numberPhysicalInspections]
           ,[numberRedFlaggedOperations]
           ,[numberYellowFlaggedOperations]
           ,[numberOfDetections]
           ,[numberOfDetectionsRedFlaggedOperations]
           ,[numberOfDetectionsYellowFlaggedOperations]
           ,[numberFalsePositivesForAllInspections]
           ,[numberTrueNegativesForAllInspections]
           ,[averageDocumentaryInspectionsByComplianceOfficer]
           ,[averagePhysicalInspectionsByComplianceOfficer]
           ,[documentaryExaminationsBasedRiskSelectivity]
           ,[physicalExaminationsBasedRiskSelectivity]
           ,[documentaryExaminationsBasedReferralsRiskAnalysisUnit]
           ,[physicalExaminationsBasedReferralsRiskAnalysisUnit]
           ,[documentaryExaminationsBasedTipOffs]
           ,[physicalExaminationsBasedTipOffs]
           ,[alertsSentFromRiskAnalysisUnit]
           ,[customsValueAlerts]
           ,[contrabandAlerts]
           ,[IPRAlerts]
           ,[narcoticsAlerts]
           ,[weaponsAlerts]
           ,[cashAlerts]
           ,[numberBorderSecurityDetections]
           ,[numberCustomsValueDetections]
           ,[numberIPRDetections]
           ,[numberIncorrectOriginDetections]
           ,[numberMisclassificationDetections]
           ,[numberNonInstrusiveInspectionsDetections]
           ,[numberOfDetectionsWithValueGreaterThanX]
           ,[numberRegulatoryNonComplianceDetections])
     VALUES
           (@dateReported
           ,@customsPostID
           ,@numberDocumentaryInspections
           ,@numberPhysicalInspections
           ,@numberRedFlaggedOperations
           ,@numberYellowFlaggedOperations
           ,@numberOfDetections
           ,@numberOfDetectionsRedFlaggedOperations
           ,@numberOfDetectionsYellowFlaggedOperations
           ,@numberFalsePositivesForAllInspections
           ,@numberTrueNegativesForAllInspections
           ,@averageDocumentaryInspectionsByComplianceOfficer
           ,@averagePhysicalInspectionsByComplianceOfficer
           ,@documentaryExaminationsBasedRiskSelectivity
           ,@physicalExaminationsBasedRiskSelectivity
           ,@documentaryExaminationsBasedReferralsRiskAnalysisUnit
           ,@physicalExaminationsBasedReferralsRiskAnalysisUnit
           ,@documentaryExaminationsBasedTipOffs
           ,@physicalExaminationsBasedTipOffs
           ,@alertsSentFromRiskAnalysisUnit
           ,@customsValueAlerts
           ,@contrabandAlerts
           ,@IPRAlerts
           ,@narcoticsAlerts
           ,@weaponsAlerts
           ,@cashAlerts
           ,@numberBorderSecurityDetections
           ,@numberCustomsValueDetections
           ,@numberIPRDetections
           ,@numberIncorrectOriginDetections
           ,@numberMisclassificationDetections
           ,@numberNonInstrusiveInspectionsDetections
           ,@numberOfDetectionsWithValueGreaterThanX
           ,@numberRegulatoryNonComplianceDetections)
END
GO 

