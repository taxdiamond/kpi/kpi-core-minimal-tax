﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_7_0_Setting_Parameter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ParameterStats",
                columns: table => new
                {
                    parameterStatsID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    parameterName = table.Column<string>(maxLength: 250, nullable: true),
                    valueDate = table.Column<DateTime>(type: "date", nullable: true),
                    timeStamp = table.Column<DateTime>(type: "datetime", nullable: true),
                    generatedValue = table.Column<decimal>(type: "decimal(21,3)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParameterStats", x => x.parameterStatsID);
                });

            migrationBuilder.CreateTable(
                name: "Settings",
                columns: table => new
                {
                    SettingID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    valueAddedTaxName = table.Column<string>(maxLength: 250, nullable: true),
                    corporateIncomeTaxname = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("settingID", x => x.SettingID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ParameterStats");

            migrationBuilder.DropTable(
                name: "Settings");
        }
    }
}
