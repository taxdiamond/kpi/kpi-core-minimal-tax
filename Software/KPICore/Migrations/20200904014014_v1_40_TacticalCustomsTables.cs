﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v1_40_TacticalCustomsTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CustomsAudit",
                columns: table => new
                {
                    customsAuditItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsAuditDate = table.Column<DateTime>(type: "date", nullable: true),
                    yieldFromDeskReviewAudits = table.Column<int>(nullable: true),
                    numberActiveDeskReviewAuditors = table.Column<int>(nullable: true),
                    numberActiveFieldAuditors = table.Column<int>(nullable: true),
                    numberAuditsMade = table.Column<int>(nullable: true),
                    numberScheduledAudits = table.Column<int>(nullable: true),
                    averageNumberDeskAuditExaminationsByStaff = table.Column<decimal>(nullable: true),
                    averageNumberFieldAuditsByStaff = table.Column<decimal>(nullable: true),
                    totalAmountAssessedFromAudits = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsAudit", x => x.customsAuditItemID);
                });

            migrationBuilder.CreateTable(
                name: "CustomsInspections",
                columns: table => new
                {
                    customsInspectionsItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsInspectionsDate = table.Column<DateTime>(type: "date", nullable: true),
                    numberDocumentaryInspections = table.Column<int>(nullable: true),
                    numberPhysicalInspections = table.Column<int>(nullable: true),
                    numberRedFlaggedOperations = table.Column<int>(nullable: true),
                    numberYellowFlaggedOperations = table.Column<int>(nullable: true),
                    numberOfDetections = table.Column<int>(nullable: true),
                    numberOfDetectionsRedFlaggedOperations = table.Column<int>(nullable: true),
                    numberOfDetectionsYellowFlaggedOperations = table.Column<int>(nullable: true),
                    numberFalsePositivesForAllInspections = table.Column<int>(nullable: true),
                    numberTrueNegativesForAllInspections = table.Column<int>(nullable: true),
                    averageDocumentaryInspectionsByComplianceOfficer = table.Column<decimal>(nullable: true),
                    averagePhysicalInspectionsByComplianceOfficer = table.Column<decimal>(nullable: true),
                    documentaryExaminationsBasedRiskSelectivity = table.Column<int>(nullable: true),
                    physicalExaminationsBasedRiskSelectivity = table.Column<int>(nullable: true),
                    documentaryExaminationsBasedReferralsRiskAnalysisUnit = table.Column<int>(nullable: true),
                    physicalExaminationsBasedReferralsRiskAnalysisUnit = table.Column<int>(nullable: true),
                    documentaryExaminationsBasedTipOffs = table.Column<int>(nullable: true),
                    physicalExaminationsBasedTipOffs = table.Column<int>(nullable: true),
                    alertsSentFromRiskAnalysisUnit = table.Column<int>(nullable: true),
                    customsValueAlerts = table.Column<int>(nullable: true),
                    contrabandAlerts = table.Column<int>(nullable: true),
                    IPRAlerts = table.Column<int>(nullable: true),
                    narcoticsAlerts = table.Column<int>(nullable: true),
                    weaponsAlerts = table.Column<int>(nullable: true),
                    cashAlerts = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsInspections", x => x.customsInspectionsItemID);
                });

            migrationBuilder.CreateTable(
                name: "CustomsStaff",
                columns: table => new
                {
                    customsStaffID = table.Column<string>(maxLength: 20, nullable: false),
                    customsStaffName = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("staffID", x => x.customsStaffID);
                });

            migrationBuilder.CreateTable(
                name: "CustomsStaffStats",
                columns: table => new
                {
                    customsStaffStatsItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsStaffStatsDate = table.Column<DateTime>(type: "date", nullable: true),
                    customsStaffID = table.Column<string>(nullable: true),
                    numberOfDocumentaryInspections = table.Column<int>(nullable: true),
                    numberOfDetectionsForDocumentaryInspections = table.Column<int>(nullable: true),
                    numberOfPhysicalInspections = table.Column<int>(nullable: true),
                    numberOfDetectionsForPhysicalInspections = table.Column<int>(nullable: true),
                    numberOfRiskAlertsReceived = table.Column<int>(nullable: true),
                    numberOfDetectionsForRiskAlerts = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsStaffStats", x => x.customsStaffStatsItemID);
                    table.ForeignKey(
                        name: "FK_CustomsStaffStats_CustomsStaff_customsStaffID",
                        column: x => x.customsStaffID,
                        principalTable: "CustomsStaff",
                        principalColumn: "customsStaffID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomsStaffStats_customsStaffID",
                table: "CustomsStaffStats",
                column: "customsStaffID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomsAudit");

            migrationBuilder.DropTable(
                name: "CustomsInspections");

            migrationBuilder.DropTable(
                name: "CustomsStaffStats");

            migrationBuilder.DropTable(
                name: "CustomsStaff");
        }
    }
}
