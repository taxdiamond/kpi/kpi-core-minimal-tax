﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_9_0_HumanResources : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HumanResources",
                columns: table => new
                {
                    hrItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    hrItemReportDate = table.Column<DateTime>(type: "date", nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    officeID = table.Column<string>(nullable: true),
                    totalHumanResources = table.Column<int>(nullable: true),
                    totalHumanResourcesInTaxCollection = table.Column<int>(nullable: true),
                    totalHumanResourcesInTaxPayerServices = table.Column<int>(nullable: true),
                    totalHumanResourcesInTaxEnforcement = table.Column<int>(nullable: true),
                    totalHumanResourcesInAudits = table.Column<int>(nullable: true),
                    totalHumanResourcesInAppeals = table.Column<int>(nullable: true),
                    numberPositionsFilledCompetitiveProcess = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HumanResources", x => x.hrItemID);
                    table.ForeignKey(
                        name: "FK_HumanResources_Office_officeID",
                        column: x => x.officeID,
                        principalTable: "Office",
                        principalColumn: "officeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HumanResources_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HumanResources_officeID",
                table: "HumanResources",
                column: "officeID");

            migrationBuilder.CreateIndex(
                name: "IX_HumanResources_regionID",
                table: "HumanResources",
                column: "regionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HumanResources");
        }
    }
}
