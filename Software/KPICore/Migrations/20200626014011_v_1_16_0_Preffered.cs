﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_16_0_Preffered : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DashboardPreferreds",
                columns: table => new
                {
                    preferredId = table.Column<Guid>(nullable: false),
                    useridRef = table.Column<string>(nullable: true),
                    dashboardRef = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DashboardPreferreds", x => x.preferredId);
                    table.ForeignKey(
                        name: "FK_DashboardPreferreds_Dashboards_dashboardRef",
                        column: x => x.dashboardRef,
                        principalTable: "Dashboards",
                        principalColumn: "DashboardId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DashboardPreferreds_AspNetUsers_useridRef",
                        column: x => x.useridRef,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DashboardPreferreds_dashboardRef",
                table: "DashboardPreferreds",
                column: "dashboardRef");

            migrationBuilder.CreateIndex(
                name: "IX_DashboardPreferreds_useridRef",
                table: "DashboardPreferreds",
                column: "useridRef");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DashboardPreferreds");
        }
    }
}
