﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_5_0_RevenueX3_Imports : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Imports",
                columns: table => new
                {
                    ImportID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    importDate = table.Column<DateTime>(type: "date", nullable: true),
                    totalImports = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Imports", x => x.ImportID);
                });

            migrationBuilder.CreateTable(
                name: "Revenue",
                columns: table => new
                {
                    revenueItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    revenueDate = table.Column<DateTime>(type: "date", nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    officeID = table.Column<string>(nullable: true),
                    segmentID = table.Column<string>(nullable: true),
                    concept = table.Column<string>(maxLength: 250, nullable: true),
                    conceptID = table.Column<string>(maxLength: 250, nullable: true),
                    amount = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Revenue", x => x.revenueItemID);
                    table.ForeignKey(
                        name: "FK_Revenue_Office_officeID",
                        column: x => x.officeID,
                        principalTable: "Office",
                        principalColumn: "officeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Revenue_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Revenue_Segment_segmentID",
                        column: x => x.segmentID,
                        principalTable: "Segment",
                        principalColumn: "segmentID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RevenueForecast",
                columns: table => new
                {
                    forecastItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    forecastDate = table.Column<DateTime>(type: "date", nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    officeID = table.Column<string>(nullable: true),
                    segmentID = table.Column<string>(nullable: true),
                    concept = table.Column<string>(maxLength: 250, nullable: true),
                    conceptID = table.Column<string>(maxLength: 250, nullable: true),
                    amount = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RevenueForecast", x => x.forecastItemID);
                    table.ForeignKey(
                        name: "FK_RevenueForecast_Office_officeID",
                        column: x => x.officeID,
                        principalTable: "Office",
                        principalColumn: "officeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RevenueForecast_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RevenueForecast_Segment_segmentID",
                        column: x => x.segmentID,
                        principalTable: "Segment",
                        principalColumn: "segmentID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RevenueSummaryByMonth",
                columns: table => new
                {
                    revenueSummaryID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    revenueDate = table.Column<DateTime>(type: "date", nullable: true),
                    totalRevenue = table.Column<decimal>(type: "money", nullable: true),
                    totalRevenuePaidInTIme = table.Column<decimal>(type: "money", nullable: true),
                    totalRevenueFromArrears = table.Column<decimal>(type: "money", nullable: true),
                    totalRevenueFromFines = table.Column<decimal>(type: "money", nullable: true),
                    totalRevenueFromAudits = table.Column<decimal>(type: "money", nullable: true),
                    totalRevenueFromEnforcementCollection = table.Column<decimal>(type: "money", nullable: true),
                    totalRevenueFromVoluntaryCompliance = table.Column<decimal>(type: "money", nullable: true),
                    totalRevenueFromTaxes = table.Column<decimal>(type: "money", nullable: true),
                    totalRevenueFromLitigation = table.Column<decimal>(type: "money", nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    officeID = table.Column<string>(nullable: true),
                    segmentID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RevenueSummaryByMonth", x => x.revenueSummaryID);
                    table.ForeignKey(
                        name: "FK_RevenueSummaryByMonth_Office_officeID",
                        column: x => x.officeID,
                        principalTable: "Office",
                        principalColumn: "officeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RevenueSummaryByMonth_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RevenueSummaryByMonth_Segment_segmentID",
                        column: x => x.segmentID,
                        principalTable: "Segment",
                        principalColumn: "segmentID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Revenue_officeID",
                table: "Revenue",
                column: "officeID");

            migrationBuilder.CreateIndex(
                name: "IX_Revenue_regionID",
                table: "Revenue",
                column: "regionID");

            migrationBuilder.CreateIndex(
                name: "IX_Revenue_segmentID",
                table: "Revenue",
                column: "segmentID");

            migrationBuilder.CreateIndex(
                name: "IX_RevenueForecast_officeID",
                table: "RevenueForecast",
                column: "officeID");

            migrationBuilder.CreateIndex(
                name: "IX_RevenueForecast_regionID",
                table: "RevenueForecast",
                column: "regionID");

            migrationBuilder.CreateIndex(
                name: "IX_RevenueForecast_segmentID",
                table: "RevenueForecast",
                column: "segmentID");

            migrationBuilder.CreateIndex(
                name: "IX_RevenueSummaryByMonth_officeID",
                table: "RevenueSummaryByMonth",
                column: "officeID");

            migrationBuilder.CreateIndex(
                name: "IX_RevenueSummaryByMonth_regionID",
                table: "RevenueSummaryByMonth",
                column: "regionID");

            migrationBuilder.CreateIndex(
                name: "IX_RevenueSummaryByMonth_segmentID",
                table: "RevenueSummaryByMonth",
                column: "segmentID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Imports");

            migrationBuilder.DropTable(
                name: "Revenue");

            migrationBuilder.DropTable(
                name: "RevenueForecast");

            migrationBuilder.DropTable(
                name: "RevenueSummaryByMonth");
        }
    }
}
