﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v1_27_CustomsRevenueForecast : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CustomsRevenueForecast",
                columns: table => new
                {
                    customsRevenueForecastItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsRevenueForecastDate = table.Column<DateTime>(type: "date", nullable: true),
                    customsPostID = table.Column<string>(nullable: true),
                    concept = table.Column<string>(maxLength: 250, nullable: true),
                    conceptID = table.Column<string>(maxLength: 250, nullable: true),
                    amount = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsRevenueForecast", x => x.customsRevenueForecastItemID);
                    table.ForeignKey(
                        name: "FK_CustomsRevenueForecast_CustomsPost_customsPostID",
                        column: x => x.customsPostID,
                        principalTable: "CustomsPost",
                        principalColumn: "customsPostID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomsRevenueForecast_customsPostID",
                table: "CustomsRevenueForecast",
                column: "customsPostID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomsRevenueForecast");
        }
    }
}
