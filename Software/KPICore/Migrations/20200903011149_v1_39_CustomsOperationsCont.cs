﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace AEWeb.Migrations
{
    public partial class v1_39_CustomsOperationsCont : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_39_CustomsOperationsCont/v1_39_CustomsOperationsCont_up.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_39_CustomsOperationsCont/v1_39_CustomsOperationsCont_down.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }
    }
}
