﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v1_43_NewTablesForIndicators : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "numberOfImportDeclarationsUnderAEOProgram",
                table: "CustomsOperations",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CustomsPayments",
                columns: table => new
                {
                    customsPaymentItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsPaymentsDate = table.Column<DateTime>(type: "date", nullable: true),
                    numberPaymentsReceived = table.Column<int>(nullable: true),
                    numberPaymentsReceivedElectronically = table.Column<int>(nullable: true),
                    totalAmountPaymentsReceived = table.Column<decimal>(type: "money", nullable: true),
                    totalAmountPaymentsReceivedElectronically = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsPayments", x => x.customsPaymentItemID);
                });

            migrationBuilder.CreateTable(
                name: "CustomsSatisfactionSurveys",
                columns: table => new
                {
                    satisfactionSurveyItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    surveyItemReportDate = table.Column<DateTime>(type: "date", nullable: true),
                    surveyArea = table.Column<string>(maxLength: 250, nullable: true),
                    surveyAreaScore = table.Column<decimal>(type: "decimal(18,3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsSatisfactionSurveys", x => x.satisfactionSurveyItemID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomsPayments");

            migrationBuilder.DropTable(
                name: "CustomsSatisfactionSurveys");

            migrationBuilder.DropColumn(
                name: "numberOfImportDeclarationsUnderAEOProgram",
                table: "CustomsOperations");
        }
    }
}
