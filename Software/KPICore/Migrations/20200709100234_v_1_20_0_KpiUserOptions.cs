﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_20_0_KpiUserOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "KpiUsers",
                columns: table => new
                {
                    KpiUserId = table.Column<Guid>(nullable: false),
                    KpiRefKpiId = table.Column<Guid>(nullable: true),
                    UserRefId = table.Column<string>(nullable: true),
                    ChartType = table.Column<int>(nullable: false),
                    NumberOfPeriods = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KpiUsers", x => x.KpiUserId);
                    table.ForeignKey(
                        name: "FK_KpiUsers_Kpis_KpiRefKpiId",
                        column: x => x.KpiRefKpiId,
                        principalTable: "Kpis",
                        principalColumn: "KpiId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_KpiUsers_AspNetUsers_UserRefId",
                        column: x => x.UserRefId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_KpiUsers_KpiRefKpiId",
                table: "KpiUsers",
                column: "KpiRefKpiId");

            migrationBuilder.CreateIndex(
                name: "IX_KpiUsers_UserRefId",
                table: "KpiUsers",
                column: "UserRefId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "KpiUsers");
        }
    }
}
