﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v1_48_CreateStrategyKPITables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "totalCarriers",
                table: "CustomsOperationsByTransportMode",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "totalCarriersLodingAdvancedElectronicInfo",
                table: "CustomsOperationsByTransportMode",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CustomsAdministrationExpenses",
                columns: table => new
                {
                    customsAdministrationExpensesItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsAdministrationExpensesDate = table.Column<DateTime>(type: "date", nullable: true),
                    customsPostID = table.Column<string>(nullable: true),
                    currentExpense = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsAdministrationExpenses", x => x.customsAdministrationExpensesItemID);
                    table.ForeignKey(
                        name: "FK_CustomsAdministrationExpenses_CustomsPost_customsPostID",
                        column: x => x.customsPostID,
                        principalTable: "CustomsPost",
                        principalColumn: "customsPostID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CustomsHRStats",
                columns: table => new
                {
                    customsHRStatsItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsHRStatsItemDate = table.Column<DateTime>(type: "date", nullable: true),
                    customsPostID = table.Column<string>(nullable: true),
                    numberFrontLineStaff = table.Column<int>(nullable: true),
                    numberSpecializedEmployees = table.Column<int>(nullable: true),
                    numberSpecializedEmployeesTrained = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsHRStats", x => x.customsHRStatsItemID);
                    table.ForeignKey(
                        name: "FK_CustomsHRStats_CustomsPost_customsPostID",
                        column: x => x.customsPostID,
                        principalTable: "CustomsPost",
                        principalColumn: "customsPostID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CustomsSeizures",
                columns: table => new
                {
                    customsSeizuresItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsSeizuresDate = table.Column<DateTime>(type: "date", nullable: true),
                    customsPostID = table.Column<string>(nullable: true),
                    seizureType = table.Column<string>(maxLength: 250, nullable: true),
                    seizureCategory = table.Column<string>(maxLength: 250, nullable: true),
                    seizureTransportMode = table.Column<string>(maxLength: 250, nullable: true),
                    seizureMode = table.Column<string>(maxLength: 250, nullable: true),
                    seizureValue = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsSeizures", x => x.customsSeizuresItemID);
                    table.ForeignKey(
                        name: "FK_CustomsSeizures_CustomsPost_customsPostID",
                        column: x => x.customsPostID,
                        principalTable: "CustomsPost",
                        principalColumn: "customsPostID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CustomsSingleWindow",
                columns: table => new
                {
                    customsSingleWindowItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsSingleWindowItemDate = table.Column<DateTime>(type: "date", nullable: true),
                    totalCustomsProceduresProcessed = table.Column<int>(nullable: true),
                    totalCustomsProceduresElectronic = table.Column<int>(nullable: true),
                    numberCertificationsIssuance = table.Column<int>(nullable: true),
                    numberOGARequirements = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsSingleWindow", x => x.customsSingleWindowItemID);
                });

            migrationBuilder.CreateTable(
                name: "CustomsTradePartnerStats",
                columns: table => new
                {
                    tradePartnerStatsItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    tradePartnerStatsItemDate = table.Column<DateTime>(type: "date", nullable: true),
                    numberOfRegisteredImporters = table.Column<int>(nullable: true),
                    numberOfRegisteredExporters = table.Column<int>(nullable: true),
                    numberOfImporters70PercentRevenue = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsTradePartnerStats", x => x.tradePartnerStatsItemID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomsAdministrationExpenses_customsPostID",
                table: "CustomsAdministrationExpenses",
                column: "customsPostID");

            migrationBuilder.CreateIndex(
                name: "IX_CustomsHRStats_customsPostID",
                table: "CustomsHRStats",
                column: "customsPostID");

            migrationBuilder.CreateIndex(
                name: "IX_CustomsSeizures_customsPostID",
                table: "CustomsSeizures",
                column: "customsPostID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomsAdministrationExpenses");

            migrationBuilder.DropTable(
                name: "CustomsHRStats");

            migrationBuilder.DropTable(
                name: "CustomsSeizures");

            migrationBuilder.DropTable(
                name: "CustomsSingleWindow");

            migrationBuilder.DropTable(
                name: "CustomsTradePartnerStats");

            migrationBuilder.DropColumn(
                name: "totalCarriers",
                table: "CustomsOperationsByTransportMode");

            migrationBuilder.DropColumn(
                name: "totalCarriersLodingAdvancedElectronicInfo",
                table: "CustomsOperationsByTransportMode");
        }
    }
}
