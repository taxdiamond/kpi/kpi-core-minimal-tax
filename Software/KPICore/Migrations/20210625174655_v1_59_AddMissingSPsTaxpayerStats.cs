﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace AEWeb.Migrations
{
    public partial class v1_59_AddMissingSPsTaxpayerStats : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_59_AddMissingSPsTaxpayerStats/v1_59_AddMissingSPsTaxpayerStats_up.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_59_AddMissingSPsTaxpayerStats/v1_59_AddMissingSPsTaxpayerStats_down.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));

        }
    }
}
