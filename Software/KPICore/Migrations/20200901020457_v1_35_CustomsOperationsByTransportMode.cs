﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v1_35_CustomsOperationsByTransportMode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CustomsOperationsByTransportMode",
                columns: table => new
                {
                    customsOperationsItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsOperationsDate = table.Column<DateTime>(type: "date", nullable: true),
                    modeOfTransport = table.Column<string>(maxLength: 250, nullable: true),
                    totalImportsAmount = table.Column<decimal>(type: "money", nullable: true),
                    totalExportsAmount = table.Column<decimal>(type: "money", nullable: true),
                    totalTemporaryImports = table.Column<int>(nullable: true),
                    freeTradeZoneOperations = table.Column<int>(nullable: true),
                    passengersProcessed = table.Column<int>(nullable: true),
                    clearanceTimeImports = table.Column<decimal>(type: "decimal(18,3)", nullable: true),
                    clearanceTimeImportsRedLane = table.Column<decimal>(type: "decimal(18,3)", nullable: true),
                    clearanceTimeImportsYellowLane = table.Column<decimal>(type: "decimal(18,3)", nullable: true),
                    clearanceTimeImportsGreenLane = table.Column<decimal>(type: "decimal(18,3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsOperationsByTransportMode", x => x.customsOperationsItemID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomsOperationsByTransportMode");
        }
    }
}
