﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v1_51_AddTablesOp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "cancelledLicences",
                table: "CustomsTradePartnerStats",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberOfRegisteredBrokers",
                table: "CustomsTradePartnerStats",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberOfRegisteredCarriers",
                table: "CustomsTradePartnerStats",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberOfRegisteredWarehouses",
                table: "CustomsTradePartnerStats",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberOperationsUnderPreferentialTreatement",
                table: "CustomsOperations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberPenalDenunciations",
                table: "CustomsAudit",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CustomsAdvancedRulings",
                columns: table => new
                {
                    customsAdvancedRulingsItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsAdvancedRulingsDate = table.Column<DateTime>(type: "date", nullable: true),
                    numberRulingRequestsOrigin = table.Column<int>(nullable: true),
                    numberRulingRequestsTariff = table.Column<int>(nullable: true),
                    numberRulingRequests = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsAdvancedRulings", x => x.customsAdvancedRulingsItemID);
                });

            migrationBuilder.CreateTable(
                name: "CustomsAEO",
                columns: table => new
                {
                    customsAEOItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsAEODate = table.Column<DateTime>(type: "date", nullable: true),
                    AEOEntityGroup = table.Column<string>(maxLength: 250, nullable: true),
                    numberEnterprisesWithAEOCert = table.Column<int>(nullable: true),
                    numberSMEEnterprisesWithAEOCert = table.Column<int>(nullable: true),
                    accreditationAvergeTime = table.Column<decimal>(type: "decimal(18,3)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsAEO", x => x.customsAEOItemID);
                });

            migrationBuilder.CreateTable(
                name: "CustomsAppeals",
                columns: table => new
                {
                    customsAppealsItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsAppealsDate = table.Column<DateTime>(type: "date", nullable: true),
                    numberAppealsConsidered = table.Column<int>(nullable: true),
                    numberAppealsResolved = table.Column<int>(nullable: true),
                    numberAppealsUndergoLegalAction = table.Column<int>(nullable: true),
                    numberJudicialDecissionsFavorableCustoms = table.Column<int>(nullable: true),
                    numberLawsuitsFinished = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsAppeals", x => x.customsAppealsItemID);
                });

            migrationBuilder.CreateTable(
                name: "CustomsTradePartner",
                columns: table => new
                {
                    customsTradePartnerID = table.Column<string>(maxLength: 20, nullable: false),
                    tradePartnerType = table.Column<string>(maxLength: 250, nullable: true),
                    tradePartnerName = table.Column<string>(maxLength: 250, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("customsTradePartnerID", x => x.customsTradePartnerID);
                });

            migrationBuilder.CreateTable(
                name: "CustomsTradePartnerOffenses",
                columns: table => new
                {
                    customsTradePartnerOffensesItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    customsTradePartnerOffensesItemDate = table.Column<DateTime>(type: "date", nullable: true),
                    customsTradePartnerID = table.Column<string>(nullable: true),
                    numberOfViolations = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomsTradePartnerOffenses", x => x.customsTradePartnerOffensesItemID);
                    table.ForeignKey(
                        name: "FK_CustomsTradePartnerOffenses_CustomsTradePartner_customsTradePartnerID",
                        column: x => x.customsTradePartnerID,
                        principalTable: "CustomsTradePartner",
                        principalColumn: "customsTradePartnerID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CustomsTradePartnerOffenses_customsTradePartnerID",
                table: "CustomsTradePartnerOffenses",
                column: "customsTradePartnerID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomsAdvancedRulings");

            migrationBuilder.DropTable(
                name: "CustomsAEO");

            migrationBuilder.DropTable(
                name: "CustomsAppeals");

            migrationBuilder.DropTable(
                name: "CustomsTradePartnerOffenses");

            migrationBuilder.DropTable(
                name: "CustomsTradePartner");

            migrationBuilder.DropColumn(
                name: "cancelledLicences",
                table: "CustomsTradePartnerStats");

            migrationBuilder.DropColumn(
                name: "numberOfRegisteredBrokers",
                table: "CustomsTradePartnerStats");

            migrationBuilder.DropColumn(
                name: "numberOfRegisteredCarriers",
                table: "CustomsTradePartnerStats");

            migrationBuilder.DropColumn(
                name: "numberOfRegisteredWarehouses",
                table: "CustomsTradePartnerStats");

            migrationBuilder.DropColumn(
                name: "numberOperationsUnderPreferentialTreatement",
                table: "CustomsOperations");

            migrationBuilder.DropColumn(
                name: "numberPenalDenunciations",
                table: "CustomsAudit");
        }
    }
}
