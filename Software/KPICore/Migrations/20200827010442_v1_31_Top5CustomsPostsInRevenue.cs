﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace AEWeb.Migrations
{
    public partial class v1_31_Top5CustomsPostsInRevenue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_31_Top5CustomsPostsInRevenue/v1_31_Top5CustomsPostsInRevenue_up.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_31_Top5CustomsPostsInRevenue/v1_31_Top5CustomsPostsInRevenue_down.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }
    }
}
