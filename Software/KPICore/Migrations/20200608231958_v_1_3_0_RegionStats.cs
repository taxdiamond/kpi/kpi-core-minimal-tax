﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_3_0_RegionStats : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RegionStats",
                columns: table => new
                {
                    regionStatsID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    statsDate = table.Column<DateTime>(type: "date", nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    gdpPerCapitaAtDate = table.Column<decimal>(type: "money", nullable: true),
                    nominalGdpAtDate = table.Column<decimal>(type: "money", nullable: true),
                    populationAtDate = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RegionStats", x => x.regionStatsID);
                    table.ForeignKey(
                        name: "FK_RegionStats_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RegionStats_regionID",
                table: "RegionStats",
                column: "regionID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RegionStats");
        }
    }
}
