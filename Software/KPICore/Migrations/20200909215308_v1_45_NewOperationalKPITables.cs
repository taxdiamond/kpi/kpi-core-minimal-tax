﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v1_45_NewOperationalKPITables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "averageTimeAtTerminalSinceArrival",
                table: "CustomsOperations",
                type: "decimal(18,3)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "averageTimeForDocumentaryCompliance",
                table: "CustomsOperations",
                type: "decimal(18,3)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberBillsForOverstayedShipments",
                table: "CustomsOperations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberLabTestsForImports",
                table: "CustomsOperations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberPreArrivalClearancesForImports",
                table: "CustomsOperations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberReleasesPriorDeterminationAndPayments",
                table: "CustomsOperations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberSelfFilingOperationsForImports",
                table: "CustomsOperations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "customsPostID",
                table: "CustomsInspections",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberBorderSecurityDetections",
                table: "CustomsInspections",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberCustomsValueDetections",
                table: "CustomsInspections",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberIPRDetections",
                table: "CustomsInspections",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberIncorrectOriginDetections",
                table: "CustomsInspections",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberMisclassificationDetections",
                table: "CustomsInspections",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberNonInstrusiveInspectionsDetections",
                table: "CustomsInspections",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberOfDetectionsWithValueGreaterThanX",
                table: "CustomsInspections",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberRegulatoryNonComplianceDetections",
                table: "CustomsInspections",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_CustomsInspections_customsPostID",
                table: "CustomsInspections",
                column: "customsPostID");

            migrationBuilder.AddForeignKey(
                name: "FK_CustomsInspections_CustomsPost_customsPostID",
                table: "CustomsInspections",
                column: "customsPostID",
                principalTable: "CustomsPost",
                principalColumn: "customsPostID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CustomsInspections_CustomsPost_customsPostID",
                table: "CustomsInspections");

            migrationBuilder.DropIndex(
                name: "IX_CustomsInspections_customsPostID",
                table: "CustomsInspections");

            migrationBuilder.DropColumn(
                name: "averageTimeAtTerminalSinceArrival",
                table: "CustomsOperations");

            migrationBuilder.DropColumn(
                name: "averageTimeForDocumentaryCompliance",
                table: "CustomsOperations");

            migrationBuilder.DropColumn(
                name: "numberBillsForOverstayedShipments",
                table: "CustomsOperations");

            migrationBuilder.DropColumn(
                name: "numberLabTestsForImports",
                table: "CustomsOperations");

            migrationBuilder.DropColumn(
                name: "numberPreArrivalClearancesForImports",
                table: "CustomsOperations");

            migrationBuilder.DropColumn(
                name: "numberReleasesPriorDeterminationAndPayments",
                table: "CustomsOperations");

            migrationBuilder.DropColumn(
                name: "numberSelfFilingOperationsForImports",
                table: "CustomsOperations");

            migrationBuilder.DropColumn(
                name: "customsPostID",
                table: "CustomsInspections");

            migrationBuilder.DropColumn(
                name: "numberBorderSecurityDetections",
                table: "CustomsInspections");

            migrationBuilder.DropColumn(
                name: "numberCustomsValueDetections",
                table: "CustomsInspections");

            migrationBuilder.DropColumn(
                name: "numberIPRDetections",
                table: "CustomsInspections");

            migrationBuilder.DropColumn(
                name: "numberIncorrectOriginDetections",
                table: "CustomsInspections");

            migrationBuilder.DropColumn(
                name: "numberMisclassificationDetections",
                table: "CustomsInspections");

            migrationBuilder.DropColumn(
                name: "numberNonInstrusiveInspectionsDetections",
                table: "CustomsInspections");

            migrationBuilder.DropColumn(
                name: "numberOfDetectionsWithValueGreaterThanX",
                table: "CustomsInspections");

            migrationBuilder.DropColumn(
                name: "numberRegulatoryNonComplianceDetections",
                table: "CustomsInspections");
        }
    }
}
