﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace AEWeb.Migrations
{
    public partial class v1_45_NewOperationalKPITablesSPs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_45_NewOperationalKPITablesSPs/v1_45_NewOperationalKPITablesSPs_up.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_45_NewOperationalKPITablesSPs/v1_45_NewOperationalKPITablesSPs_down.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }
    }
}
