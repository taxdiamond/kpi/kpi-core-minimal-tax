﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_10_0_DashboardConfig : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "DashboardRefDashboardId",
                table: "Kpis",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "RowRefDashboardRowId",
                table: "Kpis",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CellNumber",
                table: "Kpis",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PeriodYear",
                table: "Kpis",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DashboardConfigurations",
                columns: table => new
                {
                    DashboardConfigurationId = table.Column<Guid>(nullable: false),
                    DashboardRefDashboardId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DashboardConfigurations", x => x.DashboardConfigurationId);
                    table.ForeignKey(
                        name: "FK_DashboardConfigurations_Dashboards_DashboardRefDashboardId",
                        column: x => x.DashboardRefDashboardId,
                        principalTable: "Dashboards",
                        principalColumn: "DashboardId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DashboardRows",
                columns: table => new
                {
                    DashboardRowId = table.Column<Guid>(nullable: false),
                    RowNumber = table.Column<int>(nullable: true),
                    ConfigurationRefDashboardConfigurationId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DashboardRows", x => x.DashboardRowId);
                    table.ForeignKey(
                        name: "FK_DashboardRows_DashboardConfigurations_ConfigurationRefDashboardConfigurationId",
                        column: x => x.ConfigurationRefDashboardConfigurationId,
                        principalTable: "DashboardConfigurations",
                        principalColumn: "DashboardConfigurationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Kpis_DashboardRefDashboardId",
                table: "Kpis",
                column: "DashboardRefDashboardId");

            migrationBuilder.CreateIndex(
                name: "IX_Kpis_RowRefDashboardRowId",
                table: "Kpis",
                column: "RowRefDashboardRowId");

            migrationBuilder.CreateIndex(
                name: "IX_DashboardConfigurations_DashboardRefDashboardId",
                table: "DashboardConfigurations",
                column: "DashboardRefDashboardId");

            migrationBuilder.CreateIndex(
                name: "IX_DashboardRows_ConfigurationRefDashboardConfigurationId",
                table: "DashboardRows",
                column: "ConfigurationRefDashboardConfigurationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Kpis_Dashboards_DashboardRefDashboardId",
                table: "Kpis",
                column: "DashboardRefDashboardId",
                principalTable: "Dashboards",
                principalColumn: "DashboardId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Kpis_DashboardRows_RowRefDashboardRowId",
                table: "Kpis",
                column: "RowRefDashboardRowId",
                principalTable: "DashboardRows",
                principalColumn: "DashboardRowId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Kpis_Dashboards_DashboardRefDashboardId",
                table: "Kpis");

            migrationBuilder.DropForeignKey(
                name: "FK_Kpis_DashboardRows_RowRefDashboardRowId",
                table: "Kpis");

            migrationBuilder.DropTable(
                name: "DashboardRows");

            migrationBuilder.DropTable(
                name: "DashboardConfigurations");

            migrationBuilder.DropIndex(
                name: "IX_Kpis_DashboardRefDashboardId",
                table: "Kpis");

            migrationBuilder.DropIndex(
                name: "IX_Kpis_RowRefDashboardRowId",
                table: "Kpis");

            migrationBuilder.DropColumn(
                name: "DashboardRefDashboardId",
                table: "Kpis");

            migrationBuilder.DropColumn(
                name: "RowRefDashboardRowId",
                table: "Kpis");

            migrationBuilder.DropColumn(
                name: "CellNumber",
                table: "Kpis");

            migrationBuilder.DropColumn(
                name: "PeriodYear",
                table: "Kpis");
        }
    }
}
