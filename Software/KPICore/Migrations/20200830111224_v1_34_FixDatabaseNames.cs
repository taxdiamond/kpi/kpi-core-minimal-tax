﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace AEWeb.Migrations
{
    public partial class v1_34_FixDatabaseNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "numberOfDetectionsForPhysicalExaminations",
                table: "CustomsOperations",
                newName: "numberOfDetectionsForPhysicalExaminationsImports");

            migrationBuilder.RenameColumn(
                name: "numberOfDetectionsForDocumentExaminations",
                table: "CustomsOperations",
                newName: "numberOfDetectionsForDocumentExaminationsImports");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "numberOfDetectionsForPhysicalExaminationsImports",
                table: "CustomsOperations",
                newName: "numberOfDetectionsForPhysicalExaminations");

            migrationBuilder.RenameColumn(
                name: "numberOfDetectionsForDocumentExaminationsImports",
                table: "CustomsOperations",
                newName: "numberOfDetectionsForDocumentExaminations");
        }
    }
}
