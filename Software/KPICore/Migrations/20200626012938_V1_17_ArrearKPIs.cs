﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace AEWeb.Migrations
{
    public partial class V1_17_ArrearKPIs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_17/v1_17_up.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));

            migrationBuilder.AddColumn<decimal>(
                name: "valueOfArrearCasesGenerated",
                table: "Arrears",
                type: "money",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "valueOfArrearCasesResolved",
                table: "Arrears",
                type: "money",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberOfArrearCasesGenerated",
                table: "Arrears",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberOfArrearCasesPending",
                table: "Arrears",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "numberOfArrearCasesResolved",
                table: "Arrears",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_17/v1_17_down.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));

            migrationBuilder.DropColumn(
                name: "valueOfArrearCasesGenerated",
                table: "Arrears");

            migrationBuilder.DropColumn(
                name: "valueOfArrearCasesResolved",
                table: "Arrears");

            migrationBuilder.DropColumn(
                name: "numberOfArrearCasesGenerated",
                table: "Arrears");

            migrationBuilder.DropColumn(
                name: "numberOfArrearCasesPending",
                table: "Arrears");

            migrationBuilder.DropColumn(
                name: "numberOfArrearCasesResolved",
                table: "Arrears");
        }
    }
}
