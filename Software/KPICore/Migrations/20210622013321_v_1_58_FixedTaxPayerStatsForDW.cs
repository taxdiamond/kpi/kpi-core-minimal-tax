﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace AEWeb.Migrations
{
    public partial class v_1_58_FixedTaxPayerStatsForDW : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_58_FixedTaxPayerStatsForDW/v1_58_FixedTaxPayerStatsForDW_up.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_58_FixedTaxPayerStatsForDW/v1_58_FixedTaxPayerStatsForDW_down.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }
    }
}
