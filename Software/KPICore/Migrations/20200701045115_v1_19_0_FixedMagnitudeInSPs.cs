﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.IO;

namespace AEWeb.Migrations
{
    public partial class v1_19_0_FixedMagnitudeInSPs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_19/v1_19_up.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            var sqlFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"DataFiles/Database/v1_19/v1_19_down.sql");
            migrationBuilder.Sql(File.ReadAllText(sqlFile));
        }
    }
}
