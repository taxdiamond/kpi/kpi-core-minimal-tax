﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_2_0_TaxRate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TaxRates",
                columns: table => new
                {
                    TaxRateId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    taxRateDate = table.Column<DateTime>(type: "date", nullable: true),
                    taxName = table.Column<string>(maxLength: 250, nullable: true),
                    taxRate = table.Column<decimal>(type: "decimal(18,3)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("taxRateID", x => x.TaxRateId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TaxRates");
        }
    }
}
