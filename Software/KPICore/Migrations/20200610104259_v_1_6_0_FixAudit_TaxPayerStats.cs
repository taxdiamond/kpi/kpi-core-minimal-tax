﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AEWeb.Migrations
{
    public partial class v_1_6_0_FixAudit_TaxPayerStats : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Arrears",
                columns: table => new
                {
                    arrearItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    arrearDate = table.Column<DateTime>(type: "date", nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    officeID = table.Column<string>(nullable: true),
                    segmentID = table.Column<string>(nullable: true),
                    concept = table.Column<string>(maxLength: 250, nullable: true),
                    conceptID = table.Column<string>(maxLength: 250, nullable: true),
                    amount = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Arrears", x => x.arrearItemID);
                    table.ForeignKey(
                        name: "FK_Arrears_Office_officeID",
                        column: x => x.officeID,
                        principalTable: "Office",
                        principalColumn: "officeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Arrears_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Arrears_Segment_segmentID",
                        column: x => x.segmentID,
                        principalTable: "Segment",
                        principalColumn: "segmentID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TaxPayerStats",
                columns: table => new
                {
                    taxPayerStatsID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    statsDate = table.Column<DateTime>(type: "date", nullable: true),
                    numberOfRegisteredTaxPayers = table.Column<int>(nullable: true),
                    numberOfActiveTaxPayers = table.Column<int>(nullable: true),
                    numberOfTaxPayersFiled = table.Column<int>(nullable: true),
                    numberTaxPayersFiledInTime = table.Column<int>(nullable: true),
                    numberOfTaxPayers70PercentRevenue = table.Column<int>(nullable: true),
                    regionID = table.Column<string>(nullable: true),
                    officeID = table.Column<string>(nullable: true),
                    segmentID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxPayerStats", x => x.taxPayerStatsID);
                    table.ForeignKey(
                        name: "FK_TaxPayerStats_Office_officeID",
                        column: x => x.officeID,
                        principalTable: "Office",
                        principalColumn: "officeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaxPayerStats_Region_regionID",
                        column: x => x.regionID,
                        principalTable: "Region",
                        principalColumn: "regionID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaxPayerStats_Segment_segmentID",
                        column: x => x.segmentID,
                        principalTable: "Segment",
                        principalColumn: "segmentID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AuditStats_officeID",
                table: "AuditStats",
                column: "officeID");

            migrationBuilder.CreateIndex(
                name: "IX_AuditStats_regionID",
                table: "AuditStats",
                column: "regionID");

            migrationBuilder.CreateIndex(
                name: "IX_AuditStats_segmentID",
                table: "AuditStats",
                column: "segmentID");

            migrationBuilder.CreateIndex(
                name: "IX_Arrears_officeID",
                table: "Arrears",
                column: "officeID");

            migrationBuilder.CreateIndex(
                name: "IX_Arrears_regionID",
                table: "Arrears",
                column: "regionID");

            migrationBuilder.CreateIndex(
                name: "IX_Arrears_segmentID",
                table: "Arrears",
                column: "segmentID");

            migrationBuilder.CreateIndex(
                name: "IX_TaxPayerStats_officeID",
                table: "TaxPayerStats",
                column: "officeID");

            migrationBuilder.CreateIndex(
                name: "IX_TaxPayerStats_regionID",
                table: "TaxPayerStats",
                column: "regionID");

            migrationBuilder.CreateIndex(
                name: "IX_TaxPayerStats_segmentID",
                table: "TaxPayerStats",
                column: "segmentID");

            migrationBuilder.AddForeignKey(
                name: "FK_AuditStats_Office_officeID",
                table: "AuditStats",
                column: "officeID",
                principalTable: "Office",
                principalColumn: "officeID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AuditStats_Region_regionID",
                table: "AuditStats",
                column: "regionID",
                principalTable: "Region",
                principalColumn: "regionID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AuditStats_Segment_segmentID",
                table: "AuditStats",
                column: "segmentID",
                principalTable: "Segment",
                principalColumn: "segmentID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuditStats_Office_officeID",
                table: "AuditStats");

            migrationBuilder.DropForeignKey(
                name: "FK_AuditStats_Region_regionID",
                table: "AuditStats");

            migrationBuilder.DropForeignKey(
                name: "FK_AuditStats_Segment_segmentID",
                table: "AuditStats");

            migrationBuilder.DropTable(
                name: "Arrears");

            migrationBuilder.DropTable(
                name: "TaxPayerStats");

            migrationBuilder.DropIndex(
                name: "IX_AuditStats_officeID",
                table: "AuditStats");

            migrationBuilder.DropIndex(
                name: "IX_AuditStats_regionID",
                table: "AuditStats");

            migrationBuilder.DropIndex(
                name: "IX_AuditStats_segmentID",
                table: "AuditStats");
        }
    }
}
