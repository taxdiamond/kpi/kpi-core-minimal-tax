﻿function doColumnChart(containerid, charttype, yAxisTitle, stackingType) {
    var container = $('#' + containerid);
    var divWithData = container.children('[data-chart-categories]');
    var seriesArray = divWithData.data('chart-series');
    var $titles = String(divWithData.data('chart-categories')).split('|');

    var stackingOption = 'normal'
    if (!stackingType || (stackingType == ''))
        stackingOption = undefined;
    else
        stackingOption = stackingType;

    var categoriesArray = new Array();
    for (var i = 0; i < $titles.length; i++) {
        categoriesArray[i] = String($titles[i]);
    }

    var showLegend = seriesArray.length > 1;

    var minValue = Number.MAX_SAFE_INTEGER;
    seriesArray.forEach(function (oneSerie) {
        oneSerie.data.forEach(function (dataValue) {
            if (dataValue < minValue)
                minValue = dataValue;
        });
    });

    if (minValue >= 0)
        minValue = 0;

    if (minValue < 0)
        minValue -= 1;

    var pointFormatString = '{series.name}: {point.y}<br/>Total: {point.stackTotal}';
    if (!showLegend)
        pointFormatString = yAxisTitle + ': {point.y}';

    Highcharts.chart(containerid, {
        chart: {
            type: charttype,
            style: {
                fontFamily: 'Calibri'
            }
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: categoriesArray
        },
        yAxis: {
            min: minValue,
            title: {
                text: yAxisTitle
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        legend: {
            enabled: showLegend,
            align: 'center',
            verticalAlign: 'bottom',
            backgroundColor:
                Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: pointFormatString
        },
        plotOptions: {
            series: {
                stacking: stackingOption
            },
            column: {
                dataLabels: {
                    enabled: false
                }
            }
        },
        series: seriesArray
    });
}