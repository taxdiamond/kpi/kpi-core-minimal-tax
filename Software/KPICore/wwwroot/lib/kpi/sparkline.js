/**
 * Create a constructor for sparklines that takes some sensible defaults and merges in the individual
 * chart options. This function is also available from the jQuery plugin as $(element).highcharts('SparkLine').
 */
Highcharts.SparkLine = function (a, b, c) {
    var hasRenderToArg = typeof a === 'string' || a.nodeName,
        options = arguments[hasRenderToArg ? 1 : 0],
        defaultOptions = {
            chart: {
                renderTo: (options.chart && options.chart.renderTo) || this,
                backgroundColor: null,
                borderWidth: 0,
                type: 'area',
                margin: [2, 0, 2, 0],
                height: 40,
                style: {
                    overflow: 'visible'
                },

                // small optimalization, saves 1-2 ms each sparkline
                skipClone: true
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            xAxis: {
                labels: {
                    enabled: false
                },
                title: {
                    text: null
                },
                startOnTick: false,
                endOnTick: false,
                tickPositions: []
            },
            yAxis: {
                endOnTick: false,
                startOnTick: false,
                labels: {
                    enabled: false
                },
                title: {
                    text: null
                },
                tickPositions: [0]
            },
            legend: {
                enabled: false
            },
            tooltip: {
                hideDelay: 0,
                outside: true,
                shared: true
            },
            plotOptions: {
                series: {
                    animation: false,
                    lineWidth: 1,
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    marker: {
                        radius: 1,
                        states: {
                            hover: {
                                radius: 2
                            }
                        }
                    },
                    fillOpacity: 0.25
                },
                column: {
                    negativeColor: '#910000',
                    borderColor: 'silver'
                }
            }
        };

    options = Highcharts.merge(defaultOptions, options);

    return hasRenderToArg ?
        new Highcharts.Chart(a, options, c) :
        new Highcharts.Chart(options, b);
};

// Creating 153 sparkline charts is quite fast in modern browsers, but IE8 and mobile
// can take some seconds, so we split the input into chunks and apply them in timeouts
// in order avoid locking up the browser process and allow interaction.
function doSparkline(container, title) {
	var divWithData = container.children('[data-sparkline-values]');
	var stringdata = String(divWithData.data('sparkline-values'));
	var $titles = String(divWithData.data('sparkline-titles')).split('|');
	
    var $td,
        dataY,
		data,
        chart;

	dataY = $.map(stringdata.split(','), parseFloat);
	chart = {};
	
	data = new Array();
	for(var i = 0; i<dataY.length; i++) {
		var item = new Array();
		item[0] = $titles[i];
		item[1] = dataY[i];
		data[i] = item;
	}

	divWithData.highcharts('SparkLine', {
		series: [{
			data: data,
			pointStart: 1
		}],
		tooltip: {
			headerFormat: '<span style="font-size: 10px">' + title + ', {point.key}:</span><br/>',
			pointFormat: '<b>{point.y}</b> %'
        },
        exporting: {
            enabled: false
        },
		chart: chart
	});
}