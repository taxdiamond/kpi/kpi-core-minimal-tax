﻿using AEWeb.ViewModels.KPI;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Components.KPI
{
    public class KpiNotSupportedViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(string kpiKey, string id)
        {

            KpiNotSupportedViewModel model = new KpiNotSupportedViewModel();

            return View("Default", model);
        }
    }
}
