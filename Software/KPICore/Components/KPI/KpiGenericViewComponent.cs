﻿using AEWeb.Utilities;
using AEWeb.ViewModels.KPI;
using Domain.CustomsPost;
using Domain.Enums.Indicators;
using Domain.Indicators;
using Domain.Office;
using Domain.Region;
using Domain.Repository;
using Domain.Security;
using Domain.Utils;
using Framework.Kpi;
using Framework.Utilities;
using Infrastructure.Mediator.DataWarehouse.Queries;
using Infrastructure.Mediator.Indicator.Commands;
using Infrastructure.Mediator.Indicator.Queries;
using KPIClassLibrary.KPI;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Components.KPI
{
    public class KpiGenericViewComponent : ViewComponent
    {
        private IWebHostEnvironment _hostingEnvironment;
        private ILogger<KpiGenericViewComponent> _logger;
        private IKpiDataGetter _kpiDataGetter;
        private IMediator _mediator;
        private UserManager<ApplicationUser> _userManager;
        private readonly IChartUtilities _chartUtilities;

        private ICustomsPostRepository _customsPostRepository;

        public KpiGenericViewComponent(IWebHostEnvironment hostingEnvironment,
            ILogger<KpiGenericViewComponent> logger,
            IKpiDataGetter kpiDataGetter,
            IMediator mediator,
            UserManager<ApplicationUser> userManager,
            IChartUtilities chartUtilities,
            ICustomsPostRepository customsPostRepository)
        {
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
            _kpiDataGetter = kpiDataGetter;
            _mediator = mediator;
            _userManager = userManager;
            _chartUtilities = chartUtilities;
            _customsPostRepository = customsPostRepository;
        }

        /// <summary>
        /// This method will do the following:
        /// - Retrieve a kpiInstance from the id given in parameter
        /// - Retrieve the options by default saved if this kpi is in a dashboard
        ///     IF this is not there, we create one with default options
        /// - Retrieve the sticky options saved by the user
        ///     IF this is not there, we create one based on the default options created just before
        /// - Finally, the kpi data getter can obtain the information with the kpiIinstance and the component
        /// can display the info with the correct chart type from the options objects.
        /// </summary>
        /// <param name="kpiKey"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IViewComponentResult> InvokeAsync(string kpiKey, string id)
        {
            string metadataPath = Utilities.ConfigUtilities.GetMetaDataPath(_hostingEnvironment);
            string jsonGroupPath = Utilities.ConfigUtilities.GetGroupsDataPath(_hostingEnvironment);
            //KPIClassLibrary.KPI.KPICatalog catalog = new KPIClassLibrary.KPI.KPICatalog(jsonGroupPath, metadataPath);

            KPIClassLibrary.KPI.KPI kpiInstance = null;

            _kpiDataGetter.SetDataPath(metadataPath, jsonGroupPath);

            // Take from the configuration the setting on whats needed
            
            Domain.Indicators.Kpi kpiInCell = null;
            if (!string.IsNullOrWhiteSpace(id))
                kpiInCell = await _mediator.Send(new GetKpiQuery() { KpiId = id, NoTracking=false });
               
            Domain.Indicators.KpiUser kpiOptionsForUser = null;
            bool withFilter = false;
            if (kpiInCell == null)
            {
                // No filter when created by default
                withFilter = false;
                kpiInstance = _chartUtilities.GetKPIFromCatalog(kpiKey, "");
                kpiInCell = BuildStandardKpiInCellOptions(kpiInstance);
                kpiOptionsForUser = BuildStandardOptionsForUser(kpiInCell);
            } 
            else
            {
                withFilter = !string.IsNullOrWhiteSpace(kpiInCell.FilterValue);
                kpiInstance = _chartUtilities.GetKPIFromCatalog(kpiKey, "");
                ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);
                kpiOptionsForUser = await _mediator.Send(new GetKpiUserQuery() { KpiId = Guid.Parse(id), UserId = user.Id });
                if (kpiOptionsForUser == null)
                {
                    kpiOptionsForUser = BuildStandardOptionsForUser(kpiInCell);

                    await _mediator.Send(new CreateUpdateKpiUserCommand()
                    {
                        KpiSource = kpiInCell,
                        User = user,
                        NumberOfPeriods = kpiInCell.NumberOfYears,
                        ChartType = kpiInCell.ViewType
                    });
                    _logger.LogDebug("Saved standard options for KPI " + kpiKey + " for user " + user.UserName);
                }                
            }
            
            _kpiDataGetter.SetKpiOptions(kpiInCell, kpiOptionsForUser);
            
            KPIChartType theChartType = Utilities.ChartUtilities.GetKPIChartTypeFrom(kpiOptionsForUser.ChartType);
            
            KpiViewModel model = null;

            if (theChartType == KPIChartType.Bar_MultiSeries ||
                theChartType == KPIChartType.Column_Multiseries ||
                theChartType == KPIChartType.Column_SingleSeries ||
                theChartType == KPIChartType.Bar_SingleSeries ||
                theChartType == KPIChartType.StackedBar_MultiSeries ||
                theChartType == KPIChartType.StackedColumn_Multiseries ||
                theChartType == KPIChartType.Line_MultiSeries ||
                theChartType == KPIChartType.Line_SingleSeries)
            {
                model = BuildStandardColumnBarPieChartVieModel(kpiInstance, kpiInCell, kpiOptionsForUser);
                model.SupportedChartTypes = kpiInstance.GetSupportedChartTypes();
                if (kpiInCell.DashboardRef != null)
                    model.DashboardId = kpiInCell.DashboardRef.DashboardId;
                model.DashboardKpiId = kpiInCell.KpiId;
                if (kpiInCell.RowRef != null)
                    model.DashboardRowId = kpiInCell.RowRef.DashboardRowId;

                model.LoadFiltersAndGroupings(kpiInstance);

                await LoadKpiFilterOptions(model);
                LoadKpiAppliedFiltersInView(model, kpiOptionsForUser);

                return View("KpiColumnBarPieChart", model);
            }

            if (theChartType == KPIChartType.SingleValue_SingleSeries ||
                theChartType == KPIChartType.SingleValue_SingleSeries_Sparkline ||
                theChartType == KPIChartType.SingleValue_Multiseries)
            {
                model = BuildStandardValueTargetViewModel(kpiInstance, kpiInCell, kpiOptionsForUser);
                model.SupportedChartTypes = kpiInstance.GetSupportedChartTypes();
                if (kpiInCell.DashboardRef != null) 
                    model.DashboardId = kpiInCell.DashboardRef.DashboardId;
                model.DashboardKpiId = kpiInCell.KpiId;
                if (kpiInCell.RowRef != null) 
                    model.DashboardRowId = kpiInCell.RowRef.DashboardRowId;

                model.LoadFiltersAndGroupings(kpiInstance);

                await LoadKpiFilterOptions(model);
                LoadKpiAppliedFiltersInView(model, kpiOptionsForUser);

                return View("KpiValueTarget", model);
            }

            KpiNotSupportedViewModel modelNotSupported = BuildNotSupported(kpiInstance, kpiInCell);
            return View("KpiViewNotSupported");
        }

        private void LoadKpiAppliedFiltersInView(KpiViewModel model, KpiUser kpiOptionsForUser)
        {
            if (!string.IsNullOrWhiteSpace(kpiOptionsForUser.FilterOffices))
                model.OfficesApplied = kpiOptionsForUser.FilterOffices.Split(',').ToList();
            if (!string.IsNullOrWhiteSpace(kpiOptionsForUser.FilterRegions))
                model.RegionsApplied = kpiOptionsForUser.FilterRegions.Split(',').ToList();
            if (!string.IsNullOrWhiteSpace(kpiOptionsForUser.FilterTaxPayerSegments))
                model.TaxPayerSegmentsApplied = kpiOptionsForUser.FilterTaxPayerSegments.Split(',').ToList();
        }

        public async Task<VoidResult> LoadKpiFilterOptions(KpiViewModel model)
        {
            List<Region> regions = await _mediator.Send(new GetListRegionsQuery());
            List<KpiFilterOption> options = new List<KpiFilterOption>();
            foreach(Region region in regions)
            {
                options.Add(new KpiFilterOption() { Value = region.regionID, Text = region.regionName });
            }
            model.FilterRegions = options;

            List<Office> offices = await _mediator.Send(new GetListOfficesQuery());
            options = new List<KpiFilterOption>();
            foreach (Office office in offices)
            {
                options.Add(new KpiFilterOption() { Value = office.officeID, Text = office.officeName });
            }
            model.FilterOffices = options;

            List<Domain.Segment.Segment> segments = await _mediator.Send(new GetListSegmentsQuery());
            options = new List<KpiFilterOption>();
            foreach (Domain.Segment.Segment segment in segments)
            {
                options.Add(new KpiFilterOption() { Value = segment.segmentID, Text = segment.segmentName });
            }
            model.FilterSegments = options;
            return new VoidResult();
        }

        private Domain.Indicators.KpiUser BuildStandardOptionsForUser(Kpi kpiInCell)
        {
            KpiUser obj = new KpiUser();
            obj.ChartType = kpiInCell.ViewType;
            obj.KpiRef = kpiInCell;
            obj.KpiUserId = Guid.Empty;
            obj.NumberOfPeriods = kpiInCell.NumberOfYears;
            obj.UserRef = null;

            return obj;
        }

        /// <summary>
        /// This method creates the KPI with standard options in the cell in a dashboard. This is used for 
        /// testing mainly or to have an initial set of options.
        /// </summary>
        /// <returns></returns>
        private Kpi BuildStandardKpiInCellOptions(KPIClassLibrary.KPI.KPI kpiInstance)
        {
            Kpi kpiInCell = new Kpi();
            KPIChartType firstViewType = kpiInstance.GetSupportedChartTypes().First();
            if (firstViewType == KPIChartType.Bar_MultiSeries ||
                firstViewType == KPIChartType.Bar_SingleSeries ||
                firstViewType == KPIChartType.Column_Multiseries ||
                firstViewType == KPIChartType.Column_SingleSeries ||
                firstViewType == KPIChartType.StackedBar_MultiSeries ||
                firstViewType == KPIChartType.StackedColumn_Multiseries ||
                firstViewType == KPIChartType.Donut_MultiSeries ||
                firstViewType == KPIChartType.SingleValue_Multiseries ||
                firstViewType == KPIChartType.Line_MultiSeries ||
                firstViewType == KPIChartType.Line_SingleSeries)
                kpiInCell.DivHeight = 2;
            if (firstViewType == KPIChartType.SingleValue_SingleSeries ||
                firstViewType == KPIChartType.SingleValue_SingleSeries_Sparkline)
                kpiInCell.DivHeight = 1;

            kpiInCell.ViewType = ChartUtilities.GetKpiViewModelFrom(firstViewType);
            kpiInCell.DivWidth = 4;
            kpiInCell.NumberOfYears = 5;
            kpiInCell.PeriodYear = DateTime.Now.Year;
            kpiInCell.FilterValue = "";
            kpiInCell.Title = kpiInstance.ID;

            return kpiInCell;
        }

        private KpiNotSupportedViewModel BuildNotSupported(KPIClassLibrary.KPI.KPI kpi, Kpi kpiInCell)
        {
            KpiNotSupportedViewModel model = new KpiNotSupportedViewModel();
            model.Title = kpi.Name;
            model.KpiId = kpi.ID;
            model.ValueDescription = kpi.Description;
            model.Period = System.Enum.Parse<PeriodType>(kpi.PeriodType.ToString());

            return model;
        }

        private KpiColumnBarPieChartViewModel BuildStandardColumnBarPieChartVieModel(
            KPIClassLibrary.KPI.KPI kpi,
            Domain.Indicators.Kpi kpiInCell,
            KpiUser kpiOptionsForUser)
        {
            // Get raw data
            _kpiDataGetter.GetData(kpi, kpiInCell.ViewType);

            // Set master info
            KpiColumnBarPieChartViewModel result = new KpiColumnBarPieChartViewModel();
            result.Title = kpi.Name;
            result.KpiId = kpi.ID;
            result.ValueDescription = kpi.Description;
            result.Period = System.Enum.Parse<PeriodType>(kpi.PeriodType.ToString());
            result.NumberOfPeriods = kpiOptionsForUser.NumberOfPeriods;
            result.LoadFiltersAndGroupings(kpi);
            result.Type = kpiInCell.ViewType;
            result.ChartType = ChartUtilities.GetKPIChartTypeFrom(kpiOptionsForUser.ChartType);
            result.DivHeight = kpiInCell.DivHeight;
            result.DivWidth = kpiInCell.DivWidth;
            result.SameColumnThanPrevious = false;

            // Get The periods, this will give us the number of columns to use
            List<DateTime> period = kpi.GetTimeData();

            result.Data = new MultiSeriesData();

            string periods = "";
            if (kpi.PeriodType == KPIPeriodType.Year)
                periods = string.Join(",", period.Select(x => x.Year).ToArray());
            else if (kpi.PeriodType == KPIPeriodType.Month)
                periods = string.Join(",", period.Select(x => x.ToString("MMMM")).ToArray());
            result.Data.CategoriesFromString(periods);
            result.YAxisTitle = kpi.YAxisTitle;

            // For every tax found
            foreach (var groupName in kpi.GetGroupCategories(kpi.Grouping))
            {
                _logger.LogInformation($"Reading the Tax group {groupName} from KPI " + result.KpiMnemonic);

                // Get the actual values for a specific tax
                List<double> data = kpi.GetData(kpi.Grouping, groupName);

                // Create the series
                SerieData serie = new SerieData();
                serie.Name = groupName;

                for (int i = 0; i < period.Count; i++)
                {
                    serie.Points.Add(new PointData { Y = Convert.ToDecimal(data[i]) });
                    //_logger.LogInformation(String.Format("{0}, {1}", period[i].Year, data[i]));
                }

                result.Data.Series.Add(serie);
            }

            if (kpi.Grouping == KPIGroupType.CustomPost)
            {
                List<CustomsPost> customsPosts = _customsPostRepository.FindAll(null);
                foreach(SerieData serie in result.Data.Series)
                {
                    CustomsPost post = customsPosts.Find(x => x.customsPostID == serie.Name);
                    if (post != null)
                        serie.Name = post.customsPostName;
                }
            }

            result.Stacked = kpiOptionsForUser.ChartType == KpiViewModelType.StackedBar_MultiSeries ||
                kpiOptionsForUser.ChartType == KpiViewModelType.StackedColumn_Multiseries ? "normal" : "";

            result.Chart = 
                (kpiOptionsForUser.ChartType == KpiViewModelType.Column_Multiseries ||
                kpiOptionsForUser.ChartType == KpiViewModelType.Column_SingleSeries ||
                kpiOptionsForUser.ChartType == KpiViewModelType.StackedColumn_Multiseries) ? "column" :
                (kpiOptionsForUser.ChartType == KpiViewModelType.Bar_MultiSeries ||
                kpiOptionsForUser.ChartType == KpiViewModelType.Bar_SingleSeries ||
                kpiOptionsForUser.ChartType == KpiViewModelType.StackedBar_MultiSeries) ? "bar" :
                kpiOptionsForUser.ChartType == KpiViewModelType.Donut_MultiSeries ? "pie" :
                (kpiOptionsForUser.ChartType == KpiViewModelType.Line_MultiSeries ||
                kpiOptionsForUser.ChartType == KpiViewModelType.Line_SingleSeries) ? "line" : "";

            return result;
        }

        private KpiValueTargetViewModel BuildStandardValueTargetViewModel(
            KPIClassLibrary.KPI.KPI kpi,
            Domain.Indicators.Kpi kpiInCell,
            KpiUser kpiOptionsForUser)
        {
            // Get raw data
            _kpiDataGetter.GetData(kpi, kpiOptionsForUser.ChartType);
            
            // Set master info
            KpiValueTargetViewModel result = new KpiValueTargetViewModel();
            result.Title = kpi.Name;
            result.KpiId = kpi.ID;
            result.ValueDescription = kpi.Description;
            result.Period = System.Enum.Parse<PeriodType>(kpi.PeriodType.ToString());
            result.LoadFiltersAndGroupings(kpi);
            result.ChartType = Utilities.ChartUtilities.GetKPIChartTypeFrom(kpiOptionsForUser.ChartType);
            result.DivHeight = result.ChartType == KPIChartType.SingleValue_Multiseries ? 2 : 1;
            result.DivWidth = kpiInCell.DivWidth;
            result.SameColumnThanPrevious = result.ChartType == KPIChartType.SingleValue_Multiseries ? false : true;

            List<string> groupCategories = new List<string>();
            if (kpi.Grouping == KPIGroupType.None)
                groupCategories.Add("NONE");
            else
                groupCategories = kpi.GetGroupCategories(kpi.Grouping);

            // titles are just one regardless of categories
            List<DateTime> dates = kpi.GetTimeData();
            foreach (DateTime d in dates)
            {
                switch (kpi.PeriodType)
                {
                    case KPIPeriodType.Year:
                        result.AllTitles.Add(d.Year.ToString());
                        break;
                    case KPIPeriodType.Month:
                        result.AllTitles.Add(d.ToString("MMMM"));
                        break;
                    default:
                        break;
                }
            }
            result.ValueSuffix = kpi.Unit;
            result.ValueNumberDecimals = 2;
            result.ShowTarget = false;

            // Now fetching data for each category
            
            foreach (string category in groupCategories)
            {
                bool noValues = false;

                List<double> values = null;
                try
                {
                    values = category == "NONE" ? kpi.GetData() : kpi.GetData(kpi.Grouping, category);
                }
                catch (Exception q)
                {
                    _logger.LogError(q, "Could not retrieve data for KPI " + kpi.Name);
                    values = new List<double>();
                    noValues = true;
                }

                if (noValues)
                    continue;
                
                result.AllValues[category] = new List<decimal>();
                
                foreach (double d in values)
                {
                    result.AllValues[category].Add(Convert.ToDecimal(d));
                }

                double lastValue = values.LastOrDefault();
                bool existsPreviousValue = false;
                if (values.Count > 1)
                    existsPreviousValue = true;

                result.NumberOfPeriods = kpiOptionsForUser.NumberOfPeriods;

                result.Value[category] = Convert.ToDecimal(lastValue);
                result.Label[category] = category;

                List<DateTime> listDates = kpi.GetTimeData();
                DateTime currentValueDate = DateTime.MinValue;
                if (listDates != null && listDates.Count() > 0)
                    currentValueDate = listDates.Last();

                // format dates, yearly
                ConfigUtilities.FormatPeriod(result, currentValueDate);

                if (existsPreviousValue)
                {
                    double previousValue = values.ElementAt(values.Count() - 2);
                    double ratioDifference = ((lastValue / previousValue) - 1.0) * 100.0;

                    result.ShowChange = true;
                    result.ChangeValue[category] = 0;
                    try
                    {
                        result.ChangeValue[category] = Convert.ToDecimal(ratioDifference);                        
                    } 
                    catch(Exception q) 
                    {
                        _logger.LogWarning(q, "Keeping in 0 the change value");
                        result.ChangeValue[category] = 0;
                    }
                    result.ChangeSuffix[category] = "%";
                    result.ChangeDirection[category] = ratioDifference > 0 ? 1 : -1;
                }
                else
                {
                    result.ShowChange = false;
                }
            }

            ChangeLabelsIfNecessary(kpi, result);

            return result;
        }

        private void ChangeLabelsIfNecessary(KPIClassLibrary.KPI.KPI kpi, KpiValueTargetViewModel result)
        {
            if (kpi.Grouping == KPIGroupType.CustomPost)
            {
                Dictionary<string, string> newLabel = new Dictionary<string, string>();

                List<CustomsPost> customsPosts = _customsPostRepository.FindAll(null);
                foreach (string customsPostId in result.Label.Keys)
                {
                    CustomsPost post = customsPosts.Find(x => x.customsPostID == customsPostId);
                    if (post != null)
                        newLabel.Add(customsPostId, post.customsPostName);
                }

                result.Label = newLabel;
            }
        }

    }
}