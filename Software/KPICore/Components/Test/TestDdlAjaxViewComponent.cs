﻿using AEWeb.ViewModels.Test;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Components.Test
{
    public class TestDdlAjaxViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke(string value)
        {
            TestAjaxViewModel model = new TestAjaxViewModel();
            model.Value = value;
            return View("Default", model);
        }
    }
}
