﻿using Infrastructure.ViewModels.Shared;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AEWeb.Components.Shared
{
    public class MessagesViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            MessagesViewModel messages;
            if (TempData["Messages"] is string s)
            {
                messages = JsonConvert.DeserializeObject<MessagesViewModel>(s);
            }
            else
                messages = new MessagesViewModel();

            return View("Default", messages);
        }
    }
}
