﻿using Domain.Security;
using AEWeb.ViewModels.Menu;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using KPIClassLibrary.KPI;
using MediatR;
using Infrastructure.Mediator.Indicator.Queries;
using Domain.Indicators;
using Infrastructure.Mediator.Indicator.Commands;
using Microsoft.Extensions.Localization;
using Infrastructure.ViewModels.Shared;

namespace AEWeb.Components.Shared
{
    public class NavigationMenuViewComponent : ViewComponent
    {
        private readonly IAuthorizationService _authorization;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IMediator _mediator;
        private readonly ILogger<NavigationMenuViewComponent> _logger;
        private IStringLocalizer<SharedResources> _localizer;

        public NavigationMenuViewComponent(
            IAuthorizationService authorization,
            IWebHostEnvironment hostingEnvironment,
            ILogger<NavigationMenuViewComponent> logger,
            UserManager<ApplicationUser> userManager,
            IMediator mediator,
            IStringLocalizer<SharedResources> localizer)
        {
            _authorization = authorization;
            _hostingEnvironment = hostingEnvironment;
            _logger = logger;
            _userManager = userManager;
            _mediator = mediator;
            _localizer = localizer;
        }
        public async Task<IViewComponentResult> InvokeAsync(string nothing)
        {
            _logger.LogInformation("Showing the navigation menu");

            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);

            NavigationMenuViewModel theMenu = new NavigationMenuViewModel();
            theMenu.Username = user.FullName;
            string contentRootPath = _hostingEnvironment.ContentRootPath;
            string menusJson = "";
            try
            {
                menusJson = System.IO.File.ReadAllText(contentRootPath + "/DataFiles/menu.json");
                _logger.LogDebug("The file with the menu configuration has been read: " + menusJson.Length + " chars");
            }
            catch (Exception q)
            {
                _logger.LogError("The menu configuration couldnot be loaded, maybe missing file or malformed", q);
            }

            dynamic menusJsonObject = JsonConvert.DeserializeObject(menusJson);

            await ReadMenuItems(theMenu, menusJsonObject);

            await ReadDomainForUser(theMenu, user.Id);

            ReadConfiguredDomains(theMenu);

            return View("Default", theMenu);
        }

        private void ReadConfiguredDomains(NavigationMenuViewModel theMenu)
        {
            theMenu.DomainsConfigured.Clear();
            string[] domains = Enum.GetNames(typeof(KPIDomain));
            foreach(string aDomain in domains)
            {
                theMenu.DomainsConfigured.Add(new ListItemViewModel() { 
                    Text = _localizer[aDomain], 
                    Value = aDomain 
                });
            }
        }

        private async Task<NavigationMenuViewModel> ReadDomainForUser(NavigationMenuViewModel theMenu, string userid)
        {
            DomainUser domainUser = await _mediator.Send(new GetDomainUser() { UserId = userid });
            KPIDomain theDomain = KPIDomain.Tax;
            
            if (domainUser != null && !string.IsNullOrWhiteSpace(domainUser.Domain))
            {
                theDomain = (KPIDomain)(Enum.Parse(typeof(KPIDomain), domainUser.Domain));
            }
            else 
            {
                domainUser = await _mediator.Send(new CreateUpdateDomainUserCommand()
                {
                    Domain = theDomain.ToString(),
                    UserId = userid
                }) ;
            }

            theMenu.Domain = theDomain;

            return theMenu;
        }

        private async Task<NavigationMenuViewModel> ReadMenuItems(NavigationMenuViewModel theMenu,
            dynamic submenus)
        {
            if (submenus == null)
                return theMenu;

            List<NavigationMenuViewModel> menus = new List<NavigationMenuViewModel>();
            foreach (var objMenu in submenus)
            {
                string policy = objMenu.claim;
                bool isAllowedToViewMenu = false;
                if (policy.Equals("NONE"))
                    isAllowedToViewMenu = true;
                else
                {
                    AuthorizationResult allowed = await _authorization.AuthorizeAsync(UserClaimsPrincipal, policy);
                    isAllowedToViewMenu = allowed.Succeeded;
                }
                if (isAllowedToViewMenu)
                {
                    NavigationMenuViewModel newMenu = new NavigationMenuViewModel();
                    newMenu.Action = objMenu.action;
                    newMenu.Controller = objMenu.controller;
                    newMenu.Url = objMenu.url;
                    newMenu.ResourceItem = objMenu.resourceItem;
                    newMenu.Icon = objMenu.icon;
                    newMenu.Menu = objMenu.menu;
                    ReadMenuItems(newMenu, objMenu.submenus);
                    menus.Add(newMenu);
                    _logger.LogDebug("Adding menu " + newMenu.Menu);
                }
            }
            theMenu.Submenus = menus;

            return theMenu;
        }
    }
}
