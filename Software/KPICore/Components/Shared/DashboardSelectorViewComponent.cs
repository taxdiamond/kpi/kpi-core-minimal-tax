﻿using AEWeb.ViewModels.Dashboard;
using Domain.Indicators;
using Domain.Security;
using Infrastructure.Mediator.Indicator.Queries;
using KPIClassLibrary.KPI;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Components.Shared
{
    public class DashboardSelectorViewComponent : ViewComponent
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private ILogger<DashboardSelectorViewComponent> _logger;
        private IMediator _mediator;

        public DashboardSelectorViewComponent(UserManager<ApplicationUser> userManager,
            ILogger<DashboardSelectorViewComponent> logger,
            IMediator mediator)
        {
            _userManager = userManager;
            _mediator = mediator;
            _logger = logger;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = await _userManager.FindByNameAsync(User.Identity.Name);
            DomainUser domain = await _mediator.Send(new GetDomainUser() { UserId = user.Id });
            DashboardSelectorViewModel model = new DashboardSelectorViewModel();
            model.userid = user.Id;
            model.domain = domain.Domain.Value;

            return View("Default", model);
        }
    }
}
