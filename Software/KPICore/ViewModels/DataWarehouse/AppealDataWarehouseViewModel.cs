﻿using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class AppealDataWarehouseViewModel
    {
        public DateTime AppealDate { get; set; }
        public string Region { get; set; }
        public string Office { get; set; }
        public string Segment { get; set; }
        public string Concept { get; set; }
        public int NumberAppealsCasesPending { get; set; }
        public decimal AmountAppealed { get; set; }
        public int NumberCasesResolved { get; set; }
        public int NumberCasesResolvedFavorTaxPayer { get; set; }
        public int NumberCasesResolvedFavorTaxAdministration { get; set; }
        public int AverageDaysToResolveCases { get; set; }
        public decimal TotalAmountResolvedFavorTaxPayer { get; set; }
        public decimal TotalAmountResolvedFavorTaxAdministration { get; set; }
        public int NumberOfAppealCasesSubmitted { get; set; }
        public decimal ValueOfAppealCasesSubmitted { get; set; }

        public string AppealDateForDisplay
        {
            get { return AppealDate.ToShortDateString(); }
        }
        
        public static AppealDataWarehouseViewModel FromItem(Appeal item)
        {
            AppealDataWarehouseViewModel result = new AppealDataWarehouseViewModel()
            {
                Concept = item.Concept,
                Office = item.OfficeID == null || item.OfficeID.officeName == null ? "" : item.OfficeID.officeName.Value,
                Region = item.RegionID == null || item.RegionID.regionName == null ? "" : item.RegionID.regionName.Value,
                NumberAppealsCasesPending = item.NumberAppealsCasesPending,
                AmountAppealed = item.AmountAppealed,
                NumberCasesResolved = item.NumberCasesResolved,
                NumberCasesResolvedFavorTaxPayer = item.NumberCasesResolvedFavorTaxPayer,
                NumberCasesResolvedFavorTaxAdministration = item.NumberCasesResolvedFavorTaxAdministration,
                AverageDaysToResolveCases = item.AverageDaysToResolveCases,
                TotalAmountResolvedFavorTaxPayer = item.TotalAmountResolvedFavorTaxPayer,
                TotalAmountResolvedFavorTaxAdministration = item.TotalAmountResolvedFavorTaxAdministration,
                NumberOfAppealCasesSubmitted = item.NumberOfAppealCasesSubmitted,
                ValueOfAppealCasesSubmitted = item.ValueOfAppealCasesSubmitted,
                AppealDate = item.AppealsDate.Value,
                Segment = item.SegmentID == null || item.SegmentID.segmentName == null ? "" : item.SegmentID.segmentName.Value
            };
            return result;
        }
    }
}
