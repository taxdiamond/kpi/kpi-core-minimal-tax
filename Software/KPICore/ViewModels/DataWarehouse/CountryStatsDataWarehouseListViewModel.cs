﻿using Domain.CountryStats;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class CountryStatsDataWarehouseListViewModel : AppListViewModel<CountryStatsDataWarehouseViewModel>
    {
        public List<CountryStatsDataWarehouseViewModel> CountryStatsList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public CountryStatsDataWarehouseListViewModel()
        {
            items = new List<CountryStatsDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }

        public static CountryStatsDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<CountryStats> source)
        {
            CountryStatsDataWarehouseListViewModel result = new CountryStatsDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<CountryStats> list)
        {
            CountryStatsList.Clear();
            foreach (CountryStats item in list)
            {
                CountryStatsList.Add(CountryStatsDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
