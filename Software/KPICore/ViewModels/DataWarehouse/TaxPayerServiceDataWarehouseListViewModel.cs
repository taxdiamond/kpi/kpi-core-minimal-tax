﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class TaxPayerServiceDataWarehouseListViewModel : AppListViewModel<TaxPayerServiceDataWarehouseViewModel>
    {
        public List<TaxPayerServiceDataWarehouseViewModel> TaxPayerServiceList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public TaxPayerServiceDataWarehouseListViewModel()
        {
            items = new List<TaxPayerServiceDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static TaxPayerServiceDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<TaxPayerService> source)
        {
            TaxPayerServiceDataWarehouseListViewModel result = new TaxPayerServiceDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<TaxPayerService> list)
        {
            TaxPayerServiceList.Clear();
            foreach (TaxPayerService item in list)
            {
                TaxPayerServiceList.Add(TaxPayerServiceDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
