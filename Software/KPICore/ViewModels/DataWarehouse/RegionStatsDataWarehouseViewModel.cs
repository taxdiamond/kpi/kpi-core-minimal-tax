﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Region;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RegionStatsDataWarehouseViewModel
    {
        [Display(Name = "Record ID")]
        public int RegionStatsId { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime StatsDate { get; set; }
        [Display(Name = "Region")]
        public string Region { get; set; }
        [Display(Name = "GDP Per Capita at Date")]
        public decimal? GdpPerCapitaAtDate { get; set; }
        [Display(Name = "Nominal GDP at Date")]
        public decimal? NominalGdpAtDate { get; set; }
        [Display(Name = "Population at Date")]
        public int PopulationAtDate { get; set; }


        public string StatsDateForDisplay { get { return StatsDate.ToShortDateString(); } }

        public static RegionStatsDataWarehouseViewModel FromItem(RegionStats item)
        {
            RegionStatsDataWarehouseViewModel result = new RegionStatsDataWarehouseViewModel()
            {
                StatsDate = item.StatsDate.Value,
                GdpPerCapitaAtDate = item.GdpPerCapitaAtDate == null ? 0 : item.GdpPerCapitaAtDate.Value,
                NominalGdpAtDate = item.NominalGdpAtDate == null ? 0 : item.NominalGdpAtDate.Value,
                PopulationAtDate = item.PopulationAtDate == null ? 0 : item.PopulationAtDate.Value,
                Region = item.RegionID == null ? "" : item.RegionID.regionName.Value,
                RegionStatsId = item.RegionStatsId
            };
            return result;
        }
    }
}
