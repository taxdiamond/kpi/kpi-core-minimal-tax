﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class AppealDataWarehouseListViewModel : AppListViewModel<AppealDataWarehouseViewModel>
    {
        public List<AppealDataWarehouseViewModel> AppealList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public AppealDataWarehouseListViewModel()
        {
            items = new List<AppealDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static AppealDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<Appeal> source)
        {
            AppealDataWarehouseListViewModel result = new AppealDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<Appeal> list)
        {
            AppealList.Clear();
            foreach (Appeal item in list)
            {
                AppealList.Add(AppealDataWarehouseViewModel.FromItem(item));
            }
        }

    }
}
