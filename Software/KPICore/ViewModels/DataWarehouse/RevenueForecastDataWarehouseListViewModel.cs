﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RevenueForecastDataWarehouseListViewModel : AppListViewModel<RevenueForecastDataWarehouseViewModel>
    {
        public List<RevenueForecastDataWarehouseViewModel> RevenueForecastList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public RevenueForecastDataWarehouseListViewModel()
        {
            items = new List<RevenueForecastDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static RevenueForecastDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<RevenueForecast> source)
        {
            RevenueForecastDataWarehouseListViewModel result = new RevenueForecastDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<RevenueForecast> list)
        {
            RevenueForecastList.Clear();
            foreach (var item in list)
            {
                RevenueForecastList.Add(RevenueForecastDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
