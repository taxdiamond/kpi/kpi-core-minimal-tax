﻿using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class ArrearDataWarehouseViewModel
    {
        public DateTime ArrearDate { get; set; }
        public string Region { get; set; }
        public string Office { get; set; }
        public string Segment { get; set; }
        public string Concept { get; set; }
        public int NumberOfArrearCasesPending { get; set; }
        public int NumberOfArrearCasesGenerated { get; set; }
        public decimal Amount { get; set; }
        public decimal ValueOfArrearCasesGenerated { get; set; }
        public decimal NumberOfArrearCasesResolved { get; set; }
        public decimal ValueOfArrearCasesResolved { get; set; }

        public string ArrearDateForDisplay
        {
            get { return ArrearDate.ToShortDateString(); }
        }

        public static ArrearDataWarehouseViewModel FromItem(Arrear item)
        {
            ArrearDataWarehouseViewModel result = new ArrearDataWarehouseViewModel()
            {
                Concept = item.Concept,
                NumberOfArrearCasesPending = item.NumberOfArrearCasesPending,
                NumberOfArrearCasesGenerated = item.NumberOfArrearCasesGenerated,
                Office = item.OfficeID == null || item.OfficeID.officeName == null ? "" : item.OfficeID.officeName.Value,
                Region = item.RegionID == null || item.RegionID.regionName == null ? "" : item.RegionID.regionName.Value,
                Amount = item.Amount,
                ValueOfArrearCasesGenerated = item.ValueOfArrearCasesGenerated,
                NumberOfArrearCasesResolved = item.NumberOfArrearCasesResolved,
                ValueOfArrearCasesResolved = item.ValueOfArrearCasesResolved,
                ArrearDate = item.ArrearDate.Value,
                Segment = item.SegmentID == null || item.SegmentID.segmentName == null ? "" : item.SegmentID.segmentName.Value
            };
            return result;
        }
    }
}
