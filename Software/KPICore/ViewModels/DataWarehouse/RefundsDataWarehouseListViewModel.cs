﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RefundsDataWarehouseListViewModel : AppListViewModel<RefundsDataWarehouseViewModel>
    {
        public List<RefundsDataWarehouseViewModel> RefundsList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public RefundsDataWarehouseListViewModel()
        {
            items = new List<RefundsDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static RefundsDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<Refund> source)
        {
            RefundsDataWarehouseListViewModel result = new RefundsDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<Refund> list)
        {
            RefundsList.Clear();
            foreach (Refund item in list)
            {
                RefundsList.Add(RefundsDataWarehouseViewModel.FromItem(item));
            }
        }

    }
}
