﻿using Domain.Office;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class OfficeDataWarehouseListViewModel : AppListViewModel<OfficeDataWarehouseViewModel>
    {
        public List<OfficeDataWarehouseViewModel> OfficeList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public OfficeDataWarehouseListViewModel()
        {
            items = new List<OfficeDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static OfficeDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<Office> source)
        {
            OfficeDataWarehouseListViewModel result = new OfficeDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<Office> list)
        {
            OfficeList.Clear();
            foreach (Office item in list)
            {
                OfficeList.Add(OfficeDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
