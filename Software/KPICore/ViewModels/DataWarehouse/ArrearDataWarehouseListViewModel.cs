﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class ArrearDataWarehouseListViewModel : AppListViewModel<ArrearDataWarehouseViewModel>
    {
        public List<ArrearDataWarehouseViewModel> ArrearList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public ArrearDataWarehouseListViewModel()
        {
            items = new List<ArrearDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static ArrearDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<Arrear> source)
        {
            ArrearDataWarehouseListViewModel result = new ArrearDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<Arrear> list)
        {
            ArrearList.Clear();
            foreach (Arrear item in list)
            {
                ArrearList.Add(ArrearDataWarehouseViewModel.FromItem(item));
            }
        }

    }
}
