﻿using Domain.Region;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RegionDataWarehouseListViewModel : AppListViewModel<RegionDataWarehouseViewModel>
    {
        public List<RegionDataWarehouseViewModel> RegionList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public RegionDataWarehouseListViewModel()
        {
            items = new List<RegionDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static RegionDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<Region> source)
        {
            RegionDataWarehouseListViewModel result = new RegionDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<Region> list)
        {
            RegionList.Clear();
            foreach (Region item in list)
            {
                RegionList.Add(RegionDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
