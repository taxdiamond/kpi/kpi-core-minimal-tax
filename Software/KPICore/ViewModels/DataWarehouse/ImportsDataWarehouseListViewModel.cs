﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class ImportsDataWarehouseListViewModel : AppListViewModel<ImportsDataWarehouseViewModel>
    {
        public List<ImportsDataWarehouseViewModel> ImportsList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public ImportsDataWarehouseListViewModel()
        {
            items = new List<ImportsDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static ImportsDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<Import> source)
        {
            ImportsDataWarehouseListViewModel result = new ImportsDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<Import> list)
        {
            ImportsList.Clear();
            foreach (Import item in list)
            {
                ImportsList.Add(ImportsDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
