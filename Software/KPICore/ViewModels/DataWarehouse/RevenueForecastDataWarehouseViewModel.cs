﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RevenueForecastDataWarehouseViewModel
    {
        [Display(Name = "Record ID")]
        public int ForecastItemID { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime ForecastDate { get; set; }
        public string Region { get; set; }
        [Display(Name = "Office")]
        public string Office { get; set; }
        [Display(Name = "Taxpayer Segment")]
        public string Segment { get; set; }
        [Display(Name = "Revenue Type")]
        public string Concept { get; set; }
        [Display(Name = "Revenue Concept")]
        public string ConceptID { get; set; }
        [Display(Name = "Amount")]
        public decimal Amount { get; set; }

        public string ForecastDateForDisplay { get { return ForecastDate.ToShortDateString(); } }

        public static RevenueForecastDataWarehouseViewModel FromItem(RevenueForecast item)
        {
            RevenueForecastDataWarehouseViewModel result = new RevenueForecastDataWarehouseViewModel()
            {
                ForecastDate = item.ForecastDate.Value,
                Amount = item.Amount == null ? 0 : item.Amount.Value,
                Concept = item.Concept,
                ConceptID = item.ConceptID,
                Office = item.OfficeID == null ? "" : item.OfficeID.officeName.Value,
                Region = item.RegionID == null ? "" : item.RegionID.regionName.Value,
                ForecastItemID = item.ForecastItemID,
                Segment = item.SegmentID == null ? "" : item.SegmentID.segmentName.Value
            };
            return result;
        }
    }
}
