﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class TaxRateDataWarehouseListViewModel : AppListViewModel<TaxRateDataWarehouseViewModel>
    {
        public List<TaxRateDataWarehouseViewModel> TaxRateList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public TaxRateDataWarehouseListViewModel()
        {
            items = new List<TaxRateDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static TaxRateDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<TaxRate> source)
        {
            TaxRateDataWarehouseListViewModel result = new TaxRateDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<TaxRate> list)
        {
            TaxRateList.Clear();
            foreach (TaxRate item in list)
            {
                TaxRateList.Add(TaxRateDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
