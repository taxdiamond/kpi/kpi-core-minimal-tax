﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Region;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RegionDataWarehouseViewModel
    {
        [Display(Name = "Region ID")]
        public string regionid { get; set; }
        [Display(Name = "Region Name")]
        public string regionname { get; set; }

        public static RegionDataWarehouseViewModel FromItem(Region item)
        {
            RegionDataWarehouseViewModel result = new RegionDataWarehouseViewModel()
            {
                regionname = item.regionName,
                regionid = item.regionID
            };
            return result;
        }
    }
}
