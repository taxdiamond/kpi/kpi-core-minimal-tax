﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class HumanResourcesDataWarehouseViewModel
    {
        [Display(Name = "Record ID")]
        public int HRItemID { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime HRItemReportDate { get; set; }
        [Display(Name = "Region")]
        public string Region { get; set; }
        [Display(Name = "Office")]
        public string Office { get; set; }
        [Display(Name = "Total Human Resources")]
        public int TotalHumanResources { get; set; }
        [Display(Name = "Total Human Resources in Tax Collection")]
        public int TotalHumanResourcesInTaxCollection { get; set; }
        [Display(Name = "Total Human Resources in Taxpayer Services")]
        public int TotalHumanResourcesInTaxPayerServices { get; set; }
        [Display(Name = "Total Human Resources in Tax Enforcement")]
        public int TotalHumanResourcesInTaxEnforcement { get; set; }
        [Display(Name = "Total Human Resources in Audits")]
        public int TotalHumanResourcesInAudits { get; set; }
        [Display(Name = "Total Human Resources in Appeals")]
        public int TotalHumanResourcesInAppeals { get; set; }
        [Display(Name = "Number of Positions Filled in Competitive Process")]
        public int NumberPositionsFilledCompetitiveProcess { get; set; }


        public string HRItemReportDateForDisplay { get { return HRItemReportDate.ToShortDateString(); } }

        public static HumanResourcesDataWarehouseViewModel FromItem(HumanResource item)
        {
            HumanResourcesDataWarehouseViewModel result = new HumanResourcesDataWarehouseViewModel()
            {
                HRItemID = item.HRItemID,
                HRItemReportDate = item.HRItemReportDate.Value,
                NumberPositionsFilledCompetitiveProcess = item.NumberPositionsFilledCompetitiveProcess,
                Office = item.OfficeID.officeName,
                Region = item.RegionID.regionName,
                TotalHumanResources = item.TotalHumanResources,
                TotalHumanResourcesInAudits = item.TotalHumanResourcesInAudits,
                TotalHumanResourcesInAppeals = item.TotalHumanResourcesInAppeals,
                TotalHumanResourcesInTaxCollection = item.TotalHumanResourcesInTaxCollection,
                TotalHumanResourcesInTaxEnforcement = item.TotalHumanResourcesInTaxEnforcement,
                TotalHumanResourcesInTaxPayerServices = item.TotalHumanResourcesInTaxPayerServices
            };
            return result;
        }
    }
}
