﻿using Domain.AuditStats;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class AuditStasDataWarehouseListViewModel : AppListViewModel<AuditStasDataWarehouseViewModel>
    {
        public List<AuditStasDataWarehouseViewModel> AuditStasList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public AuditStasDataWarehouseListViewModel()
        {
            items = new List<AuditStasDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static AuditStasDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<AuditStats> source)
        {
            AuditStasDataWarehouseListViewModel result = new AuditStasDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<AuditStats> list)
        {
            AuditStasList.Clear();
            foreach (AuditStats item in list)
            {
                AuditStasList.Add(AuditStasDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
