﻿using Domain.DoingBusinessStats;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class DoingBusinessTaxStatsDataWarehouseViewModel
    {
        public DateTime StatsDate { get; set; }
        public decimal Time { get; set; }

        public string StatsDateForDisplay
        {
            get { return StatsDate.ToShortDateString(); }
        }

        public static DoingBusinessTaxStatsDataWarehouseViewModel FromItem(DoingBusinessTaxStats item)
        {
            DoingBusinessTaxStatsDataWarehouseViewModel result = new DoingBusinessTaxStatsDataWarehouseViewModel()
            {
                StatsDate = item.StatsDate.Value,
                Time = item.Time == null ? 0 : item.Time.Value
            };
            return result;
        }
    }
}
