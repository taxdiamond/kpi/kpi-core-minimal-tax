﻿using Domain.AuditStats;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class AuditStasDataWarehouseViewModel
    {
        [Display(Name = "Record ID")]
        public Int32 AuditDataID { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime StatsDate { get; set; }
        [Display(Name = "Number of Audits Performed")]
        public int NumberOfAuditsPerformed { get; set; }
        [Display(Name = "Number of Audits Resulted in Additional Assessments")]
        public int NumberOfAuditsResultedInAdditionalAssessments { get; set; }
        [Display(Name = "Total Additional Assessment")]
        public decimal TotalAdditionalAssessment { get; set; }
        [Display(Name = "Region")]
        public string Region { get; set; }
        [Display(Name = "Office")]
        public string Office { get; set; }
        [Display(Name = "Taxpayer Segment")]
        public string Segment { get; set; }


        public string StatsDateForDisplay { get { return StatsDate.ToShortDateString(); } }

        public static AuditStasDataWarehouseViewModel FromItem(AuditStats item)
        {
            AuditStasDataWarehouseViewModel result = new AuditStasDataWarehouseViewModel()
            {
                AuditDataID = item.AuditDataID,
                NumberOfAuditsPerformed = item.NumberOfAuditsPerformed,
                NumberOfAuditsResultedInAdditionalAssessments = item.NumberOfAuditsResultedInAdditionalAssessments,
                Office = item.OfficeID.officeName,
                Region = item.RegionID.regionName,
                Segment = item.SegmentID.segmentName,
                StatsDate = item.StatsDate.Value,
                TotalAdditionalAssessment = item.TotalAdditionalAssessment == null ? 0 : item.TotalAdditionalAssessment.Value
            };
            return result;
        }
    }
}
