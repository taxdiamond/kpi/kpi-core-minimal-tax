﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class FilingStatsDataWarehouseListViewModel : AppListViewModel<FilingStatsDataWarehouseViewModel>
    {
        public List<FilingStatsDataWarehouseViewModel> FilingStatsList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public FilingStatsDataWarehouseListViewModel()
        {
            items = new List<FilingStatsDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static FilingStatsDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<FilingStats> source)
        {
            FilingStatsDataWarehouseListViewModel result = new FilingStatsDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<FilingStats> list)
        {
            FilingStatsList.Clear();
            foreach (FilingStats item in list)
            {
                FilingStatsList.Add(FilingStatsDataWarehouseViewModel.FromItem(item));
            }
        }

    }
}
