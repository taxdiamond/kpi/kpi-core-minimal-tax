﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class TaxAdministrationExpenseDataWarehouseListViewModel : AppListViewModel<TaxAdministrationExpenseDataWarehouseViewModel>
    {
        public List<TaxAdministrationExpenseDataWarehouseViewModel> TaxAdministrationExpenseList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public TaxAdministrationExpenseDataWarehouseListViewModel()
        {
            items = new List<TaxAdministrationExpenseDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static TaxAdministrationExpenseDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<TaxAdministrationExpense> source)
        {
            TaxAdministrationExpenseDataWarehouseListViewModel result = new TaxAdministrationExpenseDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<TaxAdministrationExpense> list)
        {
            TaxAdministrationExpenseList.Clear();
            foreach (var item in list)
            {
                TaxAdministrationExpenseList.Add(TaxAdministrationExpenseDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
