﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RevenueSummaryByMonthDataWarehouseViewModel
    {
        [Display(Name = "Record ID")]
        public int RevenueSummaryId { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime RevenueDate { get; set; }
        [Display(Name = "Total Revenue")]
        public decimal TotalRevenue { get; set; }
        [Display(Name = "Total Revenue Paid in Time")]
        public decimal TotalRevenuePaidInTIme { get; set; }
        [Display(Name = "Total Revenue From Arrears")]
        public decimal TotalRevenueFromArrears { get; set; }
        [Display(Name = "Total Revenue From Fines")]
        public decimal TotalRevenueFromFines { get; set; }
        [Display(Name = "Total Revenue From Audits")]
        public decimal TotalRevenueFromAudits { get; set; }
        [Display(Name = "Total Revenue From Enforcement Collection")]
        public decimal TotalRevenueFromEnforcementCollection { get; set; }
        [Display(Name = "Total Revenue From Voluntary Compliance")]
        public decimal TotalRevenueFromVoluntaryCompliance { get; set; }
        [Display(Name = "Total Revenue From Taxes")]
        public decimal TotalRevenueFromTaxes { get; set; }
        [Display(Name = "Total Revenue From Litigation")]
        public decimal TotalRevenueFromLitigation { get; set; }
        [Display(Name = "Region")]
        public string Region { get; set; }
        [Display(Name = "Office")]
        public string Office { get; set; }
        [Display(Name = "Taxpayer Segment")]
        public string Segment { get; set; }

        public string RevenueDateForDisplay { get { return RevenueDate.ToShortDateString(); } }

        public static RevenueSummaryByMonthDataWarehouseViewModel FromItem(RevenueSummaryByMonth item)
        {
            RevenueSummaryByMonthDataWarehouseViewModel result = new RevenueSummaryByMonthDataWarehouseViewModel()
            {
                RevenueDate = item.RevenueDate.Value,
                Office = item.OfficeID == null ? "" : item.OfficeID.officeName.Value,
                Region = item.RegionID == null ? "" : item.RegionID.regionName.Value,
                Segment = item.SegmentID == null ? "" : item.SegmentID.segmentName.Value,
                RevenueSummaryId = item.RevenueSummaryID,
                TotalRevenue = item.TotalRevenue,
                TotalRevenueFromArrears = item.TotalRevenueFromArrears,
                TotalRevenueFromAudits = item.TotalRevenueFromAudits,
                TotalRevenueFromEnforcementCollection = item.TotalRevenueFromEnforcementCollection,
                TotalRevenueFromFines = item.TotalRevenueFromFines,
                TotalRevenueFromLitigation = item.TotalRevenueFromLitigation,
                TotalRevenueFromTaxes = item.TotalRevenueFromTaxes,
                TotalRevenueFromVoluntaryCompliance = item.TotalRevenueFromVoluntaryCompliance,
                TotalRevenuePaidInTIme = item.TotalRevenuePaidInTIme
            };
            return result;
        }
    }
}
