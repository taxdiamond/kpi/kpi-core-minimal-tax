﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Region;
using Domain.Segment;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class SegmentDataWarehouseViewModel
    {
        [Display(Name = "Taxpayer Segment ID")]
        public string segmentID { get; set; }
        [Display(Name = "Taxpayer Segment Name")]
        public string segmentName { get; set; }

        public static SegmentDataWarehouseViewModel FromItem(Segment item)
        {
            SegmentDataWarehouseViewModel result = new SegmentDataWarehouseViewModel()
            {
                segmentID = item.segmentID,
                segmentName = item.segmentName
            };
            return result;
        }
    }
}
