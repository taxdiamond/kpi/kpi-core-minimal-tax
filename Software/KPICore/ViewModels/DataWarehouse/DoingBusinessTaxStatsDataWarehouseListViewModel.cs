﻿using Domain.DoingBusinessStats;
using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class DoingBusinessTaxStatsDataWarehouseListViewModel : AppListViewModel<DoingBusinessTaxStatsDataWarehouseViewModel>
    {
        public List<DoingBusinessTaxStatsDataWarehouseViewModel> DoingBusinessTaxStatsList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public DoingBusinessTaxStatsDataWarehouseListViewModel()
        {
            items = new List<DoingBusinessTaxStatsDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static DoingBusinessTaxStatsDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<DoingBusinessTaxStats> source)
        {
            DoingBusinessTaxStatsDataWarehouseListViewModel result = new DoingBusinessTaxStatsDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<DoingBusinessTaxStats> list)
        {
            DoingBusinessTaxStatsList.Clear();
            foreach (DoingBusinessTaxStats item in list)
            {
                DoingBusinessTaxStatsList.Add(DoingBusinessTaxStatsDataWarehouseViewModel.FromItem(item));
            }
        }

    }
}
