﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RevenueDataWarehouseViewModel
    {
        [Display(Name = "Record ID")]
        public int RevenueItemID { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime RevenueDate { get; set; }
        [Display(Name = "Region")]
        public string Region { get; set; }
        [Display(Name = "Office")]
        public string Office { get; set; }
        [Display(Name = "Taxpayer Segment")]
        public string Segment { get; set; }
        [Display(Name = "Revenue Type")]
        public string Concept { get; set; }
        [Display(Name = "Revenue Concept")]
        public string ConceptID { get; set; }
        [Display(Name = "Amount")]
        public decimal Amount { get; set; }

        public string RevenueDateForDisplay { get { return RevenueDate.ToShortDateString(); } }

        public static RevenueDataWarehouseViewModel FromItem(Revenue item)
        {
            RevenueDataWarehouseViewModel result = new RevenueDataWarehouseViewModel()
            {
                RevenueDate = item.RevenueDate.Value,
                Amount = item.Amount == null ? 0 : item.Amount.Value,
                Concept = item.Concept,
                ConceptID = item.ConceptID,
                Office = item.OfficeID == null ? "" : item.OfficeID.officeName.Value,
                Region = item.RegionID == null ? "" : item.RegionID.regionName.Value,
                RevenueItemID = item.RevenueItemID,
                Segment = item.SegmentID == null ? "" : item.SegmentID.segmentName.Value
            };
            return result;
        }
    }
}
