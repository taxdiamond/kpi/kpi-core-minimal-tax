﻿using Domain.Tax;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RevenueDataWarehouseListViewModel : AppListViewModel<RevenueDataWarehouseViewModel>
    {
        public List<RevenueDataWarehouseViewModel> RevenueList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public RevenueDataWarehouseListViewModel()
        {
            items = new List<RevenueDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static RevenueDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<Revenue> source)
        {
            RevenueDataWarehouseListViewModel result = new RevenueDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<Revenue> list)
        {
            RevenueList.Clear();
            foreach (Revenue item in list)
            {
                RevenueList.Add(RevenueDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
