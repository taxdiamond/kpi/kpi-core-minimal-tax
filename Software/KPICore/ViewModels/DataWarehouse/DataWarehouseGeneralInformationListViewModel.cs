﻿using Domain.DataWarehouse;
using Domain.Enums.DataWareHouse;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class DataWarehouseGeneralInformationListViewModel : AppListViewModel<DataWarehouseGeneralInformationViewModel>
    {
        public List<string> Errors { get; set; }
        public DataWareHouseType DWTypeSelected { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime UntilDate { get; set; }        
        public bool AloneNextDate { get; set; }
        public List<DataWarehouseGeneralInformationViewModel> CollectionList
        {
            get
            {
                return this.items;
            }
        }

        public DataWarehouseGeneralInformationListViewModel()
        {
            items = new List<DataWarehouseGeneralInformationViewModel>();
        }

        public static DataWarehouseGeneralInformationListViewModel FromDataWarehouseRecordList(List<DataWarehouseRecord> source)
        {
            DataWarehouseGeneralInformationListViewModel result = new DataWarehouseGeneralInformationListViewModel();

            result.ReadFromList(source);
            return result;
        }

        public void ReadFromList(List<DataWarehouseRecord> list)
        {
            CollectionList.Clear();
            foreach (var item in list)
            {
                CollectionList.Add(DataWarehouseGeneralInformationViewModel.FromItem(item));
            }
        }
    }
}
