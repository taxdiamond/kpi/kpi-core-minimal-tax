﻿using Domain.AuditStats;
using Domain.CountryStats;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class CountryStatsDataWarehouseViewModel
    {
        
        public int CountryStatsID { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime StatsDate { get; set; }
        [Display(Name = "GDP Per Capita at Date")]
        public decimal? GdpPerCapitaAtDate { get; set; }
        [Display(Name = "Nominal GDP at Date")]
        public decimal? NominalGdpAtDate { get; set; }
        [Display(Name = "Population at Date")]
        public int PopulationAtDate { get; set; }

        public static CountryStatsDataWarehouseViewModel FromItem(CountryStats item)
        {
            CountryStatsDataWarehouseViewModel result = new CountryStatsDataWarehouseViewModel()
            {
                CountryStatsID = item.CountryStatsID,
                GdpPerCapitaAtDate = item.GdpPerCapitaAtDate,
                NominalGdpAtDate = item.NominalGdpAtDate,
                PopulationAtDate = item.PopulationAtDate,
                StatsDate = item.StatsDate.Value
            };
            return result;
        }
    }
}
