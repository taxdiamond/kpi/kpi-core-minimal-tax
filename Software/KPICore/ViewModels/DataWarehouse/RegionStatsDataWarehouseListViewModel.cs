﻿using Domain.Region;
using Domain.Utils;
using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class RegionStatsDataWarehouseListViewModel : AppListViewModel<RegionStatsDataWarehouseViewModel>
    {
        public List<RegionStatsDataWarehouseViewModel> RegionStatsList
        {
            get
            {
                return this.items;
            }
        }

        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public RegionStatsDataWarehouseListViewModel()
        {
            items = new List<RegionStatsDataWarehouseViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }


        public static RegionStatsDataWarehouseListViewModel FromPagedResultBase(PagedResultBase<RegionStats> source)
        {
            RegionStatsDataWarehouseListViewModel result = new RegionStatsDataWarehouseListViewModel();
            result.PageNumber = source.CurrentPage;
            result.TotalNumber = source.TotalRows;

            result.ReadFromList(source.ResultList);
            return result;
        }

        public void ReadFromList(List<RegionStats> list)
        {
            RegionStatsList.Clear();
            foreach (RegionStats item in list)
            {
                RegionStatsList.Add(RegionStatsDataWarehouseViewModel.FromItem(item));
            }
        }
    }
}
