﻿using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class FilingStatsDataWarehouseViewModel
    {
        public DateTime ReportDate { get; set; }
        public string TaxType { get; set; }
        public string Segment { get; set; }
        public int ExpectedFilers { get; set; }
        public int TotalFilings { get; set; }
        public int StopFilers { get; set; }
        public int EFilings { get; set; }
        public int TotalFilingsRequiringPayment { get; set; }
        public int FilingsPaidElectronically { get; set; }
        public int LateFilers { get; set; }

        public string ReportDateDateForDisplay
        {
            get { return ReportDate.ToShortDateString(); }
        }

        public static FilingStatsDataWarehouseViewModel FromItem(FilingStats item)
        {
            FilingStatsDataWarehouseViewModel result = new FilingStatsDataWarehouseViewModel()
            {
                ReportDate = item.ReportDate.Value,
                Segment = item.SegmentID == null || item.SegmentID.segmentName == null ? "" : item.SegmentID.segmentName.Value,
                ExpectedFilers = item.ExpectedFilers,
                TotalFilings = item.TotalFilings,
                StopFilers = item.StopFilers,
                EFilings = item.EFilings,
                TotalFilingsRequiringPayment = item.TotalFilingsRequiringPayment,
                FilingsPaidElectronically = item.FilingsPaidElectronically,
                LateFilers = item.LateFilers
            };
            return result;
        }
    }
}
