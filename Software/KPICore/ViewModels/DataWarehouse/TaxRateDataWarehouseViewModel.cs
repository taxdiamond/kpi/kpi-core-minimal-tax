﻿using Domain.AuditStats;
using Domain.CountryStats;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.DataWarehouse
{
    public class TaxRateDataWarehouseViewModel
    {
        [Display(Name = "Record ID")]
        public int TaxRateId { get; set; }
        [Display(Name = "Date of Report")]
        public DateTime TaxRateDate { get; set; }
        [Display(Name = "Tax Name")]
        public string TaxName { get; set; }
        [Display(Name = "Tax Rate")]
        public decimal Rate { get; set; }

        public string TaxRateDateForDisplay { get { return TaxRateDate.ToShortDateString(); } }

        public static TaxRateDataWarehouseViewModel FromItem(TaxRate item)
        {
            TaxRateDataWarehouseViewModel result = new TaxRateDataWarehouseViewModel()
            {
                Rate = item.Rate,
                TaxName = item.TaxName,
                TaxRateDate = item.TaxRateDate.Value,
                TaxRateId = item.TaxRateId
            };
            return result;
        }
    }
}
