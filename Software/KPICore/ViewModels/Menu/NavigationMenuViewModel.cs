﻿using Infrastructure.ViewModels.Shared;
using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.Menu
{
    public class NavigationMenuViewModel 
    {
        public string Menu { get; set; }
        public string Url { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string ResourceItem { get; set; }
        public bool Public { get; set; }
        public string Icon { get; set; }
        public string Username { get; set; }
        public KPIDomain Domain { get; set; }
        public List<ListItemViewModel> DomainsConfigured { get; set; }

        public List<NavigationMenuViewModel> Submenus { get; set; }

        public NavigationMenuViewModel()
        {
            Submenus = new List<NavigationMenuViewModel>();
            DomainsConfigured = new List<ListItemViewModel>();
        }
    }
}
