﻿using AEWeb.ViewModels.KPI;
using Domain.Dashboard;
using Framework.Utilities;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;

namespace AEWeb.ViewModels.Dashboard
{
    public class DashboardConfigurationViewModel
    {
        public Guid DashboardConfigurationId { get; set; }
        public List<DashboardRowViewModel> Rows { get; set; }

        public DashboardConfigurationViewModel()
        {
            Rows = new List<DashboardRowViewModel>();
        }

        /// <summary>
        /// The method goes through the rows marking the variable
        /// SameColumnThanPrevious as true when there is still room for the current kpi.
        /// This depends on the maxheight that the row has.
        /// </summary>
        public void Render()
        {
            foreach (DashboardRowViewModel row in Rows)
                row.Render();
        }

        public static DashboardConfigurationViewModel FromDashboardConfiguration(
            DashboardConfiguration dashboardConfiguration, 
            KPIClassLibrary.KPI.KPICatalog catalog)
        {
            DashboardConfigurationViewModel model = new DashboardConfigurationViewModel();

            model.DashboardConfigurationId = dashboardConfiguration.DashboardConfigurationId;

            foreach(DashboardRow row in dashboardConfiguration.Rows)
            {
                DashboardRowViewModel rowModel = new DashboardRowViewModel();
                rowModel.RowNumber = row.RowNumber;
                rowModel.DashboardRowID = row.DashboardRowId;

                List<string> rowDimensions = new List<string>();
                foreach(Domain.Indicators.Kpi kpi in row.Kpis)
                {
                    KpiViewModel kpiModel = KpiBuilderViewModel.Get(kpi, catalog);
                    kpiModel.KpiId = kpi.KpiId.ToString();
                    rowModel.Append(kpiModel);
                    string foundDimension = rowDimensions.Find(o => o.Equals(kpiModel.Dimension));
                    if (string.IsNullOrWhiteSpace(foundDimension))
                        rowDimensions.Add(kpiModel.Dimension);
                }

                rowModel.Dimension = string.Join(", ", rowDimensions);

                model.Rows.Add(rowModel);
            }

            return model;
        }

        public static DashboardConfigurationViewModel FromDashboardConfiguration(
            DashboardConfiguration dashboardConfiguration)
        {
            DashboardConfigurationViewModel model = new DashboardConfigurationViewModel();

            model.DashboardConfigurationId = dashboardConfiguration.DashboardConfigurationId;
            return model;
        }
    }
}
