﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.Dashboard
{
    public class DashboardComboViewModel
    {
        public string name { get; set; }
        public string id { get; set; }
    }
}
