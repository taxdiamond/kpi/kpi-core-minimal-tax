﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.Dashboard
{
    public class DashboardSelectorViewModel
    {
        public string userid { get; set; }
        public string domain { get; set; }
    }
}
