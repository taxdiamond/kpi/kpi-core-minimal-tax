﻿using Domain.Security;
using Infrastructure.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.Dashboard
{
    public class DashboardListViewModel : AppListViewModel<DashboardViewModel>
    {
        [Display(Name = "Dashboards")]
        public List<DashboardViewModel> Dashboards
        {
            get
            {
                return this.items;
            }
        }

        public string Domain { get; set; }

        public DashboardListViewModel()
        {
            items = new List<DashboardViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }

        public static DashboardListViewModel FromListDashboard(List<Domain.Dashboard.Dashboard> listDashboards)
        {
            DashboardListViewModel result = new DashboardListViewModel();
            result.ReadFromList(listDashboards);
            return result;
        }

        public void ReadFromList(List<Domain.Dashboard.Dashboard> listDashboards, ApplicationUser user = null)
        {
            Dashboards.Clear();
            foreach (Domain.Dashboard.Dashboard itemDashboard in listDashboards)
            {
                DashboardViewModel data = DashboardViewModel.FromDashboard(itemDashboard);
                if (user != null)
                {
                    if(user.Id == itemDashboard.Creator.Id)
                        data.EditPermision = true;
                    else
                        data.EditPermision = false;
                }
                else
                    data.EditPermision = true;
                Dashboards.Add(data);
            }
        }
    }
}
