﻿using Domain.ValueObjects.Dashboard;
using Infrastructure.ViewModels.Shared;
using System;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.Dashboard
{
    public class DashboardViewModel : MessagesViewModel
    {
        public Guid DashboardId { get; set; }
        public string DashboardRowId { get; set; }

        [Required(ErrorMessage = "NameRequiredMessage")]
        [MaxLength(250)]
        public string Name { get; set; }

        [Required(ErrorMessage = "TypeRequiredMessage")]
        public DashboardType Type { get; set; }

        [Required(ErrorMessage = "DashboardStatusRequiredMessage")]
        public DashboardStatus Status { get; set; }
        public bool IsPreferred { get; set; }

        public string Creator { get; set; }
        public bool EditPermision { get; set; }
        public string TypeForDisplay
        {
            get
            {
                return Type.ToString();
            }
        }

        public string StatusForDisplay
        {
            get
            {
                return Status.ToString();
            }
        }

        public DashboardConfigurationViewModel Config { get; set; }

        public DashboardViewModel()
        {

        }

        public static DashboardViewModel FromDashboard(Domain.Dashboard.Dashboard obj)
        {
            DashboardViewModel result = new DashboardViewModel();

            result.Config = null;
            result.Creator = obj.Creator != null ? obj.Creator.FullName : "";
            result.DashboardId = obj.DashboardId;
            result.Name = obj.Name;
            result.Status = obj.Status;
            result.Type = obj.Type;

            return result;
        }

        public Domain.Dashboard.Dashboard ToDashboard()
        {
            return new Domain.Dashboard.Dashboard()
            {
                DashboardId = DashboardId,
                Name = Name,
                Status = Status,
                Type = Type,
            };
        }

        /// <summary>
        /// Just redirecting to Render from the DashboardConfiguration
        /// </summary>
        public void Render()
        {
            Config.Render();
        }
    }
}
