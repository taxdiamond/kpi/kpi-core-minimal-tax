﻿using Infrastructure.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.Dashboard
{
    public class DashboardRowListViewModel : AppListViewModel<DashboardRowViewModel>
    {
        [Display(Name = "DashboardRows")]
        public List<DashboardRowViewModel> DashboardRows
        {
            get
            {
                return this.items;
            }
        }

        public DashboardViewModel Parent { get; set; }
        public DashboardConfigurationViewModel Configuration { get; set; }

        public DashboardRowListViewModel()
        {
            items = new List<DashboardRowViewModel>();
            PageNumber = 1;
            PageSize = 30;
            Configuration = new DashboardConfigurationViewModel();
            Parent = new DashboardViewModel();
        }

        public static DashboardRowListViewModel FromListDashboardRow(List<Domain.Dashboard.DashboardRow> listDashboardRows)
        {
            DashboardRowListViewModel result = new DashboardRowListViewModel();
            result.ReadFromList(listDashboardRows);
            return result;
        }

        public void ReadFromList(List<Domain.Dashboard.DashboardRow> listDashboardRows)
        {
            DashboardRows.Clear();
            foreach (Domain.Dashboard.DashboardRow itemRow in listDashboardRows)
            {
                DashboardRows.Add(DashboardRowViewModel.FromDashboardRow(itemRow));
            }
        }
    }
}
