﻿using AEWeb.ViewModels.User;
using Domain.Dashboard;
using Infrastructure.ViewModels.Shared;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.Dashboard
{
    public class DashboardUserViewModel : AppListViewModel<ApplicationUserViewModel>
    {
        [Display(Name = "Users")]
        public List<ApplicationUserViewModel> Users
        {
            get
            {
                return this.items;
            }
        }

        public static DashboardUserViewModel FromModel(Domain.Dashboard.Dashboard obj, List<DashboardUser> usersInDashboard)
        {
            DashboardUserViewModel result = new DashboardUserViewModel();

            if(obj != null)
                result.Dashboard = DashboardViewModel.FromDashboard(obj);

            if(usersInDashboard != null)
            {
                foreach (var item in usersInDashboard)
                {
                    result.items.Add(ApplicationUserViewModel.FromApplicationUser(item.SharedDashboardUser));
                }
            }

            return result;
        }

        public Dashboard.DashboardViewModel Dashboard { get; set; }

        public string SelectedUserId { get; set; }

        public DashboardUserViewModel()
        {
            items = new List<ApplicationUserViewModel>();
            PageNumber = 1;
            PageSize = int.MaxValue;
            Dashboard = null;
        }
    }
}