﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.KPI
{
    public class SerieData
    {
        public List<PointData> Points { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// { name: 'VAT', data: [5, 3] }, 
        /// { name: 'Property', data: [2, 2] }, 
        /// { name: 'Income', data: [3, 4] }, 
        /// { name: 'Other', data: [1, 2] }
        /// </summary>
        public string SerieToString(bool forPie)
        {

            StringBuilder result = new StringBuilder();            

            if (forPie)
            {
                result.Append("{ \"name\":\"").Append(Name).Append("\", \"y\": ");
                foreach (PointData p in Points)
                {
                    string valueP_Y = FormatValueWith2SignificantDigits(p.Y);
                    result.Append(valueP_Y);
                }
                result.Append("}");
            }
            else
            {
                result.Append("{ \"name\":\"").Append(Name).Append("\", \"data\": [");
                string sep = "";
                foreach (PointData p in Points)
                {
                    string valueP_Y = FormatValueWith2SignificantDigits(p.Y);
                    result.Append(sep).Append(valueP_Y);
                    sep = ",";
                }
                result.Append("] }");
            }
            return result.ToString();
        }

        private string FormatValueWith2SignificantDigits(decimal y)
        {
            if (Math.Abs(y) >= 10)
                return y.ToString("0.#", CultureInfo.InvariantCulture);

            if (Math.Abs(y) >= 1)
                return y.ToString("0.##", CultureInfo.InvariantCulture);

            if (Math.Abs(y) >= 0.1m)
                return y.ToString("0.###", CultureInfo.InvariantCulture);

            if (Math.Abs(y) >= 0.01m)
                return y.ToString("0.####", CultureInfo.InvariantCulture);

            return y.ToString(CultureInfo.InvariantCulture);
        }

        public SerieData()
        {
            Points = new List<PointData>();
        }
    }
}
