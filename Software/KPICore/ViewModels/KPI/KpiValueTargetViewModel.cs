﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace AEWeb.ViewModels.KPI
{
    public class KpiValueTargetViewModel : KpiViewModel
    {
        public Dictionary<string,decimal> Value { get; set; }
        public Dictionary<string,string> Label { get; set; }
        public Dictionary<string,List<decimal>> AllValues { get; set; }
        public List<string> AllTitles { get; set; }
        public string ValueSuffix { get; set; }
        public int ValueNumberDecimals { get; set; }
        public string SparklineDiv(string category)
        {
            //<div data-sparkline-values="71,78,39,66" data-sparkline-titles="Q1|Q2|Q3|Q4"/>
            StringBuilder result = new StringBuilder();

            List<decimal> actualValues = null;
            if (!AllValues.ContainsKey("NONE"))
            {
                //_logger.LogWarning("Using first set of values instead of NONE");
                var first = AllValues.First();
                actualValues = first.Value;
            } else
                actualValues = AllValues["NONE"];

            result.Append("<div data-sparkline-values=\"")
                .Append(String.Join(",", actualValues))
                .Append("\" data-sparkline-titles=\"")
                .Append(String.Join("|", AllTitles))
                .Append("\"></div>");

            return result.ToString();
        }

        public string ValueForDisplay(string grouping)
        {   
            string decimalFormat = "0." + ("#").PadLeft(ValueNumberDecimals, '0');
            string result = Value[grouping].ToString(decimalFormat, CultureInfo.InvariantCulture);
            if (!string.IsNullOrWhiteSpace(this.ValueSuffix))
                return result + " " + this.ValueSuffix;
            return result;
        }

        public string ValueForDisplay() => ValueForDisplay("NONE");

        #region Target not used yet
        public decimal TargetValue { get; set; }
        public string TargetValueSuffix { get; set; }
        public bool ShowTarget { get; set; }

        public string TargetForDisplay
        {
            get
            {
                if (!ShowTarget)
                    return "N/A";

                return TargetValue.ToString("0.#", CultureInfo.InvariantCulture) + " " +
                    TargetValueSuffix;
            }
        }
        #endregion

        public bool ShowChange { get; set; }
        public Dictionary<string,decimal> ChangeValue { get; set; }
        public Dictionary<string,string> ChangeSuffix { get; set; }
        public Dictionary<string,int> ChangeDirection { get; set; }
        public string ChangeCssClass(string grouping)
            => ChangeDirection[grouping] < 0 ? "text-danger" : "text-info";
        public string ChangeCssClass() => ChangeCssClass("NONE");
        public string ChangeForDisplay(string grouping)
            => ChangeValue[grouping].ToString("0.#", CultureInfo.InvariantCulture) + " " + 
                    ChangeSuffix[grouping];
        public string ChangeForDisplay() => ChangeForDisplay("NONE");
        public string ChangeIcon(string grouping)
        {
            string icon = "fa-level-up";
            if (ChangeDirection[grouping] < 0)
                icon = "fa fa-level-down";
            return icon;
        }
        public string ChangeIcon() => ChangeIcon("NONE");
        
        public KpiValueTargetViewModel()
        {
            CreatedNewOne();
            // default values
            PeriodName = "Yearly";
            Period = PeriodType.Year;
            ShowChange = false;
            ShowTarget = false;
            ValueNumberDecimals = 1;

            AllTitles = new List<string>();
            AllValues = new Dictionary<string, List<decimal>>();
            Value = new Dictionary<string, decimal>();
            Label = new Dictionary<string, string>();
            ChangeDirection = new Dictionary<string, int>();
            ChangeValue = new Dictionary<string, decimal>();
            ChangeSuffix = new Dictionary<string, string>();
        }
    }
}
