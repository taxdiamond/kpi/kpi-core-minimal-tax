﻿using Infrastructure.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.KPI
{
    public class KpiListViewModel : AppListViewModel<KpiViewModel>
    {
        [Display(Name = "DashboardKpis")]
        public List<KpiViewModel> Kpis
        {
            get
            {
                return this.items;
            }
        }

        public KpiListViewModel()
        {
            items = new List<KpiViewModel>();
            PageNumber = 1;
            PageSize = 30;
        }
        public Guid RowId { get; set; }
        public Guid DashboardId { get; set; }

        public static KpiListViewModel FromListKpi(List<Domain.Indicators.Kpi> listKpis, 
            KPIClassLibrary.KPI.KPICatalog catalog)
        {
            KpiListViewModel result = new KpiListViewModel();
            result.ReadFromList(listKpis, catalog);
            return result;
        }

        public void ReadFromList(List<Domain.Indicators.Kpi> listKpis,
            KPIClassLibrary.KPI.KPICatalog catalog)
        {
            if (listKpis.Count > 0)
                RowId = listKpis[0].RowRef.DashboardRowId;
            Kpis.Clear();
            foreach (Domain.Indicators.Kpi item in listKpis)
            {
                KpiViewModel kpiView = KpiBuilderViewModel.Get(item, catalog);
                if (item.RowRef != null)
                    kpiView.DashboardRowId = item.RowRef.DashboardRowId;
                if (item.DashboardRef != null)
                    kpiView.DashboardId = item.DashboardRef.DashboardId;
                Kpis.Add(kpiView);
            }
        }

    }
}
