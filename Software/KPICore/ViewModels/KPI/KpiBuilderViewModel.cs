﻿using Domain.Indicators;
using KPIClassLibrary.KPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;

namespace AEWeb.ViewModels.KPI
{
    /// <summary>
    /// This class is a buillder that links the actual KPI to an initial suggested visualization 
    /// scheme.
    /// </summary>
    public class KpiBuilderViewModel
    {
        public static KpiViewModel Get(Domain.Indicators.Kpi sourceKpi, 
            KPIClassLibrary.KPI.KPICatalog catalog)
        {
            KpiViewModel kpi = CreateKpiViewBasedOnClass(sourceKpi, catalog);

            return kpi;
        }

        private static KpiViewModel CreateKpiViewBasedOnClass(Kpi domainKpi, 
            KPIClassLibrary.KPI.KPICatalog catalog)
        {
            KpiViewModel kpiView = null;
            KPICatalogArea area = catalog.FindAreaForKPI(domainKpi.Title.Value);
            KPICatalogItem itemInArea = area.KPIs.First<KPICatalogItem>(x => x.ClassName.Equals(domainKpi.Title.Value));

            ObjectHandle handle = Activator.CreateInstance("KPIClassLibrary", "KPIClassLibrary.KPI." + 
                domainKpi.Title.Value);
            KPIClassLibrary.KPI.KPI catalogKpi = (KPIClassLibrary.KPI.KPI)handle.Unwrap();
            List<KPIClassLibrary.KPI.KPIFilterType> filters = catalogKpi.GetAllowedFilters();

            if (catalogKpi.Grouping == KPIClassLibrary.KPI.KPIGroupType.None &&
                filters.Count() == 0)
            {
                kpiView = CreateStandardValueTargetViewModel(catalogKpi, domainKpi, area);
                return kpiView;
            }
            if (catalogKpi.Grouping == KPIClassLibrary.KPI.KPIGroupType.None &&
                filters.Count() > 0)
            {
                kpiView = CreateStandardValueFiltersViewModel(catalogKpi, domainKpi, area);
                return kpiView;
            }
            if (catalogKpi.Grouping != KPIGroupType.None)
            {
                kpiView = CreateStandardColumnBarPieChartViewModel(catalogKpi, domainKpi, area);
                return kpiView;
            }

            kpiView = CreateNotSupportedViewModel(catalogKpi);
            return kpiView;
        }


        private static KpiViewModel CreateNotSupportedViewModel(KPIClassLibrary.KPI.KPI catalogKpi)
        {
            KpiViewModel model = new KpiNotSupportedViewModel();
            return model;
        }

        private static KpiValueTargetViewModel CreateStandardValueTargetViewModel(
            KPIClassLibrary.KPI.KPI kpi, Domain.Indicators.Kpi sourceKpi,
            KPIClassLibrary.KPI.KPICatalogArea area)
        {
            KpiValueTargetViewModel result = new KpiValueTargetViewModel();
            result.CellNumber = sourceKpi.CellNumber;
            result.Title = area.Name;
            result.KpiMnemonic = kpi.ID;
            result.Type = sourceKpi.ViewType;
            result.KpiId = sourceKpi.KpiId.ToString();
            result.DivHeight = sourceKpi.DivHeight;
            result.DivWidth = sourceKpi.DivWidth;
            result.SameColumnThanPrevious = false;
            result.Dimension = area.Dimension;
            return result;
        }


        private static KpiViewModel CreateStandardValueFiltersViewModel(
            KPIClassLibrary.KPI.KPI kpi, Domain.Indicators.Kpi sourceKpi,
            KPIClassLibrary.KPI.KPICatalogArea area)
        {
            KpiValueFiltersViewModel result = new KpiValueFiltersViewModel();
            result.CellNumber = sourceKpi.CellNumber;
            result.Title = area.Name;
            result.KpiMnemonic = kpi.ID;
            result.Type = sourceKpi.ViewType;
            result.KpiId = sourceKpi.KpiId.ToString();
            result.DivHeight = sourceKpi.DivHeight;
            result.DivWidth = sourceKpi.DivWidth;
            result.SameColumnThanPrevious = false;
            result.Dimension = area.Dimension;
            return result;
        }

        private static KpiViewModel CreateStandardColumnBarPieChartViewModel(
            KPIClassLibrary.KPI.KPI kpi, Domain.Indicators.Kpi sourceKpi,
            KPIClassLibrary.KPI.KPICatalogArea area)
        {
            KpiColumnBarPieChartViewModel result = new KpiColumnBarPieChartViewModel();
            result.CellNumber = sourceKpi.CellNumber;
            result.Title = area.Name;
            result.KpiMnemonic = kpi.ID;
            result.Type = sourceKpi.ViewType;
            result.KpiId = sourceKpi.KpiId.ToString();
            result.DivHeight = sourceKpi.DivHeight;
            result.DivWidth = sourceKpi.DivWidth;
            result.SameColumnThanPrevious = false;
            result.Dimension = area.Dimension;
            return result;
        }

    }
}
