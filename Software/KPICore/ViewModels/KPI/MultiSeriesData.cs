﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.KPI
{
    public class MultiSeriesData
    {
        public List<string> Categories { get; set; }
        public List<SerieData> Series { get; set; }
        public string CategoriesToString
        {
            get {
                StringBuilder result = new StringBuilder();
                result.Append("[");
                string sep = "";
                foreach(string s in Categories)
                {
                    result.Append(sep).Append("'").Append(s).Append("'");
                    sep = ",";
                }
                result.Append("]");
                return result.ToString();
            }
        }

        

        public MultiSeriesData()
        {
            Categories = new List<string>();
            Series = new List<SerieData>();
        }

        public void CategoriesFromString(string categoriesInString)
        {
            if (string.IsNullOrWhiteSpace(categoriesInString))
                return;

            char[] sep = { ',' };
            string[] values = categoriesInString.Split(sep);
            Categories = values.ToList<string>();
        }

        public string SeriesToString(bool forPie)
        {
            StringBuilder result = new StringBuilder();
            if (forPie && Categories.Count == 1)
            {
                result.Append("[{\"name\":\"")
                    .Append(CategoriesToString)
                    .Append("\",\"data\":[");
                string sep = "";
                foreach (SerieData serie in Series)
                {
                    result.Append(sep).Append(serie.SerieToString(forPie));
                    sep = ",";
                }
                result.Append("]}]");
            }
            else
            {
                
                result.Append("[");
                string sep = "";
                foreach (SerieData serie in Series)
                {
                    result.Append(sep).Append(serie.SerieToString(false));
                    sep = ",";
                }
                result.Append("]");
            }
            return result.ToString();
        }

    }
}
