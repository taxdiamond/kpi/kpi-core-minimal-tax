﻿using AEWeb.ViewModels.Dashboard;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.KPI
{
    public class KpiValueFiltersViewModel : KpiViewModel
    {
        public decimal Value { get; set; }
        public string ValueSuffix { get; set; }
        public int ValueNumberDecimals { get; set; }
        public List<decimal> AllValues { get; set; }
        public List<string> AllTitles { get; set; }
        public string SparklineDiv
        {
            get
            {
                //<div data-sparkline-values="71,78,39,66" data-sparkline-titles="Q1|Q2|Q3|Q4"/>
                StringBuilder result = new StringBuilder();
                result.Append("<div data-sparkline-values=\"")
                    .Append(String.Join(",", AllValues))
                    .Append("\" data-sparkline-titles=\"")
                    .Append(String.Join("|", AllTitles))
                    .Append("\"></div>");

                return result.ToString();
            }
        }
        public string ValueForDisplay
        {
            get
            {
                string decimalFormat = "0." + ("#").PadLeft(ValueNumberDecimals, '0');
                string result = Value.ToString(decimalFormat, CultureInfo.InvariantCulture);
                if (!string.IsNullOrWhiteSpace(this.ValueSuffix))
                    return result + " " + this.ValueSuffix;
                return result;
            }
        }
        public decimal TargetValue { get; set; }
        public string TargetValueSuffix { get; set; }
        public bool ShowTarget { get; set; }

        public string TargetForDisplay
        {
            get
            {
                if (!ShowTarget)
                    return "N/A";

                return TargetValue.ToString("0.#", CultureInfo.InvariantCulture) + " " +
                    TargetValueSuffix;
            }
        }
        public bool ShowChange { get; set; }
        public decimal ChangeValue { get; set; }
        public string ChangeSuffix { get; set; }
        public int ChangeDirection { get; set; }
        public string ChangeCssClass { get { return ChangeDirection < 0 ? "text-danger" : "text-info"; } }
        public string ChangeForDisplay
        {
            get
            {
                return ChangeValue.ToString("0.#", CultureInfo.InvariantCulture) + " " + 
                    ChangeSuffix;
            }
        }
        public string ChangeIcon
        {
            get
            {
                string icon = "fa-level-up";
                if (ChangeDirection < 0)
                    icon = "fa fa-level-down";
                return icon;
            }
        }
        
        public KpiValueFiltersViewModel()
        {
            CreatedNewOne();
            // default values
            PeriodName = "Yearly";
            Period = PeriodType.Year;
            ShowChange = false;
            ShowTarget = false;
            ValueNumberDecimals = 1;
            AllTitles = new List<string>();
            AllValues = new List<decimal>();
        }
    }
}
