﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.TestKPIs
{
    public class TestParameter
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public TestParameter()
        {
        }
    }
    public class TestFilter
    {
        public string Type { get; set; }
        public string Value { get; set; }

        public TestFilter()
        {
        }
    }
    public class TestResult
    {
        public string GroupName { get; set; }
        public List<String> ResultLine { get; set; }

        public TestResult()
        {
            GroupName = "";
            ResultLine = new List<string>();
        }
    }

    public class KPITest
    {
        public KPITest()
        {
            TestResults = new List<TestResult>();
            Parameters = new List<TestParameter>();
            Filters = new List<TestFilter>();
        }
        public string KPIID { get; set; }
        public string KPIName { get; set; }
        public string Description { get; set; }
        public string Dimension { get; set; }
        public string GroupType { get; set; }
        public string PeriodType { get; set; }

        public List<TestParameter> Parameters { get; set; }
        public List<TestResult> TestResults { get; set; }
        public List<TestFilter> Filters { get; set; }
        public bool Success { get; set; }
        public string Error { get; set; }
    }

    public class TestKPIsViewModel
    {
        public TestKPIsViewModel()
        {
            Results = new List<KPITest>();
        }
        public List<KPITest> Results { get; set; }
        public int NumberOfSuccess()
        {
            return Results.Where(p => p.Success).Count();
        }
        public int NumberOfErrors()
        {
            return Results.Where(p => !p.Success).Count();
        }
    }
}
