﻿using Domain.Security;
using AEWeb.ViewModels.Security;
using System;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.User
{
    public class ApplicationUserViewModel
    {
        [Display(Name = "LastName")] 
        public string LastName { get; set; }
        [Display(Name = "FirstName")] 
        public string FirstName { get; set; }
        public bool Active { get; set; }
        public bool Unlock { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string Id { get; set; }
        public NewUserPasswordGenerateViewModel NewUserPassword { get; set; }

        public static ApplicationUserViewModel FromApplicationUser(ApplicationUser user)
        {
            ApplicationUserViewModel result = new ApplicationUserViewModel();
            result.Email = user.Email;
            result.EmailConfirmed = user.EmailConfirmed;
            result.FirstName = user.FirstName;
            result.LastName = user.LastName;
            result.Username = user.UserName;
            result.Unlock = (user.LockoutEnd == null) ? false : (DateTime.Now >= user.LockoutEnd.Value) ? false : true ;
            result.Active = user.Active;
            result.Id = user.Id;
            result.NewUserPassword = null;
            return result;
        }
    }
}
