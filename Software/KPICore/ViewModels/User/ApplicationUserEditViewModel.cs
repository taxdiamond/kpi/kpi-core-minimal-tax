﻿using Domain.Security;
using Infrastructure.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.User
{
    public class ApplicationUserEditViewModel : MessagesViewModel
    {
        public string Id
        {
            get; set;
        }

        [Required]
        [StringLength(250)]
        [Display(Name = "LastName")]
        public string LastName { get; set; }
        [Required]
        [StringLength(250)]
        [Display(Name = "FirstName")]
        public string FirstName { get; set; }
        public string Username { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "EmailAddressUsernameFormatError")]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public static ApplicationUserEditViewModel FromApplicationUser(ApplicationUser user)
        {
            ApplicationUserEditViewModel result = new ApplicationUserEditViewModel();
            result.Id = user.Id;
            result.Email = user.Email;
            result.FirstName = user.FirstName;
            result.LastName = user.LastName;
            result.Username = user.UserName;
            
            return result;
        }
    }
}
