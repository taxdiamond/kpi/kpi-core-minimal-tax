﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.Role
{
    public class RoleViewModel
    {
        public string RoleId { get; set; }

        [Required(ErrorMessage = "RoleReqFld")]
        [StringLength(100)]
        public string Role { get; set; }
        public bool IsInRole { get; set; }
    }
}
