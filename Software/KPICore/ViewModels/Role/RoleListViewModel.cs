﻿using Infrastructure.ViewModels.Shared;
using System.Collections.Generic;

namespace AEWeb.ViewModels.Role
{
    public class RoleListViewModel : AppListViewModel<RoleViewModel>
    {
        public List<RoleViewModel> Roles { get { return items; } }

        public RoleViewModel NewRole { get; set; }

        public RoleListViewModel()
        {
            PageSize = 20;
            items = new List<RoleViewModel>();
        }
    }
}
