﻿using Infrastructure.ViewModels.Shared;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.ApplicationPermission
{
    public class ApplicationPermissionListViewModel : AppListViewModel<ApplicationPermissionViewModel>
    {
        [Display(Name = "Permissions")]
        public List<ApplicationPermissionViewModel> Permissions
        {
            get
            {
                return this.items;
            }
        }

        public IdentityRole Role { get; set; }
        public string SelectedPermissionId { get; set; }

        public ApplicationPermissionListViewModel()
        {
            items = new List<ApplicationPermissionViewModel>();
            PageNumber = 1;
            PageSize = 200;
            Role = null;
        }
    }
}
