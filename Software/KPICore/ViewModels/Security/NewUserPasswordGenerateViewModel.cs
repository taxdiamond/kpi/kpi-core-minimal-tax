﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.Security
{
    public class NewUserPasswordGenerateViewModel
    {
        public string UserId { get; set; }
        public string NewPassword { get; set; }

        public NewUserPasswordGenerateViewModel(string id, string password)
        {
            UserId = id;
            NewPassword = password;
        }
    }
}
