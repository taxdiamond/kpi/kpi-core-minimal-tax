﻿using Infrastructure.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.Security
{
    public class ResetPasswordViewModel : MessagesViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password",ErrorMessage ="PasswordMatchLbl")]
        public string PasswordConfirmation { get; set; }
        
        public string Token { get; set; }

        public ResetPasswordViewModel() 
        {

        }
    }
}
