﻿using Infrastructure.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.Security
{
    public class LoginViewModel : MessagesViewModel
    {
        [Required(ErrorMessage="UsernameReqFld")]
        [EmailAddress]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public LoginViewModel() 
        {

        }
    }
}
