﻿using Infrastructure.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace AEWeb.ViewModels.Security
{
    public class ForgotPasswordViewModel : MessagesViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public ForgotPasswordViewModel() 
        {

        }
    }
}
