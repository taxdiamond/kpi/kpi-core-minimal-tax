﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.SampleDashboard
{
    public class SampleDashboardKPI
    {
        public string ClassName { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public String ChartType { get; set; }

        public SampleDashboardKPI()
        {
                
        }

        public void Validate()
        {
            if (String.IsNullOrEmpty(ClassName))
                throw new Exception($"Invalid empty class name");
            if (Height <= 0 || Height >= 3)
                throw new Exception($"Invalid Height {Height} for KPI Class Name {ClassName}");
            object dummy;
            if (!Enum.TryParse(typeof(KPIClassLibrary.KPI.KPIChartType), ChartType, out dummy))
                throw new Exception($"Invalid Chart Type {ChartType} for KPI Class Name {ClassName}");
        }
    }
}
