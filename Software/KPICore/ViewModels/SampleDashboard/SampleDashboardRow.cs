﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.SampleDashboard
{
    public class SampleDashboardRow
    {
        public List<SampleDashboardKPI> KPIS { get; set; }

        public SampleDashboardRow()
        {

        }

        public void Validate()
        {
            int kpiNumber = 0;
            int totalWidth = 0;
            foreach (var item in KPIS)
            {
                kpiNumber += 1; 

                try
                {
                    item.Validate();
                }
                catch (Exception q)
                {
                    throw new Exception($"Invalid KPI number {kpiNumber}: {q.Message}");
                }

                totalWidth += item.Width;
            }

            //if(totalWidth > 12)
            //{
            //    throw new Exception($"Total width for row is {totalWidth}, and this should not be greated than 12");
            //}
        }
    }
}
