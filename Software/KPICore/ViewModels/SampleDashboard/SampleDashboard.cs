﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.SampleDashboard
{
    public class SampleDashboard
    {
        public String Name { get; set; }
        public string Domain { get; set; }
        public String Type { get; set; }
        public List<SampleDashboardRow> Rows { get; set; }

        public SampleDashboard()
        {               
        }

        public void Validate(string kpiJSONGroupDataConfigurationString, string kpiJSONMetaDataConfigurationString)
        {
            if (String.IsNullOrEmpty(Name))
                throw new Exception($"Invalid empty Sample Dashboard name");

            if (String.IsNullOrEmpty(Domain))
                throw new Exception($"Invalid empty Sample Dashboard Domain for sample Dashboard {Name}");

            object dummy;
            if (!Enum.TryParse(typeof(KPIClassLibrary.KPI.KPIDomain), Domain, out dummy))
                throw new Exception($"Invalid Sample Dashboard Domain {Domain} for sample Dashboard {Name}");

            if (String.IsNullOrEmpty(Type))
                throw new Exception($"Invalid empty Sample Dashboard Type for sample Dashboard {Name}");

            if (!Enum.TryParse(typeof(Domain.ValueObjects.Dashboard.DashboardType), Type, out dummy))
                throw new Exception($"Invalid Sample Dashboard Type {Type} for sample Dashboard {Name}");

            int rowNumber = 0;
            foreach (var item in Rows)
            {
                rowNumber += 1;
                try
                {
                    item.Validate();
                }
                catch (Exception q)
                {
                    throw new Exception($"Invalid row number {rowNumber} for Sample Dashboard {Name}: {q.Message}");
                }
            }

            KPIClassLibrary.KPI.KPICatalog myCatalog = new
                KPIClassLibrary.KPI.KPICatalog(kpiJSONGroupDataConfigurationString, kpiJSONMetaDataConfigurationString);

            // Validate each KPI in the Dashboard
            foreach (var row in Rows) { 
                foreach(var kpi in row.KPIS)
                {
                    KPIClassLibrary.KPI.KPICatalogItem theKPIItemInsideDashboard = myCatalog.FindKPI(kpi.ClassName);
                    if (theKPIItemInsideDashboard == null)
                        throw new Exception($"Invalid KPI class {kpi.ClassName} for Sample Dashboard {Name}");

                    KPIClassLibrary.KPI.KPIDomain theSampleDashboardDomain = (KPIClassLibrary.KPI.KPIDomain)Enum.Parse(typeof(KPIClassLibrary.KPI.KPIDomain), Domain);
                    if(theSampleDashboardDomain != theKPIItemInsideDashboard.TheKPIArea.Domain)
                        throw new Exception($"Sample Dashboard {Name} has a Domain {Domain} but has a KPI class {kpi.ClassName} with a different Domain {theKPIItemInsideDashboard.TheKPIArea.Domain.ToString()}");

                    KPIClassLibrary.KPI.KPIType theSampleDashboardType = (KPIClassLibrary.KPI.KPIType)Enum.Parse(typeof(KPIClassLibrary.KPI.KPIType), Type, true);
                    // TODO-IK:  Review this change
                    //if (theSampleDashboardType != theKPIItemInsideDashboard.TheKPIArea.Type)
                    if (!theKPIItemInsideDashboard.TheKPIArea.Type.Contains(theSampleDashboardType))
                        throw new Exception($"Sample Dashboard {Name} has the type {Type} but has a KPI class {kpi.ClassName} with a different Type {theKPIItemInsideDashboard.TheKPIArea.Type.ToString()}");
                }
            }

        }
    }
}
