﻿using Microsoft.AspNetCore.Razor.Language.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.Builder
{
    public class BuilderRowViewModel
    {
        private BuilderDashboardViewModel Parent { get; set; }
        public int MaxNumberCellsWidth { get; set; }

        public List<BuilderComponentViewModel> Cells { get; set; }

        public BuilderRowViewModel()
        {
            Cells = new List<BuilderComponentViewModel>();
            MaxNumberCellsWidth = 12; // Bootstrap MAX
        }

        public BuilderRowViewModel(BuilderDashboardViewModel builderDashboardViewModel) : this()
        {
            this.Parent = builderDashboardViewModel;
        }

        public BuilderComponentViewModel RemoveByIndex(int index)
        {
            if (index < 0 || index >= Cells.Count)
                throw new Exception("Index out of range of row");

            BuilderComponentViewModel rowToRemove = Cells.ElementAt(index);
            Cells.RemoveAt(index);
            return rowToRemove;
        }

        public void MoveUp()
        {
            int index = this.Parent.Rows.FindIndex(x => x.GetHashCode() == this.GetHashCode());
            this.Parent.MoveRow(index, index - 1);
        }

        public void MoveDown()
        {
            int index = this.Parent.Rows.FindIndex(x => x.GetHashCode() == this.GetHashCode());
            this.Parent.MoveRow(index, index + 1);
        }

        public void MoveComponent(int from, int to)
        {
            if (from == to) return;

            if (from < 0 || from >= Cells.Count)
                throw new Exception("From argument out of range of row");
            if (to < 0 || to >= Cells.Count)
                throw new Exception("To argument out of range of row");

            BuilderComponentViewModel fromComponent = RemoveByIndex(from);
            // Everything gets reordered, the to index must be updated if its bigger than from
            if (to > from) to = to - 1;
            Cells.Insert(to, fromComponent);
        }
        public int GetRealMaxWidthByParent() => Parent.MaxWidth;

        public void RecalculateWidths()
        {
            // Suggested cumulated width
            int totalSuggestedWidth = 0;
            foreach (BuilderComponentViewModel cell in Cells)
            {
                totalSuggestedWidth += cell.SuggestedWidth;
                cell.CalculatedWidth = cell.SuggestedWidth;
            }
            if (totalSuggestedWidth == MaxNumberCellsWidth)
                return;

            int index = 0;
            
            while (totalSuggestedWidth < MaxNumberCellsWidth)
            {
                bool addedAtLeastOne = false;
                foreach (BuilderKpiViewModel cell in Cells)
                {
                    if (totalSuggestedWidth == MaxNumberCellsWidth)
                        break;
                    int widthAdded = Cells.ElementAt(index).AddWidthIfPossible();
                    // To be able to check whether it was able to add width to at least one cell
                    addedAtLeastOne = (widthAdded > 0) ? true : addedAtLeastOne;
                    // update width of whole row
                    totalSuggestedWidth += widthAdded;
                }

                if (!addedAtLeastOne)
                {
                    // No more adding is possible
                    break;
                }
            }

            // All good here, we are done if this is true
            if (totalSuggestedWidth <= MaxNumberCellsWidth)
                return;
        }
    }
}
