﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.ViewModels.Builder
{
    /// <summary>
    /// The attributes in this class give us teh idea on how to manipulate the size of the 
    /// inner component to display it in a cell in the dashboard. The main idea is that
    /// eklements will flow alonfg the row adjusting their widths as necessary to alway complete
    /// up to 12 in size (bootstrap grid) or the closest possible
    /// </summary>
    public class BuilderComponentViewModel
    {
        public int OriginalSuggestedWidth { get; set; }
        public int OriginalMinWidth { get; set; }
        public int OriginalMaxWidth { get; set; }
        /// <summary>
        /// If this is MaxCells then the SuggestedWidth will be the same than the original
        /// suggested width. Because 
        /// originalWidth * Max(12) / Factor
        /// </summary>
        public int ContainerFactor { get; set; }
        public int SuggestedWidth {
            get
            {
                return (int)(OriginalSuggestedWidth * 12.0 / (double)ContainerFactor);
            }
        }
        public int MinWidth
        {
            get
            {
                return (int)(OriginalMinWidth * 12.0 / (double)ContainerFactor);
            }
        }
        public int MaxWidth {
            get
            {
                return (int)(OriginalMaxWidth * 12.0 / (double)ContainerFactor);
            }
        }
        public int CalculatedWidth { get; set; }
        public BuilderRowViewModel Parent { get; set; }
        public int Id { get; internal set; }

        protected static int nextId;

        public BuilderComponentViewModel(object a)
        {

        }

        public void CreatedNew()
        {
            nextId++;
            this.Id = nextId;
        }

        public void MoveLeft()
        {
            int index = this.Parent.Cells.FindIndex(x => x.GetHashCode() == this.GetHashCode());
            this.Parent.MoveComponent(index, index - 1);
        }

        public void MoveRight()
        {
            int index = this.Parent.Cells.FindIndex(x => x.GetHashCode() == this.GetHashCode());
            this.Parent.MoveComponent(index, index + 1);
        }

        public int AddWidthIfPossible()
        {
            if (CalculatedWidth < MaxWidth)
                CalculatedWidth++;

            if (this is BuilderDashboardViewModel)
            {
                this.RecalculateWidthForAllRows();
            }
            return CalculatedWidth;
        }

        private void RecalculateWidthForAllRows()
        {
            
        }
    }
}
