﻿using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Globalization;

namespace AEWeb.Controllers.Shared
{
    public abstract class I18nController : Controller
    {
        /// <summary>
        /// This is called before the action method is invoked, but this belongs to ControllerBase
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!string.IsNullOrEmpty(context.HttpContext.Request.Query["culture"]))
            {
                Response.Cookies.Append(
                    CookieRequestCultureProvider.DefaultCookieName,
                    CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(context.HttpContext.Request.Query["culture"])),
                    new Microsoft.AspNetCore.Http.CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
                    );
            }
            var locale = Request.HttpContext.Features.Get<IRequestCultureFeature>();

            base.OnActionExecuting(context);
        }
    }
}
