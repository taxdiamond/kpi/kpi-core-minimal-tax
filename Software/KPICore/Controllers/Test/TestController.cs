﻿using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.DataWarehouse;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Controllers.Test
{
    public class TestController : I18nController
    {

        public IMediator _mediator { get; set; }

        public TestController(IMediator mediator)
        {
            _mediator = mediator;
        }

        public IActionResult TestVCAjax()
        {
            return View();
        }

        public IActionResult OnlyVC(string value)
        {
            return ViewComponent("TestDdlAjax", value);
        }

        public async Task<JsonResult> Region_Read()
        {
            List<Domain.Region.Region> regions = await _mediator.Send(new GetRegionQuery());

            List<RegionDataWarehouseViewModel> result = new List<RegionDataWarehouseViewModel>();

            foreach (Domain.Region.Region obj in regions)
            {
                RegionDataWarehouseViewModel model = RegionDataWarehouseViewModel.FromItem(obj);
                result.Add(model);
            }
            return Json(result);
        }
    }
}
