﻿using Domain.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using AEWeb.ViewModels.Security;
using Microsoft.Extensions.Logging;
using System.Text;
using Framework.Email;
using System;
using AEWeb.Controllers.Shared;
using Microsoft.Extensions.Localization;

namespace AEWeb.Controllers.Security
{
    public class SecurityController : I18nController
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private ILogger<SecurityController> _logger;
        private IStringLocalizer<SharedResources> _localizer;
        private IEmailSender _emailSender;

        public SecurityController(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            SignInManager<ApplicationUser> signInManager,
            ILogger<SecurityController> logger,
            IStringLocalizer<SharedResources> localizer,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _logger = logger;
            _localizer = localizer;
            _emailSender = emailSender;
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            
            if (User != null && User.Identity != null && User.Identity.Name != null)
                _logger.LogInformation("The user " + User.Identity.Name + " has been logged out");

            return RedirectToAction("Index","Landing");
        }

        public IActionResult Login()
        {
            return View("Login_2", new LoginViewModel());
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            model.ErrorMessages.Clear();

            if (!ModelState.IsValid)
            {
                _logger.LogInformation("The credentials used for " + model.Username + " were not valid");
                model.ErrorMessages.Add(_localizer["InvalidFieldsCredentialsLbl"]);
                return View("Login_2", model);
            }
            var user = await _userManager.FindByNameAsync(model.Username);

            if (user != null)
            {
                bool confirmedEmail = await _userManager.IsEmailConfirmedAsync(user);
                if (!confirmedEmail)
                {
                    _logger.LogWarning("The user " + user.UserName + " cannot login, email not confirmed");
                    model.ErrorMessages.Add(_localizer["EmailNotConfirmedError"]);
                    return View("Login_2", model);
                }


                if(!user.Active)
                {
                    _logger.LogWarning("The user " + user.UserName + " cannot login, the user was deactivated");
                    model.ErrorMessages.Add(_localizer["UserDesactivated"]);
                    return View("Login_2", model);
                }

                var signInResult = await _signInManager.PasswordSignInAsync(user, model.Password, false, true);
                if (signInResult.Succeeded)
                {
                    _logger.LogInformation("The user " + user.UserName + " just logged in");
                    
                    return RedirectToAction("CheckMinimalDashboards", "Home");
                }
                _logger.LogWarning("The user " + model.Username + " could not be logged, wrong password.");
            }
            else
            {
                _logger.LogInformation("The user " + model.Username + " could not be found or could not be retrieved.");
            }

            model.ErrorMessages.Add(_localizer["UsernameOrPasswordErrorLbl"].ToString());

            return View("Login_2", model);
        }

        #region Register/Create User
        public IActionResult Register()
        {
            return View("Register", new RegisterApplicationUser());
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterApplicationUser model)
        {
            if (!ModelState.IsValid)
            {
                _logger.LogInformation("The registration model was not valid");
                model.ErrorMessages.Add(_localizer["RegistrationErrorLbl"].ToString());
                return View(model);
            }

            var objUser = new ApplicationUser();
            objUser.Email = model.Email;
            objUser.UserName = model.Email;
            objUser.FirstName = model.FirstName;
            objUser.LastName = model.LastName;
            objUser.Active = true;

            // Register functionality
            IdentityResult userCreated = await _userManager.CreateAsync(objUser, model.Password);
            
            if (userCreated.Succeeded)
            {
                _logger.LogInformation("Created the user " + model.Email + ", about to send email validation");
                // Send the email with the link to confirm the email
                var theRegisteredUser = await _userManager.FindByEmailAsync(model.Email);
                var token = await _userManager.GenerateEmailConfirmationTokenAsync(theRegisteredUser);
                var confirmationLink = Url.Action("ConfirmEmail", "Security",
                    new { userId = theRegisteredUser.Id, token = token }, Request.Scheme);

                _logger.LogInformation("Confirmation link for " + model.Email + ": " + confirmationLink);

                if (!SendEmailConfirmationEmail(model.Email, confirmationLink))
                {
                    model.ErrorMessages.Add("The user has been created but the email could not be sent. Register again with other email.");
                    return View("Register", model);
                }
            }
            else
            {
                StringBuilder allErrors = new StringBuilder();
                foreach (IdentityError anError in userCreated.Errors)
                {
                    allErrors.Append("(").Append(anError.Code).Append(") ").Append(anError.Description);
                    allErrors.AppendLine();
                }
                _logger.LogInformation("The user " + model.Email + " could not be created: " + allErrors);
            }
            return View("Login_2", new LoginViewModel());
        }


        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (string.IsNullOrWhiteSpace(userId) || string.IsNullOrWhiteSpace(token))
            {
                _logger.LogWarning("Tried to validate email without proper user and token");
            }
            var theUser = await _userManager.FindByIdAsync(userId);
            if (theUser != null)
            {
                IdentityResult result = await _userManager.ConfirmEmailAsync(theUser, token);
                if (result.Succeeded)
                {
                    _logger.LogInformation("The user " + theUser.UserName + " validated the email " + theUser.Email);
                    return View("ValidatedEmailConfirmation");
                }
                else
                    _logger.LogWarning("The user " + theUser.UserName + " provided a false or expired token");
            }
            else
            {
                _logger.LogWarning("Somebody tried to validate an email with a false userId: " + userId);
            }

            return View("ValidatedEmailConfirmationError");
        }
        private bool SendEmailConfirmationEmail(string email, string confirmationLink)
        {
            string subject = _localizer["EmailConfirm_Subject"].ToString();
            string emailBody = "Hi,\\n here is the link " + confirmationLink + " \\nBest regards";

            try
            {
                _emailSender.Send(email, subject, emailBody);
                _logger.LogInformation("An email containing the confirmation link has been sent to " + email);
                return true;
            }
            catch (Exception q)
            {
                _logger.LogError("The confirmation email could not be sent", q);
                return false;
            }
        }
        #endregion

        #region Forgot Password

        public IActionResult ForgotPassword()
        {
            return View("ForgotPassword", new ForgotPasswordViewModel());
        }
        
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null && await _userManager.IsEmailConfirmedAsync(user))
                {
                    var token = await _userManager.GeneratePasswordResetTokenAsync(user);

                    var passwordResetLink = Url.Action("ResetPassword", "Security",
                        new { email = model.Email, token = token }, Request.Scheme);
                    _logger.LogInformation("The link to reset password for user " + 
                        model.Email + ": " + passwordResetLink);

                    if (!SendResetPasswordLink(model.Email, passwordResetLink))
                    {
                        model.ErrorMessages.Add(_localizer["SendMailResetPasswordErrorLbl"].ToString());
                        return View("ForgotPassword", model);
                    }

                    return View("ForgotPasswordConfirmation");
                }
            } 
            else
            {
                _logger.LogWarning("The model for forgot password is invalid");
            }

            return View("ForgotPasswordConfirmation");
        }

        public async Task<IActionResult> ResetPassword(string email, string token)
        {
            if (string.IsNullOrWhiteSpace(email))
                return View("SecurityError");

            if (string.IsNullOrWhiteSpace(token))
                return View("SecurityError");

            var theUser = await _userManager.FindByEmailAsync(email);
            if (theUser != null && await _userManager.IsEmailConfirmedAsync(theUser))
            {
                ResetPasswordViewModel model = new ResetPasswordViewModel();
                model.Token = token;
                model.Email = email;
                return View("ResetPassword", model); 
            }

            return View("SecurityError");
        }

        [HttpPost]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var theUser = await _userManager.FindByEmailAsync(model.Email);

                if (theUser != null && await _userManager.IsEmailConfirmedAsync(theUser))
                {
                    IdentityResult result =
                        await _userManager.ResetPasswordAsync(theUser, model.Token, model.Password);

                    if (result.Succeeded)
                    {
                        _logger.LogInformation("Password for user " + theUser.UserName + " has been reset");
                        return View("Login_2", new LoginViewModel());
                    } 
                    else
                    {
                        _logger.LogError("The token " + model.Token +
                            " used for the user " + theUser.UserName +
                            " was not valid to reset the password");
                    }
                }
            }
            return View("SecurityError");
        }

        private bool SendResetPasswordLink(string email, string passwordResetLink)
        {
            string subject = _localizer["EmailResetPassword_Subject"].ToString();
            string emailBody = "Hi,\\n here is the link to reset your password " + passwordResetLink + " \\nBest regards";

            try
            {
                _emailSender.Send(email, subject, emailBody);
                _logger.LogInformation("An email containing the reset password link has been sent to " + email);
                return true;
            }
            catch (Exception q)
            {
                _logger.LogError("The reset password email could not be sent", q);
                return false;
            }
        }
        #endregion

        #region Miscelaneous

        public IActionResult AccessDenied(string ReturnUrl)
        {
            return View();
        }
        #endregion
    }
}
