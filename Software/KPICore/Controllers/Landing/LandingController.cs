﻿using AEWeb.Controllers.Shared;
using Microsoft.AspNetCore.Mvc;

namespace AEWeb.Controllers.Landing
{
    public class LandingController : I18nController
    {
        public LandingController()
        {

        }

        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Main", "Home");
            return View();
        }
    }
}
