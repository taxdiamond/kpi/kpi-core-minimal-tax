﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.Dashboard;
using Domain.Dashboard;
using Domain.Repository;
using Domain.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Serilog;

namespace AEWeb.Controllers.Dashboard
{
    public class DashboardUserController : I18nController
    {

        private ILogger<DashboardUserController> _logger;
        private IStringLocalizer<SharedResources> _localizer;
        private Framework.Service.IUnitOfWork _unitOfWork;
        private IDashboardUserRepository _dashboardUserRepository;
        private UserManager<ApplicationUser> _userManager;
        private IDashboardRepository _dashboardRepository;

        public DashboardUserController(
            ILogger<DashboardUserController> logger,
            IDashboardUserRepository dashboardUserRepository,
            IDashboardRepository dashboardRepository,
            IStringLocalizer<SharedResources> localizer,
            UserManager<ApplicationUser> userManager,
            Framework.Service.IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _localizer = localizer;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _dashboardUserRepository = dashboardUserRepository;
            _dashboardRepository = dashboardRepository;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Index(string dashboardId)
        {
            Guid dashboardGuid = new Guid(dashboardId);
            Domain.Dashboard.Dashboard objDashboard = await _dashboardRepository.FindById(dashboardGuid);
            List<DashboardUser> usersInDashboard = await _dashboardUserRepository.FindByDashboardId(dashboardGuid);

            DashboardUserViewModel viewModel = DashboardUserViewModel.FromModel(objDashboard, usersInDashboard);

            return View(viewModel);
        }


        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddUserInDashboard(DashboardUserViewModel model)
        {
            bool exists = await _dashboardUserRepository.Exists(new Guid(model.SelectedUserId), model.Dashboard.DashboardId);
            if (!exists)
            {
                ApplicationUser objUser = await _userManager.FindByIdAsync(model.SelectedUserId);
                Domain.Dashboard.Dashboard objDashboard = await _dashboardRepository.FindById(model.Dashboard.DashboardId);
                DashboardUser objToInsert = new DashboardUser()
                {
                    Dashboard = objDashboard,
                    DashboardUserId = Guid.NewGuid(),
                    SharedDashboardUser = objUser
                };
                await _dashboardUserRepository.Create(objToInsert);
                await _unitOfWork.Commit();
            }
            return RedirectToAction(nameof(Index), new { dashboardId = model.Dashboard.DashboardId });
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> RemoveUserFromDashboard(string userid, string dashboardid)
        {
            await _dashboardUserRepository.Remove(new Guid(dashboardid), new Guid(userid));
            await _unitOfWork.Commit();
            return RedirectToAction(nameof(Index), new { dashboardId = dashboardid });
        }
    }
}
