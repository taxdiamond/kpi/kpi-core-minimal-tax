﻿using AEWeb.Controllers.Shared;
using AEWeb.Utilities;
using AEWeb.ViewModels.KPI;
using Domain.Dashboard;
using Domain.Indicators;
using Domain.Security;
using Framework.Kpi;
using Framework.Utilities;
using Infrastructure.Mediator.Dashboard.Queries;
using Infrastructure.Mediator.Indicator.Commands;
using Infrastructure.Mediator.Indicator.Queries;
using Infrastructure.ViewModels.KPI;
using KPIClassLibrary.KPI;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Controllers.Dashboard
{
    public class DashboardKpiController : I18nController
    {
        private ILogger<DashboardKpiController> _logger;
        private IStringLocalizer<SharedResources> _localizer;
        private IMediator _mediator;
        private UserManager<ApplicationUser> _userManager;
        private Framework.Service.IUnitOfWork _unitOfWork;
        private IKpiDataGetter _kpiDataGetter;
        private IWebHostEnvironment _hostingEnvironment;
        private IChartUtilities _chartUtilities;

        public DashboardKpiController(ILogger<DashboardKpiController> logger,
            IStringLocalizer<SharedResources> localizer,
            IMediator mediator,
            Framework.Service.IUnitOfWork unitOfWork,
            IKpiDataGetter kpiDataGetter,
            IWebHostEnvironment hostingEnvironment,
            UserManager<ApplicationUser> userManager,
            IChartUtilities chartUtilities)
        {
            _logger = logger;
            _localizer = localizer;
            _mediator = mediator;
            _userManager = userManager;
            _unitOfWork = unitOfWork;
            _kpiDataGetter = kpiDataGetter;
            _hostingEnvironment = hostingEnvironment;
            _chartUtilities = chartUtilities;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Index(string rowid)
        {
            string metadataPath = Utilities.ConfigUtilities.GetMetaDataPath(_hostingEnvironment);
            string jsonGroupPath = Utilities.ConfigUtilities.GetGroupsDataPath(_hostingEnvironment);
            KPIClassLibrary.KPI.KPICatalog catalog = new KPIClassLibrary.KPI.KPICatalog(jsonGroupPath, metadataPath);

            DashboardRow row = await _mediator.Send(new GetDashboardRowQuery() { RowId = Guid.Parse(rowid) });
            DashboardConfiguration dashboardConfig = await _mediator.Send(new GetDashboardConfigurationQuery() { 
                DashboardConfigurationGuid = row.ConfigurationRef.DashboardConfigurationId.ToString()
            });            

            List<Domain.Indicators.Kpi> kpis = 
                await _mediator.Send(new GetListKpisQuery() { DashboardRowId = rowid });

            KpiListViewModel model = KpiListViewModel.FromListKpi(kpis, catalog);
            model.RowId = Guid.Parse(rowid);
            model.DashboardId = dashboardConfig.DashboardRef.DashboardId;
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> MoveKpiUp(string rowid, string kpiid)
        {
            DashboardRow dashboardRow = 
                await _mediator.Send(new GetDashboardRowQuery() { RowId = Guid.Parse(rowid) });
            List<Domain.Indicators.Kpi> kpis =
                 await _mediator.Send(new GetListKpisQuery() { DashboardRowId = rowid });

            Domain.Indicators.Kpi upKpi = kpis.Single(x => x.KpiId == new Guid(kpiid));
            Domain.Indicators.Kpi tempKpi = kpis.Single(x => x.CellNumber == upKpi.CellNumber - 1);
            tempKpi.CellNumber++;
            upKpi.CellNumber--;
            await _mediator.Send(new UpdateKpiCommand(tempKpi));
            await _mediator.Send(new UpdateKpiCommand(upKpi));
            await _unitOfWork.Commit();

            return RedirectToAction("EasyEdit", "Dashboard", 
                new { id = dashboardRow.ConfigurationRef.DashboardRef.DashboardId });
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> MoveKpiDown(string rowid, string kpiid)
        {
            DashboardRow dashboardRow =
                await _mediator.Send(new GetDashboardRowQuery() { RowId = Guid.Parse(rowid) });

            List<Domain.Indicators.Kpi> kpis =
                  await _mediator.Send(new GetListKpisQuery() { DashboardRowId = rowid });

            Domain.Indicators.Kpi upKpi = kpis.Single(x => x.KpiId == new Guid(kpiid));
            Domain.Indicators.Kpi tempKpi = kpis.Single(x => x.CellNumber == upKpi.CellNumber + 1);
            tempKpi.CellNumber--;
            upKpi.CellNumber++;
            await _mediator.Send(new UpdateKpiCommand(tempKpi));
            await _mediator.Send(new UpdateKpiCommand(upKpi));
            await _unitOfWork.Commit();

            return RedirectToAction("EasyEdit", "Dashboard",
                new { id = dashboardRow.ConfigurationRef.DashboardRef.DashboardId });
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Create(Guid rowid)
        {
            Domain.Dashboard.DashboardRow row = await _mediator.Send(new GetDashboardRowQuery() { RowId = rowid });
            KpiViewModel kpi = new KpiViewModel();
            kpi.DashboardRowId = row.DashboardRowId;
            return View("Edit", kpi);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Insert(KpiViewModel model)
        {
            Kpi kpiToInsert = new Kpi();
            Guid rowId = model.DashboardRowId;
            kpiToInsert.DivHeight = 1;
            kpiToInsert.DivWidth = 4;
            kpiToInsert.FilterValue = "";
            kpiToInsert.Title = model.KpiMnemonic;
            kpiToInsert.NumberOfYears = 5;
            kpiToInsert.PeriodYear = DateTime.Now.Year;

            KPI baseKpi = _chartUtilities.GetKPIFromCatalog(model.KpiMnemonic, "");
            kpiToInsert.ViewType = ChartUtilities.GetKpiViewModelFrom(baseKpi.GetSupportedChartTypes().First());

            Domain.Dashboard.DashboardRow dashboardRow = await _mediator.Send(new GetDashboardRowQuery()
            {
                RowId = rowId
            });

            List<Kpi> kpis = await _mediator.Send(new GetListKpisQuery() { DashboardRowId = rowId.ToString() });
            int maxCellNumber = 0;
            if (kpis.Count > 0)
                maxCellNumber = kpis.Aggregate((curMax, x) => (curMax == null || (x.CellNumber ?? 0) >
                    curMax.CellNumber ? x : curMax)).CellNumber;

            kpiToInsert.CellNumber = maxCellNumber + 1;

            Domain.Dashboard.DashboardConfiguration dashboardConfiguration =
                await _mediator.Send(new GetDashboardConfigurationQuery()
                {
                    DashboardConfigurationGuid = dashboardRow.ConfigurationRef.DashboardConfigurationId.ToString()
                });
            

            Kpi insertedKpi = await _mediator.Send(new CreateKpiCommand()
            {
                NewKpi = kpiToInsert,
                DashboardRef = dashboardConfiguration.DashboardRef,
                RowRef = dashboardRow
            });

            _logger.LogInformation("The KPI " + model.KpiMnemonic + " has been inserted to row " + model.DashboardRowId);

            return RedirectToAction("Edit", new { id = insertedKpi.KpiId.ToString() });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Update(KpiViewModel model)
        {
            string metadataPath = Utilities.ConfigUtilities.GetMetaDataPath(_hostingEnvironment);
            string jsonGroupPath = Utilities.ConfigUtilities.GetGroupsDataPath(_hostingEnvironment);

            Kpi kpiInCell = await _mediator.Send(new GetKpiQuery() 
            { 
                KpiId = model.DashboardKpiId.ToString(), 
                NoTracking = false 
            });

            KPIClassLibrary.KPI.KPI kpiInstance = _chartUtilities.GetKPIFromCatalog(model.KpiMnemonic, "");

            kpiInstance.GetKPIMetadata(jsonGroupPath, metadataPath);

            model.GroupId = kpiInstance.Name;
            model.SupportedChartTypes = kpiInstance.GetSupportedChartTypes();
            model.KpiMnemonic = kpiInCell.Title;

            // Updating 
            kpiInCell.DivWidth = model.DivWidth;
            kpiInCell.ViewType = ChartUtilities.GetKpiViewModelFrom(model.ChartType);
            await _mediator.Send(new UpdateKpiCommand(kpiInCell));
            _logger.LogInformation("the KPI " + kpiInCell.KpiId.ToString() + " has been updated");

            return RedirectToAction("Edit", new { id = kpiInCell.KpiId.ToString() });
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Delete(string id)
        {
            Kpi kpiInCell = await _mediator.Send(new GetKpiQuery() { KpiId = id, NoTracking = true });

            Guid dashboardRowId = kpiInCell.RowRef.DashboardRowId;

            await _mediator.Send(new DeleteKpiCommand() { KpiId = Guid.Parse(id), RowId = dashboardRowId });

            return RedirectToAction("Index", new { rowid = dashboardRowId.ToString() });
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Edit(string id)
        {
            string metadataPath = Utilities.ConfigUtilities.GetMetaDataPath(_hostingEnvironment);
            string jsonGroupPath = Utilities.ConfigUtilities.GetGroupsDataPath(_hostingEnvironment);

            Kpi kpiInCell = await _mediator.Send(new GetKpiQuery() { KpiId = id, NoTracking = true });

            KpiViewModel model = KpiViewModel.FromKpi(kpiInCell);
            KPIClassLibrary.KPI.KPI kpiInstance = _chartUtilities.GetKPIFromCatalog(model.KpiId, "");

            kpiInstance.GetKPIMetadata(jsonGroupPath, metadataPath);

            model.GroupId = kpiInstance.Name;
            model.SupportedChartTypes = kpiInstance.GetSupportedChartTypes();
            model.KpiMnemonic = kpiInCell.Title;
            model.KpiFlavor = _chartUtilities.GetFlavor(kpiInstance);

            return View("Edit", model);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> UpdateKpiUserOptions(KpiViewModel model)
        {
            if (model.DashboardKpiId == Guid.Empty)
                return RedirectToAction("TestKpi", "Catalog", new { kpiView = model });

            Domain.Indicators.Kpi kpi = await _mediator.Send(new GetKpiQuery() { 
                KpiId = model.DashboardKpiId.ToString(), 
                NoTracking = false });
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);

            await _mediator.Send(new CreateUpdateKpiUserCommand()
            {
                User = user,
                KpiSource = kpi,
                ChartType = Utilities.ChartUtilities.GetKpiViewModelFrom(model.ChartType),
                NumberOfPeriods = model.NumberOfPeriods
            });

            Domain.Dashboard.Dashboard dashboard = await _mediator.Send(new GetDashboardQuery() { 
                DashboardGuid = model.DashboardId.ToString() 
            });

            return RedirectToAction("DisplayDashboard", "Dashboard", 
                new { key = dashboard.DashboardId.ToString() });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> UpdateKpiFilterOptions(KpiViewModel model)
        {
            if (model.DashboardKpiId == Guid.Empty)
                return RedirectToAction("TestKpi", "Catalog", new { kpiView = model });

            Domain.Indicators.Kpi kpi = await _mediator.Send(new GetKpiQuery()
            {
                KpiId = model.DashboardKpiId.ToString(),
                NoTracking = false
            });
            ApplicationUser user = await _userManager.FindByNameAsync(User.Identity.Name);

            await _mediator.Send(new CreateUpdateKpiUserCommand()
            {
                User = user,
                KpiSource = kpi,
                ChartType = Domain.Enums.Indicators.KpiViewModelType.None,
                FilterOffices = model.OfficesApplied == null ? "" : String.Join(",", model.OfficesApplied),
                FilterRegions = model.RegionsApplied == null ? "" : String.Join(",", model.RegionsApplied),
                FilterTaxPayerSegments = model.TaxPayerSegmentsApplied == null ? "" : String.Join(",", model.TaxPayerSegmentsApplied)
            }) ;

            Domain.Dashboard.Dashboard dashboard = await _mediator.Send(new GetDashboardQuery()
            {
                DashboardGuid = model.DashboardId.ToString()
            });

            return RedirectToAction("DisplayDashboard", "Dashboard",
                   new { key = dashboard.DashboardId.ToString() });
        }

        [HttpGet]
        public JsonResult GetKpiGroupBySearchJSON(string text)
        {
            string metadataPath = Utilities.ConfigUtilities.GetMetaDataPath(_hostingEnvironment);
            string jsonGroupPath = Utilities.ConfigUtilities.GetGroupsDataPath(_hostingEnvironment);
            KPIClassLibrary.KPI.KPICatalog catalog = new KPIClassLibrary.KPI.KPICatalog(jsonGroupPath, metadataPath);

            KpiCatalogAreaListViewModel areaList = KpiCatalogAreaListViewModel.FromList( catalog.KPIAreas);

            List<KpiCatalogAreaViewModel> model = new List<KpiCatalogAreaViewModel>();
            if (!string.IsNullOrWhiteSpace(text))
            {
                model = areaList.KpiCatalogAreas.Where(x => x.name.Contains(text) ||
                x.description.Contains(text)).ToList();
            }
            return Json(model);
        }

        [HttpGet]
        public JsonResult GetKpiBySearchJSON(string? groups, string productFilter)
        {
            string metadataPath = Utilities.ConfigUtilities.GetMetaDataPath(_hostingEnvironment);
            string jsonGroupPath = Utilities.ConfigUtilities.GetGroupsDataPath(_hostingEnvironment);
            KPIClassLibrary.KPI.KPICatalog catalog = new KPIClassLibrary.KPI.KPICatalog(jsonGroupPath, metadataPath);

            //KpiCatalogAreaListViewModel areaList = KpiCatalogItemViewModel.FromList(catalog.KPIAreas.First(x=> x.Name.Equals(groups)).KPIs);
            List<KPICatalogItem> items = catalog.KPIAreas.First(x => x.GroupID.Equals(groups)).KPIs;
            List<KpiCatalogItemViewModel> model = new List<KpiCatalogItemViewModel>();
            foreach (KPICatalogItem item in items)
            {
                KpiCatalogItemViewModel itemView = KpiCatalogItemViewModel.FromCatalogItem(item);
                KPI theKpi = _chartUtilities.GetKPIFromCatalog(item.ClassName, "");
                itemView.flavor = _chartUtilities.GetFlavor(theKpi);
                model.Add(itemView);
            }
            //x.description.Contains(text)).ToList();
            return Json(model);
        }
    }
}
