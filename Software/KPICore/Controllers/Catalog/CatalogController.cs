﻿using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.Dashboard;
using AEWeb.ViewModels.KPI;
using Domain.Indicators;
using Domain.Security;
using Framework.Utilities;
using Infrastructure.Mediator.Catalog.Handlers;
using Infrastructure.Mediator.Catalog.Queries;
using Infrastructure.Mediator.Indicator.Queries;
using Infrastructure.ViewModels.Dashboard;
using KPIClassLibrary.KPI;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Controllers.Catalog
{
    public class CatalogController : I18nController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private ILogger<HomeController> _logger;
        private IWebHostEnvironment _hostingEnvironment;
        private readonly IChartUtilities _chartUtilities;
        private readonly MediatR.IMediator _mediator;

        public CatalogController(ILogger<HomeController> logger,
            IWebHostEnvironment hostingEnvironment,
            IChartUtilities chartUtilities,
            IMediator mediator,
            UserManager<ApplicationUser> userManager)
        {
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
            _chartUtilities = chartUtilities;
            _mediator = mediator;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index(string type)
        {
            _logger.LogInformation("Catalog page to be displayed");
            string metaDataPath = Utilities.ConfigUtilities.GetMetaDataPath(_hostingEnvironment);
            string groupsDataPath = Utilities.ConfigUtilities.GetGroupsDataPath(_hostingEnvironment);

            ApplicationUser user = await _userManager.FindByEmailAsync(User.Identity.Name);
            DomainUser domain = await _mediator.Send(new GetDomainUser() { UserId = user.Id });

            KPIType theType = KPIType.Strategic;
            if (!string.IsNullOrWhiteSpace(type))
            {
                object? readType;
                if (Enum.TryParse(typeof(KPIType), type, out readType))
                    theType = (KPIType)readType;
            }

            DimensionListViewModel model = await _mediator.Send(new GetListCatalogDimensionsQuery()
            {
                GroupsDataPath = groupsDataPath,
                MetaDataPath = metaDataPath,
                Domain = Utilities.ChartUtilities.GetKPIDomainFrom(domain.Domain),
                Type = theType
            });

            return View(model);
        }

        [HttpGet]
        public IActionResult TestKpi(string kpiid)
        {
            if (string.IsNullOrWhiteSpace(kpiid))
                return View();

            string metadataPath = Utilities.ConfigUtilities.GetMetaDataPath(_hostingEnvironment);
            string jsonGroupPath = Utilities.ConfigUtilities.GetGroupsDataPath(_hostingEnvironment);

            KpiViewModel model = new KpiViewModel();
            model.KpiMnemonic = kpiid;
            KPI kpiinstance = _chartUtilities.GetKPIFromCatalog(kpiid, "");
            kpiinstance.GetKPIMetadata(jsonGroupPath, metadataPath);
            model.GroupId = kpiinstance.Name;
            return View(model);
        }

        [HttpPost]
        public IActionResult TestKpi(KpiViewModel kpiView)
        {
            //_logger.LogInformation("Testing now the kpi with id " + kpiView.KpiMnemonic);
            //kpiView.KpiId = "1AE43615-F336-4B59-BDB6-12C2F24518BB";

            _logger.LogInformation("Testing with kpi without id");
            return View(kpiView);
        }
    }
}
