﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.DataWarehouse;
using Domain.Segment;
using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AEWeb.Controllers.DataWarehouse
{
    public class SegmentDataWarehouseController : I18nController
    {
        private ILogger<SegmentDataWarehouseController> _logger;
        private ISegmentRepository _repository;

        public SegmentDataWarehouseController(ILogger<SegmentDataWarehouseController> logger,
            ISegmentRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public async Task<IActionResult> Index(int? pageNumber)
        {
            if (pageNumber == null)
                pageNumber = 1;
            PagedResultBase<Segment> result = await _repository.FindAll(30, pageNumber.Value);
            SegmentDataWarehouseListViewModel model = SegmentDataWarehouseListViewModel.FromPagedResultBase(result);

            return View(model);
        }

        [HttpPost]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public IActionResult Search(AuditStasDataWarehouseListViewModel inputModel)
        {
            return RedirectToAction("Index", "SegmentDataWarehouse", new
            {
                pageNumber = 1,
                dateFrom = inputModel.DateFrom,
                dateTo = inputModel.DateTo
            });
        }
    }
}
