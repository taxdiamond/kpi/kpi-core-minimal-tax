﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AEWeb.ViewModels.DataWarehouse;
using Domain.Repository;
using Domain.Utils;
using Framework.Service;
using Infrastructure.ViewModels.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AEWeb.Controllers.DataWarehouse
{
    public class DataWarehouseGeneralInformationController : Controller
    {
        private ILogger<DataWarehouseGeneralInformationController> _logger;
        private IDataWarehouseRepository _repository;
        private IDataWareHouseGenericSQLRepository _SQLDWRepository;
        private IWebHostEnvironment _hostingEnvironment;
        private IExcelReader _excelReader;
        public DataWarehouseGeneralInformationController(ILogger<DataWarehouseGeneralInformationController> logger,
            IDataWarehouseRepository repository, 
            IWebHostEnvironment hostingEnvironment, 
            IDataWareHouseGenericSQLRepository sqlRepository,
            IExcelReader excelReader)
        {
            _logger = logger;
            _repository = repository;
            _SQLDWRepository = sqlRepository;
            _hostingEnvironment = hostingEnvironment;
            _excelReader = excelReader;
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Index()
        {
            List<Domain.DataWarehouse.DataWarehouseRecord> list = await _repository.GetDataWarehouseRecords();
            DataWarehouseGeneralInformationListViewModel model = DataWarehouseGeneralInformationListViewModel.FromDataWarehouseRecordList(list);

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public IActionResult ResetAllDataWerehouse()
        {
            _SQLDWRepository.DeleteAllDataWarehouseRecords();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Authorize]
        public IActionResult DeleteDataWareHouseByDate(DataWarehouseGeneralInformationListViewModel data)
        {
            MessagesViewModel messages = new MessagesViewModel();
            if (data.FromDate == null || data.UntilDate == null)
            {
                messages.ErrorMessages.Add("Error in dates");
                TempData["Messages"] = messages.GetJSONMessages();
                return RedirectToAction(nameof(Index));
            }

            if(data.FromDate >= data.UntilDate)
            {
                messages.ErrorMessages.Add("Error the from date cannot be greater than until date");
                TempData["Messages"] = messages.GetJSONMessages();
                return RedirectToAction(nameof(Index));
            }

            _SQLDWRepository.DeleteTableDataWarehouseByDate(data.DWTypeSelected, data.FromDate, data.UntilDate);
            messages.Messages.Add("Delete succsesfully");
            TempData["Messages"] = messages.GetJSONMessages();
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        [Authorize]
        public IActionResult ResetClassifierDataWerehouse()
        {
            _SQLDWRepository.ResetClassifierDataWareHouse();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> UploadExcelDataWareHouse(DataWarehouseGeneralInformationListViewModel model, IFormFile file)
        {
            MessagesViewModel messages = new MessagesViewModel();
            if (file == null)
                return RedirectToAction(nameof(Index));
            string JsonData = System.IO.File.ReadAllText(_hostingEnvironment.ContentRootPath + "/DataFiles/SpName.json");
            
            List<TableSPDatawareHouse> listTableSP = 
                JsonConvert.DeserializeObject<List<TableSPDatawareHouse>>(JsonData);

            var fileContent = ContentDispositionHeaderValue.Parse(file.ContentDisposition);
            using (var stream = file.OpenReadStream())
            {
                List<string> errors = new List<string>();
                List<DataWareHouseDataSet> list = _excelReader.ReadExcelSpreadhseetForDataWareHouseSP(stream, errors, listTableSP);
                if(errors.Count > 0)
                {
                    messages.ErrorMessages.Add("Error in excel");
                    List<Domain.DataWarehouse.DataWarehouseRecord> listDW = await _repository.GetDataWarehouseRecords();

                    model = DataWarehouseGeneralInformationListViewModel.FromDataWarehouseRecordList(listDW);
                    model.Errors = errors;
                    return View("Index", model);
                }
                else
                {
                    try
                    {
                        _SQLDWRepository.InsertDatawareHouse(list, model.AloneNextDate);
                        messages.Messages.Add("Inserted successfully");
                    } catch(Exception q)
                    {
                        _logger.LogError(q, "Failed to insert in datawarehouse");
                        messages.ErrorMessages.Add("Failed to insert data, view logs");
                    }
                }
            }
            TempData["Messages"] = messages.GetJSONMessages();
            return RedirectToAction(nameof(Index));
        }
    }
}
