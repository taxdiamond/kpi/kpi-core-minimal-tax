﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.DataWarehouse;
using Domain.CountryStats;
using Domain.Repository;
using Domain.Utils;
using Infrastructure.Persistence.Repository.CountryStats;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AEWeb.Controllers.DataWarehouse
{
    public class CountryStatsDataWarehouseController : I18nController
    {
        private ILogger<CountryStatsDataWarehouseController> _logger;
        private ICountryStatsRepository _countryStatsRepository;

        public CountryStatsDataWarehouseController(ILogger<CountryStatsDataWarehouseController> logger,
            ICountryStatsRepository countryStatsRepository)
        {
            _logger = logger;
            _countryStatsRepository = countryStatsRepository;
        }

        [HttpGet]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public async Task<IActionResult> Index(int? pageNumber, DateTime? dateFrom, DateTime? dateTo)
        {
            if (pageNumber == null)
                pageNumber = 1;
            PagedResultBase<CountryStats> result = await _countryStatsRepository.FindAll(30, pageNumber.Value, dateFrom, dateTo);
            CountryStatsDataWarehouseListViewModel model = CountryStatsDataWarehouseListViewModel.FromPagedResultBase(result);
            model.DateFrom = dateFrom;
            model.DateTo = dateTo;


            return View(model);
        }

        [HttpPost]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public IActionResult Search(AuditStasDataWarehouseListViewModel inputModel)
        {
            return RedirectToAction("Index", "CountryStatsDataWarehouse", new
            {
                pageNumber = 1,
                dateFrom = inputModel.DateFrom,
                dateTo = inputModel.DateTo
            });
        }
    }
}
