﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.DataWarehouse;
using Domain.AuditStats;
using Domain.Repository;
using Domain.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AEWeb.Controllers.DataWarehouse
{
    public class AuditStatsDataWarehouseController : I18nController
    {
        private ILogger<AuditStatsDataWarehouseController> _logger;
        private IAuditStatsRepository _auditStatsRepository;

        public AuditStatsDataWarehouseController(ILogger<AuditStatsDataWarehouseController> logger,
            IAuditStatsRepository auditStatsRepository)
        {
            _logger = logger;
            _auditStatsRepository = auditStatsRepository;
        }

        [HttpGet]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public async Task<IActionResult> Index(int? pageNumber, DateTime? dateFrom, DateTime? dateTo)
        {
            if (pageNumber == null)
                pageNumber = 1;
            PagedResultBase<Domain.AuditStats.AuditStats> result = await _auditStatsRepository.FindAll(30, pageNumber.Value, dateFrom, dateTo);
            AuditStasDataWarehouseListViewModel model = AuditStasDataWarehouseListViewModel.FromPagedResultBase(result);
            model.DateFrom = dateFrom;
            model.DateTo = dateTo;
        
            return View(model);
        }

        [HttpPost]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public IActionResult Search(AuditStasDataWarehouseListViewModel inputModel)
        {
            return RedirectToAction("Index", "AuditStatsDataWarehouse", new { 
                pageNumber = 1,
                dateFrom = inputModel.DateFrom,
                dateTo = inputModel.DateTo
            });
        }
    }
}
