﻿using AEWeb.ViewModels.DataWarehouse;
using Domain.Repository;
using Domain.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Controllers.DataWarehouse
{
    public class AppealDataWarehouseController : Controller
    {
        private ILogger<AppealDataWarehouseController> _logger;
        private IAppealRepository _AppealRepository;

        public AppealDataWarehouseController(ILogger<AppealDataWarehouseController> logger,
            IAppealRepository AppealRepository)
        {
            _logger = logger;
            _AppealRepository = AppealRepository;
        }

        [HttpGet]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public async Task<IActionResult> IndexAsync(int? pageNumber, DateTime? dateFrom, DateTime? dateTo)
        {
            if (pageNumber == null)
                pageNumber = 1;
            PagedResultBase<Domain.Tax.Appeal> result = await _AppealRepository.FindAll(30, pageNumber.Value, dateFrom, dateTo);
            AppealDataWarehouseListViewModel model = AppealDataWarehouseListViewModel.FromPagedResultBase(result);
            model.DateFrom = dateFrom;
            model.DateTo = dateTo;

            return View(model);
        }

        [HttpPost]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public IActionResult Search(AppealDataWarehouseListViewModel inputModel)
        {
            return RedirectToAction("Index", "AppealDataWarehouse", new
            {
                pageNumber = 1,
                dateFrom = inputModel.DateFrom,
                dateTo = inputModel.DateTo
            });
        }
    }
}
