﻿using AEWeb.ViewModels.DataWarehouse;
using Domain.Repository;
using Domain.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Controllers.DataWarehouse
{
    public class RefundsDataWarehouseController : Controller
    {
        private ILogger<RefundsDataWarehouseController> _logger;
        private IRefundRepository _refundsRepository;

        public RefundsDataWarehouseController(ILogger<RefundsDataWarehouseController> logger,
            IRefundRepository refundsRepository)
        {
            _logger = logger;
            _refundsRepository = refundsRepository;
        }

        [HttpGet]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public async Task<IActionResult> IndexAsync(int? pageNumber, DateTime? dateFrom, DateTime? dateTo)
        {
            if (pageNumber == null)
                pageNumber = 1;
            PagedResultBase<Domain.Tax.Refund> result = await _refundsRepository.FindAll(30, pageNumber.Value, dateFrom, dateTo);
            RefundsDataWarehouseListViewModel model = RefundsDataWarehouseListViewModel.FromPagedResultBase(result);
            model.DateFrom = dateFrom;
            model.DateTo = dateTo;

            return View(model);
        }

        [HttpPost]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public IActionResult Search(RefundsDataWarehouseListViewModel inputModel)
        {
            return RedirectToAction("Index", "RefundsDataWarehouse", new
            {
                pageNumber = 1,
                dateFrom = inputModel.DateFrom,
                dateTo = inputModel.DateTo
            });
        }
    }
}
