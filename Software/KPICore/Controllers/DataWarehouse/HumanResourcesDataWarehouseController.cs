﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.DataWarehouse;
using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AEWeb.Controllers.DataWarehouse
{
    public class HumanResourcesDataWarehouseController : I18nController
    {
        private ILogger<HumanResourcesDataWarehouseController> _logger;
        private IHumanResourceRepository _hrRepository;

        public HumanResourcesDataWarehouseController(ILogger<HumanResourcesDataWarehouseController> logger,
            IHumanResourceRepository hrRepository)
        {
            _logger = logger;
            _hrRepository = hrRepository;
        }

        [HttpGet]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public async Task<IActionResult> Index(int? pageNumber, DateTime? dateFrom, DateTime? dateTo)
        {
            if (pageNumber == null)
                pageNumber = 1;
            PagedResultBase<HumanResource> result = await _hrRepository.FindAll(30, pageNumber.Value, dateFrom, dateTo);
            HumanResourcesDataWarehouseListViewModel model = HumanResourcesDataWarehouseListViewModel.FromPagedResultBase(result);
            model.DateFrom = dateFrom;
            model.DateTo = dateTo;

            return View(model);
        }

        [HttpPost]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public IActionResult Search(AuditStasDataWarehouseListViewModel inputModel)
        {
            return RedirectToAction("Index", "HumanResourcesDataWarehouse", new
            {
                pageNumber = 1,
                dateFrom = inputModel.DateFrom,
                dateTo = inputModel.DateTo
            });
        }
    }
}
