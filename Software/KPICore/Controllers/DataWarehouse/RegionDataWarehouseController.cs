﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AEWeb.Controllers.Shared;
using AEWeb.ViewModels.DataWarehouse;
using Domain.Region;
using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AEWeb.Controllers.DataWarehouse
{
    public class RegionDataWarehouseController : I18nController
    {
        private ILogger<RegionDataWarehouseController> _logger;
        private IRegionRepository _repository;

        public RegionDataWarehouseController(ILogger<RegionDataWarehouseController> logger,
            IRegionRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public async Task<IActionResult> Index(int? pageNumber)
        {
            if (pageNumber == null)
                pageNumber = 1;
            PagedResultBase<Region> result = await _repository.FindAll(30, pageNumber.Value);
            RegionDataWarehouseListViewModel model = RegionDataWarehouseListViewModel.FromPagedResultBase(result);

            return View(model);
        }

        [HttpPost]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public IActionResult Search(AuditStasDataWarehouseListViewModel inputModel)
        {
            return RedirectToAction("Index", "RegionDataWarehouse", new
            {
                pageNumber = 1,
                dateFrom = inputModel.DateFrom,
                dateTo = inputModel.DateTo
            });
        }
    }
}
