﻿using AEWeb.ViewModels.DataWarehouse;
using Domain.Repository;
using Domain.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AEWeb.Controllers.DataWarehouse
{
    public class ArrearDataWarehouseController : Controller
    {
        private ILogger<ArrearDataWarehouseController> _logger;
        private IArrearRepository _arrearRepository;

        public ArrearDataWarehouseController(ILogger<ArrearDataWarehouseController> logger,
            IArrearRepository arrearRepository)
        {
            _logger = logger;
            _arrearRepository = arrearRepository;
        }

        [HttpGet]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public async Task<IActionResult> IndexAsync(int? pageNumber, DateTime? dateFrom, DateTime? dateTo)
        {
            if (pageNumber == null)
                pageNumber = 1;
            PagedResultBase<Domain.Tax.Arrear> result = await _arrearRepository.FindAll(30, pageNumber.Value, dateFrom, dateTo);
            ArrearDataWarehouseListViewModel model = ArrearDataWarehouseListViewModel.FromPagedResultBase(result);
            model.DateFrom = dateFrom;
            model.DateTo = dateTo;

            return View(model);
        }

        [HttpPost]
        [Microsoft.AspNetCore.Authorization.Authorize]
        public IActionResult Search(ArrearDataWarehouseListViewModel inputModel)
        {
            return RedirectToAction("Index", "ArrearDataWarehouse", new
            {
                pageNumber = 1,
                dateFrom = inputModel.DateFrom,
                dateTo = inputModel.DateTo
            });
        }
    }
}
