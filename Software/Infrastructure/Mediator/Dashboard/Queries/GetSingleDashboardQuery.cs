﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Queries
{
    public class GetSingleDashboardQuery : IRequest<Domain.Dashboard.Dashboard>
    {
        public Guid DashboardId { get; set; }

        public GetSingleDashboardQuery(Guid dashboardId)
        {
            DashboardId = dashboardId;
        }
    }
}
