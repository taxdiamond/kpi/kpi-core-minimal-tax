﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Queries
{
    public class GetDashboardsByUserIdQuery : IRequest<List<Domain.Dashboard.Dashboard>>
    {
        public string UserId { get; set; }
        public string KpiDomain { get; set; }
    }
}
