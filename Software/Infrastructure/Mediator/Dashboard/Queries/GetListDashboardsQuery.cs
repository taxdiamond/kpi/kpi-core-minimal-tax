﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Queries
{
    public class GetListDashboardsQuery : IRequest<List<Domain.Dashboard.Dashboard>>
    {
        public string UserId { get; set; }
        public string SearchQuery { get; set; }
        public string KpiDomain { get; set; }
    }
}
