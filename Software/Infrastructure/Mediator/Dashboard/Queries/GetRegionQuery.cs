﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Queries
{
    public class GetRegionQuery : IRequest<List<Domain.Region.Region>>
    {
        public string Id { get; set; }
        public string Query { get; set; }
    }
}
