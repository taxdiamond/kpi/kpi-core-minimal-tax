﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Queries
{
    public class GetDashboardRowQuery : IRequest<Domain.Dashboard.DashboardRow>
    {
        public Guid RowId { get; set; }
    }
}
