﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Queries
{
    public class GetDashboardPreferredQuery : IRequest<Domain.Dashboard.DashboardPreferred>
    {
        public string UserId { get; set; }
        public string KpiDomain { get; set; }
    }
}
