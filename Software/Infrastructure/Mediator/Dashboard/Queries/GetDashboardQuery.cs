﻿
using MediatR;

namespace Infrastructure.Mediator.Dashboard.Queries
{
    public class GetDashboardQuery : IRequest<Domain.Dashboard.Dashboard>
    {
        public string DashboardGuid { get; set; }
    }
}
