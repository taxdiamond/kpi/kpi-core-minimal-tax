﻿using Domain.Repository;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class GetListDashboardRowsHandler : IRequestHandler<GetListDashboardRowsQuery, List<Domain.Dashboard.DashboardRow>>
    {
        private readonly IDashboardRowRepository _repository;

        public GetListDashboardRowsHandler(IDashboardRowRepository repository)
        {
            _repository = repository;
        }

        public async Task<List<Domain.Dashboard.DashboardRow>> Handle(
            GetListDashboardRowsQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                return _repository.FindByDashboard(request.DashboardConfigurationGuid);
            });
        }

    }
}
