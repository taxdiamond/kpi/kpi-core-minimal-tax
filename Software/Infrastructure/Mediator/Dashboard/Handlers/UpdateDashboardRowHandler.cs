﻿using Domain.Repository;
using Framework.Service;
using Infrastructure.Mediator.Dashboard.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class UpdateDashboardRowHandler : IRequestHandler<UpdateDashboardRowCommand, Domain.Dashboard.DashboardRow>
    {
        private IDashboardRowRepository _repository;
        private IUnitOfWork _unitOfWork;

        public UpdateDashboardRowHandler(IDashboardRowRepository repository,
            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<Domain.Dashboard.DashboardRow> Handle(UpdateDashboardRowCommand request, CancellationToken cancellationToken)
        {
            var result =  _repository.Update(request.DashboardRowToBeUpdated);
            await _unitOfWork.Commit();
            return result;
        }
    }
}
