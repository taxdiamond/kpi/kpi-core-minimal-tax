﻿using Domain.Repository;
using Framework.Service;
using Infrastructure.Mediator.Dashboard.Commands;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class DeleteDashboardHandler : IRequestHandler<DeleteDashboardCommand, Domain.Dashboard.Dashboard>
    {
        private readonly IKpiUserRepository _kpiUserRepository;
        private readonly IKpiRepository _kpiRepository;
        private readonly IDashboardRowRepository _dashboardRowRepository;
        private readonly IDashboardConfigurationRepository _dashboardConfigurationRepository;
        private readonly IDashboardRepository _dashboardRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IDashboardUserRepository _dashboardUserRepository;
        private readonly IDashboardPreferredRepository _dashboardPreferredRepository;

        public DeleteDashboardHandler(
            IKpiUserRepository kpiUserRepository,
            IKpiRepository kpiRepository,
            IDashboardRowRepository dashboardRowRepository,
            IDashboardConfigurationRepository dashboardConfigurationRepository,
            IDashboardRepository dashboardRepository,
            IUnitOfWork unitOfWork,
            IDashboardUserRepository dashboardUserRepository,
            IDashboardPreferredRepository dashboardPreferredRepository)
        {
            _kpiUserRepository = kpiUserRepository;
            _kpiRepository = kpiRepository;
            _dashboardRowRepository = dashboardRowRepository;
            _dashboardConfigurationRepository = dashboardConfigurationRepository;
            _dashboardRepository = dashboardRepository;
            _unitOfWork = unitOfWork;
            _dashboardUserRepository = dashboardUserRepository;
            _dashboardPreferredRepository = dashboardPreferredRepository;
        }

        public async Task<Domain.Dashboard.Dashboard> Handle(DeleteDashboardCommand request, CancellationToken cancellationToken)
        {
            Domain.Dashboard.Dashboard dashboard = await _dashboardRepository.FindById(request.DashboardId);
            if (dashboard == null)
                return null;

            Domain.Dashboard.DashboardConfiguration dashboardConfiguration =
                _dashboardConfigurationRepository.FindByDashboardId(dashboard.DashboardId);

            List<Domain.Dashboard.DashboardPreferred> preferreds =
                _dashboardPreferredRepository.FindByDashboardId(dashboard.DashboardId);

            List<Domain.Dashboard.DashboardRow> rows =
                _dashboardRowRepository.FindByDashboard(dashboardConfiguration.DashboardConfigurationId);

            if (rows != null) {
                foreach (Domain.Dashboard.DashboardRow row in rows)
                {
                    List<Domain.Indicators.Kpi> kpis =
                        _kpiRepository.FindByRow(row.DashboardRowId);

                    if (kpis != null)
                    {
                        foreach (Domain.Indicators.Kpi kpi in kpis)
                        {
                            _kpiUserRepository.RemoveByKpi(kpi.KpiId);
                            await _kpiRepository.Delete(kpi.KpiId, kpi);
                        }
                    }

                    _dashboardRowRepository.Remove(row);
                }
            }

            foreach(Domain.Dashboard.DashboardPreferred preferred in preferreds)
            {
                _dashboardPreferredRepository.Remove(preferred);
            }

            _dashboardConfigurationRepository.Remove(dashboardConfiguration);            
            _dashboardRepository.Remove(dashboard);
            await _unitOfWork.Commit();

            return dashboard;
        }
    }
}
