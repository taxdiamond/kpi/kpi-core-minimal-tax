﻿using Domain.Repository;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class GetDashboardPreferredHandler : IRequestHandler<GetDashboardPreferredQuery, Domain.Dashboard.DashboardPreferred>
    {
        private readonly IDashboardPreferredRepository _dashboardRepository;
        public GetDashboardPreferredHandler(IDashboardPreferredRepository dashboardRepository)
        {
            _dashboardRepository = dashboardRepository;
        }
        public Task<Domain.Dashboard.DashboardPreferred> Handle(GetDashboardPreferredQuery request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                return _dashboardRepository.FindByUserId(new Guid(request.UserId), request.KpiDomain);
            });

        }
    }
}
