﻿using Domain.Repository;
using Framework.Service;
using Infrastructure.Mediator.Dashboard.Commands;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class UpdateDashboardPreferredHandler : IRequestHandler<UpdateDashboardPreferredCommand, Domain.Dashboard.DashboardPreferred>
    {
        private IDashboardPreferredRepository _repository;
        private IUnitOfWork _unitOfWork;

        public UpdateDashboardPreferredHandler(IDashboardPreferredRepository repository,
            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<Domain.Dashboard.DashboardPreferred> Handle(UpdateDashboardPreferredCommand request, CancellationToken cancellationToken)
        {
            var result = _repository.Update(request.DashboardPreferredToBeUpdated);
            await _unitOfWork.Commit();
            return result;
        }
    }
}
