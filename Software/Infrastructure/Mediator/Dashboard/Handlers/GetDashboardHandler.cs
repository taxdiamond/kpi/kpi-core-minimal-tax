﻿using Domain.Repository;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using Org.BouncyCastle.Ocsp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class GetDashboardHandler : IRequestHandler<GetDashboardQuery, Domain.Dashboard.Dashboard>
    {
        private readonly IDashboardRepository _dashboardRepository;
        public GetDashboardHandler(IDashboardRepository dashboardRepository)
        {
            _dashboardRepository = dashboardRepository;
        }
        public async Task<Domain.Dashboard.Dashboard> Handle(GetDashboardQuery request, CancellationToken cancellationToken)
        {
            Domain.Dashboard.Dashboard result = 
                await _dashboardRepository.FindById(new Guid(request.DashboardGuid));

            return result;
        }
    }
}
