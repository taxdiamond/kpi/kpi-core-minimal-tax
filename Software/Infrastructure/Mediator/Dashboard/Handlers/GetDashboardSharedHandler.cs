﻿using Domain.Repository;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class GetDashboardSharedHandler : IRequestHandler<GetDashboardSharedQuery, List<Domain.Dashboard.DashboardUser>>
    {
        private readonly IDashboardUserRepository _dashboardUserRepository;
        public GetDashboardSharedHandler(IDashboardUserRepository dashboardRepository)
        {
            _dashboardUserRepository = dashboardRepository;
        }
        public async Task<List<Domain.Dashboard.DashboardUser>> Handle(GetDashboardSharedQuery request, CancellationToken cancellationToken)
        {
            List<Domain.Dashboard.DashboardUser> result =
                await _dashboardUserRepository.FindByUserId(new Guid(request.UserId));

            return result;
        }

    }
}
