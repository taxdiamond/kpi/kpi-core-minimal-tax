﻿using Domain.Dashboard;
using Domain.Repository;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class GetDashboardRowHandler : IRequestHandler<GetDashboardRowQuery, DashboardRow>
    {
        private readonly IDashboardRowRepository _repository;

        public GetDashboardRowHandler(IDashboardRowRepository repository)
        {
            _repository = repository;
        }
        public async Task<DashboardRow> Handle(GetDashboardRowQuery request, CancellationToken cancellationToken)
            => await _repository.FindById(request.RowId);
    }
}
