﻿using Domain.Repository;
using Infrastructure.Mediator.Dashboard.Queries;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class GetSingleDashboardHandler : IRequestHandler<GetSingleDashboardQuery, Domain.Dashboard.Dashboard>
    {
        private readonly IDashboardRepository _repository;

        public GetSingleDashboardHandler(IDashboardRepository repository)
        {
            _repository = repository;
        }

        public Task<Domain.Dashboard.Dashboard> Handle(GetSingleDashboardQuery request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                return _repository.FindById(request.DashboardId);
            });
        }
    }
}
