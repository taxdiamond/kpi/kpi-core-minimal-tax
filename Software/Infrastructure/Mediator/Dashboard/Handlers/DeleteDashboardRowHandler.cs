﻿using Domain.Dashboard;
using Domain.Repository;
using Domain.ValueObjects.Dashboard;
using Framework.Service;
using Infrastructure.Mediator.Dashboard.Commands;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class DeleteDashboardRowHandler : IRequestHandler<DeleteDashboardRowCommand, Domain.Dashboard.DashboardRow>
    {
        public IKpiUserRepository _kpiUserRepository { get; set; }
        public IKpiRepository _kpiRepository { get; set; }
        public IDashboardRowRepository _rowRepository { get; set; }
        public IUnitOfWork _unitOfWork { get; set; }
        public ILogger<DeleteDashboardRowHandler> _logger { get; set; }
        public DeleteDashboardRowHandler(
            IDashboardRowRepository repository,
            IKpiRepository kpiRepository,
            IKpiUserRepository kpiUserRepository,
            IUnitOfWork unitOfWork,
            ILogger<DeleteDashboardRowHandler> logger)
        {
            _rowRepository = repository;
            _kpiRepository = kpiRepository;
            _kpiUserRepository = kpiUserRepository;
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task<DashboardRow> Handle(DeleteDashboardRowCommand request, CancellationToken cancellationToken)
        {
            DashboardRow row = await _rowRepository.FindById(new DashboardRowGuid(Guid.Parse(request.RowId)));

            List<Domain.Indicators.Kpi> kpis =
                        _kpiRepository.FindByRow(row.DashboardRowId);

            if (kpis != null)
            {
                foreach (Domain.Indicators.Kpi kpi in kpis)
                {
                    _kpiUserRepository.RemoveByKpi(kpi.KpiId);
                    await _kpiRepository.Delete(kpi.KpiId, kpi);
                }
            }

            _rowRepository.Remove(row);
            await _unitOfWork.Commit();

            _logger.LogInformation("The row with id " + request.RowId + " has been deleted");
            return row;
        }
    }
}
