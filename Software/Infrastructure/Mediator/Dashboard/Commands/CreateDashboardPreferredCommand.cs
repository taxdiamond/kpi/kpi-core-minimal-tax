﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Commands
{
    public class CreateDashboardPreferredCommand : IRequest<Domain.Dashboard.DashboardPreferred>
    {
        public Domain.Dashboard.DashboardPreferred Dashboard { get; set; }
        public CreateDashboardPreferredCommand(Domain.Dashboard.DashboardPreferred dashboard)
        {
            this.Dashboard = dashboard;
        }

    }
}
