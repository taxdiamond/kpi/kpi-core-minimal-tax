﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Commands
{
    public class UpdateDashboardPreferredCommand : IRequest<Domain.Dashboard.DashboardPreferred>
    {
        public Domain.Dashboard.DashboardPreferred DashboardPreferredToBeUpdated { get; set; }

        public UpdateDashboardPreferredCommand(Domain.Dashboard.DashboardPreferred obj)
        {
            this.DashboardPreferredToBeUpdated = obj;
        }
    }
}
