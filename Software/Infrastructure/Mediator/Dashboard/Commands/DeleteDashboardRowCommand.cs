﻿using MediatR;

namespace Infrastructure.Mediator.Dashboard.Commands
{
    public class DeleteDashboardRowCommand : IRequest<Domain.Dashboard.DashboardRow>
    {
        public string RowId { get; set; }
    }
}
