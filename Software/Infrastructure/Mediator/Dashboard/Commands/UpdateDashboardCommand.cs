﻿using Domain.Dashboard;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Commands
{
    public class UpdateDashboardCommand : IRequest<Domain.Dashboard.Dashboard>
    {
        public Domain.Dashboard.Dashboard DashboardToBeUpdated { get; set; }

        public UpdateDashboardCommand(Domain.Dashboard.Dashboard obj)
        {
            this.DashboardToBeUpdated = obj;
        }

    }
}
