﻿using Domain.Dashboard;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Dashboard.Commands
{
    public class CreateDashboardCommand : IRequest<Domain.Dashboard.Dashboard>
    {
        public Domain.Dashboard.Dashboard DashboardToBeCreated { get; set; }

        public CreateDashboardCommand(Domain.Dashboard.Dashboard obj)
        {
            this.DashboardToBeCreated = obj;
        }

    }
}
