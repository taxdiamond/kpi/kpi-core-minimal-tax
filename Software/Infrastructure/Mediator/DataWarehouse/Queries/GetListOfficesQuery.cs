﻿using Domain.Office;
using MediatR;
using System.Collections.Generic;

namespace Infrastructure.Mediator.DataWarehouse.Queries
{
    public class GetListOfficesQuery : IRequest<List<Office>>
    {
        public string Query { get; set; }
    }
}
