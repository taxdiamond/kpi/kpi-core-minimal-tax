﻿using Domain.Segment;
using MediatR;
using System.Collections.Generic;

namespace Infrastructure.Mediator.DataWarehouse.Queries
{
    public class GetListSegmentsQuery : IRequest<List<Segment>>
    {
        public string Query { get; set; }
    }
}
