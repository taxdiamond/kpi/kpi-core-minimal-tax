﻿using Domain.Region;
using MediatR;
using System.Collections.Generic;

namespace Infrastructure.Mediator.DataWarehouse.Queries
{
    public class GetListRegionsQuery : IRequest<List<Region>>
    {
        public string Query { get; set; }
    }
}
