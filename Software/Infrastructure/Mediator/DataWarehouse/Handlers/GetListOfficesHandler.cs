﻿using Domain.Office;
using Domain.Repository;
using Infrastructure.Mediator.DataWarehouse.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.DataWarehouse.Handlers
{
    public class GetListOfficesHandler : IRequestHandler<GetListOfficesQuery, List<Office>>
    {
        private readonly IOfficeRepository _repository;

        public GetListOfficesHandler(IOfficeRepository repository)
        {
            _repository = repository;
        }
        public Task<List<Office>> Handle(GetListOfficesQuery request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                return _repository.FindAll(request.Query);
            });
        }
    }
}
