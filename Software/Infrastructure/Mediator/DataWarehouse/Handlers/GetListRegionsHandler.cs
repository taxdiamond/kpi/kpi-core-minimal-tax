﻿using Domain.Region;
using Domain.Repository;
using Infrastructure.Mediator.DataWarehouse.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.DataWarehouse.Handlers
{
    public class GetListRegionsHandler : IRequestHandler<GetListRegionsQuery, List<Region>>
    {
        private readonly IRegionRepository _repository;

        public GetListRegionsHandler(IRegionRepository repository)
        {
            _repository = repository;
        }
        public Task<List<Region>> Handle(GetListRegionsQuery request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                return _repository.FindAll(request.Query);
            });
        }
    }
}
