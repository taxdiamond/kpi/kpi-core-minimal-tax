﻿using Domain.Segment;
using Domain.Repository;
using Infrastructure.Mediator.DataWarehouse.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.DataWarehouse.Handlers
{
    public class GetListSegmentsHandler : IRequestHandler<GetListSegmentsQuery, List<Segment>>
    {
        private readonly ISegmentRepository _repository;

        public GetListSegmentsHandler(ISegmentRepository repository)
        {
            _repository = repository;
        }
        public Task<List<Segment>> Handle(GetListSegmentsQuery request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                return _repository.FindAll(request.Query);
            });
        }
    }
}
