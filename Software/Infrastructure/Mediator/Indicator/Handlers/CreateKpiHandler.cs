﻿using Domain.Repository;
using Framework.Service;
using Infrastructure.Mediator.Indicator.Commands;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Indicator.Handlers
{
    public class CreateKpiHandler : IRequestHandler<CreateKpiCommand, Domain.Indicators.Kpi>
    {
        private readonly IKpiRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateKpiHandler(IKpiRepository repository,
            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }
        public async Task<Domain.Indicators.Kpi> Handle(CreateKpiCommand request, CancellationToken cancellationToken)
        {
            Domain.Indicators.Kpi entity = request.NewKpi;
            entity.KpiId = Guid.NewGuid();
            entity.DashboardRef = request.DashboardRef;
            entity.RowRef = request.RowRef;

            await _repository.Add(entity);
            await _unitOfWork.Commit();
            return entity;
        }
    }
}
