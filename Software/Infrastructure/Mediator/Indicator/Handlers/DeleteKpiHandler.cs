﻿using Domain.Repository;
using Domain.Utils;
using Framework.Service;
using Infrastructure.Mediator.Indicator.Commands;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Indicator.Handlers
{
    public class DeleteKpiHandler : IRequestHandler<DeleteKpiCommand, VoidResult>
    {
        private readonly IKpiUserRepository _kpiUserRepository;
        private readonly IKpiRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        public DeleteKpiHandler(IKpiRepository repository,
            IKpiUserRepository kpiUserRepository,
            IUnitOfWork unitOfWork)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _kpiUserRepository = kpiUserRepository;
        }
        public async Task<VoidResult> Handle(DeleteKpiCommand request, CancellationToken cancellationToken)
        {
            _kpiUserRepository.RemoveByKpi(request.KpiId);

            List<Domain.Indicators.Kpi> kpis = _repository.FindByRow(request.RowId);
            Domain.Indicators.Kpi toDelete = null;
            int maxCellNumber = kpis.Select(o => o.CellNumber).Max();
            int startNumber = 1;
            foreach(Domain.Indicators.Kpi obj in kpis)
            {
                if (obj.KpiId == request.KpiId)
                {
                    toDelete = obj;
                    startNumber = obj.CellNumber;
                }
            }

            foreach (Domain.Indicators.Kpi obj in kpis)
            {
                if (obj.CellNumber > startNumber)
                    obj.CellNumber = obj.CellNumber - 1;
            }

            toDelete.CellNumber = maxCellNumber;

            await _repository.Delete(request.KpiId, toDelete);
            await _unitOfWork.Commit();
            return new VoidResult();
        }
    }
}
