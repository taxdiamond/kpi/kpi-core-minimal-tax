﻿using Domain.Repository;
using Infrastructure.Mediator.Indicator.Queries;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Dashboard.Handlers
{
    public class GetKpiHandler : IRequestHandler<GetKpiQuery, Domain.Indicators.Kpi>
    {
        private readonly IKpiRepository _kpiRepository;

        public GetKpiHandler(IKpiRepository kpiRepository)
        {
            _kpiRepository = kpiRepository;
        }

        public async Task<Domain.Indicators.Kpi> Handle(GetKpiQuery request, CancellationToken cancellationToken)
        {
            return await _kpiRepository.Load(request.KpiId, request.NoTracking);
        }
    }
}
