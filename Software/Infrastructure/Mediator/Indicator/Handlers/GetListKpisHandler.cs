﻿using Domain.Repository;
using Infrastructure.Mediator.Indicator.Queries;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Indicator.Handlers
{
    public class GetListKpisHandler : IRequestHandler<GetListKpisQuery, List<Domain.Indicators.Kpi>>
    {
        private readonly IKpiRepository _repository;

        public GetListKpisHandler(IKpiRepository repository)
        {
            _repository = repository;
        }

        public Task<List<Domain.Indicators.Kpi>> Handle(
            GetListKpisQuery request, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                return _repository.FindByRow(System.Guid.Parse(request.DashboardRowId));
            });
        }

    }
}
