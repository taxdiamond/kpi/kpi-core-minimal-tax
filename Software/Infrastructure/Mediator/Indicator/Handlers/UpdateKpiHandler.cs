﻿using Domain.Repository;
using Framework.Service;
using Infrastructure.Mediator.Indicator.Commands;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Indicator.Handlers
{
    public class UpdateKpiHandler : IRequestHandler<UpdateKpiCommand, Domain.Indicators.Kpi>
    {
        private IKpiRepository _repository;
        private IUnitOfWork _unitOfWork;

        public UpdateKpiHandler(IKpiRepository repository, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        public async Task<Domain.Indicators.Kpi> Handle(UpdateKpiCommand request, CancellationToken cancellationToken)
        {
            _repository.Update(request.KpiToBeUpdated);
            await _unitOfWork.Commit();

            return request.KpiToBeUpdated;
        }
    }
}
