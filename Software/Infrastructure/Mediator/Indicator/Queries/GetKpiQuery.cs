﻿using MediatR;

namespace Infrastructure.Mediator.Indicator.Queries
{
    public class GetKpiQuery : IRequest<Domain.Indicators.Kpi>
    {
        public string KpiId { get; set; }
        public bool NoTracking { get; set; }
    }
}
