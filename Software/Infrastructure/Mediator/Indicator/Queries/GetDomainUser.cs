﻿using Domain.Indicators;
using MediatR;

namespace Infrastructure.Mediator.Indicator.Queries
{
    public class GetDomainUser : IRequest<DomainUser>
    {
        public string  UserId { get; set; }
    }
}
