﻿using MediatR;

namespace Infrastructure.Mediator.Indicator.Commands
{
    public class CreateKpiCommand : IRequest<Domain.Indicators.Kpi>
    {
        public Domain.Indicators.Kpi NewKpi { get; set; }
        public Domain.Dashboard.Dashboard DashboardRef { get; set; }
        public Domain.Dashboard.DashboardRow RowRef { get; set; }
    }
}
