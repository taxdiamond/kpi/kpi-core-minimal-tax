﻿using MediatR;

namespace Infrastructure.Mediator.Indicator.Commands
{
    public class UpdateKpiCommand : IRequest<Domain.Indicators.Kpi>
    {
        public Domain.Indicators.Kpi KpiToBeUpdated { get; set; }

        public UpdateKpiCommand(Domain.Indicators.Kpi obj)
        {
            this.KpiToBeUpdated = obj;
        }

    }
}
