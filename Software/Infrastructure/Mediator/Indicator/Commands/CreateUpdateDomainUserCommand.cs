﻿using Domain.Indicators;
using Domain.Security;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mediator.Indicator.Commands
{
    public class CreateUpdateDomainUserCommand : IRequest<DomainUser>
    {
        public string Domain { get; set; }
        public string UserId { get; set; }
    }
}
