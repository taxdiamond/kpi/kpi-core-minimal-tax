﻿using Infrastructure.ViewModels.Dashboard;
using KPIClassLibrary.KPI;
using MediatR;

namespace Infrastructure.Mediator.Catalog.Queries
{
    public class GetListCatalogDimensionsQuery : IRequest<DimensionListViewModel>
    {
        public string MetaDataPath { get; set; }
        public string GroupsDataPath { get; set; }
        public KPIDomain Domain { get; set; }
        public KPIType Type { get; set; }
    }
}
