﻿using Infrastructure.ViewModels.KPI;
using KPIClassLibrary.KPI;
using MediatR;

namespace Infrastructure.Mediator.Catalog.Queries
{
    public class GetListKpiCatalogAreaQuery : IRequest<KpiCatalogAreaListViewModel>
    {
        public string MetaDataPath { get; set; }
        public string GroupsDataPath { get; set; }
        public KPIDomain Domain { get; set; }
        public KPIType Type { get; set; }
        public string Dimension { get; set; }
    }
}
