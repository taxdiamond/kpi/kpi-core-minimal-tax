﻿using Infrastructure.Mediator.Catalog.Queries;
using Infrastructure.ViewModels.KPI;
using KPIClassLibrary.KPI;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Mediator.Catalog.Handlers
{
    public class GetListKpiCatalogAreaHandler : IRequestHandler<GetListKpiCatalogAreaQuery, KpiCatalogAreaListViewModel>
    {
        private readonly ILogger<GetListKpiCatalogAreaHandler> _logger;
        public string groupsDataPath { get; set; }
        public string metaDataPath { get; set; }

        public GetListKpiCatalogAreaHandler(ILogger<GetListKpiCatalogAreaHandler> logger)
        {
            _logger = logger;
        }

        public async Task<KpiCatalogAreaListViewModel> Handle(GetListKpiCatalogAreaQuery request, CancellationToken cancellationToken)
        {
            return await Task.Run(() =>
            {
                this.groupsDataPath = request.GroupsDataPath;
                this.metaDataPath = request.MetaDataPath;

                KPIClassLibrary.KPI.KPICatalog catalog = new KPIClassLibrary.KPI.KPICatalog(groupsDataPath, metaDataPath);

                List<KPICatalogArea> items = catalog.KPIAreas
                        // TODO-IK:  Review this change
                        //.Where(x => x.Type == request.Type &&
                        .Where(x => x.Type.Contains(request.Type) &&
                            x.Dimension.Equals(request.Dimension) &&
                            x.Domain == request.Domain)
                        .ToList();

                KpiCatalogAreaListViewModel model = KpiCatalogAreaListViewModel.FromList(items);
                // Now we load all the KPI base library characteristics
                foreach (KpiCatalogAreaViewModel area in model.KpiCatalogAreas)
                {
                    foreach (KpiCatalogItemViewModel kpiModel in area.kpis)
                    {
                        _logger.LogDebug("Instantiates class " + kpiModel.classname);
                        ObjectHandle handle = Activator.CreateInstance("KPIClassLibrary", "KPIClassLibrary.KPI." +
                            kpiModel.classname);
                        KPIClassLibrary.KPI.KPI kpiInstance = (KPIClassLibrary.KPI.KPI)handle.Unwrap();

                        if (kpiInstance != null)
                        {
                            kpiModel.Filters = kpiInstance.GetAllowedFilters();
                        }
                    }
                }

                return model;
            });
        }
    }
}
