﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class SatisfactionSurveyEntityTypeConfiguration : IEntityTypeConfiguration<SatisfactionSurvey>
    {
        public void Configure(EntityTypeBuilder<SatisfactionSurvey> builder)
        {
            builder.Property(x => x.SatisfactionSurveyItemID)
                .HasColumnName("satisfactionSurveyItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.SatisfactionSurveyItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.SurveyItemReportDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("surveyItemReportDate");
            // region / office already there
            builder.OwnsOne(x => x.SurveyArea)
                .Property(y => y.Value)
                .HasColumnName("surveyArea")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.SurveyAreaScore)
                .Property(y => y.Value)
                .HasColumnName("surveyAreaScore")
                .HasColumnType("decimal(18,3)");
        }
    }
}
