﻿using Domain.Dashboard;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class DashboardRowEntityTypeConfiguration : IEntityTypeConfiguration<DashboardRow>
    {
        public void Configure(EntityTypeBuilder<DashboardRow> builder)
        {
            builder.HasKey(x => x.DashboardRowId);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.RowNumber)
                .Property(y => y.Value)
                .HasColumnName("RowNumber");
        }
    }
}
