﻿using Domain.Segment;
using Domain.ValueObjects.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityConfiguration
{
    class SegmentEntityTypeConfiguration : IEntityTypeConfiguration<Segment>
    {
        public void Configure(EntityTypeBuilder<Segment> builder)
        {
            builder.Property(x => x.segmentID).HasConversion(y => y.Value, v => Id20.FromString(v)).HasMaxLength(20);
            builder.HasKey(x => x.segmentID).HasName("segmentID");
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.segmentName)
                .Property(y => y.Value)
                .HasColumnName("segmentName")
                .HasMaxLength(250);
        }
    }
}
