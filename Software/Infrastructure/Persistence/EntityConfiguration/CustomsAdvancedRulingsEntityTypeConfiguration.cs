﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsAdvancedRulingsEntityTypeConfiguration : IEntityTypeConfiguration<CustomsAdvancedRulings>
    {
        public void Configure(EntityTypeBuilder<CustomsAdvancedRulings> builder)
        {
            builder.Property(x => x.CustomsAdvancedRulingsItemID)
                .HasColumnName("customsAdvancedRulingsItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsAdvancedRulingsItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsAdvancedRulingsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsAdvancedRulingsDate");
            builder.OwnsOne(x => x.NumberRulingRequestsOrigin)
                .Property(y => y.Value)
                .HasColumnName("numberRulingRequestsOrigin");
            builder.OwnsOne(x => x.NumberRulingRequestsTariff)
                .Property(y => y.Value)
                .HasColumnName("numberRulingRequestsTariff");
            builder.OwnsOne(x => x.NumberRulingRequests)
                .Property(y => y.Value)
                .HasColumnName("numberRulingRequests");
        }
    }
}
