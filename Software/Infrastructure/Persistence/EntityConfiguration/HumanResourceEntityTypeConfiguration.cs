﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class HumanResourceEntityTypeConfiguration : IEntityTypeConfiguration<HumanResource>
    {
        public void Configure(EntityTypeBuilder<HumanResource> builder)
        {
            builder.Property(x => x.HRItemID)
                .HasColumnName("hrItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.HRItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.HRItemReportDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("hrItemReportDate");

            builder.OwnsOne(x => x.TotalHumanResources)
                .Property(y => y.Value)
                .HasColumnName("totalHumanResources");
            builder.OwnsOne(x => x.TotalHumanResourcesInAppeals)
                .Property(y => y.Value)
                .HasColumnName("totalHumanResourcesInAppeals");
            builder.OwnsOne(x => x.TotalHumanResourcesInAudits)
                .Property(y => y.Value)
                .HasColumnName("totalHumanResourcesInAudits");
            builder.OwnsOne(x => x.TotalHumanResourcesInTaxCollection)
                .Property(y => y.Value)
                .HasColumnName("totalHumanResourcesInTaxCollection");
            builder.OwnsOne(x => x.TotalHumanResourcesInTaxEnforcement)
                .Property(y => y.Value)
                .HasColumnName("totalHumanResourcesInTaxEnforcement");
            builder.OwnsOne(x => x.TotalHumanResourcesInTaxPayerServices)
                .Property(y => y.Value)
                .HasColumnName("totalHumanResourcesInTaxPayerServices");
            builder.OwnsOne(x => x.NumberPositionsFilledCompetitiveProcess)
                .Property(y => y.Value)
                .HasColumnName("numberPositionsFilledCompetitiveProcess");
            
        }
    }
}
