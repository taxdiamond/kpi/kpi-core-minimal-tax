﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    class ImportEntityTypeConfiguration : IEntityTypeConfiguration<Import>
    {
        public void Configure(EntityTypeBuilder<Import> builder)
        {
            builder.Property(x => x.ImportID)
                .HasColumnName("ImportID")
                .UseIdentityColumn();
            builder.HasKey(x => x.ImportID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.ImportDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("importDate");
            builder.OwnsOne(x => x.TotalImports)
                .Property(y => y.Value)
                .HasColumnName("totalImports")
                .HasColumnType("money");
        }
    }
}
