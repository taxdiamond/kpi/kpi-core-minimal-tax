﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsTradePartnerStatsEntityTypeConfiguration : IEntityTypeConfiguration<CustomsTradePartnerStats>
    {
        public void Configure(EntityTypeBuilder<CustomsTradePartnerStats> builder)
        {
            builder.Property(x => x.TradePartnerStatsItemID)
                .HasColumnName("tradePartnerStatsItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.TradePartnerStatsItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.TradePartnerStatsItemDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("tradePartnerStatsItemDate");
            builder.OwnsOne(x => x.NumberOfRegisteredImporters)
                .Property(y => y.Value)
                .HasColumnName("numberOfRegisteredImporters");
            builder.OwnsOne(x => x.NumberOfRegisteredExporters)
                .Property(y => y.Value)
                .HasColumnName("numberOfRegisteredExporters");
            builder.OwnsOne(x => x.NumberOfImporters70PercentRevenue)
                .Property(y => y.Value)
                .HasColumnName("numberOfImporters70PercentRevenue");
            builder.OwnsOne(x => x.NumberOfRegisteredBrokers)
                .Property(y => y.Value)
                .HasColumnName("numberOfRegisteredBrokers");
            builder.OwnsOne(x => x.NumberOfRegisteredCarriers)
                .Property(y => y.Value)
                .HasColumnName("numberOfRegisteredCarriers");
            builder.OwnsOne(x => x.NumberOfRegisteredWarehouses)
                .Property(y => y.Value)
                .HasColumnName("numberOfRegisteredWarehouses");
            builder.OwnsOne(x => x.CancelledLicences)
                .Property(y => y.Value)
                .HasColumnName("cancelledLicences");
        }
    }
}
