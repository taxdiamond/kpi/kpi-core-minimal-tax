﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsSeizuresEntityTypeConfigurationCustomsSeizures : IEntityTypeConfiguration<CustomsSeizures>
    {
        public void Configure(EntityTypeBuilder<CustomsSeizures> builder)
        {
            builder.Property(x => x.CustomsSeizuresItemID)
                .HasColumnName("customsSeizuresItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsSeizuresItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsSeizuresDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsSeizuresDate");
            builder.OwnsOne(x => x.SeizureType)
                .Property(y => y.Value)
                .HasColumnName("seizureType")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.SeizureCategory)
                .Property(y => y.Value)
                .HasColumnName("seizureCategory")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.SeizureTransportMode)
                .Property(y => y.Value)
                .HasColumnName("seizureTransportMode")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.SeizureMode)
                .Property(y => y.Value)
                .HasColumnName("seizureMode")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.SeizureValue)
                .Property(y => y.Value)
                .HasColumnName("seizureValue")
                .HasColumnType("money");
        }
    }
}
