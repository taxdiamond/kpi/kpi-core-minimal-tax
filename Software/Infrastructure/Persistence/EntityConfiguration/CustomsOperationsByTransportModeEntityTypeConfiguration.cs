﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsOperationsByTransportModeEntityTypeConfiguration : IEntityTypeConfiguration<CustomsOperationsByTransportMode>
    {
        public void Configure(EntityTypeBuilder<CustomsOperationsByTransportMode> builder)
        {
            builder.Property(x => x.CustomsOperationID)
                .HasColumnName("customsOperationsItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsOperationID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsOperationDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsOperationsDate");
            builder.OwnsOne(x => x.ModeOfTransport)
                .Property(y => y.Value)
                .HasColumnName("modeOfTransport")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.TotalImportsAmount)
                .Property(y => y.Value)
                .HasColumnName("totalImportsAmount")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalExportsAmount)
                .Property(y => y.Value)
                .HasColumnName("totalExportsAmount")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalTemporaryImportsAmount)
                .Property(y => y.Value)
                .HasColumnName("totalTemporaryImportsAmount");
            builder.OwnsOne(x => x.FreeTradeZoneOperations)
                .Property(y => y.Value)
                .HasColumnName("freeTradeZoneOperations");
            builder.OwnsOne(x => x.PassengersProcessed)
                .Property(y => y.Value)
                .HasColumnName("passengersProcessed");
            builder.OwnsOne(x => x.ClearanceTimeImports)
                .Property(y => y.Value)
                .HasColumnName("clearanceTimeImports")
                .HasColumnType("decimal(18,3)");
            builder.OwnsOne(x => x.ClearanceTimeImportsRedLane)
                .Property(y => y.Value)
                .HasColumnName("clearanceTimeImportsRedLane")
                .HasColumnType("decimal(18,3)");
            builder.OwnsOne(x => x.ClearanceTimeImportsYellowLane)
                .Property(y => y.Value)
                .HasColumnName("clearanceTimeImportsYellowLane")
                .HasColumnType("decimal(18,3)");
            builder.OwnsOne(x => x.ClearanceTimeImportsGreenLane)
                .Property(y => y.Value)
                .HasColumnName("clearanceTimeImportsGreenLane")
                .HasColumnType("decimal(18,3)");
            builder.OwnsOne(x => x.TotalCarriers)
                .Property(y => y.Value)
                .HasColumnName("totalCarriers");
            builder.OwnsOne(x => x.TotalCarriersLodingAdvancedElectronicInfo)
                .Property(y => y.Value)
                .HasColumnName("totalCarriersLodingAdvancedElectronicInfo");
        }
    }
}
