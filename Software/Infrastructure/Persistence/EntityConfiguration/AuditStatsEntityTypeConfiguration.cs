﻿using Domain.AuditStats;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class AuditStatsEntityTypeConfiguration : IEntityTypeConfiguration<AuditStats>
    {
        public void Configure(EntityTypeBuilder<AuditStats> builder)
        {
            builder.Property(x => x.AuditDataID)
                .HasColumnName("auditDataID")
                .UseIdentityColumn();
            builder.HasKey(x => x.AuditDataID);
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.StatsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("statsDate");
            builder.OwnsOne(x => x.NumberOfAuditsPerformed)
                .Property(y => y.Value)
                .HasColumnName("numberOfAuditsPerformed");
            builder.OwnsOne(x => x.NumberOfAuditsResultedInAdditionalAssessments)
                .Property(y => y.Value)
                .HasColumnName("numberOfAuditsResultedInAdditionalAssessments");
            builder.Property(x => x.TotalAdditionalAssessment)
                .HasColumnType("money")
                .HasColumnName("totalAdditionalAssessment");
        }
    }
}
