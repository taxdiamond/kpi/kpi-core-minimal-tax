﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsAdministrationExpensesEntityTypeConfiguration : IEntityTypeConfiguration<CustomsAdministrationExpenses>
    {
        public void Configure(EntityTypeBuilder<CustomsAdministrationExpenses> builder)
        {
            builder.Property(x => x.CustomsAdministrationExpensesItemID)
                .HasColumnName("customsAdministrationExpensesItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsAdministrationExpensesItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsAdministrationExpensesDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsAdministrationExpensesDate");
            builder.OwnsOne(x => x.CurrentExpense)
                .Property(y => y.Value)
                .HasColumnName("currentExpense")
                .HasColumnType("money");
        }
    }
}
