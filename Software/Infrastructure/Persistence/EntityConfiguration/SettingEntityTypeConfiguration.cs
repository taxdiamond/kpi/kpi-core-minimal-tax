﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class SettingEntityTypeConfiguration : IEntityTypeConfiguration<Setting>
    {
        public void Configure(EntityTypeBuilder<Setting> builder)
        {
            builder.Property(x => x.SettingID).UseIdentityColumn();
            builder.HasKey(x => x.SettingID).HasName("settingID");
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.ValueAddedTaxName)
                .Property(y => y.Value)
                .HasColumnName("valueAddedTaxName")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.CorporateIncomeTaxName)
                .Property(y => y.Value)                
                .HasColumnName("corporateIncomeTaxname")
                .HasMaxLength(250);

        }
    }
}
