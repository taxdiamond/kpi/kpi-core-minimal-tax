﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class DashboardEntityTypeConfiguration : IEntityTypeConfiguration<Domain.Dashboard.Dashboard>
    {
        public void Configure(EntityTypeBuilder<Domain.Dashboard.Dashboard> builder)
        {
            builder.HasKey(x => x.DashboardId);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.Name)
                .Property(y => y.Value)
                .HasColumnName("Name")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.Domain)
                .Property(y => y.Value)
                .HasColumnName("Domain")
                .HasMaxLength(50);
        }
    }
}
