﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsAppealsEntityTypeConfiguration : IEntityTypeConfiguration<CustomsAppeals>
    {
        public void Configure(EntityTypeBuilder<CustomsAppeals> builder)
        {
            builder.Property(x => x.CustomsAppealsItemID)
                .HasColumnName("customsAppealsItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsAppealsItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsAppealsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsAppealsDate");
            builder.OwnsOne(x => x.NumberAppealsConsidered)
                .Property(y => y.Value)
                .HasColumnName("numberAppealsConsidered");
            builder.OwnsOne(x => x.NumberAppealsResolved)
                .Property(y => y.Value)
                .HasColumnName("numberAppealsResolved");
            builder.OwnsOne(x => x.NumberAppealsUndergoLegalAction)
                .Property(y => y.Value)
                .HasColumnName("numberAppealsUndergoLegalAction");
            builder.OwnsOne(x => x.NumberJudicialDecissionsFavorableCustoms)
                .Property(y => y.Value)
                .HasColumnName("numberJudicialDecissionsFavorableCustoms");
            builder.OwnsOne(x => x.NumberLawsuitsFinished)
                .Property(y => y.Value)
                .HasColumnName("numberLawsuitsFinished");
            builder.OwnsOne(x => x.AverageTimeToResolveAppeals)
                .Property(y => y.Value)
                .HasColumnName("averageTimeToResolveAppeals")
                .HasColumnType("decimal(18,3)");
        }
    }
}
