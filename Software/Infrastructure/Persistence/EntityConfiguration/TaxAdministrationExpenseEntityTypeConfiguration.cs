﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class TaxAdministrationExpenseEntityTypeConfiguration : IEntityTypeConfiguration<TaxAdministrationExpense>
    {
        public void Configure(EntityTypeBuilder<TaxAdministrationExpense> builder)
        {
            builder.Property(x => x.TaxAdministrationExpenseItemID)
                .HasColumnName("adminExpensesItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.TaxAdministrationExpenseItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.TaxAdministrationExpenseDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("adminExpensesDate");
            builder.OwnsOne(x => x.CurrentExpense)
                .Property(y => y.Value)
                .HasColumnName("currentExpense")
                .HasColumnType("money");
        }
    }
}
