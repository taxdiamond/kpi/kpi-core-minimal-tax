﻿using Domain.CustomsPost;
using Domain.ValueObjects.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityConfiguration
{
    class CustomsPostEntityTypeConfiguration : IEntityTypeConfiguration<CustomsPost>
    {
        public void Configure(EntityTypeBuilder<CustomsPost> builder)
        {
            builder.Property(x => x.customsPostID)
                .HasConversion(y => y.Value, v => Id20.FromString(v))
                .HasMaxLength(20);
            builder.HasKey(x => x.customsPostID)
                .HasName("customsPostID");
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.customsPostName)
                .Property(y => y.Value)
                .HasColumnName("customsPostName")
                .HasMaxLength(250);
        }
    }
}
