﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class TaxPayerServiceEntityTypeConfiguration : IEntityTypeConfiguration<TaxPayerService>
    {
        public void Configure(EntityTypeBuilder<TaxPayerService> builder)
        {
            builder.Property(x => x.ServicesItemID)
                .HasColumnName("servicesItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.ServicesItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.ServicesItemReportDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("servicesItemReportDate");
            builder.OwnsOne(x => x.NumberOfServicesProvidedTaxpayers)
                .Property(y => y.Value)
                .HasColumnName("numberOfServicesProvidedTaxpayers");
            builder.OwnsOne(x => x.NumberOfElectronicServicesProvidedTaxpayers)
                .Property(y => y.Value)
                .HasColumnName("numberOfElectronicServicesProvidedTaxpayers");
        }
    }
}
