﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class RevenueSummaryByMonthEntityTypeConfiguration : IEntityTypeConfiguration<RevenueSummaryByMonth>
    {
        public void Configure(EntityTypeBuilder<RevenueSummaryByMonth> builder)
        {
            builder.Property(x => x.RevenueSummaryID)
                .HasColumnName("revenueSummaryID")
                .UseIdentityColumn();
            builder.HasKey(x => x.RevenueSummaryID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.RevenueDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("revenueDate");
            builder.OwnsOne(x => x.TotalRevenue)
                .Property(y => y.Value)
                .HasColumnName("totalRevenue")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalRevenueFromArrears)
                .Property(y => y.Value)
                .HasColumnName("totalRevenueFromArrears")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalRevenueFromAudits)
                .Property(y => y.Value)
                .HasColumnName("totalRevenueFromAudits")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalRevenueFromEnforcementCollection)
                .Property(y => y.Value)
                .HasColumnName("totalRevenueFromEnforcementCollection")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalRevenueFromFines)
                .Property(y => y.Value)
                .HasColumnName("totalRevenueFromFines")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalRevenueFromLitigation)
                .Property(y => y.Value)
                .HasColumnName("totalRevenueFromLitigation")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalRevenueFromTaxes)
                .Property(y => y.Value)
                .HasColumnName("totalRevenueFromTaxes")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalRevenueFromVoluntaryCompliance)
                .Property(y => y.Value)
                .HasColumnName("totalRevenueFromVoluntaryCompliance")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalRevenuePaidInTIme)
                .Property(y => y.Value)
                .HasColumnName("totalRevenuePaidInTIme")
                .HasColumnType("money");
        }
    }
}
