﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class RevenueEntityTypeConfiguration : IEntityTypeConfiguration<Revenue>
    {
        public void Configure(EntityTypeBuilder<Revenue> builder)
        {
            builder.Property(x => x.RevenueItemID)
                .HasColumnName("revenueItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.RevenueItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.RevenueDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("revenueDate");
            builder.OwnsOne(x => x.Concept)
                .Property(y => y.Value)
                .HasColumnName("concept")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.ConceptID)
                .Property(y => y.Value)
                .HasColumnName("conceptID")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.Amount)
                .Property(y => y.Value)
                .HasColumnName("amount")
                .HasColumnType("money");
        }
    }
}
