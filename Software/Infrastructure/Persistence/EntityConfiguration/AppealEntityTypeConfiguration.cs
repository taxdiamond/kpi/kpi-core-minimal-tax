﻿using Domain.Tax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class AppealEntityTypeConfiguration : IEntityTypeConfiguration<Appeal>
    {
        public void Configure(EntityTypeBuilder<Appeal> builder)
        {
            builder.Property(x => x.AppealsItemID)
                .HasColumnName("appealsItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.AppealsItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.AppealsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("appealsDate");
            builder.OwnsOne(x => x.Concept)
                .Property(y => y.Value)
                .HasColumnName("concept")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.ConceptID)
                .Property(y => y.Value)
                .HasColumnName("conceptID")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.AmountAppealed)
                .Property(y => y.Value)
                .HasColumnName("amountAppealed")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalAmountResolvedFavorTaxAdministration)
                .Property(y => y.Value)
                .HasColumnName("totalAmountResolvedFavorTaxAdministration")
                .HasColumnType("money");
            builder.OwnsOne(x => x.TotalAmountResolvedFavorTaxPayer)
                .Property(y => y.Value)
                .HasColumnName("totalAmountResolvedFavorTaxPayer")
                .HasColumnType("money");
            builder.OwnsOne(x => x.AverageDaysToResolveCases)
                .Property(y => y.Value)
                .HasColumnName("averageDaysToResolveCases");
            builder.OwnsOne(x => x.NumberAppealsCasesPending)
                .Property(y => y.Value)
                .HasColumnName("numberAppealsCasesPending");
            builder.OwnsOne(x => x.NumberCasesResolved)
                .Property(y => y.Value)
                .HasColumnName("numberCasesResolved");
            builder.OwnsOne(x => x.NumberCasesResolvedFavorTaxAdministration)
                .Property(y => y.Value)
                .HasColumnName("numberCasesResolvedFavorTaxAdministration");
            builder.OwnsOne(x => x.NumberCasesResolvedFavorTaxPayer)
                .Property(y => y.Value)
                .HasColumnName("numberCasesResolvedFavorTaxPayer");
            builder.OwnsOne(x => x.NumberOfAppealCasesSubmitted)
                .Property(y => y.Value)
                .HasColumnName("numberOfAppealCasesSubmitted");
            builder.OwnsOne(x => x.ValueOfAppealCasesSubmitted)
                .Property(y => y.Value)
                .HasColumnName("valueOfAppealCasesSubmitted");            
        }
    }
}
