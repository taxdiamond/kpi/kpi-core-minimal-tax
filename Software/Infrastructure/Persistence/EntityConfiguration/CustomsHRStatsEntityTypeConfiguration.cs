﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsHRStatsEntityTypeConfiguration : IEntityTypeConfiguration<CustomsHRStats>
    {
        public void Configure(EntityTypeBuilder<CustomsHRStats> builder)
        {
            builder.Property(x => x.CustomsHRStatsItemID)
                .HasColumnName("customsHRStatsItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsHRStatsItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsHRStatsItemDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsHRStatsItemDate");
            builder.OwnsOne(x => x.NumberFrontLineStaff)
                .Property(y => y.Value)
                .HasColumnName("numberFrontLineStaff");
            builder.OwnsOne(x => x.NumberSpecializedEmployees)
                .Property(y => y.Value)
                .HasColumnName("numberSpecializedEmployees");
            builder.OwnsOne(x => x.NumberSpecializedEmployeesTrained)
                .Property(y => y.Value)
                .HasColumnName("numberSpecializedEmployeesTrained");
        }
    }
}
