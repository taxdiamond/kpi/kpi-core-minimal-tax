﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsInspectionsEntityTypeConfiguration : IEntityTypeConfiguration<CustomsInspections>
    {
        public void Configure(EntityTypeBuilder<CustomsInspections> builder)
        {
            builder.Property(x => x.CustomsInspectionsItemID)
                .HasColumnName("customsInspectionsItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsInspectionsItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsInspectionsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsInspectionsDate");
            builder.OwnsOne(x => x.NumberDocumentaryInspections)
                .Property(y => y.Value)
                .HasColumnName("numberDocumentaryInspections");
            builder.OwnsOne(x => x.NumberPhysicalInspections)
                .Property(y => y.Value)
                .HasColumnName("numberPhysicalInspections");
            builder.OwnsOne(x => x.NumberRedFlaggedOperations)
                .Property(y => y.Value)
                .HasColumnName("numberRedFlaggedOperations");
            builder.OwnsOne(x => x.NumberYellowFlaggedOperations)
                .Property(y => y.Value)
                .HasColumnName("numberYellowFlaggedOperations");
            builder.OwnsOne(x => x.NumberOfDetections)
                .Property(y => y.Value)
                .HasColumnName("numberOfDetections");
            builder.OwnsOne(x => x.NumberOfDetectionsRedFlaggedOperations)
                .Property(y => y.Value)
                .HasColumnName("numberOfDetectionsRedFlaggedOperations");
            builder.OwnsOne(x => x.NumberOfDetectionsYellowFlaggedOperations)
                .Property(y => y.Value)
                .HasColumnName("numberOfDetectionsYellowFlaggedOperations");
            builder.OwnsOne(x => x.NumberFalsePositivesForAllInspections)
                .Property(y => y.Value)
                .HasColumnName("numberFalsePositivesForAllInspections");
            builder.OwnsOne(x => x.NumberTrueNegativesForAllInspections)
                .Property(y => y.Value)
                .HasColumnName("numberTrueNegativesForAllInspections");
            builder.OwnsOne(x => x.AverageDocumentaryInspectionsByComplianceOfficer)
                .Property(y => y.Value)
                .HasColumnName("averageDocumentaryInspectionsByComplianceOfficer");
            builder.OwnsOne(x => x.AveragePhysicalInspectionsByComplianceOfficer)
                .Property(y => y.Value)
                .HasColumnName("averagePhysicalInspectionsByComplianceOfficer");
            builder.OwnsOne(x => x.DocumentaryExaminationsBasedRiskSelectivity)
                .Property(y => y.Value)
                .HasColumnName("documentaryExaminationsBasedRiskSelectivity");
            builder.OwnsOne(x => x.PhysicalExaminationsBasedRiskSelectivity)
                .Property(y => y.Value)
                .HasColumnName("physicalExaminationsBasedRiskSelectivity");
            builder.OwnsOne(x => x.DocumentaryExaminationsBasedReferralsRiskAnalysisUnit)
                .Property(y => y.Value)
                .HasColumnName("documentaryExaminationsBasedReferralsRiskAnalysisUnit");
            builder.OwnsOne(x => x.PhysicalExaminationsBasedReferralsRiskAnalysisUnit)
                .Property(y => y.Value)
                .HasColumnName("physicalExaminationsBasedReferralsRiskAnalysisUnit");
            builder.OwnsOne(x => x.DocumentaryExaminationsBasedTipOffs)
                .Property(y => y.Value)
                .HasColumnName("documentaryExaminationsBasedTipOffs");
            builder.OwnsOne(x => x.PhysicalExaminationsBasedTipOffs)
                .Property(y => y.Value)
                .HasColumnName("physicalExaminationsBasedTipOffs");
            builder.OwnsOne(x => x.AlertsSentFromRiskAnalysisUnit)
                .Property(y => y.Value)
                .HasColumnName("alertsSentFromRiskAnalysisUnit");
            builder.OwnsOne(x => x.CustomsValueAlerts)
                .Property(y => y.Value)
                .HasColumnName("customsValueAlerts");
            builder.OwnsOne(x => x.ContrabandAlerts)
                .Property(y => y.Value)
                .HasColumnName("contrabandAlerts");
            builder.OwnsOne(x => x.IPRAlerts)
                .Property(y => y.Value)
                .HasColumnName("IPRAlerts");
            builder.OwnsOne(x => x.NarcoticsAlerts)
                .Property(y => y.Value)
                .HasColumnName("narcoticsAlerts");
            builder.OwnsOne(x => x.WeaponsAlerts)
                .Property(y => y.Value)
                .HasColumnName("weaponsAlerts");
            builder.OwnsOne(x => x.CashAlerts)
                .Property(y => y.Value)
                .HasColumnName("cashAlerts");
            builder.OwnsOne(x => x.NumberOfDetectionsWithValueGreaterThanX)
                .Property(y => y.Value)
                .HasColumnName("numberOfDetectionsWithValueGreaterThanX");
            builder.OwnsOne(x => x.NumberCustomsValueDetections)
                .Property(y => y.Value)
                .HasColumnName("numberCustomsValueDetections");
            builder.OwnsOne(x => x.NumberMisclassificationDetections)
                .Property(y => y.Value)
                .HasColumnName("numberMisclassificationDetections");
            builder.OwnsOne(x => x.NumberIncorrectOriginDetections)
                .Property(y => y.Value)
                .HasColumnName("numberIncorrectOriginDetections");
            builder.OwnsOne(x => x.NumberBorderSecurityDetections)
                .Property(y => y.Value)
                .HasColumnName("numberBorderSecurityDetections");
            builder.OwnsOne(x => x.NumberRegulatoryNonComplianceDetections)
                .Property(y => y.Value)
                .HasColumnName("numberRegulatoryNonComplianceDetections");
            builder.OwnsOne(x => x.NumberIPRDetections)
                .Property(y => y.Value)
                .HasColumnName("numberIPRDetections");
            builder.OwnsOne(x => x.NumberNonInstrusiveInspectionsDetections)
                .Property(y => y.Value)
                .HasColumnName("numberNonInstrusiveInspectionsDetections");
        }
    }
}
