﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class KpiEntityTypeConfiguration : IEntityTypeConfiguration<Domain.Indicators.Kpi>
    {
        public void Configure(EntityTypeBuilder<Domain.Indicators.Kpi> builder)
        {
            builder.HasKey(x => x.KpiId);
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.Title)
                .Property(y => y.Value)
                .HasColumnName("Title")
                .HasMaxLength(250);
            builder.Property(x => x.TimeStamp)
                .HasColumnName("Timestamp")
                .IsRowVersion();
            builder.OwnsOne(x => x.CellNumber)
                .Property(y => y.Value)
                .HasColumnName("CellNumber");

            builder.OwnsOne(x => x.PeriodYear)
                .Property(y => y.Value)
                .HasColumnName("PeriodYear").HasColumnType("int");
                
            builder.OwnsOne(x => x.DivWidth)
                .Property(y => y.Value)
                .HasColumnName("DivWidth").HasColumnType("int");
            builder.OwnsOne(x => x.DivHeight)
                .Property(y => y.Value)
                .HasColumnName("DivHeight").HasColumnType("int");

            builder.OwnsOne(x => x.NumberOfYears)
                .Property(y => y.Value)
                .HasColumnName("NumberOfYears").HasColumnType("int");


        }
    }
}
