﻿using Domain.Indicators;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class DomainUserEntityTypeConfiguration : IEntityTypeConfiguration<DomainUser>
    {
        public void Configure(EntityTypeBuilder<DomainUser> builder)
        {
            builder.HasKey(x => x.DomainUserId);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);

            builder.OwnsOne(x => x.Domain)
                .Property(y => y.Value)
                .HasColumnName("Domain")
                .HasMaxLength(100);
        }
    }
}
