﻿using Domain.CountryStats;
using Domain.DoingBusinessStats;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class DoingBusinessTaxStatsEntityTypeConfiguration : IEntityTypeConfiguration<DoingBusinessTaxStats>
    {

        public void Configure(EntityTypeBuilder<DoingBusinessTaxStats> builder)
        {
            builder.Property(x => x.DoingBusinessStatsID)
                .HasColumnName("doingBusinessStatsID")
                .UseIdentityColumn();
            builder.HasKey(x => x.DoingBusinessStatsID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.StatsDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("statsDate");
            builder.Property(x => x.Time)
                .HasColumnType("decimal(18,3)")
                .HasColumnName("time");
        }
    }
}
