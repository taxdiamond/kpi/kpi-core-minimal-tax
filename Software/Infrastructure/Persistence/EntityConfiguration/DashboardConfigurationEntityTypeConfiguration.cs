﻿using Domain.Dashboard;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class DashboardConfigurationEntityTypeConfiguration : IEntityTypeConfiguration<DashboardConfiguration>
    {
        public void Configure(EntityTypeBuilder<DashboardConfiguration> builder)
        {
            builder.HasKey(x => x.DashboardConfigurationId);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
        }
    }
}
