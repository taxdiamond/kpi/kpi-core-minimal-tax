﻿using Domain.Tax;
using Domain.ValueObjects.Shared;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class TaxRateEntityTypeConfiguration : IEntityTypeConfiguration<TaxRate>
    {
        public void Configure(EntityTypeBuilder<TaxRate> builder)
        {
            builder.Property(x => x.TaxRateId).UseIdentityColumn();
            builder.HasKey(x => x.TaxRateId).HasName("taxRateID");
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.TaxName)
                .Property(y => y.Value)
                .HasColumnName("taxName")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.TaxRateDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("taxRateDate");
            builder.Property(x => x.Rate)
                .HasColumnType("decimal(18,3)")
                .HasColumnName("taxRate");
        }
    }
}
