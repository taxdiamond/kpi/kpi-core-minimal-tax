﻿using Domain.Customs;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Persistence.EntityConfiguration
{
    public class CustomsRevenueForecastEntityTypeConfiguration : IEntityTypeConfiguration<CustomsRevenueForecast>
    {
        public void Configure(EntityTypeBuilder<CustomsRevenueForecast> builder)
        {
            builder.Property(x => x.CustomsRevenueForecastItemID)
                .HasColumnName("customsRevenueForecastItemID")
                .UseIdentityColumn();
            builder.HasKey(x => x.CustomsRevenueForecastItemID);
            // This is very important to ignore the GUID of the value object
            builder.Ignore(x => x.Id);
            builder.OwnsOne(x => x.CustomsRevenueForecastDate)
                .Property(y => y.Value)
                .HasColumnType("date")
                .HasColumnName("customsRevenueForecastDate");
            builder.OwnsOne(x => x.Concept)
                .Property(y => y.Value)
                .HasColumnName("concept")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.ConceptID)
                .Property(y => y.Value)
                .HasColumnName("conceptID")
                .HasMaxLength(250);
            builder.OwnsOne(x => x.Amount)
                .Property(y => y.Value)
                .HasColumnName("amount")
                .HasColumnType("money");
        }
    }
}
