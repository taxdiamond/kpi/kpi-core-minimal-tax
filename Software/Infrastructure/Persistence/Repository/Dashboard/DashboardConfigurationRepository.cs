﻿using Domain.Dashboard;
using Domain.Repository;
using Domain.ValueObjects.Dashboard;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Dashboard
{
    public class DashboardConfigurationRepository : IDashboardConfigurationRepository
    {
        private readonly ApplicationDbContext _context;

        public DashboardConfigurationRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DashboardConfiguration> Create(DashboardConfiguration obj)
        {
            await _context.DashboardConfigurations.AddAsync(obj);
            return obj;
        }

        public Task<bool> Exists(DashboardConfigurationGuid id)
        {
            throw new NotImplementedException();
        }

        public List<DashboardConfiguration> FindAll(string query)
        {
            throw new NotImplementedException();
        }

        public async Task<DashboardConfiguration> FindById(DashboardConfigurationGuid id)
        {
            DashboardConfiguration obj =
                await _context.DashboardConfigurations
                    .Include(x => x.DashboardRef)
                    .SingleAsync(x => x.DashboardConfigurationId == id.Value);

            return obj;
        }
        public DashboardConfiguration FindByDashboardId(Guid id)
        {
            List<DashboardConfiguration> configurations = _context.DashboardConfigurations
                .Where(x => x.DashboardRef.DashboardId == id).ToList();

            if (configurations.Count > 0)
                return configurations.FirstOrDefault();

            return null;
        }

        public async Task Remove(DashboardConfigurationGuid id)
        {
            DashboardConfiguration cfg = await _context.DashboardConfigurations
                .SingleAsync(o => o.DashboardConfigurationId == id.Value);
            if (cfg == null)
                return;

            _context.DashboardConfigurations.Remove(cfg);
        }

        public DashboardConfiguration Update(DashboardConfiguration obj)
        {
            throw new NotImplementedException();
        }

        public void Remove(DashboardConfiguration obj)
        {
            _context.DashboardConfigurations.Remove(obj);
        }
    }
}
