﻿using Domain.Dashboard;
using Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata.Ecma335;

namespace Infrastructure.Persistence.Repository.Dashboard
{
    public class DashboardUserRepository : IDashboardUserRepository
    {
        private readonly ApplicationDbContext _context;

        public DashboardUserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DashboardUser> Create(DashboardUser obj)
        {
            await _context.DashboardUsers.AddAsync(obj);
            return obj;
        }

        public async Task<bool> Exists(Guid dashboardGuid, Guid userId)
        {
            List<DashboardUser> list = await
                _context.DashboardUsers.Include(o => o.SharedDashboardUser).Include(o => o.Dashboard)
                    .Where(o => o.Dashboard.DashboardId == dashboardGuid && o.SharedDashboardUser.Id == userId.ToString()).ToListAsync<DashboardUser>();
            return list != null && list.Count > 0;
        }

        public async Task<List<DashboardUser>> FindByDashboardId(Guid id)
        {
            return await
                _context.DashboardUsers.Include(o => o.SharedDashboardUser).Include(o => o.Dashboard)
                    .Where(o => o.Dashboard.DashboardId == id).ToListAsync<DashboardUser>();
        }

        public Task<DashboardUser> FindById(Guid id)
        {
            DashboardUser obj = _context.DashboardUsers.Include(o => o.SharedDashboardUser).Include(o => o.Dashboard)
                .SingleOrDefault(o => o.DashboardUserId == id);
            return new Task<DashboardUser>(() => obj);
        }

        public async Task<List<DashboardUser>> FindByUserId(Guid id)
        {
            return await
               _context.DashboardUsers.Include(o => o.SharedDashboardUser).Include(o => o.Dashboard)
                   .Where(o => o.SharedDashboardUser.Id == id.ToString()).ToListAsync<DashboardUser>();
        }

        public async Task Remove(Guid id)
        {
            DashboardUser obj = await _context.DashboardUsers.FindAsync(id);
            _context.Remove(obj);
        }

        public async Task Remove(Guid dashboardGuid, Guid userId)
        {
            DashboardUser obj = await
                _context.DashboardUsers.Include(o => o.SharedDashboardUser).Include(o => o.Dashboard)
                    .Where(o => o.Dashboard.DashboardId == dashboardGuid && o.SharedDashboardUser.Id == userId.ToString()).SingleOrDefaultAsync<DashboardUser>();
            
            if(obj != null)
                _context.Remove(obj);
        }
    }
}
