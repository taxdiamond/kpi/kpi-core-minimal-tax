﻿using Domain.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Dashboard
{
    public class DashboardRepository : IDashboardRepository
    {
        private readonly ApplicationDbContext _context;

        public DashboardRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Domain.Dashboard.Dashboard> Create(Domain.Dashboard.Dashboard obj)
        {
            await _context.Dashboards.AddAsync(obj);
            return obj;
        }

        public async Task<bool> Exists(Guid dashboardId)
        {
            Domain.Dashboard.Dashboard obj = await _context.Dashboards.FindAsync(dashboardId);
            if (obj == null)
                return false;
            return true;
        }

        public List<Domain.Dashboard.Dashboard> FindAll(string query)
        {
            return FindAll(query, KPIClassLibrary.KPI.KPIDomain.Tax.ToString());
        }
        public List<Domain.Dashboard.Dashboard> FindAll(string query, string domain)
        {
            if (string.IsNullOrWhiteSpace(domain))
                throw new ArgumentException("Domain must be non empty");

            if (string.IsNullOrWhiteSpace(query))
                return _context.Dashboards.Include(x => x.Creator)
                    .Where(o => o.Domain.Value == domain)
                    .OrderBy(o => o.Name).ToList<Domain.Dashboard.Dashboard>();
            else
                return _context.Dashboards.Include(x => x.Creator)
                    .Where(o => o.Name.Value.Contains(query) && o.Domain.Value == domain)
                    .OrderBy(o => o.Name).ToList<Domain.Dashboard.Dashboard>();
        }

        public List<Domain.Dashboard.Dashboard> FindAllForUserId(string userid, string search, string domain)
        {
            if (string.IsNullOrWhiteSpace(domain))
                throw new ArgumentException("Domain must be non empty");

            List< Domain.Dashboard.Dashboard> ownedDashboards = 
                _context.Dashboards.Include(x => x.Creator)
                    .Where(o => o.Creator.Id.Equals(userid) && o.Domain.Value == domain)
                    .OrderBy(o => o.Name).ToList<Domain.Dashboard.Dashboard>();

            var sharedDashboardsRaw = (from p in _context.Dashboards
                          join e in _context.DashboardUsers
                          on p.DashboardId equals e.Dashboard.DashboardId
                          where p.Creator.Id != userid && p.Domain.Value == domain
                          select new
                          {
                              Creator = p.Creator,
                              DashboardId = p.DashboardId,
                              Name = p.Name,
                              Status = p.Status,
                              Type = p.Type
                          }).AsNoTracking().ToList();

            List<Domain.Dashboard.Dashboard> sharedDashboards = new List<Domain.Dashboard.Dashboard>();
            foreach(var row in sharedDashboardsRaw)
            {
                Domain.Dashboard.Dashboard rowdb = new Domain.Dashboard.Dashboard();
                rowdb.Creator = row.Creator;
                rowdb.DashboardId = row.DashboardId;
                //rowdb.Id = row.Id;
                rowdb.Name = row.Name;
                rowdb.Status = row.Status;
                rowdb.Type = row.Type;
                sharedDashboards.Add(rowdb);
            }

            List<Domain.Dashboard.Dashboard> finalList = new List<Domain.Dashboard.Dashboard>();
            finalList.AddRange(ownedDashboards);
            finalList.AddRange(sharedDashboards);

            return finalList.OrderBy(x => x.Name.Value).ToList();
        }

        public async Task<Domain.Dashboard.Dashboard> FindById(Guid id)
        {
            Domain.Dashboard.Dashboard obj =  await _context.Dashboards.Include(x => x.Creator).FirstAsync( o => o.DashboardId == id);
            return obj;
        }

        public List<Domain.Dashboard.Dashboard> FindByUserId(string userid, string domain)
        {
            if (string.IsNullOrWhiteSpace(domain))
                throw new ArgumentException("Domain must be non empty");

            return _context.Dashboards.Include(x => x.Creator)
                .Where(o => o.Creator.Id == userid && o.Domain.Value == domain)
                .OrderBy(o => o.Name).ToList<Domain.Dashboard.Dashboard>();
        }

        public async Task Remove(Guid id)
        {
            Domain.Dashboard.Dashboard obj = await FindById(id);
            if (obj == null)
                return;
            _context.Dashboards.Remove(obj);
        }

        public void Remove(Domain.Dashboard.Dashboard obj)
        {
            _context.Dashboards.Remove(obj);
        }

        public Domain.Dashboard.Dashboard Update(Domain.Dashboard.Dashboard obj)
        {
            _context.Dashboards.Update(obj);
            return obj;
        }
    }
}
