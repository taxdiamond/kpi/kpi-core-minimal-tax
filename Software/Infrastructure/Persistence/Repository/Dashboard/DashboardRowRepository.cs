﻿using Domain.Dashboard;
using Domain.Indicators;
using Domain.Repository;
using Domain.ValueObjects.Dashboard;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Dashboard
{
    public class DashboardRowRepository : IDashboardRowRepository
    {

        private readonly ApplicationDbContext _context;

        public DashboardRowRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<DashboardRow> Create(DashboardRow obj)
        {
            await _context.AddAsync(obj);
            return obj;
        }

        public Task<bool> Exists(DashboardRowGuid id)
        {
            throw new NotImplementedException();
        }

        public List<DashboardRow> FindAll(string query)
        {
            throw new NotImplementedException();
        }

        public List<DashboardRow> FindByDashboard(Guid guid)
        {
            List<DashboardRow> rows =
                _context.DashboardRows.Where(x => x.ConfigurationRef.DashboardConfigurationId.Equals(guid))
                    .ToList();

            SortByRowNumber sorter = new SortByRowNumber();
            rows.Sort(sorter);

            return rows;
        }

        public async Task<DashboardRow> FindById(DashboardRowGuid id)
        {
            DashboardRow row = await _context.DashboardRows
                .Include(x => x.ConfigurationRef)
                .SingleAsync(o => o.DashboardRowId == id.Value);
            if (row != null)
                return row;

            return null;
        }

        public void Remove(DashboardRow obj)
        {
            _context.DashboardRows.Remove(obj);
        }

        public async Task Remove(DashboardRowGuid id)
        {
            DashboardRow row = await _context.DashboardRows
                .SingleAsync(o => o.DashboardRowId == id.Value);
            if (row == null)
                return;

            _context.DashboardRows.Remove(row);
        }

        public DashboardRow Update(DashboardRow obj)
        {
            _context.DashboardRows.Update(obj);
            return obj;
        }
    }
}
