﻿using Domain.DoingBusinessStats;
using Domain.Repository;
using Domain.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.DoingBusinessStats
{
    public class DoingBusinessTaxStatsRepository : IDoingBusinessTaxStatsRepository
    {
        private readonly ApplicationDbContext _context;

        public DoingBusinessTaxStatsRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public async Task<PagedResultBase<DoingBusinessTaxStats>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<DoingBusinessTaxStats> result = new PagedResultBase<DoingBusinessTaxStats>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<DoingBusinessTaxStats> query = _context.DoingBusinessTaxStats
                .OrderByDescending(o => o.StatsDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.StatsDate.Value >= dateFrom && x.StatsDate.Value <= dateTo);
            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<DoingBusinessTaxStats> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }
    }
}
