﻿using Domain.Repository;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repository.Segment
{
    public class SegmentRepository : ISegmentRepository
    {
        private readonly ApplicationDbContext _context;

        public SegmentRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public Task<Domain.Segment.Segment> Create(Domain.Segment.Segment obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(string SegmentID)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Segment.Segment> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.Segment.OrderBy(o => o.segmentName).ToList<Domain.Segment.Segment>();
            else
                return _context.Segment.Where(o => o.segmentName.Value.Contains(query)).
                    OrderBy(o => o.segmentName).ToList<Domain.Segment.Segment>();
        }

        public async Task<PagedResultBase<Domain.Segment.Segment>> FindAll(int pageSize, int currentPage)
        {
            PagedResultBase<Domain.Segment.Segment> result = new PagedResultBase<Domain.Segment.Segment>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Domain.Segment.Segment> query = _context.Segment
                .OrderBy(x => x.segmentName);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<Domain.Segment.Segment> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }


        public Task<Domain.Segment.Segment> FindById(string id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(string id)
        {
            throw new NotImplementedException();
        }

        public Domain.Segment.Segment Update(Domain.Segment.Segment obj)
        {
            throw new NotImplementedException();
        }
    }
}
