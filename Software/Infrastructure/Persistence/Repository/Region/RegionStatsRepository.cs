﻿using Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain.Utils;

namespace Infrastructure.Persistence.Repository.Region
{
    public class RegionStatsRepository : IRegionStatsRepository
    {
        private readonly ApplicationDbContext _context;

        public RegionStatsRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public Task<Domain.Region.RegionStats> Create(Domain.Region.RegionStats obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<Domain.Region.RegionStats> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.RegionStats.OrderBy(o => o.StatsDate).ToList<Domain.Region.RegionStats>();
            else
                return _context.RegionStats.Where(o => o.RegionID.regionName.Value.Contains(query)).
                    OrderBy(o => o.StatsDate).ToList<Domain.Region.RegionStats>();
        }

        public async Task<PagedResultBase<Domain.Region.RegionStats>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<Domain.Region.RegionStats> result = new PagedResultBase<Domain.Region.RegionStats>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Domain.Region.RegionStats> query = _context.RegionStats
                .Include(x => x.RegionID)
                .OrderByDescending(x => x.StatsDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.StatsDate.Value >= dateFrom && x.StatsDate.Value <= dateTo);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<Domain.Region.RegionStats> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }


        public Task<Domain.Region.RegionStats> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public Domain.Region.RegionStats Update(Domain.Region.RegionStats obj)
        {
            throw new NotImplementedException();
        }
    }
}
