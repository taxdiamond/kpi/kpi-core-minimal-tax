﻿using Domain.Enums.DataWareHouse;
using Domain.Repository;
using Domain.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.DataWarehouse
{
    public class DataWareHouseGenericSQLRepository : IDataWareHouseGenericSQLRepository
    {
        private string _conectionString { get; set; }
        private ILogger<DataWareHouseGenericSQLRepository> _logger;
        public DataWareHouseGenericSQLRepository(ILogger<DataWareHouseGenericSQLRepository> logger, IConfiguration configuration)
        {
            _conectionString = configuration.GetConnectionString("DBConnectionString");
            _logger = logger;
        }

        public void DeleteAllDataWarehouseRecords()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_conectionString))
                {
                    string spName = "[dbo].[UTIL_ResetKPIDW]";

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(spName, conn);

                    //open connection
                    conn.Open();

                    //set the SqlCommand type to stored procedure and execute
                    cmd.CommandType = CommandType.StoredProcedure;
                    int result = cmd.ExecuteNonQuery();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error to delet allRecords in DataWareHouse", ex);
            }
        }

        public void DeleteTableDataWarehouseByDate(DataWareHouseType dwType, DateTime from, DateTime until)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_conectionString))
                {
                    string spName = "[dbo].[UTIL_DeleteKPIDWByDate]";

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(spName, conn);

                    SqlParameter parameter = new SqlParameter();
                    parameter.ParameterName = "@enumDW";
                    parameter.SqlDbType = SqlDbType.Int;
                    parameter.Value = (int)dwType;
                    cmd.Parameters.Add(parameter);

                    parameter = new SqlParameter();
                    parameter.ParameterName = "@fromDate";
                    parameter.SqlDbType = SqlDbType.DateTime;
                    parameter.Value = from;
                    cmd.Parameters.Add(parameter);

                    parameter = new SqlParameter();
                    parameter.ParameterName = "@untilDate";
                    parameter.SqlDbType = SqlDbType.DateTime;
                    parameter.Value = until;
                    cmd.Parameters.Add(parameter);


                    //open connection
                    conn.Open();

                    //set the SqlCommand type to stored procedure and execute
                    cmd.CommandType = CommandType.StoredProcedure;
                    int result = cmd.ExecuteNonQuery();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error at delete " + dwType.ToString() + " for date from" + from.ToString("dd/MM/yyyy") + " to " + until.ToString("dd/MM/yyyy"), ex);
            }
        }

        public void ResetClassifierDataWareHouse()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_conectionString))
                {
                    string spName = "[dbo].[UTIL_ResetClassifierKPIDW]";

                    //define the SqlCommand object
                    SqlCommand cmd = new SqlCommand(spName, conn);

                    //open connection
                    conn.Open();

                    //set the SqlCommand type to stored procedure and execute
                    cmd.CommandType = CommandType.StoredProcedure;
                    int result = cmd.ExecuteNonQuery();

                    //close connection
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Error to delete allRecords Classifier", ex);
            }
        }

        public void InsertDatawareHouse(List<DataWareHouseDataSet> list, bool aloneNextDate)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_conectionString))
                {                   
                    foreach (var dwds in list)
                    {
                        try
                        {
                            string spName = "[dbo].[" + dwds.tableSpDW.SpNameInsert + "]";
                            //define the SqlCommand object
                            SqlCommand cmd;

                            DateTime from = DateTime.MinValue;
                            if (aloneNextDate)
                            {
                                //open connection
                                conn.Open();
                                string spName2 = "[dbo].[" + dwds.tableSpDW.SpNameGetLastDate + "]";
                                cmd = new SqlCommand(spName2, conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                SqlDataReader read = cmd.ExecuteReader();
                                if(read.HasRows)
                                {
                                    read.Read();
                                    try
                                    {
                                        from = read.GetDateTime(0);
                                    }
                                    catch { }
                                }
                                //close connection
                                conn.Close();
                            }

                            foreach (DataRow row in dwds.DataSetDW.Tables[0].Rows)
                            {                                
                                try
                                {                                    
                                    if (from >= Convert.ToDateTime(row[dwds.tableSpDW.ColumnDate]))
                                        continue;

                                    cmd = new SqlCommand(spName, conn);
                                    //open connection
                                    conn.Open();
                                    foreach (DataColumn column in dwds.DataSetDW.Tables[0].Columns)
                                    {
                                        SqlParameter parameter = new SqlParameter();
                                        parameter.ParameterName = "@" + column.ColumnName;
                                        SetSqlDbType(column.DataType, ref parameter);
                                        parameter.Value = row[column.ColumnName];
                                        cmd.Parameters.Add(parameter);
                                    }

                                    //set the SqlCommand type to stored procedure and execute
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    int result = cmd.ExecuteNonQuery();
                                }
                                catch(Exception ex)
                                {
                                    _logger.LogError(ex, "Error to execute in SpName: " + spName);
                                    throw ex;
                                }
                                //close connection
                                conn.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex, "Error to insert list of DataWareHouse.");
                            throw ex;
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error to insert list of DataWareHouse with conection.");
                throw ex;
            }

        }

        private void SetSqlDbType(Type theType, ref SqlParameter p1)
        {
            System.ComponentModel.TypeConverter tc;            
            tc = System.ComponentModel.TypeDescriptor.GetConverter(p1.DbType);
            if (tc.CanConvertFrom(theType))
            {
                p1.DbType = (DbType)tc.ConvertFrom(theType.Name);
            }
            else
            {
                //Try brute force
                try
                {
                    p1.DbType = (DbType)tc.ConvertFrom(theType.Name);
                }
                catch (Exception)
                {
                    //Do Nothing; will return NVarChar as default
                }
            }
        }
    }
}
