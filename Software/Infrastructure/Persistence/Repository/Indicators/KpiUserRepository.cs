﻿using Domain.Indicators;
using Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Indicators
{
    public class KpiUserRepository : IKpiUserRepository
    {
        private ApplicationDbContext _dbContext;
        public KpiUserRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<KpiUser> Create(KpiUser obj)
        {
            await _dbContext.KpiUsers.AddAsync(obj);
            return obj;
        }

        public async Task<bool> Exists(Guid id)
        {
            KpiUser obj = await _dbContext.KpiUsers.FindAsync(id);
            return obj != null;
        }

        public List<KpiUser> FindAll(string query)
        {
            return null;
        }

        public async Task<KpiUser> FindById(Guid id)
        {
            return await _dbContext.KpiUsers.FindAsync(id);
        }

        public KpiUser FindByKpiAndUser(Guid kpiId, string userId)
        {
            KpiUser obj = _dbContext.KpiUsers.Where(o => 
                o.UserRef.Id.Equals(userId) && 
                o.KpiRef.KpiId == kpiId)
                .FirstOrDefault();

            return obj;
        }

        public async Task Remove(Guid id)
        {
            KpiUser obj = await _dbContext.KpiUsers.FindAsync(id);
            _dbContext.KpiUsers.Remove(obj);
        }

        public void RemoveByKpi(Guid kpiId)
        {            
            List<KpiUser> kpiUsers = _dbContext.KpiUsers.Where(x => x.KpiRef.KpiId == kpiId).ToList();
            
            foreach(KpiUser kpiUser in kpiUsers)
            {
                _dbContext.KpiUsers.Remove(kpiUser);
            }
        }

        public KpiUser Update(KpiUser obj)
        {
            _dbContext.KpiUsers.Update(obj);
            return obj;
        }
    }
}
