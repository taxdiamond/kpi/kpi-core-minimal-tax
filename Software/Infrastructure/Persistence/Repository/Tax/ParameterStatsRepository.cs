﻿using Domain.Repository;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class ParameterStatsRepository : IParameterStatsRepository
    {
        private readonly ApplicationDbContext _context;

        public ParameterStatsRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<ParameterStats> Create(ParameterStats obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(string regionID)
        {
            throw new NotImplementedException();
        }

        public List<ParameterStats> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.ParameterStats.OrderBy(o => o.ValueDate).ToList<Domain.Tax.ParameterStats>();
            else
                return _context.ParameterStats.Where(o =>
                    o.ParameterName.Value.Contains(query)).OrderBy(o => o.ValueDate).ToList<Domain.Tax.ParameterStats>();
        }

        public Task<ParameterStats> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public ParameterStats Update(ParameterStats obj)
        {
            throw new NotImplementedException();
        }
    }
}
