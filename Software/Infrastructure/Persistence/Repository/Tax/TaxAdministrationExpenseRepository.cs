﻿using Domain.Repository;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain.Utils;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class TaxAdministrationExpenseRepository : ITaxAdministrationExpenseRepository
    {
        private readonly ApplicationDbContext _context;

        public TaxAdministrationExpenseRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public Task<TaxAdministrationExpense> Create(TaxAdministrationExpense obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<TaxAdministrationExpense> FindAll(string query)
        {

            if (string.IsNullOrWhiteSpace(query))
                return _context.TaxAdministrationExpenses
                    .OrderBy(o => o.TaxAdministrationExpenseDate.Value)
                    .ToList<Domain.Tax.TaxAdministrationExpense>();
            else
                return _context.TaxAdministrationExpenses.Where(o =>
                    o.OfficeID.officeName.Value.Contains(query) ||
                    o.RegionID.regionName.Value.Contains(query))
                    .OrderBy(o => o.TaxAdministrationExpenseDate.Value)
                    .ToList<Domain.Tax.TaxAdministrationExpense>();
        }

        public async Task<PagedResultBase<TaxAdministrationExpense>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<TaxAdministrationExpense> result = new PagedResultBase<TaxAdministrationExpense>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<TaxAdministrationExpense> query = _context.TaxAdministrationExpenses
                .Include(x => x.RegionID)
                .Include(x => x.OfficeID)
                .OrderByDescending(x => x.TaxAdministrationExpenseDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.TaxAdministrationExpenseDate.Value >= dateFrom && x.TaxAdministrationExpenseDate.Value <= dateTo);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<TaxAdministrationExpense> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<TaxAdministrationExpense> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public TaxAdministrationExpense Update(TaxAdministrationExpense obj)
        {
            throw new NotImplementedException();
        }
    }
}
