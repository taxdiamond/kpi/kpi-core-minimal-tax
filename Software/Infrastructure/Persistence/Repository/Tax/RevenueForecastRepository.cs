﻿using Domain.Repository;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain.Utils;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class RevenueForecastRepository : IRevenueForecastRepository
    {
        private readonly ApplicationDbContext _context;

        public RevenueForecastRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<RevenueForecast> Create(RevenueForecast obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<RevenueForecast> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.RevenueForecast.OrderBy(o => o.ForecastDate).ToList<Domain.Tax.RevenueForecast>();
            else
                return _context.RevenueForecast.Where(o => 
                    o.Concept.Value.Contains(query) ||
                    o.ConceptID.Value.Contains(query)).OrderBy(o => o.ForecastDate).ToList<Domain.Tax.RevenueForecast>();
        }

        public async Task<PagedResultBase<RevenueForecast>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<RevenueForecast> result = new PagedResultBase<RevenueForecast>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<RevenueForecast> query = _context.RevenueForecast
                .Include(x => x.RegionID)
                .Include(x => x.OfficeID)
                .Include(x => x.SegmentID)
                .OrderByDescending(x => x.ForecastDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.ForecastDate.Value >= dateFrom && x.ForecastDate.Value <= dateTo);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<RevenueForecast> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<RevenueForecast> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public RevenueForecast Update(RevenueForecast obj)
        {
            throw new NotImplementedException();
        }
    }
}
