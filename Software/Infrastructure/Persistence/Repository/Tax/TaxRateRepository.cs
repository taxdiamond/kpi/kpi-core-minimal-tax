﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class TaxRateRepository : ITaxRateRepository
    {
        private readonly ApplicationDbContext _context;

        public TaxRateRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<TaxRate> Create(TaxRate obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(string TaxRateID)
        {
            throw new NotImplementedException();
        }

        public List<TaxRate> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.TaxRates.OrderBy(o => o.TaxName).ToList<Domain.Tax.TaxRate>();
            else
                return _context.TaxRates.Where(o => o.TaxName.Value.Contains(query)).
                    OrderBy(o => o.TaxName).ToList<Domain.Tax.TaxRate>();
        }

        public async Task<PagedResultBase<TaxRate>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<TaxRate> result = new PagedResultBase<TaxRate>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<TaxRate> query = _context.TaxRates
                .OrderByDescending(x => x.TaxRateDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.TaxRateDate.Value >= dateFrom && x.TaxRateDate.Value <= dateTo);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<TaxRate> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<TaxRate> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(string id)
        {
            throw new NotImplementedException();
        }

        public TaxRate Update(TaxRate obj)
        {
            throw new NotImplementedException();
        }
    }
}
