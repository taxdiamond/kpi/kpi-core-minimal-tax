﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class ArrearRepository : IArrearRepository
    {
        private readonly ApplicationDbContext _context;

        public ArrearRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<Arrear> Create(Arrear obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<Arrear> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.Arrears.OrderBy(o => o.ArrearDate).ToList<Domain.Tax.Arrear>();
            else
                return _context.Arrears.Where(o => 
                    o.Concept.Value.Contains(query) ||
                    o.ConceptID.Value.Contains(query)).OrderBy(o => o.ArrearDate).ToList<Domain.Tax.Arrear>();
        }

        public async Task<PagedResultBase<Arrear>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<Arrear> result = new PagedResultBase<Arrear>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Arrear> query = _context.Arrears
                .Include(x => x.OfficeID)
                .Include(x => x.RegionID)
                .Include(x => x.SegmentID)
                .OrderByDescending(o => o.ArrearDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.ArrearDate.Value >= dateFrom && x.ArrearDate.Value <= dateTo);
            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<Arrear> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<Arrear> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public Arrear Update(Arrear obj)
        {
            throw new NotImplementedException();
        }
    }
}
