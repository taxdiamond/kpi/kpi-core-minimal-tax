﻿using Domain.Repository;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class TrainingCourseRepository : ITrainingCourseRepository
    {
        private readonly ApplicationDbContext _context;

        public TrainingCourseRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<TrainingCourse> Create(TrainingCourse obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<TrainingCourse> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.Courses.OrderBy(o => o.TrainingCoursesReportDate).ToList<Domain.Tax.TrainingCourse>();
            else
                return _context.Courses.Where(o =>
                    o.CourseArea.Value.Contains(query))
                    .OrderBy(o => o.TrainingCoursesReportDate).ToList<Domain.Tax.TrainingCourse>();
        }

        public Task<TrainingCourse> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public TrainingCourse Update(TrainingCourse obj)
        {
            throw new NotImplementedException();
        }
    }
}
