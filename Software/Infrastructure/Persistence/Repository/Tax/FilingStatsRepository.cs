﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class FilingStatsRepository : IFilingStatsRepository
    {
        private readonly ApplicationDbContext _context;
        public FilingStatsRepository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public Task<FilingStats> Create(FilingStats obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<FilingStats> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.FilingStats.OrderBy(o => o.ReportDate)
                    .ToList<Domain.Tax.FilingStats>();
            else
                return _context.FilingStats.Where(o => o.TaxType.Value.Contains(query))
                    .OrderBy(o => o.ReportDate)
                    .ToList<Domain.Tax.FilingStats>();
        }



        public async Task<PagedResultBase<FilingStats>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<FilingStats> result = new PagedResultBase<FilingStats>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<FilingStats> query = _context.FilingStats
                .Include(x => x.SegmentID)
                .OrderByDescending(o => o.ReportDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.ReportDate.Value >= dateFrom && x.ReportDate.Value <= dateTo);
            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<FilingStats> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<FilingStats> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public FilingStats Update(FilingStats obj)
        {
            throw new NotImplementedException();
        }
    }
}
