﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class RevenueRepository : IRevenueRepository
    {
        private readonly ApplicationDbContext _context;

        public RevenueRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<Revenue> Create(Revenue obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<Revenue> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.Revenue.OrderBy(o => o.RevenueDate).ToList<Domain.Tax.Revenue>();
            else
                return _context.Revenue.Where(o => 
                    o.Concept.Value.Contains(query) ||
                    o.ConceptID.Value.Contains(query)).OrderBy(o => o.RevenueDate).ToList<Domain.Tax.Revenue>();
        }

        public async Task<PagedResultBase<Revenue>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<Revenue> result = new PagedResultBase<Revenue>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Revenue> query = _context.Revenue
                .Include(x => x.RegionID)
                .Include(x => x.OfficeID)
                .Include(x => x.SegmentID)
                .OrderByDescending(x => x.RevenueDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.RevenueDate.Value >= dateFrom && x.RevenueDate.Value <= dateTo);

            result.TotalRows = await query.CountAsync();

            var skip = (currentPage - 1) * pageSize;
            List<Revenue> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<Revenue> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public Revenue Update(Revenue obj)
        {
            throw new NotImplementedException();
        }
    }
}
