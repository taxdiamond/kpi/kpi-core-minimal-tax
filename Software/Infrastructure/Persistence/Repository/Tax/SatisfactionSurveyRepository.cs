﻿using Domain.Repository;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class SatisfactionSurveyRepository : ISatisfactionSurveyRepository
    {
        private readonly ApplicationDbContext _context;

        public SatisfactionSurveyRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<SatisfactionSurvey> Create(SatisfactionSurvey obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<SatisfactionSurvey> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.SatisfactionSurveys.OrderBy(o => o.SurveyItemReportDate)
                    .ToList<Domain.Tax.SatisfactionSurvey>();
            else
                return _context.SatisfactionSurveys.Where(o =>
                    o.SurveyArea.Value.Contains(query))
                    .OrderBy(o => o.SurveyItemReportDate)
                    .ToList<Domain.Tax.SatisfactionSurvey>();
        }

        public Task<SatisfactionSurvey> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public SatisfactionSurvey Update(SatisfactionSurvey obj)
        {
            throw new NotImplementedException();
        }
    }
}
