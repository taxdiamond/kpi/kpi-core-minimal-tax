﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class ImportRepository : IImportRepository
    {
        private readonly ApplicationDbContext _context;

        public ImportRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<Import> Create(Import obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<Import> FindAll(string query)
        {
            return _context.Imports.OrderBy(o => o.ImportDate).ToList<Domain.Tax.Import>();
            
            /*if (string.IsNullOrWhiteSpace(query))
                
            else
                return _context.Imports.Where(o =>
                    o.Concept.Value.Contains(query) ||
                    o.ConceptID.Value.Contains(query)).OrderBy(o => o.RevenueDate).ToList<Domain.Tax.Import>();*/
        }

        public async Task<PagedResultBase<Import>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<Import> result = new PagedResultBase<Import>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<Import> query = _context.Imports
                .OrderByDescending(x => x.ImportDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.ImportDate.Value >= dateFrom && x.ImportDate.Value <= dateTo);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<Import> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<Import> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public Import Update(Import obj)
        {
            throw new NotImplementedException();
        }
    }
}
