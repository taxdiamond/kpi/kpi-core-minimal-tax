﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class HumanResourceRepository : IHumanResourceRepository
    {
        private readonly ApplicationDbContext _context;

        public HumanResourceRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<HumanResource> Create(HumanResource obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<HumanResource> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.HumanResources.OrderBy(o => o.HRItemReportDate).ToList<Domain.Tax.HumanResource>();
            else
                return _context.HumanResources.Where(o =>
                    o.RegionID.regionName.Value.Contains(query) ||
                    o.OfficeID.officeName.Value.Contains(query)).OrderBy(o => o.HRItemReportDate).ToList<Domain.Tax.HumanResource>();
        }

        public async Task<PagedResultBase<HumanResource>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<HumanResource> result = new PagedResultBase<HumanResource>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<HumanResource> query = _context.HumanResources
                .Include(x => x.OfficeID)
                .Include(x => x.RegionID)
                .OrderByDescending(x => x.HRItemReportDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.HRItemReportDate.Value >= dateFrom && x.HRItemReportDate.Value <= dateTo);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<HumanResource> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }


        public Task<HumanResource> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public HumanResource Update(HumanResource obj)
        {
            throw new NotImplementedException();
        }
    }
}
