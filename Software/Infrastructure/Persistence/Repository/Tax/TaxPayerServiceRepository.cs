﻿using Domain.Repository;
using Domain.Tax;
using Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class TaxPayerServiceRepository : ITaxPayerServiceRepository
    {
        private readonly ApplicationDbContext _context;

        public TaxPayerServiceRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<TaxPayerService> Create(TaxPayerService obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<TaxPayerService> FindAll(string query)
        {
            return _context.TaxPayerServices.OrderBy(o => o.ServicesItemReportDate).ToList<Domain.Tax.TaxPayerService>();
        }

        public async Task<PagedResultBase<TaxPayerService>> FindAll(int pageSize, int currentPage, DateTime? dateFrom, DateTime? dateTo)
        {
            PagedResultBase<TaxPayerService> result = new PagedResultBase<TaxPayerService>();
            result.PageSize = pageSize;
            result.CurrentPage = currentPage;
            IQueryable<TaxPayerService> query = _context.TaxPayerServices
                .OrderByDescending(x => x.ServicesItemReportDate);

            if (dateFrom != null && dateTo != null)
                query = query.Where(x => x.ServicesItemReportDate.Value >= dateFrom && x.ServicesItemReportDate.Value <= dateTo);

            result.TotalRows = query.Count();

            var skip = (currentPage - 1) * pageSize;
            List<TaxPayerService> list = await query.Skip(skip).Take(pageSize).ToListAsync();
            result.ResultList = list;

            return result;
        }

        public Task<TaxPayerService> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public TaxPayerService Update(TaxPayerService obj)
        {
            throw new NotImplementedException();
        }
    }
}
