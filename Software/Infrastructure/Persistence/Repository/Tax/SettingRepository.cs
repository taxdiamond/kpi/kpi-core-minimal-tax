﻿using Domain.Repository;
using Domain.Tax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Persistence.Repository.Tax
{
    public class SettingRepository : ISettingRepository
    {
        private readonly ApplicationDbContext _context;

        public SettingRepository(ApplicationDbContext context)
        {
            this._context = context;
        }
        public Task<Setting> Create(Setting obj)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exists(int id)
        {
            throw new NotImplementedException();
        }

        public List<Setting> FindAll(string query)
        {
            if (string.IsNullOrWhiteSpace(query))
                return _context.Settings.OrderBy(o => o.ValueAddedTaxName).ToList<Setting>();
            else
                return _context.Settings.Where(o =>
                    o.ValueAddedTaxName.Value.Contains(query) ||
                    o.CorporateIncomeTaxName.Value.Contains(query)).OrderBy(o => o.ValueAddedTaxName).ToList<Setting>();
        }

        public Task<Setting> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public Task Remove(int id)
        {
            throw new NotImplementedException();
        }

        public Setting Update(Setting obj)
        {
            throw new NotImplementedException();
        }
    }
}
