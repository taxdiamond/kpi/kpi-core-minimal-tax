﻿using KPIClassLibrary.KPI;
using System.Collections.Generic;

namespace Infrastructure.ViewModels.KPI
{
    public class KpiCatalogAreaViewModel
    {
        public string description { get; set; }
        public string name { get; set; }
        public string groupid { get; set; }
        public string dimension { get; set; }
        public string Formula { get; set; }
        public string Unit { get; set; }
        public string YAxisLabel { get; set; }
        public List<KPIVariable> Variables { get; set; }
        public List<KpiCatalogItemViewModel> kpis { get; set; }
        public KpiCatalogAreaViewModel()
        {
            kpis = new List<KpiCatalogItemViewModel>();
        }

        public static KpiCatalogAreaViewModel FromCatalogArea(KPICatalogArea area)
        {
            KpiCatalogAreaViewModel model = new KpiCatalogAreaViewModel();
            model.description = area.Description;
            model.dimension = area.Dimension;
            model.groupid = area.GroupID;
            model.name = area.Name;
            model.Formula = area.Formula;
            model.Unit = area.Unit;
            model.YAxisLabel = area.YAxisLabel;
            model.Variables = area.Variables;

            foreach(KPICatalogItem item in area.KPIs)
            {
                model.kpis.Add(KpiCatalogItemViewModel.FromCatalogItem(item));
            }

            return model;
        }
    }
}
