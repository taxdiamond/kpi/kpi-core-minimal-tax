﻿using KPIClassLibrary.KPI;

namespace Infrastructure.ViewModels.Dashboard
{
    public class DimensionViewModel
    {
        public KPIType Type { get; set; }
        public KPIDomain Domain { get; set; }
        public string Name { get; set; }
        public int NumberOfKpis { get; set; }
    }
}
