﻿using Infrastructure.ViewModels.Shared;
using KPIClassLibrary.KPI;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.ViewModels.Dashboard
{
    public class DimensionListViewModel : AppListViewModel<DimensionViewModel>
    {
        [Display(Name = "Dimensions")]
        public List<DimensionViewModel> Dimensions
        {
            get
            {
                return this.items;
            }
        }

        public KPIType Type { get; set; }
        public KPIDomain Domain { get; set; }

        public int TotalNumberKpis
        {
            get
            {
                int result = 0;
                foreach (DimensionViewModel dim in Dimensions)
                    result += dim.NumberOfKpis;
                return result;
            }
        }

        public DimensionListViewModel()
        {
            items = new List<DimensionViewModel>();
            Type = KPIType.Strategic;
            Domain = KPIDomain.Tax;
            PageNumber = 1;
            PageSize = 30;
        }
    }
}
