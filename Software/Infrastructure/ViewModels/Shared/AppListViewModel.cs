﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Infrastructure.ViewModels.Shared
{
    public abstract class AppListViewModel<T> : MessagesViewModel
    {
        private int _totalNumber;

        [Display(Name = "Items")]
        public List<T> items { get; set; }

        [Display(Name = "PageNumber")]
        public int PageNumber { get; set; }

        [Display(Name = "TotalNumber")]
        public int TotalNumber
        {
            get
            {
                if (_totalNumber == 0 && items.Count > 0)
                    _totalNumber = items.Count;
                return _totalNumber;
            }
            set
            {
                _totalNumber = value;
            }
        }

        [Display(Name = "PageSize")]
        public int PageSize { get; set; }
        [Display(Name ="Query")]
        public string Query { get; set; }

        public string MessageNumberEntries
        {
            get {
                if (TotalNumber == 0)
                    return "No entries to show";

                if (TotalNumber == 1)
                    return "Showing " + TotalNumber + " entry";

                if (PageSize > TotalNumber)
                    return "Showing " + TotalNumber + " entries";
                else
                {
                    int start = PageSize * (PageNumber - 1);
                    int to = Math.Min(TotalNumber, PageSize > 0 ? (start + PageSize) : Int32.MaxValue);
                    return "Showing " + (start + 1) + " to " + to + " of " + TotalNumber + " entries";
                }
            }
        }

        public int PageCount
        {
            get
            {
                return TotalNumber / PageSize;
            }
        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageNumber > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageNumber < TotalPages);
            }
        }

        public int TotalPages
        {
            get
            {
                return (int)Math.Ceiling(TotalNumber / (double)PageSize);
            }
        }

    }
}
