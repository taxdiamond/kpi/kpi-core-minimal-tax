﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.ViewModels.Shared
{
    public class ListItemViewModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
