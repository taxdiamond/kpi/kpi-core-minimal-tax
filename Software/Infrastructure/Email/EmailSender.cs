﻿using Framework.Email;
using Microsoft.Extensions.Configuration;
using MimeKit;

namespace Infrastructure.Email
{
    public class EmailSender : IEmailSender
    {
        private IConfiguration _configuration;

        private string _smtpServer;
        private int _smtpPort;
        private string _fromAddress;
        private string _username;
        private string _password;
        private bool _enableSsl;

        public EmailSender(IConfiguration configuration)
        {
            _configuration = configuration;

            _smtpServer = _configuration.GetSection("Email").GetValue<string>("SmtpServer");
            _smtpPort = _configuration.GetSection("Email").GetValue<int>("SmtpPort");
            _fromAddress = _configuration.GetSection("Email").GetValue<string>("FromEmail");
            _username = _configuration.GetSection("Email").GetValue<string>("Username");
            _password = _configuration.GetSection("Email").GetValue<string>("Password");
            _enableSsl = _configuration.GetSection("Email").GetValue<bool>("EnableSsl");
        }
        public async void Send(string toAddress, string subject, string body, bool sendAsync = true)
        {
            var mimeMessage = new MimeMessage(); // MIME : Multipurpose Internet Mail Extension
            mimeMessage.From.Add(new MailboxAddress("App", _fromAddress));
            mimeMessage.To.Add(new MailboxAddress("Username", toAddress));
            mimeMessage.Subject = subject;

            var bodyBuilder = new MimeKit.BodyBuilder
            {
                HtmlBody = body
            };
            mimeMessage.Body = bodyBuilder.ToMessageBody();

            using (var client = new MailKit.Net.Smtp.SmtpClient())
            {
                client.Connect(_smtpServer, _smtpPort, _enableSsl);
                client.Authenticate(_username, _password); // If using GMail this requires turning on LessSecureApps : https://myaccount.google.com/lesssecureapps
                if (sendAsync)
                {
                    await client.SendAsync(mimeMessage);
                }
                else
                {
                    client.Send(mimeMessage);
                }
                client.Disconnect(true);
            }
        }
    }
}
